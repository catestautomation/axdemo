﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ashley.QA.Automation.Web.Framework.Selenium;
using Ashley.QA.Automation.Web.Framework.Enum;
using System.Runtime.InteropServices;
using OpenQA.Selenium.Support.UI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System.Diagnostics;
using System.Collections.ObjectModel;
using OpenQA.Selenium.Interactions;

namespace Ashley.QA.Dynamics.AX2012.Automation.Utils
{
    public static class PortalCommonFunctionsSelenium
    {

        public static void FocusBrowserWindow(this IWebDriver driver)
        {
            Microsoft.VisualStudio.TestTools.UITesting.BrowserWindow.Locate(driver.Title).SetFocus();
        }

        public static String cdd = DateTime.Today.AddDays(13).ToString("M/d/yyyy").Replace("-", "/");
        public static String add = DateTime.Today.ToString("MM/dd/yyyy").Replace("-", "/");


        public static IWebDriver LaunchPortal(string url, string username, string password)
        {
            #region Local Variables

            IWebDriver driver = null;

            #endregion            

            driver = Selenium.LaunchBrowser("local", url, Browser.IE);

            driver.WaitForReady();

            return driver;
        }

        public static IWebDriver SelectTab(this IWebDriver driver, string tab)
        {
            #region Local Variables

            IWebElement hyperlink_Tab = null;

            #endregion

            try
            {
                hyperlink_Tab = driver.FindIdenticalControls(Attributes.Class, "menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode", PropertyExpression.Contains).First(x => x.Text.ProcessString() == tab.ProcessString());
                hyperlink_Tab.Focus();
                hyperlink_Tab.MouseClick();
            }
            catch (Exception ex)
            {
                throw new Exception(tab + " not available. " + ex.Message.ToString());
            }

            driver.WaitForReady();
            return driver;
        }

        public static void SelectFromRibbon(this IWebDriver driver, string tab, string section, string item, [Optional]string subItem, bool doDefaultClick = false)
        {
            // driver.FocusBrowserWindow();

            #region Local Variables

            IWebElement hyperlink_Tab = null;
            IWebElement ctrl_Section = null;
            IWebElement hyperlink_Item = null;
            IWebElement hyperlink_SubItem = null;

            #endregion

            try
            {
                hyperlink_Tab = driver.FindIdenticalControls(Attributes.Class, "ms-cui-tt", PropertyExpression.Contains).First(x => x.Text.ProcessString() == tab.ProcessString()).FindElement(By.TagName("a"));
                hyperlink_Tab.Focus();
                hyperlink_Tab.MouseClick();
            }
            catch (Exception ex)
            {
                throw new Exception(tab + " not available.");
            }

            try
            {
                ctrl_Section = driver.FindIdenticalControls(Attributes.Class, "ms-cui-groupTitle").First(x => x.Text.ProcessString() == section.ProcessString());
            }
            catch (Exception ex)
            {
                throw new Exception(section + " not available.");
            }

            try
            {
                hyperlink_Item = ctrl_Section.GetParent().FindElements(By.TagName("a")).First(x => x.Text.ProcessString() == item.ProcessString());
                hyperlink_Item.Focus();
                hyperlink_Item.Click();
                driver.WaitForReady();
            }
            catch (Exception ex)
            {
                throw new Exception(item + " not available.");
            }

            if (subItem != null)
            {
                try
                {
                    hyperlink_SubItem = driver.FindIdenticalControls(Attributes.Class, "ms-cui-ctl-menu ").First(x => x.Text.ProcessString() == subItem.ProcessString());
                    hyperlink_SubItem.Focus();
                    if (doDefaultClick == false)
                    {
                        hyperlink_SubItem.MouseClick();
                    }
                    else
                    { hyperlink_SubItem.Click(); }
                }
                catch (Exception ex)
                {
                    throw new Exception(subItem + " not available.");
                }
            }

            driver.WaitForReady();
        }

        public static IWebDriver PortalFilter(this IWebDriver driver, string field, string value)
        {
            #region Local Variables

            IWebElement span_QF = null;
            IWebElement edit_QF = null;
            IWebElement combobox_QF = null;

            #endregion

            span_QF = driver.FindControl(Attributes.Class, "dynQuickFilterOuter");
            ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].children;", span_QF)).ToList()[0].MouseClick();
            ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].children;", span_QF)).ToList()[0].SetText(value);

            combobox_QF = span_QF.FindControl(Attributes.Class, "dynQuickFilterField");
            var select = new SelectElement(combobox_QF);
            select.SelectByText(field);

            span_QF.FindControl(Attributes.Class, "dynQuickFilterGoButton").MouseClick();

            driver.WaitForReady();

            driver.WaitForLoadingInformation();

            return driver;
        }


        public static bool VerifyPOAvailabilityInCueList(this IWebDriver driver, string cueName, string purchaseOrder)
        {
            bool flag = false;

            driver.SelectFromRibbon("Purchase order", "Stages", cueName);
            driver = driver.PortalFilter("Purchase order", purchaseOrder);
            driver.WaitForReady();

            IWebElement elm_table = driver.FindControl(Attributes.Class, "dynGridViewTable");

            IWebElement elm_row = elm_table.FindControl(Attributes.Class, "dynGridViewEmptyRowTr", PropertyExpression.Contains, true);
            if (elm_row != null)
            {
                if (!elm_row.Text.Contains("This grid is empty."))
                    flag = true;
                else
                    flag = false;
            }
            return flag;
        }


        public static void ClickACellinTable(this IWebDriver driver, string columnName, string columnValue)
        {
            #region Local Variables

            IWebElement elm_table = null;
            bool flag = false;
            #endregion

            elm_table = driver.FindControl(Attributes.Class, "dynGridViewTable");
            List<IWebElement> list_column = elm_table.FindIdenticalControls(Attributes.Class, "dynGridViewHeaderRowTd");
            int columnIndex = list_column.IndexOf(list_column.First(x => x.Text.ProcessString() == columnName.ProcessString()));

            List<IWebElement> list_row = elm_table.FindIdenticalControls(Attributes.Class, "dynGridViewDataRowTr", PropertyExpression.Contains);
            for (int i = 0; i < list_row.Count; i++)
            {
                List<IWebElement> list_cell = list_row[i].FindIdenticalControls(Attributes.Class, "dynGridViewDataRowTd", PropertyExpression.Contains);
                // IWebElement elm_cell = list_cell.First(x => x.Text.ProcessString() == columnValue.ProcessString());
                IWebElement elm_cell = list_cell[1].FindElement(By.TagName("a"));
                elm_cell.Focus();
                elm_cell.MouseClick();
                driver.WaitForLoadingInformation();
                break;
            }
        }

        public static void EnterERPNumberAndConfirmedDeliveryDate(this IWebDriver driver, string ERPNumber, string captionName, string groupName, [Optional] bool shouldEnterERPAlone,bool doSaveAndClose = true)
        {
            #region Local Variables

            IWebElement ctrl_IFrame = null;
            IWebDriver driver_frame = null;
            IWebElement elm_ERPNumber = null;

            IWebElement elm_ConfirmDate = null;
            IWebElement elm_ApplyToLines = null;
            IWebElement elm_SaveAndClose = null;
            IWebElement elm_SchduleReason = null;
            IWebElement elm_ReasonCode = null;
            IWebElement elm_PurchaseOrder = null;

            #endregion
            driver.WaitForReady();
            ctrl_IFrame = driver.FindControl(Attributes.Class, "ms-dlgFrame");
            driver_frame = driver.SwitchTo().Frame(ctrl_IFrame);

            IWebElement elm_parent = driver_frame.FindIdenticalControls(Attributes.Class, "dynMultiSection").First(x => x.GetAttribute("Id").ToString() != string.Empty);
            IWebElement div_panel = null;
            foreach (IWebElement children in ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].children;", elm_parent)))
            {
                IWebElement elm_caption = children.FindIdenticalControls(Attributes.Class, "dynSectionHeaderCaption").First();
                if (elm_caption.Text.ProcessString() == captionName.ProcessString())
                {
                    div_panel = children;
                    break;
                }
            }


            List<IWebElement> coll_group = div_panel.FindIdenticalControls(Attributes.Class, "dynColumn");
            IWebElement elm_group = null;
            IWebElement elm_tableCaption = null;
            foreach (IWebElement group in coll_group)
            {
                IWebElement elm_caption = group.FindElements(By.TagName("caption")).First();
                if (elm_caption.Text.ProcessString() == groupName.ProcessString())
                {
                    elm_group = group;
                    elm_tableCaption = elm_caption.GetParent();
                    break;
                }
            }

            IWebElement elm_edit = null;
            List<IWebElement> coll = elm_tableCaption.FindElements(By.TagName("tr")).ToList<IWebElement>();
            for (int i = 0; i < coll.Count; i++)
            {
                IWebElement row = coll[i];

                IWebElement elm_cell = row.FindElements(By.TagName("td"))[0];

                if ((elm_cell).Text.Contains("ERP number"))
                {
                    if (ERPNumber != string.Empty)
                    {
                        elm_edit = row.FindElements(By.TagName("td"))[1];

                        ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver_frame).ExecuteScript("return arguments[0].children;", elm_edit)).ToList()[0].MouseClick();
                        ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver_frame).ExecuteScript("return arguments[0].children;", elm_edit)).ToList()[0].SetText(ERPNumber.Trim());
                        ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver_frame).ExecuteScript("return arguments[0].children;", elm_edit)).ToList()[0].MouseClick();

                        OpenQA.Selenium.Interactions.Actions action = new OpenQA.Selenium.Interactions.Actions(driver_frame);
                        action.SendKeys(Keys.Tab).Build().Perform();
                        if (shouldEnterERPAlone)
                        { break; }
                    }
                }
                else if ((elm_cell).Text.ToLower().Contains("confirmed delivery"))
                {
                    elm_edit = row.FindElements(By.TagName("td"))[1];

                    IWebElement elm_table = elm_edit.FindElements(By.TagName("table"))[0];

                    elm_edit = elm_table.FindIdenticalControls(Attributes.Class, "dynJoiningRest")[0];

                    //cdd = DateTime.Today.AddDays(13).ToString("M/d/yyyy").Replace("-", "/");
                    ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver_frame).ExecuteScript("return arguments[0].children;", elm_edit)).ToList()[0].MouseClick();
                    ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver_frame).ExecuteScript("return arguments[0].children;", elm_edit)).ToList()[0].SetText(cdd);
                    break;
                }
            }

            if (shouldEnterERPAlone == false)
            {
                IWebElement btn_applyToLines = elm_group.FindElements(By.TagName("input")).First(x => x.GetAttribute("value") == "Apply to lines");
                btn_applyToLines.Focus();
                btn_applyToLines.MouseClick();
                driver_frame.WaitForLoadingInformation();
                //  IWebElement PO_Confirmation = elm_group.FindElement(By.Id("ctl00_m_g_772d6b06_6626_4e02_bd0e_a9c8e037a0a9_ctl01_afmConfirmationErrorPanel"));

                driver = driver.SwitchTo().DefaultContent();
                try
                {
                    elm_SchduleReason = driver.FindIdenticalControls(Attributes.Class, "ms-dlgContent").Last();

                    IWebDriver driver_confirm = driver.SwitchTo().Frame(elm_SchduleReason.FindControl(Attributes.Class, "ms-dlgFrame"));

                    elm_ReasonCode = driver_confirm.FindElements(By.TagName("input")).ToList().First(x => x.GetAttribute("value") == "OK");
                    elm_ReasonCode.Focus();
                    elm_ReasonCode.MouseClick();
                }
                catch { }
                finally { driver = driver.SwitchTo().DefaultContent(); }
            }
            else
            {
                if (ERPNumber == " ")
                {
                    driver_frame.SelectFromRibbon("Purchase order", "Commit", "Close", null, true);
                }
                else
                {
                    driver = driver.SwitchTo().DefaultContent();
                }
                
            }

            if (doSaveAndClose)
            {
                ctrl_IFrame = driver.FindControl(Attributes.Class, "ms-dlgFrame");
                driver_frame = driver.SwitchTo().Frame(ctrl_IFrame);

                driver_frame.SelectFromRibbon("Purchase order", "Commit", "Save and close", null, true);
                driver_frame.WaitForReady();
            }
            if (shouldEnterERPAlone)
            {
                if(ERPNumber != " ")
                {
                    driver = driver.SwitchTo().DefaultContent();
                    driver.WaitForLoadingInformation();
                    IWebDriver driver_confirm = driver.SwitchTo().Frame(driver.FindControl(Attributes.Class, "ms-dlgFrame"));

                    IWebElement elm_yes = driver_confirm.FindElements(By.TagName("input")).ToList().First(x => x.GetAttribute("value") == "Yes");
                    elm_yes.Focus();
                    elm_yes.MouseClick();
                }
            }
            // elm_SaveAndClose = driver_frame.FindElements(By.TagName("input")).ToList().First(x => x.GetAttribute("value") == "Save and Close");            
            //  elm_SaveAndClose.ScrollIntoView();
            // elm_SaveAndClose.Focus();
            // elm_SaveAndClose.MouseClick();

            driver = driver.SwitchTo().DefaultContent();
            driver.WaitForLoadingInformation();

            driver.WaitForReady();
        }


        public static Dictionary<string, bool> VerifyPOHeaderValues(this IWebDriver driver, string captionName, string groupName, Dictionary<string, string> dic_headerValues,bool needToClosePO = false)
        {
            #region Local Variables
            Dictionary<string,bool> dic_output = new Dictionary<string, bool>();

            IWebElement elm_group = null;
            IWebElement elm_label = null;

            List<bool> coll = new List<bool>();
            string headerName = string.Empty;
            string headerValue = string.Empty;

            #endregion

            IWebElement div_panel = driver.ExpandFastTab(captionName);
            if (!String.IsNullOrEmpty(groupName))
            {
                List<IWebElement> coll_group = div_panel.FindIdenticalControls(Attributes.Class, "dynGroup");
                foreach (IWebElement group in coll_group)
                {
                    bool flag = false;
                    try
                    {
                        elm_group = group.FindElements(By.TagName("caption")).First();
                        if (elm_group.Text.ProcessString() == groupName.ProcessString())
                        {
                            flag = true;
                        }
                    }
                    catch (InvalidOperationException ex)
                    {
                        if (ex.Message.Contains("Sequence contains no elements"))
                        {
                            if (group.Text.Contains(groupName))
                            {
                                flag = true;
                            }
                        }
                    }
                    if (flag)
                    {
                        foreach (KeyValuePair<string, string> keyvaluPair in dic_headerValues)
                        {
                            headerName = keyvaluPair.Key;
                            headerValue = keyvaluPair.Value;
                            List<IWebElement> coll_header = group.FindIdenticalControls(Attributes.Class, "dynGroupHeaderCell");

                            foreach (IWebElement elm_header in coll_header)
                            {
                                elm_label = elm_header.FindElement(By.TagName("label"));
                                if (elm_label.Text.ProcessString() == headerName.ProcessString())
                                {
                                    if (headerValue != "ToVerifyHeaderAlone")
                                    {
                                        IWebElement elm_headerValueCell = elm_header.GetParent().FindElements(By.TagName("td")).Last();
                                        IWebElement elm_span;
                                        String checktext = string.Empty;
                                        try
                                        {
                                            elm_span = elm_headerValueCell.FindElement(By.TagName("span"));
                                            checktext = elm_span.Text.ProcessString();
                                            if (headerValue == "true" || headerValue == "false")
                                            {
                                                IWebElement elm_check = elm_span.FindElement(By.TagName("input"));
                                                string cc = elm_check.GetAttribute("disabled");
                                                checktext = (elm_check.GetAttribute("disabled") == "true") ? "false" : "true";
                                            }

                                        }
                                        catch (Exception)
                                        {
                                            try
                                            {
                                                elm_span = elm_headerValueCell.FindElement(By.TagName("input"));
                                                checktext = elm_span.GetAttribute("value").ToString();
                                            }
                                            catch
                                            {
                                                elm_span = elm_headerValueCell.FindElement(By.TagName("textarea"));
                                                checktext = elm_span.GetAttribute("value").ToString();
                                            }
                                        }

                                        if (!headerName.ToLower().Contains("date"))
                                        {
                                            if (checktext.ProcessString() == headerValue.ProcessString())
                                            {
                                                dic_output.Add(headerName, true);
                                                break;
                                            }
                                            else { dic_output.Add("'" + headerName + "'" + " - Expected : " + headerValue + " , Actual : " + checktext, false); break; }
                                        }
                                        else
                                        {
                                            if (headerValue.ProcessString() != "nodate")
                                            {
                                                DateTime date_fromAX = DateTime.ParseExact(headerValue, "dd-MMM-yyyy hh:mm:ss tt", null);
                                                DateTime date_fromPTL = DateTime.ParseExact(elm_span.Text, "MM/dd/yyyy hh:mm:ss tt", null);
                                                var differenceInSecs = (date_fromAX - date_fromPTL).TotalSeconds;
                                                if (differenceInSecs <= 3)
                                                {
                                                    dic_output.Add(headerName, true);
                                                    break;
                                                }
                                                else { dic_output.Add("'" + headerName + "'" + " | Expected : " + headerValue + " | Actual : " + elm_span.Text + " |", false); break; }
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    DateTime date_fromPTL = DateTime.ParseExact(elm_span.Text, "MM/d/yyyy h:mm:ss tt", null);
                                                    dic_output.Add(headerName, true);
                                                    break;
                                                }
                                                catch (Exception ex)
                                                {
                                                    dic_output.Add("'" + headerName + "'" + " | Expected : " + headerValue + " | Actual : " + elm_span.Text + " |", false); break;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dic_output.Add(headerName, true);
                                        break;
                                    }
                                }
                                if(dic_output.Keys.Count == 0 && headerValue == "ToVerifyHeaderAlone")
                                {
                                    dic_output.Add("Given Header : " + headerName + " is not present inside the Group : " + groupName + " at the caption " + captionName, false);
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
            else
            {
                IWebElement elm_table = div_panel.FindElements(By.TagName("table")).First(x => x.GetAttribute("class") == "dynGridViewTable");
                foreach (KeyValuePair<string, string> keyvaluePair in dic_headerValues)
                {
                    string sourceColumnName = string.Empty;
                    string sourceColumnValue = string.Empty;
                    string destColumnName = string.Empty;
                    string destColumnValue = string.Empty;
                    int sourceColumnIndex = -1;
                    if (keyvaluePair.Key.ProcessString() != "multiple")
                    {
                        if (keyvaluePair.Key != "All")
                        {
                            sourceColumnName = keyvaluePair.Key.Split('|')[0];
                            sourceColumnValue = keyvaluePair.Key.Split('|')[1];
                        }
                        destColumnName = keyvaluePair.Value.Split('|')[0];
                        destColumnValue = keyvaluePair.Value.Split('|')[1] == "RunTime" ? cdd : keyvaluePair.Value.Split('|')[1];
                    }
                    else
                    {
                        sourceColumnName = keyvaluePair.Value.Split('|')[0].Split('&')[0];
                        destColumnName = keyvaluePair.Value.Split('|')[0].Split('&')[1];

                        sourceColumnValue = keyvaluePair.Value.Split('|')[1].Split('&')[0];
                        destColumnValue = keyvaluePair.Value.Split('|')[1].Split('&')[1];
                    }
                    List<IWebElement> list_columnHeaders = elm_table.FindElements(By.TagName("th")).ToList(); 
                    if (sourceColumnName != string.Empty)
                    {
                        sourceColumnIndex = list_columnHeaders.IndexOf(list_columnHeaders.First(x => x.Text.ProcessString() == sourceColumnName.ProcessString()));
                    }

                    int destColumnIndex = list_columnHeaders.IndexOf(list_columnHeaders.First(x => x.Text.ProcessString() == destColumnName.ProcessString()));

                    List<IWebElement> list_rows = elm_table.FindElement(By.TagName("tbody")).FindElements(By.TagName("tr")).Where(x => x.GetAttribute("id") != null && x.GetAttribute("id") != string.Empty).ToList();

                    for (int i=0;i< list_rows.Count;i++)
                    {
                        IWebElement row = list_rows[i];
                        bool flag = false;
                        List<IWebElement> list_cell = new List<IWebElement>();
                        ReadOnlyCollection<IWebElement> coll_elements = (ReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].children;", row);
                        coll_elements.ToList().ForEach(x => list_cell.Add(x));

                        if (keyvaluePair.Key.ProcessString() != "multiple")
                        {
                            if (sourceColumnIndex > 0)
                            {
                                if (list_cell[sourceColumnIndex].Text.ProcessString() == sourceColumnValue.ProcessString())
                                {
                                    flag = true;
                                }
                            }
                            else
                            {
                                if (keyvaluePair.Key == "All")
                                {
                                    flag = true;
                                    sourceColumnValue = captionName + "Row" + (i + 1).ToString();
                                }
                            }
                            if (flag)
                            {
                                string runTimeText = string.Empty;
                                IWebElement elm_input = null;
                                try
                                {
                                    elm_input = list_cell[destColumnIndex].FindElement(By.TagName("input"));
                                    if (elm_input.GetAttribute("type").ToString() == "checkbox")
                                    {
                                        runTimeText = (elm_input.GetAttribute("disabled") == "true") ? "disabled" : "enabled";
                                    }
                                    else
                                    {
                                        runTimeText = elm_input.GetAttribute("value");
                                    }
                                }
                                catch (NoSuchElementException ex)
                                {
                                    if (ex.Message.Contains("Unable to find element with tag name == input"))
                                    {
                                        elm_input = list_cell[destColumnIndex].FindElement(By.TagName("span"));
                                        runTimeText = elm_input.Text;
                                    }
                                }
                                if (runTimeText.ProcessString() == destColumnValue.ProcessString())
                                {
                                    dic_output.Add(sourceColumnValue + " : " + destColumnValue, true);
                                    if (keyvaluePair.Key != "All")
                                    {
                                        break;
                                    }
                                }
                                else
                                {
                                    dic_output.Add("'" + sourceColumnValue + " : " + destColumnValue + "'" + " | Expected : " + destColumnValue + " | Actual : " + list_cell[destColumnIndex].Text + " |", false);
                                    if (keyvaluePair.Key != "All")
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if(list_cell[destColumnIndex].Text.ProcessString() == destColumnValue)
                            {
                                if(!String.IsNullOrEmpty(list_cell[sourceColumnIndex].Text))
                                {
                                    dic_output.Add("The '" + sourceColumnName + "' is not empty, when the '" + destColumnName + "' value is- " + destColumnValue, false);
                                }
                            }
                        }                      

                    }

                }
            }

            if (needToClosePO)
            {
                driver.SelectFromRibbon("Purchase order", "Commit", "Save and close", null, true);
               // driver.WaitForLoadingInformation(); // always getting fail at this step
                try {
                    IWebElement elm_ok = driver.FindControl(Attributes.Id, "mainPopupWindow").FindElements(By.TagName("input")).First(x => x.GetAttribute("value") == "Yes");
                    elm_ok.Focus();
                    elm_ok.Click();
                }catch(Exception)
                { }
            }

            driver = driver.SwitchTo().DefaultContent();
            driver.WaitForLoadingInformation();
            driver.WaitForReady();

            return dic_output;
        }

        public static string GetSpecificColumnValues(this IWebDriver driver, string captionName, string columnName, int Index = 1, bool shouldSwitchFromFrame = true, string InvoiceQuantity = null, string Date = null)
        {
            string column_Value = string.Empty;
            IWebElement elm_table = null;
            if (captionName != null)
            {
                IWebElement div_panel = driver.ExpandFastTab(captionName,false,InvoiceQuantity);
                elm_table = div_panel.FindElements(By.TagName("table")).First(x => x.GetAttribute("class") == "dynGridViewTable");
            }
            else
            {
                elm_table = driver.FindElements(By.TagName("table")).First(x => x.GetAttribute("class") == "dynGridViewTable");
            }

            List<IWebElement> list_columnHeaders = elm_table.FindElements(By.TagName("th")).ToList();
            int columnIndex = list_columnHeaders.IndexOf(list_columnHeaders.First(x => x.Text.ProcessString() == columnName.ProcessString()));
            List<IWebElement> list_rows = elm_table.FindElement(By.TagName("tbody")).FindElements(By.TagName("tr")).Where(x => x.GetAttribute("id") != null && x.GetAttribute("id") != string.Empty).ToList();

            IWebElement row = list_rows[Index];
            List<IWebElement> list_cell = new List<IWebElement>();
            ReadOnlyCollection<IWebElement> coll_elements = (ReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].children;", row);
            coll_elements.ToList().ForEach(x => list_cell.Add(x));
            if (columnName == "Quantity")
            {
                
                list_cell[columnIndex].DrawHighlight();
                list_cell[columnIndex].Focus();
                list_cell[columnIndex].Click();
                IWebElement edit_Text = list_cell[columnIndex].FindElement(By.TagName("input"));
                edit_Text.SetText(InvoiceQuantity);
                driver.WaitForLoadingInformation();
            } else if(columnName == "Confirmed delivery date")
            {
                list_cell[columnIndex].DrawHighlight();
                list_cell[columnIndex].Focus();
                list_cell[columnIndex].Click();
                IWebElement edit_Text = list_cell[columnIndex].FindElement(By.TagName("input"));
                if (Date == null)
                {
                    edit_Text.SetText(cdd);
                }
                else
                {
                    edit_Text.SetText(Date);
                }
                column_Value = list_cell[columnIndex].Text;

                driver.WaitForLoadingInformation();
                driver.SelectFromRibbon("Purchase order", "Commit", "Save and close", null, true);
                driver.WaitForReady();
                driver.WaitForLoadingInformation();

                driver.SwitchTo().DefaultContent();
               
                //IWebElement elm_SchduleReason = driver.FindIdenticalControls(Attributes.Class, "ms-dlgContent").Last();
                // IWebDriver driver_confirm = driver.SwitchTo().Frame(elm_SchduleReason.FindControl(Attributes.Class, "ms-dlgFrame"));
                List<IWebElement> iframes = driver.FindElements(By.TagName("iframe")).ToList();
                iframes = iframes.FindAll(x => x.GetAttribute("Class") == "ms-dlgFrame");

                IWebDriver driver_Subiframe = driver.SwitchTo().Frame(iframes.Last());

                IWebElement elm_ReasonCode = driver_Subiframe.FindElements(By.TagName("input")).ToList().First(x => x.GetAttribute("value") == "OK");
                elm_ReasonCode.Focus();
                elm_ReasonCode.MouseClick();
                driver.WaitForLoadingInformation();

                if (Date == " ")
                {
                    driver.SwitchTo().DefaultContent();
                    IWebElement ctrl_IFrame = driver.FindControl(Attributes.Class, "ms-dlgFrame");
                    IWebDriver driver_frame = driver.SwitchTo().Frame(ctrl_IFrame);
                    IWebElement elm_yes = driver_frame.FindControl(Attributes.Id, "mainPopupWindow").FindElements(By.TagName("input")).First(x => x.GetAttribute("value") == "Yes");
                    elm_yes.Focus();
                    elm_yes.Click(false);
                    Thread.Sleep(10000);
                }

            }
            
            if (shouldSwitchFromFrame)
            {
                driver = driver.SwitchTo().DefaultContent();
                driver.WaitForReady();
            }
            return column_Value;
        }

        public static void Invoice(this IWebDriver driver, string group = null, string actions = null, string InvoiceQuantity = null)
        {
            #region Local Variables

            IWebElement main_IFrame = null;
            IWebElement sub_IFrame = null;
            IWebElement elm_ok = null;
            string ele_Tab = null;

            #endregion

            driver.FocusBrowserWindow();

            main_IFrame = driver.FindControl(Attributes.Class, "ms-dlgFrame");

            IWebDriver driver_iframe = driver.SwitchTo().Frame(main_IFrame);

            driver_iframe.SelectFromRibbon("Invoice process", "Default", "New vendor invoice");

            driver = driver.SwitchTo().DefaultContent();

            if (InvoiceQuantity != null)
            {
                ele_Tab = driver.GetSpecificColumnValues("Lines", "Quantity", 0, true, InvoiceQuantity);
            }

            List<IWebElement> iframes = driver.FindElements(By.TagName("iframe")).ToList();
            iframes = iframes.FindAll(x => x.GetAttribute("Class") == "ms-dlgFrame");

            IWebDriver driver_Subiframe = driver.SwitchTo().Frame(iframes[1]);
            if (actions == null)
            {
                driver_Subiframe.SelectFromRibbon("Edit", "Post", "Post invoice");
            }

            driver_Subiframe.WaitForLoadingInformation();

            if (actions == "Save and close")
            {
                driver_Subiframe.SelectFromRibbon("Edit", "Maintain", "Delete");
                Thread.Sleep(10000);
                driver.SwitchTo().Alert().Accept();

            }
            else
            {
                elm_ok = driver_Subiframe.FindControl(Attributes.Id, "mainPopupWindow").FindElements(By.TagName("input")).First(x => x.GetAttribute("value") == "OK");
                elm_ok.Focus();
                elm_ok.Click(false);
            }

            // driver_Subiframe.WaitForLoadingInformation();
            Thread.Sleep(90000);
            driver = driver.SwitchTo().DefaultContent();
            driver.WaitForLoadingInformation();

            driver.WaitForReady();

        }

        public static IWebElement ExpandFastTab(this IWebDriver driver, string captionName, bool shouldSwitchFromFrame = false, string InvoiceQuantity = null)
        {
            #region Local Variables
            IWebElement ctrl_IFrame = null;
            IWebDriver driver_frame = null;
            #endregion

            if (InvoiceQuantity!= null)
            {
                List<IWebElement> iframes = driver.FindElements(By.TagName("iframe")).ToList();
                iframes = iframes.FindAll(x => x.GetAttribute("Class") == "ms-dlgFrame");

                driver_frame = driver.SwitchTo().Frame(iframes[1]);
            }
            else
            {
                ctrl_IFrame = driver.FindControl(Attributes.Class, "ms-dlgFrame");
                driver_frame = driver.SwitchTo().Frame(ctrl_IFrame);
            }

            IWebElement elm_parent = driver_frame.FindIdenticalControls(Attributes.Class, "dynMultiSection").First(x => x.GetAttribute("Id").ToString() != string.Empty);
            IWebElement div_panel = null;
            foreach (IWebElement children in ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].children;", elm_parent)))
            {
                IWebElement elm_caption = children.FindIdenticalControls(Attributes.Class, "dynSectionHeaderCaption").First();

                if (elm_caption.Text.ProcessString() == captionName.ProcessString())
                {
                    IWebElement elm_fastTab = children.FindIdenticalControls(Attributes.Class, "dynSectionBody").First();
                    string Expanded = elm_fastTab.GetCssValue("display");
                    if (Expanded == "block")
                    {
                        div_panel = children;
                        break;
                    }
                    else if (Expanded == "none")
                    {
                        div_panel = children;
                        div_panel.Focus();
                        div_panel.Click();
                        break;
                    }

                }
            }

            if(shouldSwitchFromFrame)
            {
                driver = driver.SwitchTo().DefaultContent();                
                driver.WaitForReady();
            }

            return div_panel;

        }

        public static List<bool> VerifyTheTableValues(this IWebDriver driver, string captionName, string valueToVerify)
        {
            List<bool> coll = new List<bool>();
            IWebElement elm_table = driver.ExpandFastTab(captionName).FindElements(By.ClassName("dynGridViewTable")).First();
            List<IWebElement> elm_coll1 = elm_table.FindElements(By.TagName("th")).ToList();
            IWebElement elm_column = elm_coll1.First(x => x.Text.ToLower() == valueToVerify);
            int columnIndex = elm_coll1.IndexOf(elm_column);
            List<IWebElement> DeliveryDate = elm_table.FindElements(By.TagName("tr")).ToList();

            int count = 0;
            IWebElement row = null;
            for (int i = 1; i < DeliveryDate.Count; i++)
            {
                if (DeliveryDate[i].GetAttribute("class").StartsWith("dynGridView"))
                {
                    row = DeliveryDate[i];
                    if (!string.IsNullOrEmpty(row.Text))
                    {
                        if (valueToVerify == "confirmed delivery date" || valueToVerify == "actual delivery date")
                        {
                            //count = count + 1;
                            //if (count == 1)
                            //{
                            //    IWebElement elm_cell = row.FindElement(By.ClassName("AxInputField"));
                            //    if (cdd == elm_cell.GetAttribute("value"))
                            //    {
                            //        coll.Add(true);
                            //    }
                            //    else
                            //    {
                            //        coll.Add(false);
                            //    }


                            //}
                            //else
                            //{
                            IWebElement elm_cell = row.FindElements(By.TagName("td"))[columnIndex];

                            string date = string.Empty;
                            if(string.IsNullOrEmpty(elm_cell.Text))
                            {
                                date = elm_cell.GetAttribute("value");
                            }
                            else
                            {
                                string dte = elm_cell.Text;
                                DateTime dt;
                                dt = DateTime.Parse(dte);
                                date = dt.ToString("dd/MM/yyyy").Replace("-", "/");
                            }
                            if(date ==null)
                            {
                                IWebElement elm_input = elm_cell.FindElement(By.ClassName("AxInputField"));
                                date = elm_input.GetAttribute("value");
                            }
                            if(valueToVerify == "actual delivery date")
                            {
                                if (add == date)
                                {
                                    coll.Add(true);
                                }
                                else
                                {
                                    coll.Add(false);
                                }
                            }
                            else
                            {
                                if (cdd == date)
                                {
                                    coll.Add(true);
                                }
                                else
                                {
                                    coll.Add(false);
                                }
                            }
                           
                            // }
                        }
                        else
                        {
                            row = DeliveryDate[i];
                            IWebElement elm_cell = row.FindElements(By.TagName("td"))[columnIndex];
                            if (elm_cell.Text == "Invoiced")
                            {
                                coll.Add(true);
                            }
                            else
                            {
                                coll.Add(false);
                            }
                        }
                    }
                }
            }
            driver = driver.SwitchTo().DefaultContent();
            driver.WaitForLoadingInformation();
            driver.WaitForReady();
            return coll;
        }

        public static List<bool> verifyTheFastTabValues(this IWebDriver driver, string captionName, List<List<string>> listOfList, [Optional] string ERPNumber, [Optional] bool? IsConfirmDeliveryDateNeeded)
        {
            List<bool> result = new List<bool>();
            IWebElement elm_table = driver.ExpandFastTab(captionName);
            if (captionName != "Schedule ")
            {
                List<IWebElement> elm_Table1 = elm_table.FindElements(By.ClassName("dynGroup")).ToList();
                int count = 0;
                foreach (IWebElement table in elm_Table1)
                {
                    if (count != 1)
                    {
                        //List<IWebElement> elm_coll1 = table.FindElements(By.TagName("label")).ToList();
                        List<IWebElement> elm_coll1 = table.FindElements(By.TagName("tr")).ToList();
                        for (int i = 0; i < elm_coll1.Count; i++)
                        {
                            if (elm_coll1[i].Text.ProcessString() != listOfList[count][i].ProcessString()) { result.Add(false); }
                            else { result.Add(true); }
                        }
                    }
                    count++;
                }
            }
            else
            {
                string columnName = listOfList[0][0].Split(':')[0];
                string columnValue = listOfList[0][0].Split(':')[1];

                int columnIndex = 0;
                IWebElement elm_headerRow = elm_table.FindElement(By.TagName("table")).FindElements(By.TagName("tr")).First(x => x.GetAttribute("class") == "dynGridViewHeaderRowTr");

                for (int i = 0; i < elm_headerRow.FindElements(By.TagName("th")).Count; i++)
                {
                    string headerText = elm_headerRow.FindElements(By.TagName("th"))[i].Text;
                    if (headerText == columnName)
                    {
                        columnIndex = i;
                        break;
                    }
                }

                IWebElement elm_row = elm_table.FindElement(By.TagName("table")).FindElements(By.TagName("tr")).First(x => x.GetAttribute("class") != "dynGridViewHeaderRowTr");
                string valueAtRuntime = elm_row.FindElements(By.TagName("td"))[columnIndex].FindElement(By.TagName("span")).Text;
                if (valueAtRuntime == columnValue)
                {
                    result.Add(true);
                }
                else
                { result.Add(false); }

            }

            driver = driver.SwitchTo().DefaultContent();

            if (IsConfirmDeliveryDateNeeded != null)
            {
                if (IsConfirmDeliveryDateNeeded == false)
                {
                    driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase Order", "Homestore data", true,true);
                    //IWebElement ctrl_IFrame = driver.FindControl(Attributes.Class, "ms-dlgFrame");
                    //driver = driver.SwitchTo().Frame(ctrl_IFrame);

                    //driver.SelectFromRibbon("Purchase order", "Commit", "Save and close");
                    //IWebElement elm_ok = driver.FindControl(Attributes.Id, "mainPopupWindow").FindElements(By.TagName("input")).First(x => x.GetAttribute("value") == "Yes");
                    //elm_ok.Focus();
                    //elm_ok.Click();
                    //Thread.Sleep(5000);
                }
                if (IsConfirmDeliveryDateNeeded == true)
                {
                    string returnVaue = driver.VerifyERPEntryDate("Purchase order", "Homestore data", "ERP entry date");

                    if (string.IsNullOrEmpty(returnVaue))
                    {
                        result.Add(false);
                    }
                    else
                    {
                        result.Add(true);
                    }
                    driver.EnterERPNumberAndConfirmedDeliveryDate(string.Empty, "Purchase Order", "Homestore data");
                }
            }
            else
            {
                string returnVaue = driver.VerifyERPEntryDate("Purchase order", "Homestore data", "ERP entry date");

                if (string.IsNullOrEmpty(returnVaue))
                {
                    result.Add(false);
                }
                else
                {
                    result.Add(true);
                }
                List<bool> list1 = driver.VerifyTheTableValues("Purchase order lines ", "confirmed delivery date");
                List<bool> list2 = driver.VerifyTheTableValues("ASN Details ", "confirmed delivery date");
                list1.ForEach(x => result.Add(x));
                list2.ForEach(x => result.Add(x));
            }
           
            //driver = driver.SwitchTo().DefaultContent();
            //driver.WaitForLoadingInformation();
            driver.WaitForReady();
            return result;

        }

        public static void VerifyLineStage(this IWebDriver driver, string LineStage)
        {
            IWebElement elm_table = driver.FindElements(By.ClassName("dynGridViewTable"))[1];
            List<IWebElement> confirmedDeliveryDate = elm_table.FindElements(By.TagName("tr")).ToList();
            for (int i = 0; i < confirmedDeliveryDate.Count; i++)
            {
                if (confirmedDeliveryDate[i].GetAttribute("Id") != null)
                {
                    IWebElement row = confirmedDeliveryDate[i];
                    IWebElement elm_cell = row.FindElements(By.TagName("td"))[1];
                    if (!(elm_cell).Text.Contains(LineStage))
                    {
                        throw new Exception("Line stage is mismatch");
                    }

                }
            }

        }

        public static void EnterERPNumber(this IWebDriver driver, string ERPNumber, string captionName, string groupName)
        {
            #region Local Variables

            IWebElement ctrl_IFrame = null;
            IWebDriver driver_frame = null;

            #endregion

            ctrl_IFrame = driver.FindControl(Attributes.Class, "ms-dlgFrame");
            driver_frame = driver.SwitchTo().Frame(ctrl_IFrame);

            IWebElement elm_parent = driver_frame.FindIdenticalControls(Attributes.Class, "dynMultiSection").First(x => x.GetAttribute("Id").ToString() != string.Empty);
            IWebElement div_panel = null;
            foreach (IWebElement children in ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].children;", elm_parent)))
            {
                IWebElement elm_caption = children.FindIdenticalControls(Attributes.Class, "dynSectionHeaderCaption").First();
                if (elm_caption.Text.ProcessString() == captionName.ProcessString())
                {
                    div_panel = children;
                    break;
                }
            }


            List<IWebElement> coll_group = div_panel.FindIdenticalControls(Attributes.Class, "dynColumn");
            IWebElement elm_group = null;
            IWebElement elm_tableCaption = null;
            foreach (IWebElement group in coll_group)
            {
                IWebElement elm_caption = group.FindElements(By.TagName("caption")).First();
                if (elm_caption.Text.ProcessString() == groupName.ProcessString())
                {
                    elm_group = group;
                    elm_tableCaption = elm_caption.GetParent();
                    break;
                }
            }

            IWebElement elm_edit = null;
            List<IWebElement> coll = elm_tableCaption.FindElements(By.TagName("tr")).ToList<IWebElement>();
            for (int i = 0; i < coll.Count; i++)
            {
                IWebElement row = coll[i];

                IWebElement elm_cell = row.FindElements(By.TagName("td"))[0];

                if ((elm_cell).Text.Contains("ERP number"))
                {
                    elm_edit = row.FindElements(By.TagName("td"))[1];

                    ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver_frame).ExecuteScript("return arguments[0].children;", elm_edit)).ToList()[0].MouseClick();
                    ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver_frame).ExecuteScript("return arguments[0].children;", elm_edit)).ToList()[0].SetText(ERPNumber);
                    ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver_frame).ExecuteScript("return arguments[0].children;", elm_edit)).ToList()[0].MouseClick();

                    OpenQA.Selenium.Interactions.Actions action = new OpenQA.Selenium.Interactions.Actions(driver_frame);
                    action.SendKeys(Keys.Tab).Build().Perform();
                }
                driver_frame.SelectFromRibbon("Purchase order", "Commit", "Save and close");
                IWebElement elm_ok = driver_frame.FindControl(Attributes.Id, "mainPopupWindow").FindElements(By.TagName("input")).First(x => x.GetAttribute("value") == "Yes");
                elm_ok.Focus();
                elm_ok.Click();
                Thread.Sleep(10000);
                break;
            }
            driver = driver.SwitchTo().DefaultContent();
            driver.WaitForLoadingInformation();
        }

        public static void WaitForLoadingInformation(this IWebDriver driver)
        {
            #region Local Variables

            IWebElement elm_loading = null;

            #endregion

            Stopwatch timer = new Stopwatch();
            timer.Start();
            int mins = 0;
            driver.WaitForReady();

            while (true)
            {
                try {
                    elm_loading = driver.FindControl(Attributes.Id, "ctl00_AxProgressControl", PropertyExpression.Equals, true);
                    mins = timer.Elapsed.Minutes;
                    if (elm_loading == null || mins > 2)
                    {
                        break;
                    }
                }
                catch { driver.WaitForLoadingInformation(); }
            }
        }

        public static String VerifyERPEntryDate(this IWebDriver driver, string captionName, string groupName, string labelName)
        {
            #region Local Variables

            IWebElement ctrl_IFrame = null;
            IWebDriver driver_frame = null;
            String returnValue = String.Empty;


            #endregion

            ctrl_IFrame = driver.FindControl(Attributes.Class, "ms-dlgFrame");
            driver_frame = driver.SwitchTo().Frame(ctrl_IFrame);

            IWebElement elm_parent = driver_frame.FindIdenticalControls(Attributes.Class, "dynMultiSection").First(x => x.GetAttribute("Id").ToString() != string.Empty);
            IWebElement div_panel = null;
            foreach (IWebElement children in ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].children;", elm_parent)))
            {
                IWebElement elm_caption = children.FindIdenticalControls(Attributes.Class, "dynSectionHeaderCaption").First();
                if (elm_caption.Text.ProcessString() == captionName.ProcessString())
                {
                    div_panel = children;
                    break;
                }
            }


            List<IWebElement> coll_group = div_panel.FindIdenticalControls(Attributes.Class, "dynColumn");
            IWebElement elm_group = null;
            IWebElement elm_tableCaption = null;
            foreach (IWebElement group in coll_group)
            {
                IWebElement elm_caption = group.FindElements(By.TagName("caption")).First();
                if (elm_caption.Text.ProcessString() == groupName.ProcessString())
                {
                    elm_group = group;
                    elm_tableCaption = elm_caption.GetParent();
                    break;
                }
            }

            IWebElement elm_edit = null;
            List<IWebElement> coll = elm_tableCaption.FindElements(By.TagName("tr")).ToList<IWebElement>();
            for (int i = 0; i < coll.Count; i++)
            {
                IWebElement row = coll[i];

                IWebElement elm_cell = row.FindElements(By.TagName("td"))[0];

                if ((elm_cell).Text.Contains(labelName))
                {
                    elm_edit = row.FindElements(By.TagName("td"))[1];
                    returnValue = elm_edit.Text;
                    break;

                }

            }

            driver = driver.SwitchTo().DefaultContent();

            return returnValue;

        }

        public static string ActualDeliveryPriorDate(this IWebDriver driver, string captionName, string groupName)
        {
            #region Local Variables

            IWebElement ctrl_IFrame = null;
            IWebElement main_IFrame = null;
            IWebElement sub_IFrame = null;
            IWebElement elm_ok = null;
            string errorMessage = string.Empty;
            string actualDeliveryDate = string.Empty;

            #endregion

            //  driver.FocusBrowserWindow();

            main_IFrame = driver.FindControl(Attributes.Class, "ms-dlgFrame");

            IWebDriver driver_frame = driver.SwitchTo().Frame(main_IFrame);

            driver_frame.SelectFromRibbon("Invoice process", "Default", "New vendor invoice");

            driver = driver.SwitchTo().DefaultContent();

            List<IWebElement> iframes = driver.FindElements(By.TagName("iframe")).ToList();
            iframes = iframes.FindAll(x => x.GetAttribute("Class") == "ms-dlgFrame");

            driver_frame = driver.SwitchTo().Frame(iframes[1]);

            IWebElement elm_parent = driver_frame.FindIdenticalControls(Attributes.Class, "dynMultiSection").First(x => x.GetAttribute("Id").ToString() != string.Empty);
            IWebElement div_panel = null;
            foreach (IWebElement children in ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].children;", elm_parent)))
            {
                IWebElement elm_caption = children.FindIdenticalControls(Attributes.Class, "dynSectionHeaderCaption").First();
                if (elm_caption.Text.ProcessString() == captionName.ProcessString())
                {
                    div_panel = children;
                    break;
                }
            }

            List<IWebElement> coll_group = div_panel.FindIdenticalControls(Attributes.Class, "dynGroup");
            IWebElement elm_group = null;
            IWebElement elm_tableCaption = null;
            foreach (IWebElement group in coll_group)
            {
                IWebElement elm_caption = group.FindElements(By.TagName("caption")).First();
                if (elm_caption.Text.ProcessString() == groupName.ProcessString())
                {
                    elm_group = group;
                    elm_tableCaption = elm_caption.GetParent();
                    break;
                }
            }

            IWebElement elm_invoiceDate = null;
            foreach (IWebElement group in coll_group)
            {
                IWebElement elm_caption = group.FindElements(By.TagName("caption")).First();
                if (elm_caption.Text.ProcessString() == "Invoice dates".ProcessString())
                {
                    elm_invoiceDate = elm_caption.GetParent();
                    break;
                }
            }

            IWebElement elm_edit = null;
            List<IWebElement> coll = elm_invoiceDate.FindElements(By.TagName("tr")).ToList<IWebElement>();
            for (int i = 0; i < coll.Count; i++)
            {
                IWebElement row = coll[i];

                IWebElement elm_cell = row.FindElements(By.TagName("td"))[0];

                if ((elm_cell).Text.ToLower().Contains("invoice date"))
                {
                    elm_edit = row.FindElements(By.TagName("td"))[1];
                    actualDeliveryDate = elm_edit.Text;
                    break;
                }
            }

            coll = elm_tableCaption.FindElements(By.TagName("tr")).ToList<IWebElement>();
            for (int i = 0; i < coll.Count; i++)
            {
                IWebElement row = coll[i];

                IWebElement elm_cell = row.FindElements(By.TagName("td"))[0];

                if ((elm_cell).Text.ToLower().Contains("actual delivery"))
                {
                    elm_edit = row.FindElements(By.TagName("td"))[1];

                    IWebElement elm_table = elm_edit.FindElements(By.TagName("table"))[0];

                    elm_edit = elm_table.FindIdenticalControls(Attributes.Class, "dynJoiningRest")[0];

                    DateTime dt1 = new DateTime(int.Parse(actualDeliveryDate.Split('/')[2]), int.Parse(actualDeliveryDate.Split('/')[0]), int.Parse(actualDeliveryDate.Split('/')[1]));

                    cdd = dt1.AddDays(-1).ToString("M/d/yyyy").Replace("-", "/");
                    ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver_frame).ExecuteScript("return arguments[0].children;", elm_edit)).ToList()[0].MouseClick();
                    ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver_frame).ExecuteScript("return arguments[0].children;", elm_edit)).ToList()[0].SetText(cdd);
                    OpenQA.Selenium.Interactions.Actions action = new OpenQA.Selenium.Interactions.Actions(driver_frame);
                    action.SendKeys(Keys.Enter).Build().Perform();
                    break;
                }
            }

            driver = driver.SwitchTo().DefaultContent();

            IWebElement elm_MessageBoxContent = driver.FindIdenticalControls(Attributes.Class, "ms-dlgContent").Last();

            IWebDriver driver_confirm = driver.SwitchTo().Frame(elm_MessageBoxContent.FindControl(Attributes.Class, "ms-dlgFrame"));
            //dynPromptMessageBox

            IWebElement elm_MessageBox = driver_confirm.FindControl(Attributes.Class, "dynPromptMessageBox");
            IWebElement elm_MessageBody = elm_MessageBox.FindControl(Attributes.Class, "dynPolymorphicPromptBody");

            errorMessage = elm_MessageBody.FindElement(By.TagName("span")).Text;

            elm_ok = driver_confirm.FindElements(By.TagName("input")).ToList().First(x => x.GetAttribute("value") == "OK");

            return errorMessage;
        }

        public static List<string> GetColumnValues(this IWebDriver driver, string tableClassName, int index = 0)
        {
            List<string> columnlist = new List<string>();
            IWebElement elm_table;          

            if (index == 0)
            {
                elm_table = driver.FindControl(Attributes.Class, tableClassName);
            }
            else
            {
                elm_table = driver.FindElements(By.ClassName(tableClassName))[index];
            }

            List<IWebElement> list_column = elm_table.FindIdenticalControls(Attributes.Class, "dynGridViewHeaderRowTd");

            foreach (IWebElement colCell in list_column)
            {
                string cellValue = colCell.Text;
                columnlist.Add(cellValue);
            }
            
            return columnlist;
        }

        public static void AddFilter(this IWebDriver driver, string dropDownValue, string enterText = null, bool isEnterText = false)
        {
            
            IWebElement AddFilter = driver.FindControl(Attributes.Class, "dynFilterToolBar");
            IWebElement AddFilterPanel = AddFilter.FindControl(Attributes.Class, "dynFilterToolBarButtonsPanel");
            IWebElement AddFilterPanelText = AddFilterPanel.FindControl(Attributes.Class, "dynFilterToolBarButtonText");
            AddFilterPanelText.Click();

            IWebElement SelectAddFilter = driver.FindControl(Attributes.Class, "dynFilterToolBar");
            IWebElement Filter_Opt = SelectAddFilter.FindControl(Attributes.Class, "dynExpressionArea").FindElement(By.TagName("table"));
            IWebElement SelectAddFilterDropdown = Filter_Opt.FindElements(By.TagName("tr")).Last();
            IWebElement SelectDropDown = SelectAddFilterDropdown.FindIdenticalControls(Attributes.Class, "dyn-propselect").Last();
            SelectDropDown.Click();

            IWebElement SelectOption = driver.FindControl(Attributes.Class, "AxFilterFields").FindElement(By.TagName("select"));
            IWebElement FilterDropDown = SelectOption.FindElements(By.TagName("option")).First(x => x.GetAttribute("value") == dropDownValue);
            FilterDropDown.DrawHighlight();
            FilterDropDown.Focus();
            FilterDropDown.Click();
            driver.WaitForLoadingInformation();

            if (isEnterText == true)
            {
                IWebElement SelectAddFilterText = driver.FindControl(Attributes.Class, "dynFilterToolBar");
                IWebElement Text_Opt = SelectAddFilterText.FindControl(Attributes.Class, "dynExpressionArea").FindElement(By.TagName("table"));
                IWebElement TextOptionFieldName = Text_Opt.FindElements(By.TagName("tr")).First(x => x.GetAttribute("fieldName") == dropDownValue);
                IWebElement EnterText = TextOptionFieldName.FindControl(Attributes.Class, "AxInputField");
                EnterText.SetText(enterText);
            }
                           
        }

        public static string InvoiceDescriptionInInvoiceForm(this IWebDriver driver, string captionName, string groupName, string descriptionName, bool swithDefaultContent = false)
        {
            #region Local Variables

            IWebElement main_IFrame = null;
            string errorMessage = string.Empty;
            string InvoiceDescriptionValue = string.Empty;

            #endregion

            //  driver.FocusBrowserWindow();

            main_IFrame = driver.FindControl(Attributes.Class, "ms-dlgFrame");

            IWebDriver driver_frame = driver.SwitchTo().Frame(main_IFrame);

            driver_frame.SelectFromRibbon("Invoice process", "Default", "New vendor invoice");

            driver = driver.SwitchTo().DefaultContent();

            List<IWebElement> iframes = driver.FindElements(By.TagName("iframe")).ToList();
            iframes = iframes.FindAll(x => x.GetAttribute("Class") == "ms-dlgFrame");

            driver_frame = driver.SwitchTo().Frame(iframes[1]);

            IWebElement elm_parent = driver_frame.FindIdenticalControls(Attributes.Class, "dynMultiSection").First(x => x.GetAttribute("Id").ToString() != string.Empty);
            IWebElement div_panel = null;
            foreach (IWebElement children in ((IReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].children;", elm_parent)))
            {
                IWebElement elm_caption = children.FindIdenticalControls(Attributes.Class, "dynSectionHeaderCaption").First();
                if (elm_caption.Text.ProcessString() == captionName.ProcessString())
                {
                    div_panel = children;
                    break;
                }
            }

            List<IWebElement> coll_group = div_panel.FindIdenticalControls(Attributes.Class, "dynGroup");
            IWebElement elm_group = null;
            IWebElement elm_tableCaption = null;
            foreach (IWebElement group in coll_group)
            {
                IWebElement elm_caption = group.FindElements(By.TagName("caption")).First();
                if (elm_caption.Text.ProcessString() == groupName.ProcessString())
                {
                    elm_group = group;
                    elm_tableCaption = elm_caption.GetParent();
                    break;
                }
            }

            IWebElement elm_invoiceDescription = null;
            foreach (IWebElement group in coll_group)
            {
                IWebElement elm_caption = group.FindElements(By.TagName("caption")).First();
                if (elm_caption.Text.ProcessString() == groupName.ProcessString())
                {
                    elm_invoiceDescription = elm_caption.GetParent();
                    break;
                }
            }

            IWebElement elm_edit = null;
            List<IWebElement> coll = elm_invoiceDescription.FindElements(By.TagName("tr")).ToList<IWebElement>();
            for (int i = 0; i < coll.Count; i++)
            {
                IWebElement row = coll[i];

                IWebElement elm_cell = row.FindElements(By.TagName("td"))[0];

                if ((elm_cell).Text.ToLower().Contains(descriptionName.ToLower()))
                {
                    elm_edit = row.FindElements(By.TagName("td"))[1];
                    if (descriptionName == "Invoice description")
                    {
                        IWebElement InvoiceDescriptionInput = elm_edit.FindElement(By.TagName("input"));
                        InvoiceDescriptionValue = InvoiceDescriptionInput.GetAttribute("value");
                    }
                    else
                    {
                        InvoiceDescriptionValue = elm_edit.FindElement(By.TagName("span")).Text;
                    }

                    break;
                }
            }       
            
            if(swithDefaultContent == true)
            {
                driver.SwitchTo().DefaultContent();
            }   

            return InvoiceDescriptionValue;
        }

    }
}
