﻿using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Automation.Framework.Web;
using Ashley.QA.Automation.Framework.Windows;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Ashley.QA.Dynamics.AX2012.Automation.Utils
{
    public static class DynamicsAxCommonFunctions
    {
        #region Revamped

        public static WinWindow LaunchDynamicsAX(this WinWindow wnd_AX, string path)
        {
            #region Local Variables

            bool flag = false;
            Process[] processes;
            int sessionID;
            Process[] sessionProcesses;
            ApplicationUnderTest application = new ApplicationUnderTest();

            #endregion

            processes = Process.GetProcesses();
            sessionID = Process.GetCurrentProcess().SessionId;
            sessionProcesses = (from c in processes where c.SessionId == sessionID select c).ToArray();

            foreach (Process process in sessionProcesses)
            {
                if (process.ProcessName.Contains("Ax32"))
                {
                    flag = true;
                    break;
                }
            }

            if (flag != true)
            {
                application = ApplicationUnderTest.Launch(path);
            }

            wnd_AX = UITestControl.Desktop.DetectControl<WinWindow>(new { ClassName = "AxMainFrame" });
            wnd_AX = (WinWindow)wnd_AX.FindMatchingControls().First(x => x.Name.ToString().StartsWith("‪Microsoft Dynamics AX‬ - ‎‪Ashley Furniture Industries, Inc."));

            if (flag != true)
            {
                WaitForNotRespondingWindowToClose();
                wnd_AX.WaitForWindowToRespond();
            }

            wnd_AX.Maximized = true;
            return wnd_AX;
        }

        public static WinWindow FindAXChildFrameWindow(this WinWindow wnd_AX)
        {
            #region Local Variables

            WinWindow wnd_childFrameWindow = new WinWindow();

            #endregion

            wnd_childFrameWindow = wnd_AX.DetectControl<WinWindow>(new { ClassName = "AxChildFrame", Name = "WindowHeaderFrame" });

            return wnd_childFrameWindow;
        }

        public static void NavigateTo(this WinWindow wnd_AX, string address)
        {
            #region Local Variables

            WinWindow wnd_AXChildFrame = new WinWindow();
            WinClient clnt_AddressBar = new WinClient();
            WinToolBar toolbar_Address = new WinToolBar();
            WinButton btn_navigate = new WinButton();

            #endregion

            wnd_AX.SetFocus();
            wnd_AXChildFrame = wnd_AX.FindAXChildFrameWindow();

            clnt_AddressBar = wnd_AXChildFrame.DetectControl<WinClient>(new { Name = "AddressBarContainer" });

            toolbar_Address = clnt_AddressBar.DetectControl<WinToolBar>(new { });
            toolbar_Address.EnterWinText(address);

            btn_navigate = toolbar_Address.DetectControl<WinButton>(new { Name = "Navigate" });
            btn_navigate.WinClick();
        }

        public static void ApplyFilter(this WinWindow wnd_AX, string filterColumn, string filterValue)
        {
            #region Local Variables

            WinWindow wnd_PopupFrame = new WinWindow();
            WinWindow wnd_LookForFilter = new WinWindow();
            WinEdit edit_Filter = new WinEdit();
            WinWindow wnd_Filter = new WinWindow();
            WinControl ctrl_MenuButton = new WinControl();
            WinWindow wnd_DropDown = new WinWindow();
            WinMenuItem menuItem_Item = new WinMenuItem();
            WinButton btn_Go = new WinButton();

            #endregion

            wnd_AX.SetFocus();
            wnd_PopupFrame = wnd_AX.DetectControl<WinWindow>(new { ClassName = "AxPopupFrame" });

            wnd_LookForFilter = wnd_PopupFrame.DetectControl<WinWindow>(new { ControlName = "LookForFilter.LookForTextBox" });
            edit_Filter = wnd_LookForFilter.DetectControl<WinEdit>(new { ControlName = "LookForFilter.LookForTextBox", Name = "Type to filter" });
            edit_Filter.EnterWinText(filterValue);


            wnd_Filter = wnd_PopupFrame.DetectControl<WinWindow>(new { ControlName = "lookForFilter" });
            ctrl_MenuButton = wnd_Filter.DetectControl<WinControl>(new { Name = "Scope", ControlType = "MenuButton" });
            ctrl_MenuButton.WinClick();

            wnd_DropDown = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" });
            menuItem_Item = wnd_DropDown.DetectControl<WinMenuItem>(new { Name = filterColumn });
            if (menuItem_Item.Exists)
            {
                menuItem_Item.WinClick();
            }
            else
            {
                throw new Exception(filterColumn + " not available.");
            }

            btn_Go = wnd_Filter.DetectControl<WinButton>(new { Name = "Action button" });
            btn_Go.WinClick();
        }

        public static WinRow FetchARecordFromATable(this WinWindow wnd_AX, string tableName, Dictionary<String, String> dict_ColumnsAndValues)
        {
            #region Local Variables

            WinTable tbl_Table = new WinTable();
            UITestControlCollection uitcc_Rows = new UITestControlCollection();
            List<String> list_Columns = new List<String>();
            List<String> list_Values = new List<String>();
            WinRow row_Result = null;

            #endregion

            try
            {
                tbl_Table = wnd_AX.DetectControl<WinTable>(new { Name = tableName });
                uitcc_Rows = tbl_Table.GetChildren();
                list_Columns = uitcc_Rows[0].GetChildren().Select(x => x.Name).ToList<String>();

                uitcc_Rows.RemoveAt(0);

                foreach (WinRow r in uitcc_Rows)
                {
                    list_Values = r.Value.ToString().Split(';').ToList<String>();
                    bool flag = true;
                    foreach (KeyValuePair<String, String> kvp in dict_ColumnsAndValues)
                    {
                        int i = list_Columns.IndexOf(kvp.Key);
                        if (list_Values[i] != kvp.Value)
                        {
                            flag = false;
                            break;
                        }
                    }

                    if (flag == true)
                    {
                        row_Result = r;
                        break;
                    }
                }

                return row_Result;
            }
            catch
            {
                return row_Result;
            }
        }

        public static UITestControlCollection GetAllRecords(this WinWindow wnd_AX, string tableName)
        {
            #region Local Variables

            WinTable tbl_Table = new WinTable();
            UITestControlCollection uitcc_Rows = new UITestControlCollection();

            #endregion

            try
            {
                tbl_Table = wnd_AX.DetectControl<WinTable>(new { Name = tableName });
                uitcc_Rows = tbl_Table.GetChildren();
                uitcc_Rows.RemoveAt(0);

                return uitcc_Rows;
            }
            catch
            {
                return null;
            }
        }

        public static void SelectARecord(this WinWindow wnd_AX, string tableName, Dictionary<String, String> dict_ColumnsAndValues, bool select, bool open)
        {
            #region Local Variables

            WinTable tbl_Table = new WinTable();
            WinRow row_Record = new WinRow();

            #endregion

            wnd_AX.SetFocus();
            row_Record = wnd_AX.FetchARecordFromATable(tableName, dict_ColumnsAndValues);
            if (row_Record.Exists)
            {
                while (row_Record.HasFocus != select)
                {
                    row_Record.SetFocus();
                    row_Record.GetChildren()[1].WinClick();
                }
                if ((select == true) && (open == true))
                {
                    Keyboard.SendKeys("{Enter}");
                }
            }
            else
            {
                throw new Exception("Record not available.");
            }
        }

        public static void SelectARecord(this WinRow row_AX, bool select, bool open)
        {
            #region Local Variables

            #endregion

            if (row_AX.Exists)
            {
                while (row_AX.Selected != select)
                {
                    row_AX.SetFocus();
                    row_AX.GetChildren()[0].WinClick();
                }
                if ((select == true) && (open == true))
                {
                    Keyboard.SendKeys("{Enter}");
                }
            }
            else
            {
                throw new Exception("Record not available.");
            }
        }

        public static string GetSpecificColumnValue(this WinWindow window, string tableName, string columnName, int rowIndex = 1)
        {
            string outputValue = string.Empty;

            WinTable table = window.DetectControl<WinTable>(new { Name = tableName });

            UITestControlCollection coll_columnHeaders = new WinColumnHeader(table).FindMatchingControls();
            int columnIndex = coll_columnHeaders.IndexOf(coll_columnHeaders.First(x => x.Name.ProcessString() == columnName.ProcessString()));

            UITestControlCollection coll_rows = new WinRow(table).FindMatchingControls();
            WinRow row = new WinRow();
            if (rowIndex == 1)
            {
                row = (WinRow)coll_rows.Last();
            }
            else
            {
                row = (WinRow)coll_rows[rowIndex + 1];
            }
            WinCell cell = (WinCell)new WinCell(row).FindMatchingControls()[columnIndex];
            outputValue = cell.Value;

            return outputValue;
        }

        public static void ClickCheckInsideACell(this WinWindow window, string tableName, string sourceColumnName, string sourceColumnValue, string destinationColumnName, bool isChecked = false, bool isTab = false)
        {
            WinTable table = window.DetectControl<WinTable>(new { Name = tableName });

            UITestControlCollection coll_columnHeaders = new WinColumnHeader(table).FindMatchingControls();
            int sourceColumnIndex = coll_columnHeaders.IndexOf(coll_columnHeaders.First(x => x.Name.ProcessString() == sourceColumnName.ProcessString()));
            int destinationColumnIndex = coll_columnHeaders.IndexOf(coll_columnHeaders.First(x => x.Name.ProcessString() == destinationColumnName.ProcessString()));      
            UITestControlCollection coll_rows = new WinRow(table).FindMatchingControls();
            WinRow row = null;
            for (int i = 1; i < coll_rows.Count; i++)
            {
                row = (WinRow)coll_rows[i];
                WinCell cell = (WinCell)row.Cells[sourceColumnIndex];
                if (cell.Value.ProcessString() == sourceColumnValue.ProcessString())
                {
                    row.WinClick();
                    if (isTab == true)
                    {
                        int tabCount = 15;
                        while (tabCount != 0)
                        {
                            Keyboard.SendKeys("{TAB}");
                            tabCount--;
                        }
                    }
                    if (isChecked == false)
                    {
                        row.Cells[destinationColumnIndex].WinClick();
                    }
                    else
                    {
                        WinCheckBox checkbox = new WinCheckBox((WinCell)row.Cells[destinationColumnIndex]);
                        checkbox.Checked = true;
                    }


                    break;
                }
            }
        }




        public static List<string> GetColumnValues(this UITestControl window, string tableName, string columnName)
        {
            List<string> outputValue = new List<string>();

            WinTable table = window.DetectControl<WinTable>(new { Name = tableName });

            UITestControlCollection coll_columnHeaders = new WinColumnHeader(table).FindMatchingControls();
            int columnIndex = coll_columnHeaders.IndexOf(coll_columnHeaders.First(x => x.Name.ProcessString() == columnName.ProcessString()));

            UITestControlCollection coll_rows = new WinRow(table).FindMatchingControls();

            foreach (WinRow row in coll_rows)
            {
                if(row.Name != "Top Row")
                {
                    WinCell cell = (WinCell)new WinCell(row).FindMatchingControls()[columnIndex];
                    outputValue.Add(cell.Value);
                }
                
            }
            return outputValue;
        }

        public static String GetColumnValueOfARecord(this WinRow row_AX, String column)
        {
            #region Local Variables

            UITestControlCollection uitcc_Rows = new UITestControlCollection();
            List<String> list_Columns = new List<String>();
            List<String> list_Values = new List<String>();

            String value = String.Empty;

            #endregion

            if (row_AX.Exists)
            {
                uitcc_Rows = row_AX.GetParent().GetChildren();
                list_Columns = uitcc_Rows[0].GetChildren().Select(x => x.Name).ToList<String>();
                if (list_Columns.Contains(column))
                {
                    list_Values = row_AX.Value.ToString().Split(';').ToList<String>();
                    value = list_Values[list_Columns.IndexOf(column)];
                }
                else
                {
                    throw new Exception(column + " not available.");
                }
            }
            else
            {
                throw new Exception("Record not available.");
            }

            return value;
        }

        public static WinCell GetCellOfARecord(this WinRow row_AX, String column)
        {
            #region Local Variables

            UITestControlCollection uitcc_Rows = new UITestControlCollection();
            List<String> list_Columns = new List<String>();
            UITestControlCollection uitcc_Cells = new UITestControlCollection();

            WinCell cell_Column = new WinCell();

            #endregion

            if (row_AX.Exists)
            {
                uitcc_Rows = row_AX.GetParent().GetChildren();
                list_Columns = uitcc_Rows[0].GetChildren().Select(x => x.Name).ToList<String>();
                if (list_Columns.Contains(column))
                {
                    uitcc_Cells = row_AX.GetChildren();
                    cell_Column = (WinCell)uitcc_Cells[list_Columns.IndexOf(column)];
                }
                else
                {
                    throw new Exception(column + " not available.");
                }
            }
            else
            {
                throw new Exception("Record not available.");
            }

            return cell_Column;
        }

        public static void EnterValueIntoANewlyAddedCell(this WinWindow window, string tableName, string columnName, string columnValue)
        {
            WinTable table = window.DetectControl<WinTable>(new { Name = tableName });

            // UITestControlCollection coll_columnHeaders = new WinColumnHeader(table).FindMatchingControls();

            // int columnIndex = coll_columnHeaders.IndexOf(coll_columnHeaders.First(x => x.Name.ProcessString() == columnName.ProcessString()));

            WinRow row = table.DetectControl<WinRow>(new { Name = "Row 1" });

            UITestControlCollection coll_cells = new WinCell(row).FindMatchingControls();

            WinCell cell = (WinCell)coll_cells.First(x => x.Name.Contains(columnName));

            cell.WinClick();
            if (columnName != "Type")
            {
                Keyboard.SendKeys("{END}");
                Keyboard.SendKeys("{HOME}", System.Windows.Input.ModifierKeys.Shift);
                WinEdit edit = new WinEdit(cell);
                edit.EnterWinText(columnValue);
            }
            else
            {
                WinComboBox cmbBox = new WinComboBox(cell);
                cmbBox.SelectValueFromComboBox(columnValue);
            }
        }

        public static void EnterValueToCell(this WinWindow wnd_AX, string tableName, Dictionary<String, String> dict_ColumnsAndValues, string column, string value)
        {
            #region Local Variables

            WinRow row_Record = new WinRow();
            List<String> list_Columns = new List<String>();
            WinCell cell_Edit = new WinCell();
            WinEdit edit_Cell = new WinEdit();

            #endregion

            wnd_AX.SetFocus();
            row_Record = wnd_AX.FetchARecordFromATable(tableName, dict_ColumnsAndValues);
            do { row_Record.WinClick(); } while (!row_Record.HasFocus);

            list_Columns = row_Record.GetParent().GetChildren()[0].GetChildren().Select(x => x.Name).ToList<String>();
            if (list_Columns.Contains(column))
            {
                cell_Edit = (WinCell)row_Record.GetChildren()[list_Columns.IndexOf(column)];
                edit_Cell = cell_Edit.DetectControl<WinEdit>(new { Name = column });
                do { cell_Edit.WinClick(); } while (!edit_Cell.Exists);
                edit_Cell.Text = "";
                //for (int i = 0; i < edit_Cell.Text.Length; i++) { Keyboard.SendKeys("{RIGHT}"); }
                //for (int i = 0; i < edit_Cell.Text.Length; i++) { Keyboard.SendKeys("{BACK}"); }
                do { edit_Cell.EnterWinTextV2(value); } while (edit_Cell.Text.ToLower() != value.ToLower());
            }
            else
            {
                throw new Exception(column + " not available.");
            }
        }

        public static void CheckInsideACell(this WinWindow wnd_AX, string tableName, Dictionary<String, String> dict_ColumnsAndValues, string column, bool check)
        {
            #region Local Variables

            WinRow row_Record = new WinRow();
            List<String> list_Columns = new List<String>();
            WinCell cell_Edit = new WinCell();
            WinCheckBox checkbox_Cell = new WinCheckBox();

            #endregion

            wnd_AX.SetFocus();
            row_Record = wnd_AX.FetchARecordFromATable(tableName, dict_ColumnsAndValues);
            do { row_Record.WinClick(); } while (!row_Record.HasFocus);

            list_Columns = row_Record.GetParent().GetChildren()[0].GetChildren().Select(x => x.Name).ToList<String>();
            if (list_Columns.Contains(column))
            {
                cell_Edit = (WinCell)row_Record.GetChildren()[list_Columns.IndexOf(column)];
                checkbox_Cell = cell_Edit.DetectControl<WinCheckBox>(new { Name = column });
                do { cell_Edit.WinClick(); } while (!checkbox_Cell.Exists);
                checkbox_Cell.Checked = check;
            }
            else
            {
                throw new Exception(column + " not available.");
            }
        }

        public static void ApplyColumnFilter(this WinWindow wnd_AX, String tableName, string column, string condition, string value)
        {
            #region Local Variables

            WinTable table_Table = new WinTable();
            WinColumnHeader column_Name = new WinColumnHeader();
            WinWindow wnd_Filter = new WinWindow();
            WinButton btn_Arrow = new WinButton();
            WinWindow wnd_Menu = new WinWindow();
            WinMenu menu_Conditions = new WinMenu();
            WinMenuItem item_Condition = new WinMenuItem();

            #endregion

            wnd_AX.SetFocus();
            table_Table = wnd_AX.DetectControl<WinTable>(new { Name = tableName });

            column_Name = table_Table.DetectControl<WinColumnHeader>(new { Name = column });
            if (column_Name.Exists)
            {
                Mouse.Click(new Point((column_Name.BoundingRectangle.Location.X + column_Name.BoundingRectangle.Width / 2), (column_Name.BoundingRectangle.Location.Y + column_Name.BoundingRectangle.Height / 3)));
            }
            else
            {
                throw new Exception(column + " not available.");
            }

            wnd_Filter = column_Name.DetectControl<WinWindow>(new { ClassName = "AxPaneWnd" });
            btn_Arrow = wnd_Filter.DetectControl<WinButton>(new { ClassName = "AxButton" });
            btn_Arrow.SetFocus();
            Keyboard.SendKeys("{SPACE}");

            wnd_Menu = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "Context" });
            menu_Conditions = wnd_Menu.DetectControl<WinMenu>(new { Name = "Context" });
            item_Condition = menu_Conditions.DetectControl<WinMenuItem>(new { Name = "Clear filter" });
            item_Condition.WinClick();

            btn_Arrow.SetFocus();
            Keyboard.SendKeys("{SPACE}");

            item_Condition = menu_Conditions.DetectControl<WinMenuItem>(new { Name = condition });
            if (item_Condition.Exists)
            {
                item_Condition.WinClick();
            }
            else
            {
                throw new Exception(condition + " not available.");
            }

            WinEdit edt_Value = wnd_Filter.DetectControl<WinEdit>(new { ClassName = "AxEdit" });
            edt_Value.WinClick();
            Keyboard.SendKeys(value);
            Keyboard.SendKeys("{ENTER}");
        }

        public static WinControl FindRibbonMenuItem(this WinWindow wnd_AX, string tab, string group, string item)
        {
            #region Local Variables

            WinWindow wnd_AXChildFrame = new WinWindow();
            WinClient clnt_TabContainer = new WinClient();
            WinButton btn_Tab = new WinButton();
            WinGroup group_Menu = new WinGroup();
            WinControl ctrl_Item = new WinControl();

            #endregion

            try
            {
                wnd_AX.SetFocus();
                wnd_AXChildFrame = wnd_AX.FindAXChildFrameWindow();
                clnt_TabContainer = wnd_AXChildFrame.DetectControl<WinClient>(new { Name = "MiddleRow" });

                btn_Tab = new WinButton(clnt_TabContainer);
                btn_Tab = (WinButton)btn_Tab.FindMatchingControls().First(x => ((WinButton)x).FriendlyName.ProcessString() == tab.ProcessString());
                if (btn_Tab.Exists)
                {
                    wnd_AX.SetFocus();
                    btn_Tab.WinClick();
                }
                else
                {
                    throw new Exception(tab + " not found.");
                }

                group_Menu = wnd_AXChildFrame.DetectControl<WinGroup>(new { Name = group });
                ctrl_Item = group_Menu.DetectControl<WinControl>(new { Name = item });
                if (ctrl_Item.Exists)
                {
                    return ctrl_Item;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public static void ClickRibbonMenuItem(this WinWindow wnd_AX, string tab, string group, string item, [Optional] bool shouldAddtabText)
        {
            #region Local Variables

            WinWindow wnd_AXChildFrame = new WinWindow();
            WinClient clnt_TabContainer = new WinClient();
            WinButton btn_Tab = new WinButton();
            WinGroup group_Menu = new WinGroup();
            WinControl ctrl_Item = new WinControl();
            string value = string.Empty;

            #endregion
            if (shouldAddtabText == false)
            {
                value = tab == "Main" ? "afm" + tab + "Tab" : tab + "Tab";
            }
            else
            {
                value = tab;
            }
            // string value = tab == "Main" ? "afm" + tab + "Tab" : tab ;


            wnd_AX.SetFocus();
            // wnd_AXChildFrame = wnd_AX.FindAXChildFrameWindow();
            // clnt_TabContainer = wnd_AX.DetectControl<WinClient>(new { Name = "MiddleRow" });
            // WinWindow wnd_tab = clnt_TabContainer.DetectControl<WinWindow>(new { Name = tab });


            btn_Tab = wnd_AX.DetectControl<WinButton>(new { Name = value });

            //btn_Tab = new WinButton(clnt_TabContainer);
            //btn_Tab = (WinButton)btn_Tab.FindMatchingControls().First(x => ((WinButton)x).Name.ProcessString() == (tab+"Tab").ProcessString());
            //  if (btn_Tab.Exists)
            //    {
            btn_Tab.WinClick();
            //  }
            //  else
            //    {
            //        throw new Exception(tab + " not found.");
            //  }

            //WinWindow wnd_group = wnd_AX.DetectControl<WinWindow>(new { ControlName = "afmMain" });

            group_Menu = wnd_AX.DetectControl<WinGroup>(new { Name = group });
            ctrl_Item = group_Menu.DetectControl<WinControl>(new { Name = item });
            if (ctrl_Item.Exists)
            {
                if (ctrl_Item.Enabled == true)
                {
                    ctrl_Item.WinClick();
                }
                else
                {
                    throw new Exception(item + " disabled.");
                }
            }
            else
            {
                throw new Exception(item + " not found.");
            }
        }

        public static void ClickTableMenuItem(this WinWindow wnd_AX, string windowControlName, string button, [Optional] string menuItem)
        {
            #region Local Variables

            WinWindow wnd_Window = new WinWindow();
            WinControl ctrl_Button = new WinControl();
            WinMenuItem menuItem_Dropdown = new WinMenuItem();

            #endregion

            wnd_AX.SetFocus();
            wnd_Window = wnd_AX.DetectControl<WinWindow>(new { ControlName = windowControlName });

            ctrl_Button = wnd_Window.DetectControl<WinControl>(new { Name = button });
            if (ctrl_Button.Exists)
            {
                if (ctrl_Button.Enabled == true)
                {
                    ctrl_Button.WinClick();

                    if (menuItem != null)
                    {
                        menuItem_Dropdown = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" }).DetectControl<WinMenu>(new { Name = "DropDown" }).DetectControl<WinMenuItem>(new { Name = menuItem });
                        if (menuItem_Dropdown.Exists)
                        {
                            if (menuItem_Dropdown.Enabled == true)
                            {
                                menuItem_Dropdown.WinClick();
                            }
                            else
                            {
                                throw new Exception(menuItem + " is disabled.");
                            }
                        }
                        else
                        {
                            throw new Exception(menuItem + " not found.");
                        }
                    }
                }
                else
                {
                    throw new Exception(button + " is disabled.");
                }
            }
            else
            {
                throw new Exception(button + " not found.");
            }
        }

        public static void SelectValueFromCustomDropdown(this WinWindow wnd_AX, string dropdownName, string column, string value, bool isNeededWindowName = true)
        {
            #region Local Variables

            WinEdit edit_Dropdown = new WinEdit();
            WinClient client_Arrow = new WinClient();
            WinWindow wnd_Dropdown = new WinWindow();
            WinTable table_Items = new WinTable();
            WinCell cell_Item = new WinCell();

            #endregion

            wnd_AX.SetFocus();
            edit_Dropdown = wnd_AX.DetectControl<WinEdit>(new { Name = dropdownName });
            client_Arrow = edit_Dropdown.DetectControl<WinClient>(new { Name = dropdownName }, PropertyExpressionOperator.Contains);
            if (client_Arrow.Exists)
            {
                client_Arrow.WinClick();
            }
            else
            {
                throw new Exception(dropdownName + " not available.");
            }
            if (isNeededWindowName)
            {
                wnd_Dropdown = (WinWindow)UIAutomationControl.Desktop.DetectControl<WinWindow>(new { ClassName = "AxPopupFrame", Name = " (‎‪1‬ - ‎‪ecm‬)" });
            }
            else
            {
                wnd_Dropdown = (WinWindow)UIAutomationControl.Desktop.DetectControl<WinWindow>(new { ClassName = "AxPopupFrame" });
            }
            table_Items = wnd_Dropdown.DetectControl<WinTable>(new { Name = "Grid" });
            cell_Item = table_Items.DetectControl<WinCell>(new { Name = column, Value = value }, PropertyExpressionOperator.Contains);
            if (cell_Item.Exists)
            {
                cell_Item.GetParent().WinClick();
            }
            else
            {
                throw new Exception(value + " not available under " + column + ".");
            }
        }

        public static void SelectValueFromCustomComboBox(this WinControl parentControl, string value)
        {
      
            WinControl lookupbutton = (WinControl)parentControl.GetChildren().First(x => ((WinControl)x).ControlType == "Window").GetChildren().First(x => ((WinControl)x).ControlType == "Client");
            //Mouse.Click(lookupbutton);
            lookupbutton.WinClick();

            WinControl popupframe = new WinControl();
            popupframe.SearchProperties.Add("ClassName", "AxPopupFrame", PropertyExpressionOperator.EqualTo);
            WinControl pframe = (WinControl)popupframe.FindMatchingControls().First(x => x.GetProperty("HasTitleBar").ToString() == "False");

            WinTable table = new WinTable(pframe);
            table.SearchProperties.Add("Name", "Grid", PropertyExpressionOperator.EqualTo);

            WinCell cell = new WinCell(table);
            cell.SearchProperties.Add("ControlType", "Cell", PropertyExpressionOperator.EqualTo);
            cell.SearchProperties.Add("Value", value, PropertyExpressionOperator.EqualTo);

            //Mouse.Click(cell);
            Keyboard.SendKeys("{ENTER}");
            // cell.SetFocus();
            // cell.WinClick();
        }


        public static List<string> HandleInfolog(bool close)
        {
            #region Local Variables

            WinWindow wnd_Infolog = new WinWindow();
            WinWindow wnd_Message = new WinWindow();
            WinTree tree_Infolog = new WinTree();
            WinTreeItem treeItem_Message = new WinTreeItem();
            List<string> list_Messages = new List<string>();
            WinButton btn_Close = new WinButton();
            Stopwatch sw = new Stopwatch();
            double elapsedMins = 0;
            bool Flag = false;
            #endregion
            sw.Start();
            while (Flag == false)
            {
                try
                {
                    elapsedMins = sw.Elapsed.Minutes;
                    wnd_Infolog = UITestControl.Desktop.FindWinWindow(new { Name = "Infolog", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
                    Flag = true;
                }
                catch (Exception ex)
                {
                    if (!ex.Message.ToLower().Contains("sequence contains no matching element"))
                    {
                        throw new Exception(ex.Message);
                    }
                }

                if (Flag == false && elapsedMins > 20)
                {
                    Flag = true;
                }
            }

            wnd_Message = wnd_Infolog.DetectControl<WinWindow>(new { ClassName = "AxPaneWnd" });
            list_Messages = wnd_Message.FindMatchingControls().GetNamesOfControls().ToList();
            list_Messages.RemoveAll(x => x == null);
            String InfologMessage = list_Messages[0];

            tree_Infolog = wnd_Infolog.DetectControl<WinTree>(new { ClassName = "AxTreeViewCtrl" });
            treeItem_Message = new WinTreeItem(tree_Infolog);
            list_Messages = treeItem_Message.FindMatchingControls().GetNamesOfControls().ToList();
            list_Messages.RemoveAll(x => x.StartsWith("Message"));
            list_Messages.RemoveAll(x => x.StartsWith("Posting"));
            list_Messages.Insert(0, InfologMessage);

            if (close == true)
            {
                btn_Close = wnd_Infolog.DetectControl<WinButton>(new { Name = "Close" });
                btn_Close.WinClick();
            }

            return list_Messages;
        }

        public static WinClient ExpandFastTab(this WinWindow wnd_AX, string fastTab, bool expand)
        {
            #region Local Variables

            List<UITestControl> list_uitc_Sections = new List<UITestControl>();
            WinClient client_FastTab = new WinClient();

            #endregion

            wnd_AX.SetFocus();
            list_uitc_Sections = wnd_AX.DetectControl<WinClient>(new { }).FindMatchingControls().ToList();
            list_uitc_Sections = list_uitc_Sections.FindAll(x => x.Name == fastTab).ToList();
            if (list_uitc_Sections.Count == 0)
            {
                throw new Exception(fastTab + " not found.");
            }
            else
            {
                if (expand == true)
                {
                    if (list_uitc_Sections.Count == 1)
                    {
                        list_uitc_Sections[0].WinClick();
                        list_uitc_Sections = wnd_AX.DetectControl<WinClient>(new { }).FindMatchingControls().ToList().FindAll(x => x.Name == fastTab).ToList();
                    }
                }

                client_FastTab = (WinClient)list_uitc_Sections.First(x => x.Height == Math.Max(list_uitc_Sections[0].Height, list_uitc_Sections[1].Height));
            }

            return client_FastTab;
        }


        public static string GetFastTabValueTabDetails(this WinWindow window, string tabNameToExpand, string tabNameToSelect, string headerGroupName, string headerName, [Optional] ControlType controlType)
        {
            string outputValue = string.Empty;

            if (controlType == null)
            {
                controlType = ControlType.Edit;
            }

            WinClient client_fastTab = window.ExpandFastTab(tabNameToExpand, true);

            if (!String.IsNullOrEmpty(tabNameToSelect))
            {
                client_fastTab.SelectTabInFastTab(tabNameToSelect);
            }
            WinClient client_headerLabel = null;
            

            if (controlType.Name == "Edit")
            {
                client_headerLabel = client_fastTab.DetectControl<WinClient>(new { Name = headerGroupName });
                List<UITestControl> coll_headerText = client_headerLabel.DetectControl<WinEdit>(new { Name = headerName }).FindMatchingControls().Distinct().ToList();

                coll_headerText.ToList().ForEach(x => outputValue += ((WinEdit)x).Text + " " + outputValue);
            }
            else if (controlType.Name == "Combobox")
            {
                client_headerLabel = client_fastTab.DetectControl<WinClient>(new { Name = headerGroupName });
                WinComboBox cmb = client_headerLabel.DetectControl<WinComboBox>(new { Name = headerName });
                outputValue = cmb.SelectedItem;
            }
            else if (controlType.Name == "Table")
            {
                List<String> columnList = client_fastTab.GetColumnValues(headerGroupName, headerName);
                bool allAreSame = columnList.All(x => x == columnList.First());
                if(allAreSame)
                {
                    outputValue = columnList.First().ToString();
                }

            }
            else
            {
                client_headerLabel = client_fastTab.DetectControl<WinClient>(new { Name = headerGroupName });
                WinCheckBox checkbox = client_headerLabel.DetectControl<WinCheckBox>(new { Name = headerName });
                outputValue = checkbox.Checked == true ? "Yes" : "No";
            }

            return outputValue;
        }

        public static void ChooseFromRibbonMenu(this WinWindow wnd_Ax, string menu, List<string> menuItems)
        {
            #region Local Variables

            WinMenuBar menuBar_Bar = new WinMenuBar();
            WinMenuItem menuItem_Item = new WinMenuItem();

            #endregion

            wnd_Ax.SetFocus();
            menuBar_Bar = wnd_Ax.DetectControl<WinMenuBar>(new { Name = menu });
            if (menuBar_Bar.Exists)
            {
                if (menuBar_Bar.Enabled)
                {
                    menuBar_Bar.WinClick();
                }
                else
                {
                    throw new Exception(menu + " disabled.");
                }
            }
            else
            {
                throw new Exception(menu + " not available.");
            }

            for (int i = 0; i < menuItems.Count; i++)
            {
                if (i == 0)
                {
                    menuItem_Item = menuBar_Bar.DetectControl<WinMenuItem>(new { Name = menuItems[i] });
                }
                else
                {
                    menuItem_Item = menuItem_Item.DetectControl<WinMenuItem>(new { Name = menuItems[i] });
                }

                if (menuItem_Item.Exists)
                {
                    if (menuItem_Item.Enabled)
                    {
                        Playback.Wait(1000);
                        menuItem_Item.WinClick();
                    }
                    else
                    {
                        throw new Exception(menuItems[i] + " disabled.");
                    }
                }
                else
                {
                    throw new Exception(menuItems[i] + " not available.");
                }
            }
        }

        public static void SelectTabInFastTab(this WinClient client_AX, string tab)
        {
            #region Local Variables

            WinTabList tabList_Tabs = new WinTabList();
            WinTabPage tabPage_Tab = new WinTabPage();

            #endregion

            client_AX.SetFocus();
            tabList_Tabs = client_AX.DetectControl<WinTabList>(new { });

            tabPage_Tab = tabList_Tabs.DetectControl<WinTabPage>(new { Name = tab });
            if (tabPage_Tab.Exists)
            {
                tabPage_Tab.WinClick();
            }
            else
            {
                throw new Exception(tab + " not available.");
            }
        }

        public static void SelectFromTreeItems(this WinWindow wnd_AX, string treeItem)
        {
            #region Local variables            
            string[] treeItems;

            WinTree parentOfTreeItem = wnd_AX.DetectControl<WinTree>(new { Name = "Tree" });
            WinTreeItem subParent = null;
            WinTreeItem childTreeItem = null;
            #endregion
            treeItems = treeItem.Split('/');
            wnd_AX.SetFocus();

            for (int i = 0; i < treeItems.Length; i++)
            {
                if (subParent == null)
                {
                    childTreeItem = parentOfTreeItem.DetectControl<WinTreeItem>(new { name = treeItems[i] });
                }
                else
                {
                    childTreeItem = subParent.DetectControl<WinTreeItem>(new { name = treeItems[i] });
                }
                if (childTreeItem.Expanded == false)
                {
                    Mouse.Hover(new Point(childTreeItem.BoundingRectangle.Location.X + childTreeItem.BoundingRectangle.Width / 2, childTreeItem.BoundingRectangle.Location.Y + childTreeItem.BoundingRectangle.Height / 2));
                    Mouse.Click(new Point(childTreeItem.BoundingRectangle.Location.X + childTreeItem.BoundingRectangle.Width / 2, childTreeItem.BoundingRectangle.Location.Y + childTreeItem.BoundingRectangle.Height / 2));
                }
                subParent = childTreeItem;
            }


        }

        public static void SearchRecordFromReport(this WinWindow wnd_AX, string orderNumber)
        {
            #region Local variables
            WinEdit edit_SearchRecord = new WinEdit();
            WinButton btn_FindRecord = new WinButton();
            #endregion

            edit_SearchRecord = wnd_AX.DetectControl<WinToolBar>(new { Name = "Toolstrip" }).DetectControl<WinEdit>(new { Name = "Find text in report" });
            edit_SearchRecord.Text = orderNumber;
            btn_FindRecord = wnd_AX.DetectControl<WinButton>(new { Name = "FindButton" });
            btn_FindRecord.WinClick();

        }


        #endregion

        #region Old

        public static void CheckSpecifiedCellCheckboxFromWinTable(this WinWindow wnd_main, string tableName, string condColumnName, string condColumnValue)
        {
            WinTable tbl_main = wnd_main.DetectControl<WinTable>(new { Name = tableName });

            WinCell cel_cond = tbl_main.DetectControl<WinCell>(new { Name = condColumnName, Value = condColumnValue }, PropertyExpressionOperator.Contains);

            //((WinRow)cel_cond.GetParent()).DrawHighlight();

            WinCell cel_req = (WinCell)((WinRow)cel_cond.GetParent()).Cells[0];

            cel_req.CheckACheckBoxInsideWincell();

        }

        public static string GetValuefromWinTable(WinWindow window, string tableName, string condColumnName, string condColumnValue, string reqColumnName)
        {
            WinTable tbl_main = window.DetectControl<WinTable>(new { Name = tableName });

            WinCell cel_cond = (WinCell)tbl_main.DetectControl<WinCell>(new { Name = condColumnName }, PropertyExpressionOperator.Contains).FindMatchingControls().First(x => ((WinCell)x).Value.Equals(condColumnValue));

            WinCell cel_req = ((WinRow)cel_cond.GetParent()).DetectControl<WinCell>(new { Name = reqColumnName }, PropertyExpressionOperator.Contains);

            return cel_req.Value;
        }

        public static string PartialCanceltheSelectedLine(this WinWindow wnd_Main, string Quantity, string DeliverRemainder, string CancelDescription)
        {
            string CancelledQty = string.Empty;

            WinEdit edt_deliverRemainder = wnd_Main.DetectControl<WinEdit>(new { Name = "Deliver remainder" });

            edt_deliverRemainder.EnterWinText(DeliverRemainder);

            wnd_Main.SelectValueFromCustomDropdown("Reason", "Cancelation reason description", CancelDescription);

            WinButton btn_OK = wnd_Main.DetectControl<WinButton>(new { Name = "OK" });

            btn_OK.WinClick();

            CancelledQty = (double.Parse(Quantity) - double.Parse(DeliverRemainder)).ToString();

            return CancelledQty;
        }

        public static void CheckACheckBoxInsideWincell(this WinCell cell)
        {
            WinCheckBox chkbox = new WinCheckBox(cell);
            if (chkbox.FindMatchingControls().Count > 0)
            {
                chkbox.Checked = true;
            }
            else
            {
                cell.WinClick();
            }
        }

        public static void SelectValueFromWinCombobox(this WinWindow wnd_main, string comboName, string valueToSelect)
        {
            WinComboBox cmb_main = wnd_main.DetectControl<WinComboBox>(new { Name = comboName });

            WinButton btn_arrow = cmb_main.DetectControl<WinButton>(new { Name = "Open" });
            btn_arrow.WinClick();

            WinList lst_cmb = cmb_main.DetectControl<WinList>(new { ClassName = "ComboLBox" });

            WinListItem lstItm_cmb = lst_cmb.DetectControl<WinListItem>(new { Name = valueToSelect });
            lstItm_cmb.WinClick();
        }



        public static int FindColumnIndexForSpecifiedColumninWinTable(this WinWindow wnd_main, string tableName, string reqColumnName)
        {
            WinTable tbl_InvoiceHeading = wnd_main.DetectControl<WinTable>(new { Name = tableName });

            WinRow Row = new WinRow(tbl_InvoiceHeading);

            UITestControlCollection coll_Rows = Row.FindMatchingControls();

            WinRow rw_Header = (WinRow)coll_Rows[0];

            WinColumnHeader col_Header = new WinColumnHeader(rw_Header);

            UITestControlCollection coll_headr = col_Header.FindMatchingControls();

            int columnIndex = 0;

            for (int i = 0; i < coll_headr.Count; i++)
            {
                if (coll_headr[i].Name == reqColumnName)
                {
                    columnIndex = i;

                    break;
                }
            }

            return columnIndex;

        }

        public static string PerformReplacementTypeOperation(this WinWindow wnd_main, string AdjustmentType, string AdjustmentReason, out string obsoleteInvoice)
        {

            string newInvoice = string.Empty;

            obsoleteInvoice = string.Empty;

            int columnIndex = wnd_main.FindColumnIndexForSpecifiedColumninWinTable("Invoice_Heading", "Invoice");

            int RowIndex = 0;

            wnd_main.SelectValueFromWinCombobox("Adjustment type", AdjustmentType);

            wnd_main.SelectValueFromCustomDropdown("Reason code", "Reason code", AdjustmentReason);

            WinTable tbl_InvoiceHeading = wnd_main.DetectControl<WinTable>(new { Name = "Invoice_Heading" });

            WinRow Row = new WinRow(tbl_InvoiceHeading);

            UITestControlCollection coll_Rows = Row.FindMatchingControls();

            for (int j = 1; j < coll_Rows.Count; j++)
            {
                WinRow Rw = (WinRow)coll_Rows[j];

                Rw.WinClick();

                WinTable tbl_InvoiceLines = wnd_main.DetectControl<WinTable>(new { Name = "Invoice_Lines" });

                if (tbl_InvoiceLines.Rows.Count > 2)
                {
                    RowIndex = j;

                    WinCell cell = tbl_InvoiceHeading.GetCell(RowIndex, columnIndex);

                    newInvoice = cell.Value;

                    columnIndex = wnd_main.FindColumnIndexForSpecifiedColumninWinTable("Invoice_Heading", "Select all");

                    cell = tbl_InvoiceHeading.GetCell(RowIndex, columnIndex);

                    cell.CheckACheckBoxInsideWincell();
                }
                else
                {
                    RowIndex = j;

                    WinCell cell = tbl_InvoiceHeading.GetCell(RowIndex, columnIndex);

                    obsoleteInvoice = cell.Value;
                }
            }

            WinButton Btn_OK = wnd_main.DetectControl<WinButton>(new { Name = "OK" });

            Btn_OK.WinClick();

            return newInvoice;
        }

        public static string PerformCustomerReturnOperation(this WinWindow wnd_main, string AdjustmentType, string AdjustmentReason, string Quantity, string ItemCode, out List<string> obsoleteInvoice)
        {

            string newInvoice = string.Empty;

            obsoleteInvoice = new List<string>();

            int columnIndex = wnd_main.FindColumnIndexForSpecifiedColumninWinTable("Invoice_Heading", "Invoice amount");

            int RowIndex = 0;

            string Invoice = string.Empty;

            string InvoiceAmount = string.Empty;

            wnd_main.SelectValueFromWinCombobox("Adjustment type", AdjustmentType);

            wnd_main.SelectValueFromCustomDropdown("Reason code", "Reason code", AdjustmentReason);

            WinTable tbl_InvoiceHeading = wnd_main.DetectControl<WinTable>(new { Name = "Invoice_Heading" });

            WinRow Row = new WinRow(tbl_InvoiceHeading);

            UITestControlCollection coll_Rows = Row.FindMatchingControls();

            for (int j = 1; j < coll_Rows.Count; j++)
            {
                WinRow Rw = (WinRow)coll_Rows[j];

                Rw.WinClick();

                WinCell cell = tbl_InvoiceHeading.GetCell(j, columnIndex);

                newInvoice = cell.Value;

                if (newInvoice == "0.00")
                {
                    RowIndex = j;

                    cell = tbl_InvoiceHeading.GetCell(RowIndex, columnIndex);

                    newInvoice = cell.Value;

                    columnIndex = wnd_main.FindColumnIndexForSpecifiedColumninWinTable("Invoice_Heading", "Select all");

                    cell = tbl_InvoiceHeading.GetCell(RowIndex, columnIndex);

                    cell.CheckACheckBoxInsideWincell();

                    WinTable tbl_InvoiceLines = wnd_main.DetectControl<WinTable>(new { Name = "Invoice_Lines" });

                    int itemColumnIndex = wnd_main.FindColumnIndexForSpecifiedColumninWinTable("Invoice_Lines", "Item");

                    int QtyColumnIndex = wnd_main.FindColumnIndexForSpecifiedColumninWinTable("Invoice_Lines", "Quantity");

                    WinRow LineRow = new WinRow(tbl_InvoiceLines);

                    UITestControlCollection coll_LineRows = LineRow.FindMatchingControls();

                    for (int i = 1; i < coll_Rows.Count; i++)
                    {
                        //WinRow LineRw = (WinRow)coll_Rows[i];

                        WinCell ItemCell = tbl_InvoiceLines.GetCell(i, itemColumnIndex);

                        if (ItemCell.Value == ItemCode)
                        {
                            WinCell QuantityCell = tbl_InvoiceLines.GetCell(i, QtyColumnIndex);

                            QuantityCell.WinClick();

                            WinEdit edt_Quantity = wnd_main.DetectControl<WinEdit>(new { Name = "Quantity" });

                            edt_Quantity.EnterWinText(Quantity);

                            break;

                        }

                    }

                }
                else
                {
                    RowIndex = j;

                    cell = tbl_InvoiceHeading.GetCell(RowIndex, columnIndex);

                    obsoleteInvoice.Add(cell.Value);
                }
            }

            WinButton Btn_OK = wnd_main.DetectControl<WinButton>(new { Name = "OK" });

            Btn_OK.WinClick();

            return newInvoice;
        }

        public static string PerformExchangeProcessOperation(this WinWindow wnd_main, string AdjustmentType, string AdjustmentReason, string Quantity, string ItemCode, out List<string> obsoleteInvoice)
        {
            string newInvoice = string.Empty;

            obsoleteInvoice = new List<string>();

            int columnIndex = wnd_main.FindColumnIndexForSpecifiedColumninWinTable("Invoice_Heading", "Invoice amount");

            int RowIndex = 0;

            string Invoice = string.Empty;

            string InvoiceAmount = string.Empty;

            wnd_main.SelectValueFromWinCombobox("Adjustment type", AdjustmentType);

            wnd_main.SelectValueFromCustomDropdown("Reason code", "Reason code", AdjustmentReason);

            WinTable tbl_InvoiceHeading = wnd_main.DetectControl<WinTable>(new { Name = "Invoice_Heading" });

            WinRow Row = new WinRow(tbl_InvoiceHeading);

            UITestControlCollection coll_Rows = Row.FindMatchingControls();

            for (int j = 1; j < coll_Rows.Count; j++)
            {
                WinRow Rw = (WinRow)coll_Rows[j];

                Rw.WinClick();

                WinCell cell = tbl_InvoiceHeading.GetCell(j, columnIndex);

                newInvoice = cell.Value;

                if (newInvoice == "0.00")
                {
                    RowIndex = j;

                    cell = tbl_InvoiceHeading.GetCell(RowIndex, columnIndex);

                    newInvoice = cell.Value;

                    WinTable tbl_InvoiceLines = wnd_main.DetectControl<WinTable>(new { Name = "Invoice_Lines" });

                    int itemColumnIndex = wnd_main.FindColumnIndexForSpecifiedColumninWinTable("Invoice_Lines", "Item");

                    int QtyColumnIndex = wnd_main.FindColumnIndexForSpecifiedColumninWinTable("Invoice_Lines", "Quantity");

                    WinRow LineRow = new WinRow(tbl_InvoiceLines);

                    UITestControlCollection coll_LineRows = LineRow.FindMatchingControls();

                    for (int i = 1; i < coll_Rows.Count; i++)
                    {
                        WinCell ItemCell = tbl_InvoiceLines.GetCell(i, itemColumnIndex);

                        if (ItemCell.Value == ItemCode)
                        {
                            WinCell QuantityCell = tbl_InvoiceLines.GetCell(i, QtyColumnIndex);

                            QuantityCell.WinClick();

                            WinEdit edt_Quantity = wnd_main.DetectControl<WinEdit>(new { Name = "Quantity" });

                            edt_Quantity.EnterWinText(Quantity);

                            Keyboard.SendKeys("{ENTER}");

                            WinButton Btn_Exchange = wnd_main.DetectControl<WinButton>(new { Name = "Exchange item" });

                            Btn_Exchange.WinClick();

                            WinWindow wnd_Return = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Find replacement item:", ClassName = "AxTopLevelFrame" });

                            WinTable tbl_GridExisting = wnd_Return.DetectControl<WinTable>(new { Name = "GridExistingItems" });

                            WinRow GridRow = new WinRow(tbl_GridExisting);

                            UITestControlCollection coll_GridRows = GridRow.FindMatchingControls();

                            int exchangeQtyIndex = wnd_Return.FindColumnIndexForSpecifiedColumninWinTable("GridExistingItems", "Exchange qty");

                            int itemNumberIndex = wnd_Return.FindColumnIndexForSpecifiedColumninWinTable("GridExistingItems", "Item number");

                            for (int k = 1; k < coll_GridRows.Count; k++)
                            {

                                WinCell itemNumberCell = tbl_GridExisting.GetCell(k, itemNumberIndex);

                                if (itemNumberCell.Value == ItemCode)
                                {

                                    WinCell exchangeQtyCell = tbl_GridExisting.GetCell(k, exchangeQtyIndex);

                                    exchangeQtyCell.WinClick();

                                    WinEdit edt_Exchange = wnd_Return.DetectControl<WinEdit>(new { Name = "Exchange qty" });

                                    edt_Exchange.EnterWinText(Quantity);

                                    WinButton Btn_Apply = wnd_Return.DetectControl<WinButton>(new { Name = "Apply" });

                                    Btn_Apply.WinClick();

                                    break;
                                }

                            }

                            break;

                        }
                        else
                        {
                            RowIndex = j;

                            cell = tbl_InvoiceHeading.GetCell(RowIndex, columnIndex);

                            obsoleteInvoice.Add(cell.Value);
                        }

                        break;

                    }
                }
            }

            WinButton Btn_OK = wnd_main.DetectControl<WinButton>(new { Name = "OK" });

            Btn_OK.WinClick();

            return newInvoice;
        }

        #endregion

        #region Wait related common functions

        public static void WaitForNotRespondingWindowToClose()
        {
            #region Local Variables

            bool flag = false;

            #endregion

            while (flag == false)
            {
                if (WindowsCommonFunctions.IsWinWindowExists("Not Responding") == false)
                {
                    flag = true;
                }
            }

        }

        public static void WaitForWindowToRespond(this WinWindow wnd_AX)
        {
            #region Local Variables

            string title = string.Empty;

            #endregion

            try
            {
                do
                {
                    title = wnd_AX.Name;
                }
                while (title.ToLower().Contains("not responding"));
            }
            catch { }
        }

        public static void WaitForSaleOrderToBeDisplayedInAX(this WinWindow wnd_AX, string saleOrder)
        {
            #region Local Variables

            Stopwatch stopwatch = new Stopwatch();
            bool isRecordAvailable = false;

            #endregion

            stopwatch.Start();

            while (isRecordAvailable == false)
            {
                wnd_AX.ApplyFilter("Sales order", saleOrder);
                isRecordAvailable = wnd_AX.FetchARecordFromATable("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }) != null ? true : false;
                if (isRecordAvailable == true)
                { break; }
                if (stopwatch.Elapsed.Minutes > 15)
                {
                    isRecordAvailable = false;
                    stopwatch.Stop();
                    break;
                }
            }

            if (isRecordAvailable == false)
            {
                throw new Exception("#" + saleOrder + " Sale Order is not displayed in AX even after 10 mins.");
            }
        }

        public static void WaitForPurchaseOrderToBeGenerated(this WinWindow wnd_AX)
        {
            #region Local Variables

            Stopwatch stopwatch = new Stopwatch();
            bool isGenerated = false;
            WinControl ctrl_PurchaseOrder = new WinControl();

            #endregion

            stopwatch.Start();

            ctrl_PurchaseOrder = wnd_AX.FindRibbonMenuItem("General", "Related information", "Purchase order");
            isGenerated = ctrl_PurchaseOrder.Enabled;
            while (isGenerated == false)
            {
                wnd_AX.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

                ctrl_PurchaseOrder = wnd_AX.FindRibbonMenuItem("General", "Related information", "Purchase order");
                if (ctrl_PurchaseOrder.Enabled == true)
                {
                    isGenerated = true;
                    break;
                }
                if (stopwatch.Elapsed.Minutes > 45)
                {
                    isGenerated = false;
                    stopwatch.Stop();
                    break;
                }
            }

            if (isGenerated == false)
            {
                throw new Exception("Purchase Order Creation Pending Status was not changed even after 10 mins.");
            }
        }

        #endregion

        #region CSV to DataTable function

        public static DataTable ConvertCSVToDataTable()
        {
            List<string[]> lines = File.ReadAllLines(Directory.GetCurrentDirectory() + "\\TestData\\SiteCoreTestDatas.csv").Select(a => a.Split(';')).ToList();

            DataTable dt_ = new DataTable();
            for (int i = 0; i < lines.Count; i++)
            {
                if (i == 0)
                {
                    foreach (string columnName in lines[i][0].ToString().Split(','))
                    {
                        dt_.Columns.Add(columnName);
                    }
                }
                else
                {
                    DataRow rw = dt_.NewRow();
                    for (int j = 0; j < lines[i][0].ToString().Split(',').Length; j++)
                    {
                        rw[j] = lines[i][0].ToString().Split(',')[j];
                    }

                    dt_.Rows.Add(rw);

                }
            }

            return dt_;
        }

        #endregion

        public static void EnterWinTextV2(this UITestControl control, string text)
        {
            control.WinClick();
            Keyboard.SendKeys("{END}");
            Keyboard.SendKeys("{HOME}", System.Windows.Input.ModifierKeys.Shift);
            if (String.IsNullOrEmpty(text))
            {
                Keyboard.SendKeys("{DELETE}");
            }
            else
            {
                Keyboard.SendKeys(text);
            }
        }

        public static void WinHover(this UITestControl control)
        {
            #region Local Variables

            Rectangle rect_Hover = new Rectangle();

            #endregion

            rect_Hover = control.BoundingRectangle;
            control.SetFocus();
            Mouse.Move(new Point(rect_Hover.Location.X + rect_Hover.Width / 2, rect_Hover.Location.Y + rect_Hover.Height / 2));
        }

        public static void HandleMessageBox(String name, String button)
        {
            #region Local Variables

            WinWindow wnd_MessageBox = new WinWindow();
            WinButton btn_Button = new WinButton();

            #endregion

            wnd_MessageBox = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = name });
            if (wnd_MessageBox.Exists)
            {
                wnd_MessageBox.SetFocus();
                btn_Button = wnd_MessageBox.DetectControl<WinButton>(new { Name = button });
                if (btn_Button.Exists)
                {
                    btn_Button.WinClick();
                }
                else
                {
                    throw new Exception(button + " Button not available.");
                }
            }
            else
            {
                throw new Exception(name + " Window not available.");
            }
        }

        public static String Extract(this List<String> list, String keyword)
        {
            try
            {
                return list.First(x => x.Contains(keyword.Split(' ')[1])).Split(',').ToList().First(x => x.Contains(keyword.Split(' ')[0]) && x.Contains(keyword.Split(' ')[1])).Trim().Split(':')[1].Trim().Replace("'", "");
            }
            catch
            {
                throw new Exception("Error in extracting '" + keyword + "' from\n" + String.Join("\n", list) + ".");
            }
        }

        public static String GetMessageBoxMessage(String name)
        {
            #region Local Variables

            WinWindow wnd_MessageBox = new WinWindow();
            String message = String.Empty;

            #endregion

            wnd_MessageBox = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = name });
            if (wnd_MessageBox.Exists)
            {
                wnd_MessageBox.SetFocus();
                message = wnd_MessageBox.DetectControl<WinButton>(new { Name = name, ControlType = "Dialog" }).DetectControl<WinText>(new { }).Name;
                return message;
            }
            else
            {
                throw new Exception(name + " Window not available.");
            }
        }

        public static void CustomTestCleanup(this TestContext testContext)
        {
            try
            {
                foreach (String s in new List<String>() { "Ax32", "IEDriverServer", "chromedriver", "phantomjs", "chrome", "iexplore", "firefox" })
                {
                    foreach (var process in Process.GetProcessesByName(s))
                    {
                        process.Kill();
                    }
                }
            }
            catch { }
            //finally
            //{
            //    const BindingFlags privateGetterFlags = System.Reflection.BindingFlags.GetField |
            //                                      System.Reflection.BindingFlags.GetProperty |
            //                                      System.Reflection.BindingFlags.NonPublic |
            //                                      System.Reflection.BindingFlags.Instance |
            //                                      System.Reflection.BindingFlags.FlattenHierarchy;
            //    if (testContext.CurrentTestOutcome != UnitTestOutcome.Passed)
            //    {
            //        var field = testContext.GetType().GetField("m_currentResult", privateGetterFlags);
            //        var m_currentResult = field.GetValue(testContext);
            //        field = m_currentResult.GetType().GetField("m_errorInfo", privateGetterFlags);
            //        var m_errorInfo = field.GetValue(m_currentResult);
            //        field = m_errorInfo.GetType().GetField("m_message", privateGetterFlags);
            //        string m_message = field.GetValue(m_errorInfo) as string;
            //        throw new Exception(m_message);
            //    }
            //}
        }
    }
}
