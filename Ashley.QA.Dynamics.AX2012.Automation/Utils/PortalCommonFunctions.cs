﻿using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Automation.Framework.Web;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ashley.QA.Automation.Framework.Windows;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Drawing;

namespace Ashley.QA.Dynamics.AX2012.Automation.Utils
{
    public static class PortalCommonFunctions
    {
        #region Revamped

        public static BrowserWindow LaunchPortal(string url, string username, string password)
        {
            #region Local Variables

            BrowserWindow browserWindow = new BrowserWindow();

            #endregion

            #region chrome
            
            browserWindow = WebCommonFunctions.LaunchBrowser("chrome", url);

            browserWindow = browserWindow.WaitForPageToLoad();

            #endregion

            return browserWindow;
        }

        public static UITestControl SelectFromRibbon(this UITestControl control, string tab, string section, string item, [Optional]string subItem)
        {
            #region Local Variables

            HtmlDocument document = new HtmlDocument();
            HtmlControl div_Ribbon = new HtmlControl();
            HtmlHyperlink hyperlink_Tab = new HtmlHyperlink();
            HtmlControl ctrl_Section = new HtmlControl();
            HtmlControl ctrl_SectionParentContainer = new HtmlControl();
            HtmlHyperlink hyperlink_Item = new HtmlHyperlink();
            HtmlHyperlink hyperlink_SubItem = new HtmlHyperlink();

            #endregion

            document = new HtmlDocument(control);
            document.WaitForControlReady();

            div_Ribbon = document.DetectControl<HtmlControl>(new { Id = "Ribbon" });

            try
            {
                hyperlink_Tab = div_Ribbon.DetectControl<HtmlHyperlink>(new { Class = "ms-cui-tt-a" });
                hyperlink_Tab = (HtmlHyperlink)hyperlink_Tab.FindMatchingControls().First(x => ((HtmlHyperlink)x).Title.ProcessString() == tab.ProcessString());
                hyperlink_Tab.Click();           
                
            }
            catch
            {
                throw new Exception(tab + " not available.");
            }

            try
            {
                ctrl_Section = div_Ribbon.DetectControl<HtmlControl>(new { Class = "ms-cui-groupTitle", TagName = "SPAN" });
                ctrl_Section = (HtmlControl)ctrl_Section.FindMatchingControls().First(x => ((HtmlControl)x).InnerText.ProcessString() == section.ProcessString());
                ctrl_SectionParentContainer = (HtmlControl)ctrl_Section.GetParent();
            }
            catch
            {
                throw new Exception(section + " not available.");
            }

            try
            {
                hyperlink_Item = new HtmlHyperlink(ctrl_SectionParentContainer);
                hyperlink_Item = (HtmlHyperlink)hyperlink_Item.FindMatchingControls().First(x => ((HtmlHyperlink)x).InnerText.ProcessString() == item.ProcessString());
                hyperlink_Item.Click();
            }
            catch
            {
                throw new Exception(item + " not available.");
            }

            if (subItem != null)
            {
                try
                {
                    hyperlink_SubItem = document.DetectControl<HtmlHyperlink>(new { Class = "ms-cui-ctl-menu " });
                    hyperlink_SubItem = (HtmlHyperlink)hyperlink_SubItem.FindMatchingControls().First(x => ((HtmlHyperlink)x).InnerText.ProcessString() == subItem.ProcessString());
                    hyperlink_SubItem.Click();
                }
                catch
                {
                    throw new Exception(subItem + " not available.");
                }
            }            

            //browserWindow = ((BrowserWindow)control).WaitForPageToLoad();
            return control;
        }

        public static BrowserWindow SelectTab(this BrowserWindow browserWindow, string tab)
        {
            #region Local Variables

            HtmlDocument document = new HtmlDocument();
            HtmlHyperlink hyperlink_Tab = new HtmlHyperlink();            

            #endregion

            document = new HtmlDocument(browserWindow);
            document.WaitForControlReady();

            try
            {
                hyperlink_Tab = new HtmlHyperlink(document);
                hyperlink_Tab.SearchProperties.Add(HtmlHyperlink.PropertyNames.Class, "menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode", PropertyExpressionOperator.Contains);
                hyperlink_Tab.SearchProperties.Add(HtmlHyperlink.PropertyNames.Class, "static", PropertyExpressionOperator.Contains);
                hyperlink_Tab = (HtmlHyperlink)hyperlink_Tab.FindMatchingControls().First(x=>x.GetProperty("InnerText").ToString().ProcessString().StartsWith(tab.ProcessString()));
                if (!hyperlink_Tab.Class.StartsWith("static selected"))
                {
                    Mouse.Click(hyperlink_Tab);
                }
            }
            catch
            {
                throw new Exception(tab + " not available.");
            }

            browserWindow = browserWindow.WaitForPageToLoad();
            return browserWindow;
        }

        public static BrowserWindow PortalFilter(this BrowserWindow browserWindow, string field, string value)
        {
            #region Local Variables

            HtmlDocument document = new HtmlDocument();
            HtmlSpan span_QF = new HtmlSpan();
            HtmlEdit edit_QF = new HtmlEdit();
            HtmlComboBox combobox_QF = new HtmlComboBox();

            #endregion

            document = new HtmlDocument(browserWindow);
            document.WaitForControlReady();

            span_QF = document.DetectControl<HtmlSpan>(new { Class = "dynQuickFilterOuter" });

            edit_QF = span_QF.DetectControl<HtmlEdit>(new { Class = "dynQuickFilterEdit" }, PropertyExpressionOperator.Contains);
            Mouse.Click(edit_QF);
            edit_QF = span_QF.DetectControl<HtmlEdit>(new { Class = "dynQuickFilterEditFocused" });
            Mouse.Click(edit_QF);
            edit_QF.Text = value;

            combobox_QF = span_QF.DetectControl<HtmlComboBox>(new { Class = "dynQuickFilterField" });
            combobox_QF.SelectedItem=field;

            span_QF.DetectControl<HtmlImage>(new { Title = "Apply filter" }).Click();

            browserWindow.WaitForLoadingInformation();
            browserWindow = browserWindow.WaitForPageToLoad();

            return browserWindow;
        }

        public static HtmlRow SearchRecord(this BrowserWindow browserWindow, Dictionary<String, String> dictionary_ColumnAndValue)
        {
            #region Local Variables

            HtmlDocument document = new HtmlDocument();
            HtmlTable table = new HtmlTable();
            HtmlDiv div_Pager = new HtmlDiv();
            HtmlHyperlink hyperlink_Page = new HtmlHyperlink();
            UITestControlCollection uitcc_Rows = new UITestControlCollection();
            HtmlRow row_Record = new HtmlRow();
            HtmlControl row_Header = new HtmlControl();

            List<String> list_Columns = new List<String>();
            #endregion

            document = new HtmlDocument(browserWindow);
            table = document.DetectControl<HtmlTable>(new { Class = "dynGridViewTable" });

            int i = 1;

            if (!table.InnerText.ToLower().Contains("This grid is empty".ToLower()))
            {
                #region Start Search from First Page

                div_Pager = document.DetectControl<HtmlDiv>(new { Class = "dynGridViewPager" });

                hyperlink_Page = div_Pager.DetectControl<HtmlHyperlink>(new { Class = "dynGridViewPagerFirstPage" });
                if (hyperlink_Page.Exists)
                {
                    hyperlink_Page.Click();
                }
                try
                {
                    hyperlink_Page = (HtmlHyperlink)(div_Pager.DetectControl<HtmlHyperlink>(new { Class = "dynGridViewPagerPage" }).FindMatchingControls().First(x => x.GetProperty("InnerText").ToString() == i.ToString()));
                    if (hyperlink_Page.Exists)
                    {
                        hyperlink_Page.Click();
                    }
                }
                catch { }

                #endregion

                for (;;)
                {
                    uitcc_Rows = table.GetChildren()[2].GetChildren();

                    row_Header = (HtmlControl)table.GetChildren()[1].GetChildren()[0];

                    list_Columns = new List<String>();
                    foreach (HtmlHeaderCell x in row_Header.GetChildren())
                    { list_Columns.Add(x.FriendlyName); }

                    foreach (KeyValuePair<String, String> kvp in dictionary_ColumnAndValue)
                    {
                        uitcc_Rows.RemoveAll(x => x.GetChildren()[list_Columns.IndexOf(kvp.Key)].GetProperty("InnerText").ToString() != kvp.Value);
                    }

                    if (uitcc_Rows.Count > 0)
                    {
                        row_Record = (HtmlRow)uitcc_Rows[0];
                        break;
                    }
                    else
                    {
                        i = i + 1;
                        hyperlink_Page = div_Pager.DetectControl<HtmlHyperlink>(new { Class = "dynGridViewPagerPage", InnerText = i.ToString() });
                        if (hyperlink_Page.Exists)
                        {
                            hyperlink_Page.Click();
                        }
                        else
                        { break; }
                    }
                }
            }

            return row_Record;
        }

        public static void CheckRecord(this HtmlRow record, bool check)
        {
            new HtmlCheckBox(record.GetChildren()[0]).Checked = check;
        }

        public static BrowserWindow EnterERPNumberAndConfirmedDeliveryDate(this BrowserWindow browserWindow, string ERPNumber)
        {
            #region Local Variables

            HtmlDocument document = new HtmlDocument();
            HtmlControl ctrl_IFrame = new HtmlControl();
            HtmlEdit edit_ERPNumber = new HtmlEdit();
            String cdd = String.Empty;
            HtmlEdit edit_ConfirmDate = new HtmlEdit();
            HtmlInputButton button_ApplyToLines = new HtmlInputButton();
            HtmlInputButton button_SaveAndClose = new HtmlInputButton();
            HtmlDiv div_SchduleReason = new HtmlDiv();
            HtmlInputButton btn_ReasonCode = new HtmlInputButton();
            HtmlDiv div_PurchaseOrder = new HtmlDiv();

            #endregion

            browserWindow = browserWindow.WaitForPageToLoad();
            document = new HtmlDocument(browserWindow);

            ctrl_IFrame = document.DetectControl<HtmlControl>(new { TagName = "IFRAME", Class = "ms-dlgFrame" });
            document = new HtmlDocument(ctrl_IFrame);


            HtmlDiv div_parent =(HtmlDiv) document.DetectControl<HtmlDiv>(new { Class = "dynMultiSection" }).FindMatchingControls().First(x => x.GetProperty("Id").ToString() != string.Empty);
            HtmlDiv div_panel = null;
            foreach(HtmlControl children in div_parent.GetChildren())
            {
                HtmlDiv div_caption =(HtmlDiv) children.DetectControl<HtmlDiv>(new { Class = "dynSectionHeaderCaption" }).FindMatchingControls().First();
                if(div_caption.InnerText.ProcessString() == "purchaseorder")
                {
                    div_panel =(HtmlDiv) children;
                    break;
                }
            }


            UITestControlCollection coll_group = div_panel.DetectControl<HtmlDiv>(new { Class = "dynColumn" }).FindMatchingControls();
            HtmlDiv div_group = null;
            HtmlTable table_caption = null;
            foreach(HtmlDiv group in coll_group)
            {
                HtmlControl caption = (HtmlControl)group.DetectControl<HtmlControl>(new { TagName = "caption" }).FindMatchingControls().First();
                if(caption.InnerText.ProcessString() =="homestoredata")
                {
                    div_group = group;
                    table_caption = (HtmlTable)caption.GetParent();
                    break;
                }
            }

            HtmlEdit edit = null;
            foreach (HtmlRow row in table_caption.Rows)
            {
                if (((HtmlCell)row.Cells[0]).Title.Contains("ERP reference number"))
                {
                    edit = new HtmlEdit((HtmlCell)row.Cells[1]);
                    edit.Click();
                    Keyboard.SendKeys(ERPNumber);
                    Keyboard.SendKeys("{TAB}");
                    cdd = DateTime.Today.AddDays(13).ToString("M/d/yyyy").Replace("-", "/");
                    Keyboard.SendKeys(cdd);
                    break;
                //    edit.Text = ERPNumber;
                //}
                //else if (((HtmlCell)row.Cells[0]).Title.Contains("confirmed delivery"))
                //{
                //    edit_ConfirmDate = (HtmlEdit) ((HtmlCell)row.Cells[1]).DetectControl<HtmlEdit>(new { Class = "AxInputField" }).FindMatchingControls().First();
                //    cdd = DateTime.Today.AddDays(13).ToString("M/d/yyyy").Replace("-", "/");
                //    edit_ConfirmDate.Click();
                //    edit_ConfirmDate.Text = cdd;
                //    //edit.Click();
                }
            }


            HtmlInputButton btn_applyToLines =(HtmlInputButton) new HtmlInputButton(div_group).FindMatchingControls().First(x => x.GetProperty("value").ToString() == "Apply to lines");
            btn_applyToLines.Click();
            ctrl_IFrame.WaitForLoadingInformation();
                        
            document = new HtmlDocument(browserWindow);

            div_SchduleReason = document.DetectControl<HtmlDiv>(new { ClassName = "ms-dlgContent" });
            div_SchduleReason = (HtmlDiv)div_SchduleReason.FindMatchingControls().Last();
            btn_ReasonCode =(HtmlInputButton) div_SchduleReason.DetectControl<HtmlControl>(new { TagName = "IFRAME", Class = "ms-dlgFrame" }).DetectControl<HtmlInputButton>(new { TagName = "INPUT"}).FindMatchingControls().First(x => x.GetProperty("value").ToString() == "OK");
            btn_ReasonCode.Click();                  


            button_SaveAndClose =(HtmlInputButton) new HtmlInputButton(document).FindMatchingControls().First(x=> x.GetProperty("value").ToString() == "Save and Close");
            button_SaveAndClose.Click();
            ctrl_IFrame.WaitForLoadingInformation();

            browserWindow = browserWindow.WaitForPageToLoad();
            return browserWindow;
        }

        public static BrowserWindow Invoice(this BrowserWindow browserWindow)
        {
            #region Local Variables

            HtmlDocument document = new HtmlDocument();
            HtmlControl ctrl_IFrame = new HtmlControl();
            HtmlInputButton button_Ok = new HtmlInputButton();
                     
            #endregion

            browserWindow = browserWindow.WaitForPageToLoad();
            document = new HtmlDocument(browserWindow);

            ctrl_IFrame = document.DetectControl<HtmlControl>(new { TagName = "IFRAME", Class = "ms-dlgFrame" });
            ctrl_IFrame = (HtmlControl)ctrl_IFrame.SelectFromRibbon("Invoice process", "Default", "New vendor invoice");

            browserWindow = browserWindow.WaitForPageToLoad();
            document = new HtmlDocument(browserWindow);

            ctrl_IFrame = document.DetectControl<HtmlControl>(new { TagName = "IFRAME", Class = "ms-dlgFrame" });
            ctrl_IFrame = (HtmlControl)ctrl_IFrame.FindMatchingControls()[1];
            ctrl_IFrame = (HtmlControl)ctrl_IFrame.SelectFromRibbon("Edit", "Post", "Post invoice");
            ctrl_IFrame.WaitForLoadingInformation();

            button_Ok = document.DetectControl<HtmlDiv>(new { Id= "mainPopupWindow" }).DetectControl<HtmlInputButton>(new { TagName = "INPUT", FriendlyName = "OK" });
            button_Ok.Click();


            return browserWindow;
        }

        #endregion

        #region Wait related functions

        public static void WaitForPortalLoading(this HtmlDocument document)
        {
            bool flag = true;

            while (flag)
            {
                HtmlDiv div_dlgWindow = document.DetectControl<HtmlDiv>(new { Class = "ms-dlgContent" });
                if (!div_dlgWindow.InnerText.StartsWith("Loading"))
                {
                    flag = false;
                }
            }
        }

        public static void WaitForLoadingInformation(this UITestControl control)
        {
            #region Local Variables

            HtmlDocument document = new HtmlDocument();
            HtmlDiv div_loading = new HtmlDiv();

            #endregion

            document = new HtmlDocument(control);
            document.WaitForControlReady();

            while (true)
            {
                div_loading = document.DetectControl<HtmlDiv>(new { Id = "ctl00_AxProgressControl" });
                if(div_loading.IsVisible() == false)
                {
                    break;
                }
            }
        }

        #endregion

        public static String FillPaymentInformation(String cardNumber, String securityCode, String cardHolderFirstName, String cardHolderLastName, String billingAddress1, String billingCity, String billingState, String billingZipCode)
        {
            #region Local Variables

            BrowserWindow browserWindow = new BrowserWindow();
            HtmlDocument document = new HtmlDocument();
            String message = String.Empty;

            #endregion

            try
            {
                browserWindow = Ashley.QA.Automation.Framework.Web.Web.FindBrowserWindow("Payment Information");

                document = new HtmlDocument(browserWindow);

                document.DetectControl<HtmlEdit>(new { Id = "CCard_CNber" }).EnterText(cardNumber);

                document.DetectControl<HtmlComboBox>(new { Id = "CCard_ExpireMonth" }).SelectedIndex = DateTime.Today.Month;

                document.DetectControl<HtmlComboBox>(new { Id = "CCard_ExpireYear" }).SelectedItem = DateTime.Today.Year.ToString();

                document.DetectControl<HtmlEdit>(new { Id = "CCard_SecurityCode" }).EnterText(securityCode);

                document.DetectControl<HtmlEdit>(new { Id = "CCard_CardHolderFirstName" }).EnterText(cardHolderFirstName);

                document.DetectControl<HtmlEdit>(new { Id = "CCard_CardHolderLastName" }).EnterText(cardHolderLastName);

                document.DetectControl<HtmlEdit>(new { Id = "CCard_BillingAddress1" }).EnterText(billingAddress1);

                document.DetectControl<HtmlEdit>(new { Id = "CCard_BillingCity" }).EnterText(billingCity);

                document.DetectControl<HtmlComboBox>(new { Id = "CCard_BillingState" }).SelectedItem = billingState;

                document.DetectControl<HtmlEdit>(new { Id = "CCard_BillingZipCode" }).EnterText(billingZipCode);

                document.DetectControl<HtmlButton>(new { InnerText = "CONTINUE", Class = "msax-Next tiny uppercase button" }).Click();

                document = new HtmlDocument(browserWindow);

                message = document.DetectControl<HtmlDiv>(new { Id = "scphbody_0_CreateAddressControl", Class = "msax-Control" }).InnerText;
                message = new Regex("[" + Environment.NewLine + "]{2,}", RegexOptions.None).Replace(message, "\n");
                message = message.Trim();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return message;
        }        
    }
}
