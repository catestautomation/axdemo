﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Ashley.QA.Automation.Framework.Web;
using OpenQA.Selenium;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Data;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.SmokeTests
{
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class Smoke_CCF
    {
        public Smoke_CCF()
        {
        }
  
        public static string saleOrder;
        public string productCode;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;

        #region Sitecore order creation 

        public void CreateASaleOrderInSitecoreCCF()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();
            
            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode ))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity"); 
                string zipCode = row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode"); 
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                string address = row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                string city = row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                string state = row.Field<string>("State") == null ? string.Empty : row.Field<string>("State"); 
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber"); 
                cardType = String.IsNullOrEmpty(cardType ) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", ""); 
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode"); 
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation
                IsInvoiceNeeded = false;
                
                if (string.IsNullOrEmpty(saleOrder))
                {
                   
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                    IsInvoiceNeeded = true;
                }

                #endregion
            }

        }
        

        #endregion

        [TestMethod]
        [TestCategory("SmokeCCF")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91908)]
        [Description("Address Maintenance")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91908", DataAccessMethod.Sequential)]
        public void AddressMaintenance()
        {
            int iterationCount = TestContext.DataRow.Table.Rows.IndexOf(TestContext.DataRow);
            if (iterationCount == 0)
            {
                #region TestData

                string PurchaseOrder = String.Empty;
                string Lastname = TestContext.DataRow["Lastname"].ToString();
                string Firstname = TestContext.DataRow["Firstname"].ToString();
                List<String> InfologWarningMessage1 = TestContext.DataRow["InfologWarningMessage1"].ToString().Split(';').ToList().Select(x => x.Trim()).ToList();
                List<String> InfologWarningMessage2 = TestContext.DataRow["InfologWarningMessage2"].ToString().Split(';').ToList().Select(x => x.Trim()).ToList();
                string InvoiceAddress1 = TestContext.DataRow["InvoiceAddress1"].ToString();
                string InvoiceZIPcode = TestContext.DataRow["InvoiceZIPcode"].ToString();
                List<String> InvoiceAddrValidationmsg = TestContext.DataRow["InvoiceAddrValidationmsg"].ToString().Split(';').ToList().Select(x => x.Trim()).ToList();
                string DeliveryAddress1 = TestContext.DataRow["DeliveryAddress1"].ToString();
                string DeliveryZIPcode = TestContext.DataRow["DeliveryZIPcode"].ToString();
                string InvoiceCounty = TestContext.DataRow["InvoiceCounty"].ToString();
                string InvoiceCity = TestContext.DataRow["InvoiceCity"].ToString();
                string InvoiceState = TestContext.DataRow["InvoiceState"].ToString();
                string DeliveryCounty = TestContext.DataRow["DeliveryCounty"].ToString();

                string Path = AX.Automation.Properties.Settings.Default.AX_Path;
                string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

                #endregion

                #region Local Variables

                WinWindow wnd_SO = new WinWindow();
                WinWindow wnd_AddressMaintenance = new WinWindow();
                WinRow row_DeliveryAddress = new WinRow();
                WinButton btn_ApplyAndClose = new WinButton();
                WinClient client_FastTab = new WinClient();

                Dictionary<String, String> dict_ColumnAndValues = new Dictionary<String, String>();
                List<String> list_InfologMessages = new List<String>();
                String deliveryAddress = String.Empty;
                String headerViewAddress = String.Empty;
                String lineViewAddress = String.Empty;

                #endregion

                #region Pre-requisite
                //  1, Create Sales Order using credit card payment type
                CreateASaleOrderInSitecoreCCF();

                #endregion

                #region Test Steps

                //  2, Open the Sales Order
                // 2.1, Go to ECM/Accounts receivable/Common/Sales orders/All sales orders                              
                wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
                wnd_AX.NavigateTo(AddressPath);
                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);

                // 2.2, Seach using the SO confirmation number in the grid and double click on the created SO
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

                //  3, Click Sales order tab
                //  Click on Address maintenance => Address maintenance in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("Main", "Address maintenance group", "Address maintenance");

                //  4, Click Add button in Address tab
                wnd_AddressMaintenance = UITestControl.Desktop.FindWinWindow(new { Name = "‪Address maintenance:", ClassName = "AxTopLevelFrame" });
                client_FastTab = wnd_AddressMaintenance.ExpandFastTab("Addresses", true);
                wnd_AddressMaintenance.ClickTableMenuItem("LogisticsPostalAddressActionPaneTab", "Add", null);

                //  5, Enter no value in First name field
                //  Enter @Lastname, Click on the next line in grid
                dict_ColumnAndValues = new Dictionary<string, string>();
                dict_ColumnAndValues.Add("Primary", "No");
                dict_ColumnAndValues.Add("Purpose", "Delivery");
                dict_ColumnAndValues.Add("Address 1", "(null)");
                dict_ColumnAndValues.Add("City", "(null)");
                dict_ColumnAndValues.Add("State", "(null)");

                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "Last name", Lastname);
                Keyboard.SendKeys("{DOWN}");

                //  6, Click Close in infolog
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                foreach (String message in InfologWarningMessage1)
                {
                    try
                    {
                        String warningMessage = list_InfologMessages.First(x => x.StartsWith(message));
                    }
                    catch
                    {
                        Assert.Fail("'" + message + "' Message is not available in the following list \n" + String.Join("\n", list_InfologMessages));
                    }
                }

                //  7, Enter @Firstname in new line
                //  Enter no value in Last name field
                //  Click on the next line in grid
                wnd_AddressMaintenance.ClickTableMenuItem("LogisticsPostalAddressActionPaneTab", "Delete", null);
                wnd_AddressMaintenance.ClickTableMenuItem("LogisticsPostalAddressActionPaneTab", "Add", null);
                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "First name", Firstname);
                Keyboard.SendKeys("{DOWN}");

                // 8, Click Close in infolog
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                foreach (String message in InfologWarningMessage2)
                {
                    try
                    {
                        String warningMessage = list_InfologMessages.First(x => x.StartsWith(message));
                    }
                    catch
                    {
                        Assert.Fail("'" + message + "' Message is not available in the following list \n" + String.Join("\n", list_InfologMessages));
                    }
                }

                //  9, Select/Highlight the new line
                //  Click Delete in Addresses tab
                wnd_AddressMaintenance.ClickTableMenuItem("LogisticsPostalAddressActionPaneTab", "Delete", null);
                Assert.IsNull(wnd_AddressMaintenance.FetchARecordFromATable("GridLogisticsPostalAddress", dict_ColumnAndValues), "New Line is not removed from the grid.");

                //  10, Change the @InvoiceAddress1 value in Invoice line (with purpose = Invoice)
                //  Change the @InvoiceZIPcode value in Invoice line
                //  Click Address Validation in Address tab
                dict_ColumnAndValues = new Dictionary<string, string>();
                dict_ColumnAndValues.Add("Purpose", "Invoice");
                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "Address 1", InvoiceAddress1);

                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "ZIP/postal code", InvoiceZIPcode);

                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "City", InvoiceCity);

                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "State", InvoiceState);

                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "County", InvoiceCounty);

                wnd_AddressMaintenance.ClickTableMenuItem("LogisticsPostalAddressActionPaneTab", "Address Validation", null);
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                foreach (String message in InvoiceAddrValidationmsg)
                {
                    try
                    {
                        String warningMessage = list_InfologMessages.First(x => x.StartsWith(message));
                    }
                    catch
                    {
                        Assert.Fail("'" + message + "' Message is not available in the following list \n" + String.Join("\n", list_InfologMessages));
                    }
                }

                //  11, Change the @DeliveryAddress1 value in Deliivery line (with purpose = Delivery)
                //  Change the @DeliveryZIPcode valie in Delivery line
                //  //  Click Address Validation in Address tab
                //  Select the Sales order in Orders grid
                //  Click Apply and close in Address maintenance form
                //  Click close in infolog
                dict_ColumnAndValues = new Dictionary<string, string>();
                dict_ColumnAndValues.Add("Purpose", "Delivery");
                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "Address 1", DeliveryAddress1);

                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "ZIP/postal code", DeliveryZIPcode);

                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "County", DeliveryCounty);
                Keyboard.SendKeys("{TAB}");

                wnd_AddressMaintenance.ClickTableMenuItem("LogisticsPostalAddressActionPaneTab", "Address Validation", null);
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                row_DeliveryAddress = wnd_AddressMaintenance.FetchARecordFromATable("GridLogisticsPostalAddress", dict_ColumnAndValues);
                deliveryAddress = row_DeliveryAddress.GetColumnValueOfARecord("Address 1") + "|" + row_DeliveryAddress.GetColumnValueOfARecord("City") + ", " + row_DeliveryAddress.GetColumnValueOfARecord("State") + "  " + row_DeliveryAddress.GetColumnValueOfARecord("ZIP/postal code") + "|" + row_DeliveryAddress.GetColumnValueOfARecord("Country/region");

                client_FastTab = wnd_AddressMaintenance.ExpandFastTab("Orders", true);
                wnd_AddressMaintenance.CheckInsideACell("GridSalesOrder", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Mark", true);

                btn_ApplyAndClose = wnd_AddressMaintenance.DetectControl<WinButton>(new { Name = "Apply and Close" });
                btn_ApplyAndClose.WinClick();

                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);

                //  12, Go to the SO form - Header view
                //  Click File =->Refresh
                //  Expand Address fast tab and verify the Delivery address
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("Main", "Show", "Header view");

                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

                client_FastTab = wnd_SO.ExpandFastTab("Address", true);
                headerViewAddress = client_FastTab.DetectControl<WinEdit>(new { Name = "Address" }).Text;
                headerViewAddress = new Regex("[" + Environment.NewLine + "]{2,}", RegexOptions.None).Replace(headerViewAddress, "|");
                Assert.AreEqual(deliveryAddress, headerViewAddress, "'Delivery Address' entered in 'Address Maintenance Form' and 'Delivery Address' available in 'Header View-Address Fast Tab' of Sales Order Form are different.");

                //  13, Click on Line view in action pane
                //  Line details fast tab -> Address tab
                //  verify the Delivery address
                wnd_SO.ClickRibbonMenuItem("Main", "Show", "Line view");

                client_FastTab = wnd_SO.ExpandFastTab("Line details", true);
                client_FastTab.SelectTabInFastTab("Address");
                client_FastTab.DetectControl<WinEdit>(new { Name = "Delivery address" }).WinHover();

                lineViewAddress = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = "EnhancedPreview" }).DetectControl<WinEdit>(new { Name = "Address" }).Text;
                lineViewAddress = new Regex("[" + Environment.NewLine + "]{2,}", RegexOptions.None).Replace(lineViewAddress, "|");
                Assert.AreEqual(deliveryAddress, lineViewAddress, "'Delivery Address' entered in 'Address Maintenance Form' and 'Delivery Address' available in 'Line View-Line details Fast Tab-Address Tab' of Sales Order Form are different.");
                wnd_SO.CloseWindow();

                #endregion

                #region Test Cleanup


                #endregion
            }
        }

        [TestMethod]
        [TestCategory("SmokeCCF")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91888)]
        [Description("Apply Discounts - Header level / Line level")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91888", DataAccessMethod.Sequential)]
        public void ApplyDiscountsHeaderLevelLineLevel()
        {
            int iterationCount = TestContext.DataRow.Table.Rows.IndexOf(TestContext.DataRow);
            if (iterationCount == 0)
            {
                #region Test Data

                string PurchaseOrder = String.Empty;
                string LineLevelDiscountAmount = TestContext.DataRow["LLDA"].ToString();
                string LineLevelDiscountReasonCode = "Third Party Vendor";//TestContext.DataRow["LLDAReasoncode"].ToString();
                string HeaderLevelDiscountPercentage = TestContext.DataRow["HLDP"].ToString();
                string HeaderLevelDiscountReasonCode = TestContext.DataRow["HLDPReasoncode"].ToString();

                string Path = AX.Automation.Properties.Settings.Default.AX_Path;
                string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

                #endregion

                #region Local Variables
                WinWindow wnd_SO = new WinWindow();
                UITestControlCollection uitcc_SOLines = new UITestControlCollection();
                WinWindow wnd_SODiscount = new WinWindow();
                WinEdit edit_DicountAmount = new WinEdit();
                WinButton btn_ApplyAndClose = new WinButton();
                WinWindow wnd_DH = new WinWindow();
                UITestControlCollection uitcc_DHLines = new UITestControlCollection();

                List<String> list_InfologMessages = new List<String>();
                List<String> list_ExpectedDiscountMessages = new List<String>();

                #endregion

                #region Pre-requisite

                //  1, Use the Sales order from Testcase #121476
                CreateASaleOrderInSitecoreCCF();
               
                #endregion

                #region Test Steps

                //  2, Go to AX application - Sale Order form
                wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
                wnd_AX.NavigateTo(AddressPath);
                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

                //  3, Apply line level discount in $ (positive) for the sales order
                //  3.1, Go to Sales order tab and Click on Modify in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

                //  3.2, Select the main line and Click Add line discount (right side drop down)
                //  3.3, Enter @LineLevelDisountAmount in Manual Disount $, Select the @LineleveldiscountReasoncode, Click on Apply and Close
                //  3.4, Click Close in infolog Infolog should get closed
                //  3.5, Note: Repeat from Step 2 to Step 4 for all the main lines available in the selected Sales order form
                //wnd_SO.ApplyColumnFilter("SalesLineGrid", "Line stage", "Is not", "Delivery charge");
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String itemNumber = row_SOLine.GetColumnValueOfARecord("Item number");
                    Double origQty = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Orig qty"));
                    Double bd_NetAmount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Net amount"));
                    Double discountAmount = (Convert.ToDouble(LineLevelDiscountAmount) / origQty);

                    row_SOLine.SelectARecord(true, false);
                    wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Add line discount", null);

                    wnd_SODiscount = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order discount:", ClassName = "AxTopLevelFrame" });
                    edit_DicountAmount = wnd_SODiscount.DetectControl<WinEdit>(new { Name = "Manual discount $" });
                    edit_DicountAmount.EnterWinTextV2(LineLevelDiscountAmount);
                    wnd_SODiscount.SelectValueFromCustomDropdown("Reason code", "Default comment", LineLevelDiscountReasonCode);
                    btn_ApplyAndClose = wnd_SODiscount.DetectControl<WinButton>(new { Name = "Apply and Close" });
                    btn_ApplyAndClose.WinClick();
                    list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                    Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                    try
                    {
                        String discountMessage = list_InfologMessages.First(x => x.StartsWith("Discount amount $" + String.Format("{0:0.00}", discountAmount) + " per unit, qty " + origQty + ", for a total of $" + String.Format("{0:0.00}", Convert.ToDouble(LineLevelDiscountAmount)) + " for item " + itemNumber));
                    }
                    catch
                    {
                        Assert.Fail("'" + "Discount amount $" + String.Format("{0:0.00}", discountAmount) + " per unit, qty " + origQty + ", for a total of $" + String.Format("{0:0.00}", Convert.ToDouble(LineLevelDiscountAmount)) + " for item " + itemNumber + "' Message is not available in the following list \n" + String.Join("\n", list_InfologMessages));
                    }

                    Double ad_NetAmount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Net amount"));
                    Assert.AreEqual(Math.Round(bd_NetAmount - Convert.ToDouble(LineLevelDiscountAmount), 2), ad_NetAmount, "Net amount for Item number '" + itemNumber + "' is not calculated properly after applying Line Level Discount Amount: $" + String.Format("{0:0.00}", Convert.ToDouble(LineLevelDiscountAmount)));
                    row_SOLine.SelectARecord(false, false);
                }

                //  3.6, Click Complete in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

                //  3.7, Click Close in infolog
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                //  4, Apply header level discount in % (positive) for the sales order
                //  4.1, Go to Sales order tab and Click on Modify in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String itemNumber = row_SOLine.GetColumnValueOfARecord("Item number");
                    Double origQty = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Orig qty"));
                    Double bd_NetAmount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Net amount"));
                    Double discountAmount = Math.Round((bd_NetAmount * (Convert.ToDouble(HeaderLevelDiscountPercentage) / 100) / origQty), 2);
                    Double total = Math.Round(bd_NetAmount * (Convert.ToDouble(HeaderLevelDiscountPercentage) / 100), 2);

                    list_ExpectedDiscountMessages.Add("Discount amount $" + String.Format("{0:0.00}", discountAmount) + " per unit, qty " + origQty + ", for a total of $" + String.Format("{0:0.00}", total) + " for item " + itemNumber);
                }

                //  4.2, Click Manual discounts => Sales order discount in action pane
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Manual discounts", "Sales order discount");

                //  4.3, Enter @HeaderLevelDisountPercentage in Manual Disount %, Select the @HeaderleveldisountReasoncode, Click on Apply and Close
                wnd_SODiscount = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order discount:", ClassName = "AxTopLevelFrame" });
                edit_DicountAmount = wnd_SODiscount.DetectControl<WinEdit>(new { Name = "Manual discount %" });
                edit_DicountAmount.EnterWinTextV2(HeaderLevelDiscountPercentage);
                wnd_SODiscount.SelectValueFromCustomDropdown("Reason code", "Default comment", HeaderLevelDiscountReasonCode);
                btn_ApplyAndClose = wnd_SODiscount.DetectControl<WinButton>(new { Name = "Apply and Close" });
                btn_ApplyAndClose.WinClick();

                //  4.4, Click Close in infolog
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                foreach (String message in list_ExpectedDiscountMessages)
                {
                    try
                    {
                        String discountMessage = list_InfologMessages.First(x => x.StartsWith(message));
                    }
                    catch
                    {
                        Assert.Fail("'" + message + "' Message is not available in the following list \n" + String.Join("\n", list_InfologMessages));
                    }
                }

                //  4.5, Click Complete in action pane            
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

                //  4.6, Click Close in infolog
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                //  5, Select the main line in SO form Click Discount history (right side drop down)
                //  6, Check the discount history
                //  7, Click Close in Discount history page
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                //wnd_SO.ApplyColumnFilter("SalesLineGrid", "Line stage", "Is not", "Delivery charge");            
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    row_SOLine.SelectARecord(true, false);
                    wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Discount history", null);

                    //  6,
                    wnd_DH = UITestControl.Desktop.FindWinWindow(new { Name = "Discount history:", ClassName = "AxTopLevelFrame" });
                    uitcc_DHLines = wnd_DH.GetAllRecords("ManualDiscounts");
                    uitcc_DHLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Off"));

                    //Before
                    Double b_NetAmountPriorToDiscount = Convert.ToDouble(((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Net amount prior to discount"));
                    Double b_DiscountPercentage = Convert.ToDouble(((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Discount %"));
                    Double b_DiscountAmount = Convert.ToDouble(((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Discount $"));
                    String b_DiscountDescription = ((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Discount description");
                    Double b_NetAmountOfSalesLine = Convert.ToDouble(((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Net amount of sales line"));

                    Assert.AreEqual(0, b_DiscountPercentage, "Discount History Before Applying Discount: Discount % is " + b_DiscountPercentage);
                    Assert.AreEqual(0, b_DiscountAmount, "Discount History Before Applying Discount: Discount $ is " + b_DiscountAmount);
                    Assert.AreEqual("(null)", b_DiscountDescription, "Discount History Before Applying Discount: Discount Description is " + b_DiscountDescription);

                    //After Line Level
                    Double l_NetAmountPriorToDiscount = Convert.ToDouble(((WinRow)uitcc_DHLines[1]).GetColumnValueOfARecord("Net amount prior to discount"));
                    Double l_DiscountAmount = Convert.ToDouble(((WinRow)uitcc_DHLines[1]).GetColumnValueOfARecord("Discount $"));
                    String l_DiscountDescription = ((WinRow)uitcc_DHLines[1]).GetColumnValueOfARecord("Discount description");
                    Double l_NetAmountOfSalesLine = Convert.ToDouble(((WinRow)uitcc_DHLines[1]).GetColumnValueOfARecord("Net amount of sales line"));

                    Assert.AreEqual(l_NetAmountOfSalesLine + l_DiscountAmount, l_NetAmountPriorToDiscount, "Discount History After Applying Line Level Discount: Net amount prior to discount is " + l_NetAmountPriorToDiscount);
                    Assert.AreEqual(Convert.ToDouble(LineLevelDiscountAmount), l_DiscountAmount, "Discount History After Applying Line Level Discount: Discount $ is " + l_DiscountAmount);
                    Assert.AreEqual(LineLevelDiscountReasonCode, l_DiscountDescription, "Discount History After Applying Line Level Discount: Discount Description is " + l_DiscountDescription);
                    Assert.AreEqual(Math.Round((l_NetAmountPriorToDiscount - l_DiscountAmount), 2), l_NetAmountOfSalesLine, "Discount History After Applying Line Level Discount: Net amount of sales line is " + l_NetAmountOfSalesLine);

                    //After Header Level
                    Double h_NetAmountPriorToDiscount = Convert.ToDouble(((WinRow)uitcc_DHLines[0]).GetColumnValueOfARecord("Net amount prior to discount"));
                    Double h_DiscountPercentage = Convert.ToDouble(((WinRow)uitcc_DHLines[0]).GetColumnValueOfARecord("Discount %"));
                    String h_DiscountDescription = ((WinRow)uitcc_DHLines[0]).GetColumnValueOfARecord("Discount description");
                    Double h_NetAmountOfSalesLine = Convert.ToDouble(((WinRow)uitcc_DHLines[0]).GetColumnValueOfARecord("Net amount of sales line"));

                    Assert.AreEqual(l_NetAmountOfSalesLine, h_NetAmountPriorToDiscount, "Discount History After Applying Header Level Discount: Net amount prior to discount is " + h_NetAmountPriorToDiscount);
                    Assert.AreEqual(Convert.ToDouble(HeaderLevelDiscountPercentage), h_DiscountPercentage, "Discount History After Applying Header Level Discount: Discount % is " + h_DiscountPercentage);
                    Assert.AreEqual(HeaderLevelDiscountReasonCode, h_DiscountDescription, "Discount History After Applying Header Level Discount: Discount Description is " + h_DiscountDescription);
                    Assert.AreEqual(Math.Round((h_NetAmountPriorToDiscount - (h_NetAmountPriorToDiscount * h_DiscountPercentage / 100))),Math.Round(h_NetAmountOfSalesLine), "Discount History After Applying Header Level Discount: Net amount of sales line is " + h_NetAmountOfSalesLine);

                    //  7,
                    wnd_DH.CloseWindow();
                    row_SOLine.SelectARecord(false, false);
                }


                #endregion

                wnd_SO.CloseWindow();

                #region Test Cleanup

              
                #endregion
            }
        }

        [TestMethod]
        [TestCategory("SmokeCCF")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91887)]
        [Description("Cancel Sales Order  - Header level / Line level")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91887", DataAccessMethod.Sequential)]
        public void CancelSalesOrderHeaderLevelLineLevel()
        {
            int iterationCount = TestContext.DataRow.Table.Rows.IndexOf(TestContext.DataRow);
            if (iterationCount == 0)
            {
                #region Test Data

                string PurchaseOrder = String.Empty;
                string DeliverRemainderQty = TestContext.DataRow["PCDeliverremainder"].ToString();
                string PartialCancelReasonCode = TestContext.DataRow["PCReasoncode"].ToString();
                string HeaderLevelCancelReasonCode = TestContext.DataRow["FCReasoncode"].ToString();
                string mLStagePartialyCanceled = TestContext.DataRow["MLStagePartialyCanceled"].ToString();
                string mLStatusPartialyCanceled = TestContext.DataRow["MLStatusPartialyCanceled"].ToString();
                string mLStageFullyCanceled = TestContext.DataRow["MLStageFullyCanceled"].ToString();
                string mLStatusFullyCanceled = TestContext.DataRow["MLStatusFullyCanceled"].ToString();
                string dLStageFullyCanceled = TestContext.DataRow["DLStageFullyCanceled"].ToString();
                string dLStatusFullyCanceled = TestContext.DataRow["DLStatusFullyCanceled"].ToString();

                string Path = AX.Automation.Properties.Settings.Default.AX_Path;
                string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

                #endregion

                #region Local Variables

              
                WinWindow wnd_SO = new WinWindow();
                UITestControlCollection uitcc_SOLines = new UITestControlCollection();
                WinWindow wnd_URQ = new WinWindow();
                WinEdit edit_DeliverRemainder = new WinEdit();
                WinButton btn_Ok = new WinButton();
                WinWindow wnd_PopUp = new WinWindow();
                WinButton btn_Yes = new WinButton();

                List<String> list_InfologMessages = new List<String>();
                Dictionary<String, String> dict_ColumnAndValues = new Dictionary<String, String>();

                #endregion

                #region Pre-requisite

                //  1, Use the Sales order from Testcase #121476
                CreateASaleOrderInSitecoreCCF();

                #endregion

                #region Test Steps

                //  2, Go to Sales Order form in AX
                wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
                wnd_AX.NavigateTo(AddressPath);
                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

                //  3, Partial Cancel the Sales Order (Line Level)
                //  3.1, Click on Modify in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.WaitForPurchaseOrderToBeGenerated();
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

                //  3.2, Select the main line in Sale order lines grid
                //  Click Update Line => Delivery Reminder
                //  3.3, Enter the @Deliverremainderqty
                //  Select the @PartialCancelReasoncode
                //  Click on OK
                //  3.4, Note: If required repeat from Step 2 to Step 3 for all the main lines available in the selected Sales order form
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {

                    row_SOLine.SelectARecord(true, false);

                    wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Update line", "Deliver remainder");

                    wnd_URQ = UITestControl.Desktop.FindWinWindow(new { Name = "Update remaining quantity:", ClassName = "AxTopLevelFrame" });
                    edit_DeliverRemainder = wnd_URQ.DetectControl<WinEdit>(new { Name = "Deliver remainder" });
                    edit_DeliverRemainder.EnterWinTextV2(DeliverRemainderQty);
                    wnd_URQ.SelectValueFromCustomDropdown("Reason", "Description", PartialCancelReasonCode);
                    btn_Ok = wnd_URQ.DetectControl<WinButton>(new { Name = "OK" });
                    btn_Ok.WinClick();
                }

                //  3.5, Click on Complete button
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

                //  3.6, Click Close in infolog
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                // 4, Go to Sales order lines grid
                // Select the partially canceled line
                // Verify the Line Stage and Line Status
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(mLStagePartialyCanceled.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(mLStatusPartialyCanceled.ProcessString(), lineStatus, PurchaseOrder + "Line stage mismatch");
                }

                //  5, Cancel the Sales Order (Header Level)
                //  5.1, Click on Modify in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

                //  5.2, Click Cancel in action pane
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Cancel");

                //  5.3, Select the @HeaderlecelCancelReasoncode
                //  Click on Yes
                wnd_PopUp = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Are you sure you want to cancel selected sales orders?‬ (‎‪1‬)", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
                wnd_PopUp.SelectValueFromCustomDropdown("Reason", "Description", HeaderLevelCancelReasonCode);
                btn_Yes = wnd_PopUp.DetectControl<WinButton>(new { Name = "Yes" });
                btn_Yes.WinClick();

                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));


                //  5.4, Click on Complete button
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                // 6, Go to Sales order lines grif
                // Select all the lines one by one
                // Verify the Line Stage and Line Status
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                    Assert.AreEqual(mLStageFullyCanceled, lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(mLStatusFullyCanceled, lineStatus, PurchaseOrder + "Line stage mismatch");
                }

                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                    Assert.AreEqual(dLStageFullyCanceled, lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(dLStatusFullyCanceled, lineStatus, PurchaseOrder + "Line stage mismatch");
                }


                //  7, Click Close in Sales order form
                wnd_SO.CloseWindow();

                saleOrder = string.Empty;

                #endregion

                #region Test Cleanup
               

                #endregion
            }
        }

        [TestMethod]
        [TestCategory("SmokeCCF")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(100168)]
        [Description("Adjustment Credit")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "100168", DataAccessMethod.Sequential)]
        public void AdjustmentCredit()
        {
            int iterationCount = TestContext.DataRow.Table.Rows.IndexOf(TestContext.DataRow);
            if (iterationCount == 0)
            {
                #region Test Data            
              
                string Path = AX.Automation.Properties.Settings.Default.AX_Path;
                string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

                string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
                string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
                string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;

                string adjustmentType = TestContext.DataRow["AdjustmentType"].ToString();
                string reasonCode = TestContext.DataRow["AdjCreditReasoncode"].ToString();
                string discountAmount = TestContext.DataRow["Discountamount"].ToString();
                string adjCreditQty = "1"; //TestContext.DataRow["AdjCreditQty"].ToString();

                string mLStagePOCreationPending = TestContext.DataRow["MLStagePOCreationPending"].ToString();
                string mLStatusPOCreationPending = TestContext.DataRow["MLStatusPOCreationPending"].ToString();
                string dLStagePOCreationPending = TestContext.DataRow["DLStagePOCreationPending"].ToString();
                string dLStatusPOCreationPending = TestContext.DataRow["DLStatusPOCreationPending"].ToString();

                string mLStageUnscheduled = TestContext.DataRow["mLStageUnscheduled"].ToString();
                string mLStatusUnscheduled = TestContext.DataRow["MLStatusUnscheduled"].ToString();
                string dLStageUnscheduled = TestContext.DataRow["DLStageUnscheduled"].ToString();
                string dLStatusUnscheduled = TestContext.DataRow["DLStatusUnscheduled"].ToString();

                string mLStageScheduled = TestContext.DataRow["MLStageScheduled"].ToString();
                string mLStatusScheduled = TestContext.DataRow["MLStatusScheduled"].ToString();
                //string dLStageScheduled = "";
                //string dLStatusScheduled = "";
                string dLStageScheduled = TestContext.DataRow["DLStageScheduled"].ToString();
                string dLStatusScheduled = TestContext.DataRow["DLStatusScheduled"].ToString();

                string mLStageInvoiced = TestContext.DataRow["MLStageInvoiced"].ToString();
                string mLStatusInvoiced = TestContext.DataRow["MLStatusInvoiced"].ToString();
                string dLStageInvoiced = TestContext.DataRow["DLStageInvoiced"].ToString();
                string dLStatusInvoiced = TestContext.DataRow["DLStatusInvoiced"].ToString();

                string aCMLStageInvoiced = TestContext.DataRow["ACMLStageInvoiced"].ToString();
                string aCMLStatusInvoiced = TestContext.DataRow["ACMLStatusInvoiced"].ToString();

                #endregion

                #region Local Variables

                WinWindow wnd_AX = new WinWindow();
                WinWindow wnd_SO = new WinWindow();
                WinWindow wnd_CreateCredit = new WinWindow();
                WinWindow wnd_Infolog = new WinWindow();
                BrowserWindow browserWindow = new BrowserWindow();
                HtmlRow row_Record = new HtmlRow();
                UITestControlCollection uitcc_SOLines = new UITestControlCollection();
                WinEdit edit_discountPercentage = new WinEdit();
                WinButton btn_OK = new WinButton();              

                IWebDriver driver = null;

                #endregion

                #region Test Steps           

                #region Pre-requisite
                //  1, Create Sales Order using credit card payment type
                //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
                CreateASaleOrderInSitecoreCCF();

                #endregion

                string PurchaseOrder = saleOrder + 1.ToString("00");

                // 2, Open the Sales Order
                // 2.1, Go to ECM/Accounts receivable/Common/Sales orders/All sales orders
                // 2.2, Seach using the SO confirmation number in the grid and double click on the created SO
                wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
                wnd_AX.NavigateTo(AddressPath);

                // 3, Select the lines in Sale order lines grid
                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

                // Verify the Line Stage and Line Status of the lines    
                //Main line        
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                  uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(mLStagePOCreationPending.ProcessString(), lineStage, PurchaseOrder + "PO creation pending");
                    Assert.AreEqual(mLStatusPOCreationPending.ProcessString(), lineStatus, PurchaseOrder + "PO creation pending");
                }

                //Delivery line
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(dLStagePOCreationPending.ProcessString(), lineStage, PurchaseOrder + "PO creation pending");
                    Assert.AreEqual(dLStatusPOCreationPending.ProcessString(), lineStatus, PurchaseOrder + "PO creation pending");
                }

                // 4, Wait till PO is auto generated for the SO
                // Enusre that PO is created for the Sales Order      
                wnd_SO.WaitForPurchaseOrderToBeGenerated();

                // 5, Select the lines in Sale order lines grid
                //  Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(mLStageUnscheduled.ProcessString(), lineStage, PurchaseOrder + "PO not generated");
                    Assert.AreEqual(mLStatusUnscheduled.ProcessString(), lineStatus, PurchaseOrder + "PO not generated");
                }

                //Delivery line
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(dLStageUnscheduled.ProcessString(), lineStage, PurchaseOrder + "PO not generated");
                    Assert.AreEqual(dLStatusUnscheduled.ProcessString(), lineStatus, PurchaseOrder + "PO not generated");
                }

                //  6, Invoice the DS lines via AS400
                //  7, Invoice the HD lines via Portal

                //  8, Schedule the HD Purchase Order(s) via Homestore Portal (PTL) - Header level                     
                driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

                //  8.1 Click HomeStore portal => Purchase order => New cue icon
                driver = driver.SelectTab("Homestore portal");
                driver.SelectFromRibbon("Purchase order", "Stages", "New");

                //  8.2, Search and click on the purchase order
                driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                //driver.ClickACellinTable("Purchase order", PurchaseOrder );               
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                //  8.3, Enter the @POERPnumber & @POConfirmeddeliverydate greater than Latest estimated delivery date
                // Click Apply to lines
                // 8.4, Select the Reason Code from dropdown and click OK
                // 8.5 Click Save and Close
                String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

                string driverTitle = driver.Title;

                // 9, Go to AX application - Sale Order form
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                // 10, Select the DS lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                // 11, Select the HD lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(mLStageScheduled.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(mLStatusScheduled.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");

                }

                //Delivery line
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(dLStageScheduled.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(dLStatusScheduled.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }


                // 12, Invoice the DS lines via AS400
                // 13, Invoice the HD lines via Portal

                // 14, Go to PTL application
                // 15, Portal: Invoice the Purchase Order
                // browserWindow = PortalCommonFunctions.LaunchPortal(portal_url, portal_Username, portal_Password);

                // 15.1, Click on the Scheduled cue icon
             
                
                driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");

                // 15.2 Seach and click on the purchase order
                driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                //driver.ClickACellinTable("Purchase order", PurchaseOrder);
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                // 15.3, Click Invoice Process => New Vendor Invoice => Post Invoice
                driver.Invoice();

                // 16, Go to AX application - Sale Order form
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                // 17, Select the lines in Sale order lines grid               
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");

                // Verify the Line Stage and Line Status of the lines
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(mLStageInvoiced.ProcessString(), lineStage, PurchaseOrder + " not Invoiced.");
                    Assert.AreEqual(mLStatusInvoiced.ProcessString(), lineStatus, PurchaseOrder + " not Invoiced.");
                }

                //Delivery line
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(dLStageInvoiced.ProcessString(), lineStage, PurchaseOrder + "not Invoiced.");
                    Assert.AreEqual(dLStatusInvoiced.ProcessString(), lineStatus, PurchaseOrder + "not Invoiced.");
                }

                // 18, Go to Sell tab in SO form and click Credit Note
                wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
                wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "‪Create credit note‬:", ClassName = "AxTopLevelFrame" });

                // 19, Select @AdjustmentType and select the @AdjCreditReasoncode
                wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", adjustmentType);
               // wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", reasonCode);
                WinEdit edt = new WinEdit(wnd_CreateCredit);
                edt.SearchProperties.Add("ControlType", "Edit", PropertyExpressionOperator.EqualTo);
                edt.SearchProperties.Add("Name", "Reason code", PropertyExpressionOperator.EqualTo);

                edt.SelectValueFromCustomComboBox("MfgDefect");

                // 20, Update the @Discountamount in Discount amount text field
                edit_discountPercentage = wnd_CreateCredit.DetectControl<WinEdit>(new { Name = "Discount amount" });
                edit_discountPercentage.EnterWinTextV2(discountAmount);

                // 21, Select the required main line(s) in grid (Lower pane)
                // Reduce the @AdjCreditQty in Quantity column
                uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));

                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                    wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", adjCreditQty);
                }


                // 22, Click OK in Create credit note form
                btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
                btn_OK.WinClick();

                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    int i = uitcc_SOLines.IndexOf(row_SOLine);
                    Double origty = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Orig qty"));
                    if (i % 2 == 0)
                    {
                        Assert.IsTrue(origty < 0, "not a negative line");
                    }
                    else
                    {
                        Assert.IsTrue(origty > 0, "not a positive line");
                    }

                }


                // 23, Click Complete in SO form (action pane)           
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");

                // Click Close in info log
                List<string> lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Adjustment credit for the products");

                // 25, Select the newly added adjustment credit lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(aCMLStageInvoiced.ProcessString(), lineStage, PurchaseOrder + " not Invoiced.");
                    Assert.AreEqual(aCMLStatusInvoiced.ProcessString(), lineStatus, PurchaseOrder + " not Invoiced.");
                }
                wnd_SO.CloseWindow();

                #endregion

            }           

            #region Test Cleanup

           
            #endregion
        }

        [TestMethod]
        [TestCategory("SmokeCCF")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(100530)]
        [Description("Replacement Item")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "100530", DataAccessMethod.Sequential)]
        public void ReplacementItem()
        {
            int iterationCount = TestContext.DataRow.Table.Rows.IndexOf(TestContext.DataRow);
            if (iterationCount == 0)
            {
                #region Test Data

                string PurchaseOrder = TestContext.DataRow["PurchaseOrder"].ToString();
                string Path = AX.Automation.Properties.Settings.Default.AX_Path;
                string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

                string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
                string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
                string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;

                string adjustmentType = TestContext.DataRow["AdjustmentType"].ToString();
                string reasonCode = TestContext.DataRow["ReasonCode"].ToString();
                string replacementqty = TestContext.DataRow["Replacementqty"].ToString();

                string rPMLStagePOCreationPending = TestContext.DataRow["RPMLStagePOCreationPending"].ToString();
                string rPMLStatusPOCreationPending = TestContext.DataRow["RPMLStatusPOCreationPending"].ToString();

                string rPMLStageUnscheduled = TestContext.DataRow["RPMLStageUnscheduled"].ToString();
                string rPMLStatusUnscheduled = TestContext.DataRow["RPMLStatusUnscheduled"].ToString();

                string rPMLStageScheduled = TestContext.DataRow["RPMLStageScheduled"].ToString();
                string rPMLStatusScheduled = TestContext.DataRow["RPMLStatusScheduled"].ToString();

                //string rPMLStageInvoiced = "";
                //string rPMLStatusInvoiced = "";
                string rPMLStageInvoiced = TestContext.DataRow["RPMLStageInvoiced"].ToString();
                string rPMLStatusInvoiced = TestContext.DataRow["RPMLStatusInvoiced"].ToString();

                #endregion

                #region Local variables

                WinWindow wnd_AX = new WinWindow();
                WinWindow wnd_SO = new WinWindow();
                WinWindow wnd_CreateCredit = new WinWindow();
                WinWindow wnd_Infolog = new WinWindow();
                BrowserWindow browserWindow = new BrowserWindow();
                HtmlRow row_Record = new HtmlRow();
                UITestControlCollection uitcc_SOLines = new UITestControlCollection();
                WinEdit edit_discountPercentage = new WinEdit();
                WinButton btn_OK = new WinButton();                
                string lineConfirmationId = string.Empty;
                IWebDriver driver = null;
                string ERPNumber = string.Empty;

                #endregion

                #region TestSteps

                #region Pre-requisite
                //  1, Create Sales Order using credit card payment type
                CreateASaleOrderInSitecoreCCF();

                #endregion

                PurchaseOrder = saleOrder + 1.ToString("00");

                // 2, Go to AX application - Sale Order form
                wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
                wnd_AX.NavigateTo(AddressPath);

                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                

                #region Invoice Process
                if (IsInvoiceNeeded)
                {
                    wnd_SO.WaitForPurchaseOrderToBeGenerated();
                    driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
                    driver = driver.SelectTab("Homestore portal");
                    driver.SelectFromRibbon("Purchase order", "Stages", "New");
                   driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                    driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
                    ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                    driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");


                    // 12.2, Search and click on the purchase order
                    // 12.3, Click Invoice Process => New Vendor Invoice => Post Invoice   
                    driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
                    driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                    driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
                    driver.Invoice();
                    

                }
                #endregion

                // 3, Go to Sell tab and click Credit note
                wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
                wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "‪Create credit note‬:", ClassName = "AxTopLevelFrame" });

                // 4, Select @AdjustmentType and select the @AdjCreditReasoncode
                wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", adjustmentType);
                //wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", reasonCode);
                WinEdit edt = new WinEdit(wnd_CreateCredit);
                edt.SearchProperties.Add("ControlType", "Edit", PropertyExpressionOperator.EqualTo);
                edt.SearchProperties.Add("Name", "Reason code", PropertyExpressionOperator.EqualTo);

                edt.SelectValueFromCustomComboBox("MfgDefect");

                // 5, Select the original invoiced sales line in upper pane
                wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", true);
                             
                // Highlight the required main line(s) in grid(Lower pane)
                // Reduce the @Replacementqty in Quantity column
                // Perform this step for all the lines in lower pane
                uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));

                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                    wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", replacementqty);
                }              
                // 6, Click OK in Create credit note form
                btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
                btn_OK.WinClick();
                wnd_SO.WaitForControlReady();
                //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                // 10, Click Complete button in action pane
                wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");

                //Click Close in infolog
                List<string> lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
                // PurchaseOrder = saleOrder + 2.ToString("00");

                // 9, Select the newly added Replacement lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
               // wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(rPMLStagePOCreationPending.ProcessString(), lineStage, PurchaseOrder + "PO creation pending");
                    Assert.AreEqual(rPMLStatusPOCreationPending.ProcessString(), lineStatus, PurchaseOrder + "PO creation pending");
                }

                
                //wnd_SO.SelectARecord("SalesLineGrid", new Dictionary<string, string>() { { "Line confirmation ID", PurchaseOrder } }, true, false); 


                // 11, Wait till PO is auto generated for the new lines in SO
                // Enusre that PO is created for the replacement lines with new Line cconfirmation ID
                wnd_SO.WaitForPurchaseOrderToBeGenerated();

                // 12, Select the Replacement lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                // wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                   
                Stopwatch timer = new Stopwatch();
                timer.Start();
                double secs = 0;
                while (true)
                {
                    wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                    secs = timer.Elapsed.TotalSeconds;
                    if (secs > 100)
                    {
                        timer.Stop();
                        break;
                    }
                }


                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();
                    lineConfirmationId = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");

                    Assert.AreEqual(rPMLStageUnscheduled.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(rPMLStatusUnscheduled.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                // 13, Schedule the DS lines via AS400
                // 14, Schedule the HD lines via Portal
                // 15, Schedule the HD Negative (Return) Purchase Order(s) via Homestore Portal (PTL)
                // 15.1 Click HomeStore portal => Purchase order => New Returns cue icon            
                driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
                driver = driver.SelectTab("Homestore portal");
                driver.SelectFromRibbon("Purchase order", "Stages", "New returns");

                // 15.2, Search and click on the purchase order
                driver = driver.PortalFilter("Purchase order", lineConfirmationId);                           
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                // 15.3, Enter the @POERPnumber & @POConfirmeddeliverydate greater than Latest estimated delivery date
                // Click Apply to lines            
                // 15.4 Click Save and Close
                ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

                // 16, Go to AX application - Sale Order form
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                // 17, Select the Replacement lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(rPMLStageScheduled.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(rPMLStatusScheduled.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                // 18, Invoice the DS lines via AS400
                // 19, Invoice the HD lines via Portal
                // 20, Go to PTL application               
                // 21, Portal: Invoice the Return (Negative) Purchase Order
                // 21.1, Click on the Scheduled returns cue icon
                driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled returns");

                // 21.2, Seach and click on the purchase order
                driver = driver.PortalFilter("Purchase order", lineConfirmationId);              
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                // 21.3, Click Invoice Process => New Vendor Invoice => Post Invoice
                driver.Invoice();

                // 22, Go to AX application - Sale Order form
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                // 23, Select the Replacement lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });                
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(lineConfirmationId));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(rPMLStageInvoiced.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(rPMLStatusInvoiced.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }
                wnd_SO.CloseWindow();

                #endregion

                #region Test Cleanup

                #endregion
            }
        }

        [TestMethod]
        [TestCategory("SmokeCCF")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91889)]
        [Description("Customer Return Process")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91889", DataAccessMethod.Sequential)]
        public void CustomerReturnProcess()
        {
            int iterationCount = TestContext.DataRow.Table.Rows.IndexOf(TestContext.DataRow);
            if (iterationCount == 0)
            {
                #region Test Data

                string PurchaseOrder = TestContext.DataRow["PurchaseOrder"].ToString();
                string Path = AX.Automation.Properties.Settings.Default.AX_Path;
                string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

                string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
                string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
                string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;

                string adjustmentType = TestContext.DataRow["AdjustmentType"].ToString();
                //string adjustmentType = "";
                //string reasonCode = "";
                //string returnQuantity = "";
                //string cRDLStageScheduled = "";
                //string cRDLStatusUnscheduled = "";
                string reasonCode = TestContext.DataRow["ReasonCode"].ToString();
                 string returnQuantity = TestContext.DataRow["ReturnQuantity"].ToString();

                string cRMLStagePOCreationPending = TestContext.DataRow["CRMLStagePOCreationPending"].ToString();
                string cRMLStatusPOCreationPending = TestContext.DataRow["CRMLStatusPOCreationPending"].ToString();
                string cRDLStagePOCreationPending = TestContext.DataRow["CRDLStagePOCreationPending"].ToString();
                string cRDLStatusPOCreationPending = TestContext.DataRow["CRDLStatusPOCreationPending"].ToString();

                string cRMLStageUnscheduled = TestContext.DataRow["CRMLStageUnscheduled"].ToString();
                string cRMLStatusUnscheduled = TestContext.DataRow["CRMLStatusUnscheduled"].ToString();
               string cRDLStageUnscheduled = TestContext.DataRow["CRDLStageUnscheduled"].ToString();
                string cRDLStatusUnScheduled = TestContext.DataRow["CRDLStatusUnscheduled"].ToString();

                string cRMLStageScheduled = TestContext.DataRow["CRMLStageScheduled"].ToString();
                string cRMLStatusScheduled = TestContext.DataRow["CRMLStatusScheduled"].ToString();
                string cRDLStageScheduled = TestContext.DataRow["CRDLStageScheduled"].ToString();
                string cRDLStatusScheduled = TestContext.DataRow["CRDLStatusScheduled"].ToString();

                string cRMLStageInvoiced = TestContext.DataRow["CRMLStageInvoiced"].ToString();
                string cRMLStatusInvoiced = TestContext.DataRow["CRMLStatusInvoiced"].ToString();
                string cRDLStageInvoiced = TestContext.DataRow["CRDLStageInvoiced"].ToString();
                string cRDLStatusInvoiced = TestContext.DataRow["CRDLStatusInvoiced"].ToString();

                string returnSaleOrder = string.Empty;

                #endregion

                #region Local Variables
                WinWindow wnd_AX = new WinWindow();
                WinWindow wnd_SO = new WinWindow();
                WinWindow wnd_CreateCredit = new WinWindow();
                WinWindow wnd_RMAForm = new WinWindow();
                BrowserWindow browserWindow = new BrowserWindow();
                HtmlRow row_Record = new HtmlRow();
                UITestControlCollection uitcc_SOLines = new UITestControlCollection();
                WinEdit edit_discountPercentage = new WinEdit();
                WinButton btn_OK = new WinButton();
                string lineConfirmationId = string.Empty;
                IWebDriver driver = null;
                string ERPNumber = string.Empty;               
                #endregion

                #region Pre-requisite
                //  1, Create Sales Order using credit card payment type
                CreateASaleOrderInSitecoreCCF();                              
               
                #endregion

                #region Test Steps

                PurchaseOrder = saleOrder + 1.ToString("00");

                // 2, Go to AX application -Sale Order form

                wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
                wnd_AX.NavigateTo(AddressPath);
                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                #region Invoice Process
                if (IsInvoiceNeeded)
                {
                    wnd_SO.WaitForPurchaseOrderToBeGenerated();
                    driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
                    driver = driver.SelectTab("Homestore portal");
                    driver.SelectFromRibbon("Purchase order", "Stages", "New");
                    driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                    driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
                    ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                    driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");


                    // 12.2, Search and click on the purchase order
                    // 12.3, Click Invoice Process => New Vendor Invoice => Post Invoice   
                    driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
                    driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                    driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
                    driver.Invoice();


                }
                #endregion
                // 3, Go to Sell tab and click Credit note
                wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
                wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "‪Create credit note‬:", ClassName = "AxTopLevelFrame" });

                // 4, Select @AdjustmentType and select the @Reasoncode
                wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", adjustmentType);
               // wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", reasonCode);
                WinEdit edt = new WinEdit(wnd_CreateCredit);
                edt.SearchProperties.Add("ControlType", "Edit", PropertyExpressionOperator.EqualTo);
                edt.SearchProperties.Add("Name", "Reason code", PropertyExpressionOperator.EqualTo);

                edt.SelectValueFromCustomComboBox("MfgDefect");

                // 5, Select the original invoiced sales line in upper pane
                wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", true);
                // Modify the @ReturnQuantity in Quantity column (for all Main lines) (Lower pane)
                // Select Delvery charge line
                uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));

                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                    wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", returnQuantity);
                }
                //List<string> lst_MessageValue = DynamicsAxCommonFunctions.HandleInfolog(true);
                //wnd_SO.CloseWindow();
                // 6, Click OK in Create credit note form
                btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
                btn_OK.WinClick();

                // 7, Click Close in RMA form
                wnd_RMAForm = UITestControl.Desktop.FindWinWindow(new { Name = "‪Return order  -  RMA number:", ClassName = "AxTopLevelFrame" });
                wnd_RMAForm.CloseWindow();

                UITestControlCollection coll = null;
                while (true)
                {
                    // 11,Click Complete in Return SO form
                    coll = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Sales order", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains).FindMatchingControls();
                    if (coll.Count > 1)
                    {
                        break;
                    }
                }

                wnd_SO = (WinWindow)coll.First(x => !x.Name.Contains(saleOrder));

                // 9, Click Complete in Return SO form
               // wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");

                // Click Close in infolog
                List<string> lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

                // 11, Go to the Retun Sales order form
              //  wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

              //  Thread.Sleep(2000);

                // 12, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    returnSaleOrder = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");
                    break;
                }
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(returnSaleOrder));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(cRMLStagePOCreationPending.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(cRMLStatusPOCreationPending.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                // Delivery line
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(returnSaleOrder));
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(cRDLStagePOCreationPending.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(cRDLStatusPOCreationPending.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                // 13, Wait till PO is auto generated for the Return SO
                // Enusre that PO is created for the Return Sales Order
                wnd_SO.WaitForPurchaseOrderToBeGenerated();

                // 14, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines            
               // wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(returnSaleOrder));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(cRMLStageUnscheduled.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(cRMLStatusUnscheduled.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                // Delivery line
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(returnSaleOrder));
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(cRDLStageUnscheduled.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(cRDLStatusUnScheduled.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                // 15, Schedule the DS lines via AS400
                // 16, Schedule the HD lines via Portal
                // 17, Go to PTL application
                driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

                // 18, Schedule the HD Negative (Return)Purchase Order(s) via Homestore Portal(PTL)
                // 18.1 Click HomeStore portal => Purchase order => New Returns cue icon
                driver = driver.SelectTab("Homestore portal");
                driver.SelectFromRibbon("Purchase order", "Stages", "New returns");

                // 18.2, Seach and click on the negative purchase order
                driver = driver.PortalFilter("Purchase order", returnSaleOrder);
                //driver.ClickACellinTable("Purchase order", PurchaseOrder );               
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                // 18.3, Enter the @NPOERPnumber & @NPOConfirmeddeliverydate
                // Click Apply to lines
                // 18.4, Click Save and Close
                ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

                // 19, Go to AX application -Sale Order form
                // wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.SetFocus();

                // 20, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(returnSaleOrder));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(cRMLStageScheduled.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(cRMLStatusScheduled.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                //Delivery Line
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(returnSaleOrder));
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(cRDLStageScheduled.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(cRDLStatusScheduled.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                // 21, Invoice the DS lines via AS400
                // 22, Invoice the HD lines via Portal
                // 23, Go to PTL application


                // 24, Portal: Invoice the Return (Negative) Purchase Order
                // 24.1, Click on the Scheduled returns cue icon
                driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled returns");

                // 24.2, Seach and click on the purchase order
                // 15.2 Seach and click on the purchase order
                driver = driver.PortalFilter("Purchase order", returnSaleOrder);
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                // 24.3, Click Invoice Process => New Vendor Invoice => Post Invoice
                driver.Invoice();

                // 25, Go to AX application -Sale Order form
                //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.SetFocus();

                // 26, Select the Replacement lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(returnSaleOrder));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(cRMLStageInvoiced.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(cRMLStatusInvoiced.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                //Deleivery Line
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(returnSaleOrder));
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(cRDLStageInvoiced.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(cRDLStatusInvoiced.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                #endregion

                wnd_SO.CloseWindow();

                #region Test Cleanup

                #endregion
            }
        }

        [TestMethod]
        [TestCategory("SmokeCCF")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91891)]
        [Description("Exchange Process")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91891", DataAccessMethod.Sequential)]
        public void ExchangeProcess()
        {
            int iterationCount = TestContext.DataRow.Table.Rows.IndexOf(TestContext.DataRow);
            if (iterationCount == 0)
            {

                #region Test Data

                string PurchaseOrder = TestContext.DataRow["PurchaseOrder"].ToString();
                string Path = AX.Automation.Properties.Settings.Default.AX_Path;
                string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

                string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
                string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
                string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;

                //string adjustmentType = "";
                //string reasonCode = "";
                string adjustmentType = TestContext.DataRow["AdjustmentType"].ToString();
                string reasonCode = TestContext.DataRow["ReasonCode"].ToString();

                string eXMLStagePOCreationPending = TestContext.DataRow["EXMLStagePOCreationPending"].ToString();
                string eXMLStatusPOCreationPending = TestContext.DataRow["EXMLStatusPOCreationPending"].ToString();

                string eXMLStageUnscheduled = TestContext.DataRow["EXMLStageUnscheduled"].ToString();
                string eXMLStatusUnscheduled = TestContext.DataRow["EXMLStatusUnscheduled"].ToString();

                string eXMLStageScheduled = TestContext.DataRow["EXMLStageScheduled"].ToString();
                string eXMLStatusScheduled = TestContext.DataRow["EXMLStatusScheduled"].ToString();

                string eXMLStageInvoiced = TestContext.DataRow["EXMLStageInvoiced"].ToString();
                string eXMLStatusInvoiced = TestContext.DataRow["EXMLStatusInvoiced"].ToString();

                //string itemCode = TestContext.DataRow["ItemCode"].ToString();
                string exchangeQty = TestContext.DataRow["ExchangeQuantity"].ToString();
                string returnQty = TestContext.DataRow["ReturnQuantity"].ToString();
                string exchangeOrderNumber = string.Empty;

                #endregion

                #region Local Variables
                WinWindow wnd_AX = new WinWindow();
                WinWindow wnd_SO = new WinWindow();
                WinWindow wnd_CreateCredit = new WinWindow();
                WinWindow wnd_RMAForm = new WinWindow();
                WinWindow wnd_FindReplacementItem = new WinWindow();
                BrowserWindow browserWindow = new BrowserWindow();
                HtmlRow row_Record = new HtmlRow();
                UITestControlCollection uitcc_SOLines = new UITestControlCollection();
                WinEdit edit_discountPercentage = new WinEdit();
                WinButton btn_OK = new WinButton();
                string lineConfirmationId = string.Empty;
                string ItemCode = string.Empty;
                WinButton btn_ExchangeItem = new WinButton();
                IWebDriver driver = null;
                string ERPNumber = string.Empty;

                #endregion

                #region Pre-requisite
                //  1, Create Sales Order using credit card payment type
                CreateASaleOrderInSitecoreCCF();

                PurchaseOrder = saleOrder + 1.ToString("00");
                #endregion

                #region Test Steps
                // 2, Go to AX application - Sale Order form
                wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
                wnd_AX.NavigateTo(AddressPath);
                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                #region Invoice Process
                if (IsInvoiceNeeded)
                {
                    wnd_SO.WaitForPurchaseOrderToBeGenerated();
                    driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
                    driver = driver.SelectTab("Homestore portal");
                    driver.SelectFromRibbon("Purchase order", "Stages", "New");
                    driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                    driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
                    ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                    driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");


                    // 12.2, Search and click on the purchase order
                    // 12.3, Click Invoice Process => New Vendor Invoice => Post Invoice   
                    driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
                    driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                    driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
                    driver.Invoice();


                }
                #endregion

                // 3, Go to Sell tab and click Credit note
                wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
                wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "Create credit note:", ClassName = "AxTopLevelFrame" });

                // 4, Select @AdjustmentType and Select the @ReasonCode
                wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", adjustmentType);
                //wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", reasonCode);
                WinEdit edt = new WinEdit(wnd_CreateCredit);
                edt.SearchProperties.Add("ControlType", "Edit", PropertyExpressionOperator.EqualTo);
                edt.SearchProperties.Add("Name", "Reason code", PropertyExpressionOperator.EqualTo);

                edt.SelectValueFromCustomComboBox("MfgDefect");

                // 5, Select the original invoiced sales line in upper pane
                wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", false);
                // Modify the @ReturnQuantity in Quantity column (Main line) in grid(lower pane) and press Enter key
                uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                    // 6, Highlight a line (item where Return Qty is modified) in lower pane
                    // Click Exchange item button
                    wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", returnQty);
                    btn_ExchangeItem = wnd_CreateCredit.DetectControl<WinButton>(new { Name = "Exchange item" });
                    btn_ExchangeItem.WinClick();
                    btn_ExchangeItem.WinClick();

                    // 7, Enter @ExchangeQuantity in Exchange Quantity column and @ReturnQuantity in Return quantity column
                    // Click on Apply
                    wnd_FindReplacementItem = UITestControl.Desktop.FindWinWindow(new { Name = "Find replacement item:", ClassName = "AxTopLevelFrame" });
                    wnd_FindReplacementItem.SelectARecord("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, true, false);
                    wnd_FindReplacementItem.EnterValueToCell("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, "Return quantity", returnQty);
                    wnd_FindReplacementItem.EnterValueToCell("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, "Exchange qty", exchangeQty);

                    WinButton btn_Apply = (WinButton)wnd_FindReplacementItem.DetectControl<WinButton>(new { Name = "Apply" });
                    btn_Apply.WinClick();
                }
               

                // 8, Click OK in Create credit note form
                btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
                btn_OK.WinClick();

                // 9, Click Close in RMA form
                // wnd_SO.CloseWindow();
                wnd_RMAForm = UITestControl.Desktop.FindWinWindow(new { Name = "Return order  -  RMA number:", ClassName = "AxTopLevelFrame" });
                wnd_RMAForm.CloseWindow();
                UITestControlCollection coll = null;
                while (true)
                {
                    // 11,Click Complete in Return SO form
                    coll = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Sales order", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains).FindMatchingControls();
                    if(coll.Count > 1)
                    {
                        break;
                    }
                }

                wnd_SO = (WinWindow)coll.First(x => !x.Name.Contains(saleOrder));
                wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");

                // Click Close in infolog
                List<string> lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

                // 13, Go to the Retun [Exchange] Sales order form
                //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                // 14, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });              
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();
                    exchangeOrderNumber = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");

                    Assert.AreEqual(eXMLStagePOCreationPending.ProcessString(), lineStage, PurchaseOrder + " PO creation pending");
                    Assert.AreEqual(eXMLStatusPOCreationPending.ProcessString(), lineStatus, PurchaseOrder + " Po creation pending");
                }

                // 15, Wait till PO is auto generated for the Return SO
                // Enusre that PO is created for the Return Sales Order
                wnd_SO.WaitForPurchaseOrderToBeGenerated();

                // 16, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(eXMLStageUnscheduled.ProcessString(), lineStage, PurchaseOrder + "PO creation pending");
                    Assert.AreEqual(eXMLStatusUnscheduled.ProcessString(), lineStatus, PurchaseOrder + "Po creation pending");
                }

                // 17, Schedule the DS lines via AS400
                // 18, Schedule the HD lines via Portal
                // 19, Go to PTL application
                driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

                // 20, Schedule the HD Negative (Return) Purchase Order(s) via Homestore Portal (PTL)
                // 20.1 Click HomeStore portal => Purchase order => New Returns cue icon
                driver = driver.SelectTab("Homestore portal");
                driver.SelectFromRibbon("Purchase order", "Stages", "New returns");


                // 20.2, Seach and click on the negative purchase order
                driver = driver.PortalFilter("Purchase order", exchangeOrderNumber);
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");


                // 20.3 Enter the @NPOERPnumber & @NPOConfirmeddeliverydate
                // Click Apply to lines
                // 20.4, Click Save and Close
                ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

                // 21, Go to AX application - Return Sale Order form
                //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.SetFocus();
                // 22, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(eXMLStageScheduled.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(eXMLStatusScheduled.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                // 23, Invoice the DS lines via AS400
                // 24, Invoice the HD lines via Portal
                // 25, Go to PTL application           
                // 26, Portal: Invoice the Return (Negative) Purchase Order
                // 26.1, Click on the Scheduled returns cue icon
                driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled returns");
                driver = driver.PortalFilter("Purchase order", exchangeOrderNumber);
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                // 26.3, Click Invoice Process => New Vendor Invoice => Post Invoice
                driver.Invoice();

                // 27, Go to AX application -Sale Order form
                // wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                // 28, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.SetFocus();
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(eXMLStageInvoiced.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(eXMLStatusInvoiced.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                wnd_SO.CloseWindow();
                #endregion
                saleOrder = string.Empty;
                #region Test Cleanup

                #endregion

            }
        }

        #region Additional test attributes

        [TestInitialize()]
        public void MyTestInitialize()
        {
            
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
