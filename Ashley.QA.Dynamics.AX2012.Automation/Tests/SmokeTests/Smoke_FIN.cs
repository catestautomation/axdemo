﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Ashley.QA.Automation.Framework.Web;
using OpenQA.Selenium;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using Microsoft.VisualStudio.TestTools.UITest.Common.UIMap;
using System.IO;
using Ashley.QA.Automation.Web.Framework.Enum;
using System.Data;
using System.Reflection;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.SmokeTests
{
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class Smoke_FIN
    {
        public Smoke_FIN()
        {

        }

        #region Global Variables

        public static string saleOrder;
        public static string cardType;
        public static string productCode;
        public bool IsInvoiceNeeded = false;
        public static string quantity = string.Empty;
        public static string zipCode = string.Empty;
        public static string address = string.Empty;
        public static string city = string.Empty;
        public static string state = string.Empty;
        public static string cardNo = string.Empty;
        public static string financingOption = string.Empty;

        #endregion


        #region Sitecore order creation 

        public void CreateASaleOrderInSitecoreFIN()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode ))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                if (String.IsNullOrEmpty(quantity ))
                {
                    quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");
                }
                if (String.IsNullOrEmpty(zipCode))
                {
                    zipCode = row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                }
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                if (String.IsNullOrEmpty(address ))
                {
                    address = row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                }
                if (String.IsNullOrEmpty(city))
                {
                    city = row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                }
                if (String.IsNullOrEmpty(state ))
                {
                    state = row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                }
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                cardType = String.IsNullOrEmpty(cardType) ? "credit" : cardType;
                if (String.IsNullOrEmpty(cardNo))
                {
                    if (cardType == "credit")
                    {
                        cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                    }
                    else
                    {
                        cardNo = row.Field<string>("AshleyCardNo") == null ? string.Empty : row.Field<string>("AshleyCardNo").Replace("'", "");
                        financingOption = row.Field<string>("FinancingOption") == null ? string.Empty : row.Field<string>("FinancingOption");
                    }
                }
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation

                IsInvoiceNeeded = false;
                //saleOrder = "725000144636";
                if (string.IsNullOrEmpty(saleOrder))
                {
                    if (cardType == "credit")
                    {
                        saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                    }
                    else
                    {
                        saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear, financingOption);
                        //saleOrder = "725000144307";
                    }
                    IsInvoiceNeeded = true;
                }

                #endregion
            }           

        }
        
        #endregion
        

        [TestMethod]
        [TestCategory("SmokeFIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(62922)]
        [Description("Verify the publish to item group functionality")]
        public void VerifyThePublishToItemGroupFunctionality()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = "ECM/Inventory management/Setup/Inventory/Ashley sales category/Sales category";
            string FinalSalesCategory = "final sales category";
            string SalesCategoryToFinalMapping = "Sales category to final category mapping";
            string AshleyParameters = "Inventory and warehouse management parameters";
            string ItemGroups = "Inventory/Item groups";
            String guid = Guid.NewGuid().ToString();
            string[] testValue = guid.Split('-');
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SC = new WinWindow();
            WinWindow wnd_FinalSalesCategory = new WinWindow();
            WinWindow wnd_InventoryAndWarehouseParameters = new WinWindow();
            WinWindow wnd_SalesToFinalSalesMapping = new WinWindow();
            WinWindow wnd_ItemGroups = new WinWindow();
            WinEdit edit_SalesCategory = new WinEdit();
            WinEdit edit_Name = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinButton btn_Close = new WinButton();
            WinTreeItem treeItem = new WinTreeItem();
            WinMenuItem menu_New = new WinMenuItem();
            WinHyperlink hyperlink_AshleyParameter = new WinHyperlink();
            WinCheckBox checkbox_EnableFinalCategory = new WinCheckBox();
            WinTable table_itemGroup = new WinTable();
            UITestControlCollection uitcc_ItemGroup = new UITestControlCollection();
            UITestControlCollection uitcc_DefaultAccounts = new UITestControlCollection();
            #endregion

            #region Test Steps
            // 1, AX 2012 Sign on
            // 1.1, Launch AX 2012 environment
            // 1.2, Put a valid username/password and hit enter
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 2, Go to Inventory management->Setup->Inventory->Ashley sales category->sales category
            wnd_AX.NavigateTo(AddressPath);

            // 3, Click on New sales category icon
            wnd_AX.ClickRibbonMenuItem("Sales category", "New", "Sales category",true);

            // 4, Give the @name and @salescategory and click ok
            wnd_SC = UITestControl.Desktop.FindWinWindow(new { Name = "‪Create sales category‬ (‎‪1‬ - ‎‪ecm‬)‎‪ :‬‬‬‬", ClassName = "AxTopLevelFrame" });

            edit_SalesCategory = wnd_SC.DetectControl<WinEdit>(new { Name = "Sales category" });
            edit_SalesCategory.EnterWinText(testValue[0]);
            edit_Name = wnd_SC.DetectControl<WinEdit>(new { Name = "Name" });
            edit_Name.EnterWinText(testValue[0]);

            btn_Ok = wnd_SC.DetectControl<WinButton>(new { Name = "Ok" });
            btn_Ok.WinClick();

            // 5, Go to Inventory management->Setup->Inventory->Ashley sales category->final sales category
            wnd_AX.SelectFromTreeItems(FinalSalesCategory);

            // 6, Click on New button in the top            
            wnd_FinalSalesCategory = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Final sales category‬(‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxTopLevelFrame" });
            menu_New = wnd_FinalSalesCategory.DetectControl<WinClient>(new {Name = "MiddleRow"}).DetectControl<WinGroup>(new { Name = "NewDeleteGroup" }).DetectControl<WinMenuItem>(new { Name = "New" });            
            Mouse.Click(menu_New);

            edit_SalesCategory = wnd_FinalSalesCategory.DetectControl<WinWindow>(new { ClassName = "AxEdit" }).DetectControl<WinEdit>(new { Name = "Final sales category" });
            edit_SalesCategory.EnterWinTextV2(testValue[1]);

            edit_Name = wnd_FinalSalesCategory.DetectControl<WinEdit>(new { Name = "Name" });
            edit_Name.EnterWinTextV2(testValue[1]);
            //edit_Name.Text = testValue[1];

            wnd_FinalSalesCategory.CloseWindow();

            // 12, Verify the enable/disable of publish to item group button
            wnd_AX.SelectFromTreeItems(AshleyParameters);
            wnd_InventoryAndWarehouseParameters = UITestControl.Desktop.FindWinWindow(new { Name = "‪Inventory and warehouse management parameters‬ :", ClassName = "AxTopLevelFrame" });
            hyperlink_AshleyParameter = wnd_InventoryAndWarehouseParameters.DetectControl<WinHyperlink>(new { Name = "Ashley parameters" });
            hyperlink_AshleyParameter.WinClick();
            checkbox_EnableFinalCategory = wnd_InventoryAndWarehouseParameters.DetectControl<WinCheckBox>(new { Name = "Enable final category" });
            if (checkbox_EnableFinalCategory.Checked == true)
            {
                checkbox_EnableFinalCategory.WinClick();
            }
            wnd_InventoryAndWarehouseParameters.CloseWindow();

            wnd_AX.SelectFromTreeItems(SalesCategoryToFinalMapping);
            wnd_SalesToFinalSalesMapping = UITestControl.Desktop.FindWinWindow(new { Name = "‪Sales category to final category mapping‬ (‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxTopLevelFrame" });
            Assert.IsNotNull(wnd_SalesToFinalSalesMapping.FindRibbonMenuItem("Overview", "Publish", "Publish all sales categories to item groups"), "Icon Enabled");
            wnd_SalesToFinalSalesMapping.CloseWindow();

            wnd_AX.SelectFromTreeItems(AshleyParameters);
            wnd_InventoryAndWarehouseParameters = UITestControl.Desktop.FindWinWindow(new { Name = "‪Inventory and warehouse management parameters‬ :", ClassName = "AxTopLevelFrame" });
            hyperlink_AshleyParameter = wnd_InventoryAndWarehouseParameters.DetectControl<WinHyperlink>(new { Name = "Ashley parameters" });
            hyperlink_AshleyParameter.WinClick();
            checkbox_EnableFinalCategory = wnd_InventoryAndWarehouseParameters.DetectControl<WinCheckBox>(new { Name = "Enable final category" });
            if (checkbox_EnableFinalCategory.Checked == false)
            {
                checkbox_EnableFinalCategory.WinClick();
            }

            wnd_InventoryAndWarehouseParameters.CloseWindow();

            // 7, Go to Inventory management->Setup->Inventory->Ashley sales category->Sales category to Final category mapping form
            wnd_AX.SelectFromTreeItems(SalesCategoryToFinalMapping);
            wnd_SalesToFinalSalesMapping = UITestControl.Desktop.FindWinWindow(new { Name = "‪Sales category to final category mapping‬ (‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxTopLevelFrame" });

            // 8, Click Create mapping Link
            // 9, click the sales category drop down and select step 4 created sales category and click final sales category dropdown and select final sales category which was created in step 6.                     
            wnd_SalesToFinalSalesMapping.ClickRibbonMenuItem("ActionPaneTab", "Maintain", "Create mapping");

            edit_SalesCategory = wnd_SalesToFinalSalesMapping.DetectControl<WinEdit>(new { Name = "Sales category.Sales category" });
            edit_SalesCategory.EnterWinText(testValue[0]);
            Keyboard.SendKeys("{TAB}");
            Keyboard.SendKeys(testValue[0]);
            Keyboard.SendKeys("{TAB}");
            Keyboard.SendKeys(testValue[1]);

            // 10, Check for the button Publish to item group
            // 11, Click on publish all sales categories to item groups button
            wnd_SalesToFinalSalesMapping.ClickRibbonMenuItem("ActionPaneTab", "Publish", "Publish all sales categories to item groups");
            DynamicsAxCommonFunctions.HandleInfolog(true);

            wnd_AX.SelectFromTreeItems(ItemGroups);
            wnd_ItemGroups = UITestControl.Desktop.FindWinWindow(new { Name = "‪Item groups‬ (‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxTopLevelFrame" });
            uitcc_ItemGroup = wnd_ItemGroups.GetAllRecords("Grid");
            uitcc_ItemGroup.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(testValue[0]));
            if (uitcc_ItemGroup.Count == 0)
            {
                throw new Exception("Item group not found");
            }
            wnd_ItemGroups.CloseWindow();
            #endregion

            #region Test Cleanup
            #endregion
        }
        
        [TestMethod]
        [TestCategory("SmokeFIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(52568)]
        [Description("FR 904 - Verify the credit note functionality")]
        public void FR904_VerifyTheCreditNoteFunctionality()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            
            quantity = "3";

            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            //string saleOrder;

            string adjustmentType = "Adjustment credit";
            string reasonCode = "ShipDmge";
            string discountAmount = "10";
            string adjCreditQty = "1";


            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_Infolog = new WinWindow();
            BrowserWindow browserWindow = new BrowserWindow();
            HtmlRow row_Record = new HtmlRow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_discountPercentage = new WinEdit();
            WinButton btn_OK = new WinButton();
            WinButton btn_Close = new WinButton();
            IWebDriver driver = null;

            #endregion

            #region Test Steps
            
            #region Pre-requisite
            // 1, Create Sales Order using credit card payment type
            // 1.1, Create SO with @ItemCode, @Quantity, @RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecoreFIN();

            #endregion


            string PurchaseOrder = saleOrder + 1.ToString("00");
            // 2, Launch Dynamics AX
            // 2.1, Give a valid @AXUsername / @AXPassword and hit Enter key
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 3, Open the Sales Order
            // 3.1, Go to ECM/Accounts receivable/Common/Sales orders/All sales orders
            // 3.2, Seach using the SO confirmation number in the grid and double click on the created SO
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

            #region check Invoice steps for ordered test Execution

            if(IsInvoiceNeeded)
            {
                // Wait till PO is auto generated for the SO
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.WaitForPurchaseOrderToBeGenerated();
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                    Assert.AreEqual("Unscheduled", lineStage, PurchaseOrder + "PO not generated");
                    Assert.AreEqual("Open order", lineStatus, PurchaseOrder + "PO not generated");
                }
                // 4, Schedule the HD Purchase Order(s) via Homestore Portal (PTL) - Header level
                // 4.1, Click HomeStore portal > Purchase order => New cue icon
                driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");


            // 4.2, Seach and click on the purchase order
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            //driver.ClickACellinTable("Purchase order", PurchaseOrder );               
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 4.3, Enter the @POERPnumber & @POConfirmeddeliverydate greater than Latest estimated delivery date
            // Click Apply to lines         
            // 4.4, Select the Reason Code from dropdown and click OK
            // Click Save and Close
            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            // 5, Go to AX application - Sale Order form
            //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            // 6, Invoice the DS lines via AS400
            // 7, Invoice the HD lines via Portal

            // 8, Go to PTL application          

            // 8, Invoice the HD Purchase Order(s) via Homestore Portal (PTL)
            // 8.1, Click on the Scheduled cue icon
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");


            // 8.2, Click on the Scheduled cue icon
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            //driver.ClickACellinTable("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 8.3, Click Invoice Process => New Vendor Invoice => Post Invoice
            driver.Invoice();
            
                IsInvoiceNeeded = false;
            }

            #endregion

            // 9, Go to AX application - Sale Order form
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            // 10, Select the lines in Sale order lines grid
            // Verify the Line Stage and Line Status of the lines
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");

            // Verify the Line Stage and Line Status of the lines
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Invoiced", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Invoiced", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            //Delivery line
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Invoiced", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Invoiced", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            // 11, Go to Sell tab in SO form and click Credit Note
            wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
            wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "Create credit note:", ClassName = "AxTopLevelFrame" });

            // 12, Select @AdjustmentType and select the @AdjCreditReasoncode
            wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", adjustmentType);
            //wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", reasonCode);
            WinEdit edt = new WinEdit(wnd_CreateCredit);
            edt.SearchProperties.Add("ControlType", "Edit", PropertyExpressionOperator.EqualTo);
            edt.SearchProperties.Add("Name", "Reason code", PropertyExpressionOperator.EqualTo);

            edt.SelectValueFromCustomComboBox(reasonCode);
            // 13, Update the @Discountamount in Discount amount text field
            edit_discountPercentage = wnd_CreateCredit.DetectControl<WinEdit>(new { Name = "Discount amount" });
            edit_discountPercentage.EnterWinTextV2(discountAmount);
            // 14, Select the required main line(s) in grid (Lower pane)
            // Reduce the @AdjCreditQty in Quantity column
            uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", adjCreditQty);
            }
            // 15, Click OK in Create credit note form
            btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
            btn_OK.WinClick();

            // 16, Select the newly added adjustment credit lines in Sale order lines grid
            // Verify the Line Stage and Line Status of the lines     

            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("PO creation pending", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Open order", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                int i = uitcc_SOLines.IndexOf(row_SOLine);
                Double origty = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Orig qty"));
                if (i % 2 == 0)
                {
                    Assert.IsTrue(origty < 0, "not a negative line");
                }
                else
                {
                    Assert.IsTrue(origty > 0, "not a positive line");
                }
            }



            // 17, Click Complete in SO form (action pane)
            // Click Close in info log
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
            List<string> lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Adjustment credit for the products");

            // 18, Select the newly added adjustment credit lines in Sale order lines grid
            // Verify the Line Stage and Line Status of the lines
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Invoiced", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Invoiced", lineStatus, PurchaseOrder + " not Invoiced.");
            }
            wnd_SO.SetFocus();
            wnd_SO.CloseWindow();

            #endregion

            #region Test Cleanup

            #endregion

        }

        [TestMethod]
        [TestCategory("SmokeFIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(60865)]
        [Description("FR-2041 Revenue Share Process starts only after Invoicing is completed and Credit Card has been charged and the full settlement of the sales invoice")]
        public void FR_2041_RevenueShareProcessStartsOnlyAfterInvoicingIsCompletedAndCreditCardHasBeenChargedAndTheFullSettlementOfTheSalesInvoice()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Address = "Accounts Payable/Common/Revenue share";

            quantity = "3";
            
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_Infolog = new WinWindow();
            WinWindow wnd_DD = new WinWindow();
            WinWindow wnd_CCHistory = new WinWindow();
            WinWindow wnd_RSR = new WinWindow();
            WinWindow wnd_RSAPJournal = new WinWindow();
            WinColumnHeader edit_SONumber = new WinColumnHeader();
            BrowserWindow browserWindow = new BrowserWindow();
            HtmlRow row_Record = new HtmlRow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_discountPercentage = new WinEdit();
            WinButton btn_OK = new WinButton();
            WinButton btn_Close = new WinButton();
            WinEdit edit_ads = new WinEdit();
            WinMenuItem dd_menuItem = new WinMenuItem();
            WinCheckBox checkbox_BatchProcess = new WinCheckBox();
            IWebDriver driver = null;

            #endregion

            #region Test steps

            // 1. Create online sale
            // 1.1, Open the Site core website 
            // 1.2, Mouse hover @product and Click on Details button
            // 1.3, Enter a @quantity and click Add to Cart
            // 1.4, Enter valid @Zip Code and click search icon
            // 1.5, Click on Check out button in lightbox
            // 1.6, Click Continue as Guest link
            // 1.7, Enter in @first name,@last name, @address, @City, @State and @Zip,@phonenumber,@email and click next
            // 1.8, Enter Billing Address/copy same as shipping address
            // 1.9, Click continue button
            // 1.10, Enter @Card-number, @security-code, @year, @month,@first name, @last name on card and click "continue" Data is accepted and user is taken to the review and confirm screen
            // 1.11, Click Submit Order
            String ERPNumber = string.Empty;

            #region Pre-requisite
            
            CreateASaleOrderInSitecoreFIN();

            #endregion
            string PurchaseOrder = saleOrder + 1.ToString("00");
            
             // 2, Login to AX 2012 ->Accounts Receivable -> common -> Sales orders -> All sales orders.
             wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 3, Open the Sales Order
            // 3.1, Go to ECM/Accounts receivable/Common/Sales orders/All sales orders
            // 3.2, Seach using the SO confirmation number in the grid and double click on the created SO
           

            #region invoice steps
            if (IsInvoiceNeeded)
            {
                wnd_AX.NavigateTo(AddressPath);
                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.WaitForPurchaseOrderToBeGenerated();

                wnd_SO.ClickRibbonMenuItem("Manage", "Customer", "Credit card");

                // 4, Goto Manage>Credit card>Authorization history and verify Authorization History
                wnd_DD = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" });

                dd_menuItem = wnd_DD.DetectControl<WinMenuItem>(new { Name = "Authorization history" });
                dd_menuItem.WinClick();

                wnd_CCHistory = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪‪‪Credit card history‬ (‎‪1‬ - ‎‪ecm‬) :‎‪", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
                uitcc_SOLines = wnd_CCHistory.GetAllRecords("Grid");

                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String type = row_SOLine.GetColumnValueOfARecord("Type");
                    String status = row_SOLine.GetColumnValueOfARecord("Status");

                    Assert.AreEqual("Authorization", type, "Mismatch");
                    Assert.AreEqual("Approved", status, "Mismatch");
                }

                // 5, Invoice the sale order through PTL
                driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
                driver = driver.SelectTab("Homestore portal");
                driver.SelectFromRibbon("Purchase order", "Stages", "New");

                driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");               

                driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");

                driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                driver.Invoice();
                driver.Close();
                IsInvoiceNeeded = false;
            }

            #endregion

            Playback.Wait(180000);

            // 6, Goto -> Accounts Payable-> common -> Revenue share -> Revenue share register
            wnd_AX.NavigateTo(Address);
            wnd_AX.SelectFromTreeItems("Revenue share register");


            // 7, Go to -> Accounts Payable-> Common Revenue share -> Revenue Share register- verify the above created sale order number 
            wnd_RSR = UITestControl.Desktop.FindWinWindow(new { Name = "‪Revenue share register‬ (‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxTopLevelFrame" });

            edit_SONumber = wnd_RSR.DetectControl<WinTable>(new { Name = "Grid" }).DetectControl<WinColumnHeader>(new { Name = "SO number" });
            edit_SONumber.WinClick();

            // 8, Filter by above created Sale order number and verify the Revenue share record for the sale order.
            edit_ads = edit_SONumber.DetectControl<WinEdit>(new { ClassName = "AxEdit" });
            edit_ads.EnterWinText(saleOrder);
            Keyboard.SendKeys("{ENTER}");

            uitcc_SOLines = wnd_RSR.GetAllRecords("Grid");

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String salesInvoicePosted = row_SOLine.GetColumnValueOfARecord("Sales invoice posted");
                String RSAPCreated = row_SOLine.GetColumnValueOfARecord("RS AP created");
                String RSPosted = row_SOLine.GetColumnValueOfARecord("RS posted");
                String RSPaid = row_SOLine.GetColumnValueOfARecord("RS AP paid");

                Assert.AreEqual("Yes", salesInvoicePosted, "Sales invoice posted is unchecked");
                Assert.AreEqual("Yes", RSAPCreated, "RS AP created is unchecked");
                Assert.AreEqual("Yes", RSPosted, "RS posted is unchecked");


            }
            wnd_RSR.SetFocus();
            wnd_RSR.CloseWindow();
            // 9, Run the revenue share batch process (Accounts Payable-> Periodic-> Batch job for AP journal.
            wnd_AX.SelectFromTreeItems("Accounts Payable/Periodic/Batch job for AP journal");
            wnd_RSAPJournal = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Revenue share AP journal‬‎", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
            checkbox_BatchProcess = wnd_RSAPJournal.DetectControl<WinCheckBox>(new { Name = "Batch processing" });
            if (checkbox_BatchProcess.Checked == false)
            {
                checkbox_BatchProcess.WinClick();
            }
            btn_OK = wnd_RSAPJournal.DetectControl<WinButton>(new { Name = "OK" });
            btn_OK.WinClick();    
            #endregion
            saleOrder = string.Empty;
            #region Test Cleanup
            #endregion
        }

        [TestMethod]
        [TestCategory("SmokeFIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(73970)]
        [Description("Order to Cash CC Orders (Bug 109103)")]
        public void OrderToCashCCOrders()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Address = "Accounts Payable/Common/Revenue share";

            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
           // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_Infolog = new WinWindow();
            WinWindow wnd_TSTT = new WinWindow();
            WinWindow wnd_DD = new WinWindow();
            WinWindow wnd_CCHistory = new WinWindow();
            WinWindow wnd_RSR = new WinWindow();
            WinColumnHeader edit_SONumber = new WinColumnHeader();
            WinMenuItem dd_menuItem = new WinMenuItem();
            BrowserWindow browserWindow = new BrowserWindow();
            HtmlRow row_Record = new HtmlRow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_discountPercentage = new WinEdit();
            WinEdit edit_TASTA = new WinEdit();
            WinEdit edit_ads = new WinEdit();
            WinButton btn_OK = new WinButton();
            WinButton btn_Close = new WinButton();
            IWebDriver driver = null;

            #endregion

            #region Test Steps

            #region Pre-requisite
            //1, Place an HD & DS item Order in Site core using Credit Card
            CreateASaleOrderInSitecoreFIN();

            #endregion

            string PurchaseOrder = saleOrder + 1.ToString("00");

            // 2, Log in to AX 
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 3, Navigate to Accounts Receivable -> All sales Order 
            wnd_AX.NavigateTo(AddressPath);

            // 4, Click on the above created SO 
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            double NetAmount = 0;
            foreach (WinRow row_SOLine in uitcc_SOLines)            
            {
                row_SOLine.SelectARecord(true, false);
                double Amount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Net amount"));

                wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Financials", "Sales tax");

                wnd_TSTT = UITestControl.Desktop.FindWinWindow(new { Name = "‪Temporary sales tax transactions‬ (‎‪1‬ - ‎‪ecm‬)‎‪ - ‎‪‪‪Sales order‬: ‎‪", ClassName = "AxTopLevelFrame" });
                edit_TASTA = wnd_TSTT.DetectControl<WinEdit>(new { Name = "Total actual sales tax amount" });
                Double Tax = Convert.ToDouble(edit_TASTA.Text);

                wnd_TSTT.CloseWindow();

                double totalAmount = Amount + Tax;
                NetAmount = NetAmount + totalAmount;
                NetAmount = Math.Round(NetAmount, 2);
                row_SOLine.SelectARecord(false, false);

            }


            // 5, Navigate to Manage -> Credit Card -> Authorization History 
           
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("Manage", "Customer", "Credit card");
            wnd_DD = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" });
            dd_menuItem = wnd_DD.DetectControl<WinMenuItem>(new { Name = "Authorization history" });
            dd_menuItem.WinClick();

            // 6, Verify the Line Confirmation ID 
            // 7, Verify the Type and Status 
            // 8, Verify the Amount 
            wnd_CCHistory = UITestControl.Desktop.FindWinWindow(new { Name = "‪Credit card history‬ (‎‪1‬ - ‎‪ecm‬)‎‪ - ‎‪‪‪Sales order‬:", ClassName = "AxTopLevelFrame" });
            uitcc_SOLines = wnd_CCHistory.GetAllRecords("Grid");
           
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String type = row_SOLine.GetColumnValueOfARecord("Type");
                String status = row_SOLine.GetColumnValueOfARecord("Status");

                Assert.AreEqual("Authorization", type, "Mismatch");
                Assert.AreEqual("Approved", status, "Mismatch");
            }
            wnd_CCHistory.SetFocus();
            wnd_CCHistory.CloseWindow();

            // Wait till PO is auto generated for the SO
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Unscheduled", lineStage, PurchaseOrder + "PO not generated");
                Assert.AreEqual("Open order", lineStatus, PurchaseOrder + "PO not generated");
            }

            // 9, Invoice the HD Order via Portal	
            // 10, Invoice the HD lines via Portal
            // 11, Go to PTL application
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            // 12, Invoice the HD Purchase Order(s) via Homestore Portal (PTL)
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            // 12.1, Click on the Scheduled cue icon

            // 12.2, Search and click on the purchase order
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 12.3, Click Invoice Process => New Vendor Invoice => Post Invoice
            driver.Invoice();
            
           
            // 13, Go to AX application - Sale Order form
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            // 14, Select the lines in Sale order lines grid
            // Verify the Line Stage and Line Status of the lines
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");

            // Verify the Line Stage and Line Status of the lines
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Invoiced", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Invoiced", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            // 15, Verify the Authorization History Form 
            wnd_SO.ClickRibbonMenuItem("Manage", "Customer", "Credit card");           

            wnd_DD = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" });
            dd_menuItem = wnd_DD.DetectControl<WinMenuItem>(new { Name = "Authorization history" });
            dd_menuItem.WinClick();
            wnd_CCHistory = UITestControl.Desktop.FindWinWindow(new { Name = "‪Credit card history‬ (‎‪1‬ - ‎‪ecm‬)‎‪ - ‎‪‪‪Sales order‬:", ClassName = "AxTopLevelFrame" });

            // 16, Verify the Type and Status 
            // 17, Verify the Amount
            uitcc_SOLines = wnd_CCHistory.GetAllRecords("Grid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                if (row_SOLine.FriendlyName == "Row 1")
                {
                    String type = row_SOLine.GetColumnValueOfARecord("Type");
                    String status = row_SOLine.GetColumnValueOfARecord("Status");
                    Double Amount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Amount"));

                    Assert.AreEqual("Authorization", type, "Mismatch");
                    Assert.AreEqual("Settled", status, "Mismatch");
                    Assert.AreEqual(Amount, NetAmount, "Amount mismatch");
                }
                else
                {
                    String type = row_SOLine.GetColumnValueOfARecord("Type");
                    String status = row_SOLine.GetColumnValueOfARecord("Status");
                    Double Amount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Amount"));

                    Assert.AreEqual("Finalization", type, "Mismatch");
                    Assert.AreEqual("Approved", status, "Mismatch");
                    Assert.AreEqual(Amount, NetAmount, "Amount mismatch");
                }

            }
            uitcc_SOLines = wnd_CCHistory.GetAllRecords("Grid");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String type = row_SOLine.GetColumnValueOfARecord("Type");
                String status = row_SOLine.GetColumnValueOfARecord("Status");

                Assert.AreEqual("Authorization", type, "Mismatch");
                Assert.AreEqual("Approved", status, "Mismatch");
            }
            wnd_CCHistory.SetFocus();
            wnd_CCHistory.CloseWindow();


            // 18, Invoice the DS Order via AX / EDI 

            // 19, Verify the Authorization History Form 

            // 20, Verify the Type and Status 

            // 21, Verify the Amount

            // 22, Go to Account Payable -> Common -> Rev share Register, verify the above invoiced sale order


            //WinWindow wnd = new WinWindow(UIAutomationControl.Desktop);
            //wnd.SearchProperties.Add("Name", "‪Microsoft Dynamics AX‬", PropertyExpressionOperator.Contains);
            //UITestControlCollection coll = wnd.FindMatchingControls();

            //wnd_AX = UITestControl.Desktop.FindWinWindow(new { Name = "‪Microsoft Dynamics AX‬ - ‏‪Ashley Furniture Industries,‏", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
            wnd_AX.SetFocus();
            wnd_AX.NavigateTo(Address);
            wnd_AX.SelectFromTreeItems("Revenue share register");


            // 7, Go to -> Accounts Payable-> Common Revenue share -> Revenue Share register- verify the above created sale order number 
            wnd_RSR = UITestControl.Desktop.FindWinWindow(new { Name = "‪Revenue share register‬ (‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxTopLevelFrame" });

            edit_SONumber = wnd_RSR.DetectControl<WinTable>(new { Name = "Grid" }).DetectControl<WinColumnHeader>(new { Name = "SO number" });
            edit_SONumber.WinClick();

            // 8, Filter by above created Sale order number and verify the Revenue share record for the sale order.
            edit_ads = edit_SONumber.DetectControl<WinEdit>(new { ClassName = "AxEdit" });
            edit_ads.EnterWinText(saleOrder);
            Keyboard.SendKeys("{ENTER}");

            uitcc_SOLines = wnd_RSR.GetAllRecords("Grid");
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String revenueShareID = row_SOLine.GetColumnValueOfARecord("Revenue share Id");
                String salesInvoicePosted = row_SOLine.GetColumnValueOfARecord("Sales invoice posted");

                Assert.IsNotNull(revenueShareID, "Revenue share ID is null");
                Assert.AreEqual(salesInvoicePosted, "Yes", "Sales Invoice posted unchecked");

            }

            uitcc_SOLines = wnd_RSR.GetAllRecords("RevenueShareLinesGrid");
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                Double AdjustedRevenueShare = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Adjusted revenue share"));
                Double Margin = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Net margin"));
                Double revenueSharePercent = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Rev share %"));
                Double totalMerchandaiseInvoiceOnly = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Line merchandise value"));
                Double lineCOGS = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Total line COGS"));
                Double creditCardFees = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Credit card fee"));
                Double creditCardFees_Distribution = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Credit card fee distribution"));
                Double totalInvoiceAmt = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Invoiced sales line amount"));
                Double estimatedCreditFee = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("E. CC Fees %"));

                Double AdjustedRevenueShare_Cal = Margin * (revenueSharePercent / 100);
                Double Margin_Cal = totalMerchandaiseInvoiceOnly - lineCOGS - creditCardFees - creditCardFees_Distribution;
                Double creditCardFees_Cal = totalInvoiceAmt * (estimatedCreditFee/100);

                Assert.AreEqual(Math.Round(AdjustedRevenueShare_Cal), Math.Round(AdjustedRevenueShare), "Revenue Share amount is not equal");
                Assert.AreEqual(Math.Round(Margin_Cal), Math.Round(Margin), "Margin amount is not equal");
                Assert.AreEqual(Math.Round(creditCardFees_Cal), Math.Round(creditCardFees), "credit Card Fees amount is not equal");

            }
            wnd_RSR.SetFocus();
            wnd_RSR.CloseWindow();
            wnd_SO.SetFocus();
            #endregion

            #region Test Cleanup

            #endregion

        }

        [TestMethod]
        [TestCategory("SmokeFIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91871)]
        [Description("Finance EOD Reports")]
        public void FinanceEodReports()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath_AshleyReport = "ECM/Accounts receivable/Reports/Ashley reports";
            string ARbalancing = "Accounts receivable/Reports/Ashley reports/AR Balancing";
            string ItemSalesDetails = "Accounts receivable/Reports/Ashley reports/Item Sales Detail";
            string CashReceiptJournalDetails = "Accounts receivable/Reports/Ashley reports/Cash receipt journal";
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;


            #endregion

            #region Local variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_Infolog = new WinWindow();
            WinWindow wnd_ISD = new WinWindow();
            WinWindow wnd_CRJ = new WinWindow();
            WinWindow wnd_ARBalancing = new WinWindow();
            HtmlRow row_Record = new HtmlRow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_discountPercentage = new WinEdit();
            WinButton btn_OK = new WinButton();

            WinButton btn_Select = new WinButton();
            WinButton btn_Ok = new WinButton();
            WinEdit edit_FromDate = new WinEdit();
            WinEdit edit_ToDate = new WinEdit();
            WinEdit edit_RetailChannel = new WinEdit();
            WinEdit edit_RetailType = new WinEdit();
            WinCheckBox checkBox_ShowTotal = new WinCheckBox();
            WinEdit edit_CustomerAccount = new WinEdit();

            #endregion

            #region Test Steps

            // 1) AX 2012 Sign on
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 2) Navigate to ECM->Accounts Receivables->Reports->Ashley Reports->Item sales Detail
            wnd_AX.NavigateTo(AddressPath_AshleyReport);
            wnd_AX.SelectFromTreeItems(ItemSalesDetails);

            // 3) Verify the @Filters present in the Report Screen
            // 4) Enter the valid filter criteria and generate the report
            wnd_ISD = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Item Sales Detail" }, PropertyExpressionOperator.Contains);
            edit_FromDate = wnd_ISD.DetectControl<WinEdit>(new { Name = "From date" });
            edit_FromDate.Text = DateTime.Now.AddDays(-2).ToString("dd/MM/yyyy");

            edit_ToDate = wnd_ISD.DetectControl<WinEdit>(new { Name = "To date" });
            edit_ToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            btn_Ok = wnd_ISD.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();
            
            while (true)
            {
                try
                {
                    wnd_Infolog = UITestControl.Desktop.FindWinWindow(new { Name = "Infolog", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);

                    if (wnd_Infolog.Exists)
                    { break; }
                }
                catch (Exception ex) { }
                wnd_ISD = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Item Sales Detail" }, PropertyExpressionOperator.Contains);
                try {
                    WinToolBar toolBar = wnd_ISD.DetectControl<WinToolBar>(new { Name = "Toolstrip" });
                    if (toolBar.BoundingRectangle.Height > 0)
                    {
                        break;
                    }
                }catch(Exception ex) { }
            }
            
            wnd_ISD.CloseWindow();

            // 5) Navigate to ECM->Accounts Receivables->Reports->Ashley Reports->Cash receipt Journal report
            // 6) Enter the valid filter criteria and generate the report
            wnd_AX.NavigateTo(AddressPath_AshleyReport);
            wnd_AX.SelectFromTreeItems(CashReceiptJournalDetails);
            wnd_CRJ = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Cash receipt journal" }, PropertyExpressionOperator.Contains);


            edit_FromDate = wnd_CRJ.DetectControl<WinEdit>(new { Name = "From date" });
            edit_FromDate.Text = DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy");

            edit_ToDate = wnd_CRJ.DetectControl<WinEdit>(new { Name = "To date" });
            edit_ToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            btn_Ok = wnd_CRJ.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            wnd_CRJ = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Cash receipt journal‬" }, PropertyExpressionOperator.Contains);
            wnd_CRJ.CloseWindow();

            // 7) Navigate to ECM->Accounts Receivables->Reports->Ashley Reports->AR balancing report
            // 8) Enter the valid filter criteria and generate the report
            wnd_AX.NavigateTo(AddressPath_AshleyReport);
            wnd_AX.SelectFromTreeItems(ARbalancing);

            wnd_ARBalancing = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪AR Balancing" }, PropertyExpressionOperator.Contains);

            edit_FromDate = wnd_ARBalancing.DetectControl<WinEdit>(new { Name = "From date" });
            edit_FromDate.Text = DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy");

            edit_ToDate = wnd_ARBalancing.DetectControl<WinEdit>(new { Name = "To date" });
            edit_ToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            edit_RetailType = wnd_ARBalancing.DetectControl<WinEdit>(new { Name = "Report type" });
            edit_RetailType.Text = "Detail";

            checkBox_ShowTotal = wnd_ARBalancing.DetectControl<WinCheckBox>(new { Name = "Show total" });
            if (checkBox_ShowTotal.Checked == false)
            {
                checkBox_ShowTotal.WinClick();
            }

            btn_Ok = wnd_ARBalancing.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            //  Verify the data in the AR balancing report
            wnd_ARBalancing = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪AR Balancing" }, PropertyExpressionOperator.Contains);
            wnd_ARBalancing.CloseWindow();


            #endregion

            #region Test Cleanup

            #endregion
        }       

        [TestMethod]
        [TestCategory("SmokeFIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(74094)]
        [Description("Customer Returns")]
        public void CustomerReturns()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Address = "Accounts Payable/Common/Revenue share";
                      
            quantity = "3";
           
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            //string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_Infolog = new WinWindow();
            WinWindow wnd_DD = new WinWindow();
            WinWindow wnd_CCHistory = new WinWindow();
            WinMenuItem dd_menuItem = new WinMenuItem();
            BrowserWindow browserWindow = new BrowserWindow();
            HtmlRow row_Record = new HtmlRow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_discountPercentage = new WinEdit();
            WinButton btn_OK = new WinButton();
            WinButton btn_Close = new WinButton();
            IWebDriver driver = null;
            string lineConfirmationId = string.Empty;
            #endregion

            #region Test steps

            // 1. Create online sale
            // 1.1, Launch the @AshleyWebPortal in the browser
            // 1.2, Select the products and click on the Details button required and click 'Add to cart'
            // 1.3, If Required update the quantity and click 'Add to cart'
            // 1.4, Click on the Checkout
            // 1.5, Enter required @quantity
            // 1.6, Provide the Promo code in the "Appply to Dicount field"(ex:20%off)
            // 1.7, Verify the subtotal,Small Parcel Delivery (Shipping and Handling Charges) and Estimated Total for the items added
            // 1.8, Click on 'Proceed to checkout'
            // 1.9, In Shipping page enter @shippingname , @Country/region, @Address, @City, @State/Province, @ShipZip/Postalcode ,@Phone number ,@Email address and click on continue
            // 1.10, Mark the checkbox at the of the billing page and Click on continue
            // 1.11, 1. Enter the @Card1 (Visa/Master/Amex)number @Card2 (AAC),@CVV, @ Zip Code, @Expiry Date,@first name and @last name and click continue 2.Enter the @Card2 (AAC, @Zipcode, @Expiry Date, @FinancingOption and click continue
            // 1.12, Verify the order details and click on "I accept terms and conditions checkbox"
            // 1.13, CLick on "I accept" and Click on 'Submit order'
            // 1.14, Note the Order Number and the date and time of the order placed
            String ERPNumber = string.Empty;

            #region Pre-requisite

            CreateASaleOrderInSitecoreFIN();

            #endregion

            string PurchaseOrder = saleOrder + 1.ToString("00");

            // 2, Log in to AX 
            // 2.1, Launch AX 2012 environment
            // 2.2, Put a valid username/password and hit enter
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 3, Navigate to Accounts Receivable -> All sales Order 
            wnd_AX.NavigateTo(AddressPath);

            // 4,  Filter Sales order field with the order confirmation number received from online sales
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);


            #region check Invoice Steps Ordered test Execution

            if(IsInvoiceNeeded)
            {
            // 5, Check the Status field of the Sales Order 
            uitcc_SOLines = wnd_AX.GetAllRecords("Grid");
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String status = row_SOLine.GetColumnValueOfARecord("Status");
                Assert.AreEqual(status, "Open order", "Status mismatch");
            }

            // 6, Double click on the sales order 
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

            // 7, Goto Manage>Credit card>Authorization history and verify Authorization History
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            wnd_SO.ClickRibbonMenuItem("Manage", "Customer", "Credit card");
            wnd_DD = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" });
            dd_menuItem = wnd_DD.DetectControl<WinMenuItem>(new { Name = "Authorization history" });
            dd_menuItem.WinClick();

            wnd_CCHistory = UITestControl.Desktop.FindWinWindow(new { Name = "‪Credit card history‬ (‎‪1‬ - ‎‪ecm‬)‎‪ - ‎‪‪‪Sales order‬:", ClassName = "AxTopLevelFrame" });
            uitcc_SOLines = wnd_CCHistory.GetAllRecords("Grid");

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String type = row_SOLine.GetColumnValueOfARecord("Type");
                String status = row_SOLine.GetColumnValueOfARecord("Status");

                Assert.AreEqual("Authorization", type, "Mismatch");
                Assert.AreEqual("Approved", status, "Mismatch");
            }
            wnd_CCHistory.SetFocus();
            wnd_CCHistory.CloseWindow();

            // 8, Invoice the HD lines via Portal
            // 9, Go to PTL application
            // 10, Invoice the HD Purchase Order(s) via Homestore Portal (PTL)
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            // 10.1, Click on the Scheduled cue icon
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");

            // 10.2, Search and click on the purchase order
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 10.3, Click Invoice Process => New Vendor Invoice => Post Invoice
            driver.Invoice();
               
            IsInvoiceNeeded = false;
            }

            #endregion

            // 11, Go to AX application - Sale Order form
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            // 12, Select the lines in Sale order lines grid
            // Verify the Line Stage and Line Status of the lines
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Invoiced", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Invoiced", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            // 13, click modify on the sales order Header menu
            //wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");

            //// 14, Check sales order Header stage 
            //wnd_SO.ClickRibbonMenuItem("Main", "Show", "Header view");

            //wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            // 15, Double click on the sales order 
            // 16, Navigate to Sell tab and click on credit note button
            wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
            wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "‪Create credit note‬:", ClassName = "AxTopLevelFrame" });

            // 17, Select the @adjustmenttype as Customer Return,@ReasonCode from the Dropdowns 
            wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", "Customer return");
            // wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", "MfgDefect",false);
            WinEdit edt = new WinEdit(wnd_CreateCredit);
            edt.SearchProperties.Add("ControlType", "Edit", PropertyExpressionOperator.EqualTo);
            edt.SearchProperties.Add("Name", "Reason code", PropertyExpressionOperator.EqualTo);

            edt.SelectValueFromCustomComboBox("MfgDefect");

            // 18, Select the line quantity by clicking the checkbox on the Mark column 
            wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", true);
            uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", "1");
            }

            // 19, Click on ok button
            btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
            btn_OK.WinClick();

            // 20, Close the Form and Click Complete
            //wnd_SO.CloseWindow();
            WinWindow wnd_RMAForm = UITestControl.Desktop.FindWinWindow(new { Name = "‪Return order  -  RMA number:", ClassName = "AxTopLevelFrame" });
            wnd_SO.SetFocus();
            wnd_SO.CloseWindow();
            wnd_RMAForm.SetFocus();
            wnd_RMAForm.CloseWindow();

            // 21, Goto Manage>Credit card>Authorization history and verify Authorization History
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                int i = uitcc_SOLines.IndexOf(row_SOLine);
                Double origty = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Orig qty"));
                lineConfirmationId = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");
                if (i % 2 == 0)
                {
                    Assert.IsTrue(origty < 0, "not a negative line");
                }
            }

            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            wnd_SO.ClickRibbonMenuItem("Manage", "Customer", "Credit card");
            wnd_DD = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" });
            dd_menuItem = wnd_DD.DetectControl<WinMenuItem>(new { Name = "Authorization history" });
            dd_menuItem.WinClick();

            wnd_CCHistory = UITestControl.Desktop.FindWinWindow(new { Name = "‪Credit card history‬ (‎‪1‬ - ‎‪ecm‬)‎‪ - ‎‪‪‪Sales order‬:", ClassName = "AxTopLevelFrame" });
            uitcc_SOLines = wnd_CCHistory.GetAllRecords("Grid");

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String type = row_SOLine.GetColumnValueOfARecord("Type");
                String status = row_SOLine.GetColumnValueOfARecord("Status");

                Assert.AreEqual("Finalization", type, "Mismatch");
                Assert.AreEqual("Approved", status, "Mismatch");
            }
            wnd_CCHistory.SetFocus();
            wnd_CCHistory.CloseWindow();
            // 22, Invoice the Sale Order
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New returns");

            driver = driver.PortalFilter("Purchase order", lineConfirmationId);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled returns");
            driver = driver.PortalFilter("Purchase order", lineConfirmationId);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.Invoice();
          

            // 23, Verify "authorization history " in Manage tab->credit card ->Authorization History
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
           
            wnd_SO.ClickRibbonMenuItem("Manage", "Customer", "Credit card");
            wnd_DD = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" });
            dd_menuItem = wnd_DD.DetectControl<WinMenuItem>(new { Name = "Authorization history" });
            dd_menuItem.WinHover();
            dd_menuItem.WinClick();

            wnd_CCHistory = UITestControl.Desktop.FindWinWindow(new { Name = "‪Credit card history‬ (‎‪1‬ - ‎‪ecm‬)‎‪ - ‎‪‪‪Sales order‬:", ClassName = "AxTopLevelFrame" });
            uitcc_SOLines = wnd_CCHistory.GetAllRecords("Grid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(lineConfirmationId));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String type = row_SOLine.GetColumnValueOfARecord("Type");
                String status = row_SOLine.GetColumnValueOfARecord("Status");

                Assert.AreEqual("Credit", type, "Mismatch");
                Assert.AreEqual("Approved", status, "Mismatch");
            }
            wnd_CCHistory.SetFocus();
            wnd_CCHistory.CloseWindow();
            wnd_SO.SetFocus();
            wnd_SO.CloseWindow();

            #endregion

            #region Test Cleanup

            #endregion


        }

        [TestMethod]
        [TestCategory("SmokeFIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(74109)]
        [Description("Item Replacement")]
        public void ItemReplacement()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Address = "Accounts Payable/Common/Revenue share";

            quantity = "3";
            
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
           // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_Infolog = new WinWindow();            
            BrowserWindow browserWindow = new BrowserWindow();
            HtmlRow row_Record = new HtmlRow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_discountPercentage = new WinEdit();
            WinButton btn_OK = new WinButton();
            WinWindow wnd_DD = new WinWindow();
            WinMenuItem dd_menuItem = new WinMenuItem();
            WinWindow wnd_CCHistory = new WinWindow();
            WinButton btn_Close = new WinButton();
            IWebDriver driver = null;
            string lineConfirmationId = string.Empty;

            double Netamount = 0.0;
            #endregion

            #region Test steps
            // 1. Create online sale
            // 1.1, Launch the @AshleyWebPortal in the browser
            // 1.2, Select the products and click on the Details button required and click 'Add to cart'
            // 1.3, If Required update the quantity and click 'Add to cart'
            // 1.4, Click on the Checkout
            // 1.5, Enter required @quantity
            // 1.6, Provide the Promo code in the "Appply to Dicount field"(ex:20%off)
            // 1.7, Verify the subtotal,Small Parcel Delivery (Shipping and Handling Charges) and Estimated Total for the items added
            // 1.8, Click on 'Proceed to checkout'
            // 1.9, In Shipping page enter @shippingname , @Country/region, @Address, @City, @State/Province, @ShipZip/Postalcode ,@Phone number ,@Email address and click on continue
            // 1.10, Mark the checkbox at the of the billing page and Click on continue
            // 1.11, 1. Enter the @Card1 (Visa/Master/Amex)number @Card2 (AAC),@CVV, @ Zip Code, @Expiry Date,@first name and @last name and click continue 2.Enter the @Card2 (AAC, @Zipcode, @Expiry Date, @FinancingOption and click continue
            // 1.12, Verify the order details and click on "I accept terms and conditions checkbox"
            // 1.13, CLick on "I accept" and Click on 'Submit order'
            // 1.14, Note the Order Number and the date and time of the order placed

            String ERPNumber = string.Empty;

            #region Pre-requisite

            CreateASaleOrderInSitecoreFIN();

            #endregion

            string PurchaseOrder = saleOrder + 1.ToString("00");

            // 2, Log in to AX 
            // 2.1, Launch AX 2012 environment
            // 2.2, Put a valid username/password and hit enter
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 3, Navigate to Accounts Receivable -> All sales Order 
            wnd_AX.NavigateTo(AddressPath);

            // 4,  Filter Sales order field with the order confirmation number received from online sales
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, false);

            #region invoice steps
            if(IsInvoiceNeeded)
            {
                // 5, Check the Status field of the Sales Order 
                uitcc_SOLines = wnd_AX.GetAllRecords("Grid");
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String status = row_SOLine.GetColumnValueOfARecord("Status");
                    Assert.AreEqual(status, "Open order", "Status mismatch");
                }

                // 6, Double click on the sales order 
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

                // 7, Goto Manage>Credit card>Authorization history and verify Authorization History
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.WaitForPurchaseOrderToBeGenerated();
                wnd_SO.ClickRibbonMenuItem("Manage", "Customer", "Credit card");
                wnd_DD = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" });
                dd_menuItem = wnd_DD.DetectControl<WinMenuItem>(new { Name = "Authorization history" });
                dd_menuItem.WinClick();

                wnd_CCHistory = UITestControl.Desktop.FindWinWindow(new { Name = "‪Credit card history‬ (‎‪1‬ - ‎‪ecm‬)‎‪ - ‎‪‪‪Sales order‬:", ClassName = "AxTopLevelFrame" });
                uitcc_SOLines = wnd_CCHistory.GetAllRecords("Grid");

                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String type = row_SOLine.GetColumnValueOfARecord("Type");
                    String status = row_SOLine.GetColumnValueOfARecord("Status");
                    Netamount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Amount"));

                    Assert.AreEqual("Authorization", type, "Mismatch");
                    Assert.AreEqual("Approved", status, "Mismatch");
                }

                wnd_CCHistory.CloseWindow();


                // 8, Invoice the HD lines via Portal
                // 9, Go to PTL application
                // 10, Invoice the HD Purchase Order(s) via Homestore Portal (PTL)
                driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
                driver = driver.SelectTab("Homestore portal");
                driver.SelectFromRibbon("Purchase order", "Stages", "New");

                driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

                // 10.1, Click on the Scheduled cue icon
                driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");

                // 10.2, Search and click on the purchase order
                driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                // 10.3, Click Invoice Process => New Vendor Invoice => Post Invoice
                driver.Invoice();
                IsInvoiceNeeded = false;
            }
            #endregion

            // 11, Go to AX application - Sale Order form
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            // 12, Select the lines in Sale order lines grid
            // Verify the Line Stage and Line Status of the lines
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Invoiced", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Invoiced", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            // 13, click modify on the sales order Header menu
            //wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");

            //// 14, Check sales order Header stage 
            //wnd_SO.ClickRibbonMenuItem("Main", "Show", "Header view");

            //wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            // 15, Double click on the sales order 
            // 16, Navigate to Sell tab and click on credit note button
            wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
            wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "Create credit note:", ClassName = "AxTopLevelFrame" });
            // 17, Select the @adjustmenttype as Customer Return,@ReasonCode from the Dropdowns 
            wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", "Replacement item");

            WinEdit edt = new WinEdit(wnd_CreateCredit);
            edt.SearchProperties.Add("ControlType", "Edit", PropertyExpressionOperator.EqualTo);
            edt.SearchProperties.Add("Name", "Reason code", PropertyExpressionOperator.EqualTo);

            edt.SelectValueFromCustomComboBox("MfgDefect");
            //wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", "MfgDefect");

            // 18, Select the line quantity by clicking the checkbox on the Mark column 
            wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", true);
            uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", "1");
            }

            // 19, Click on ok button
            btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
            btn_OK.WinClick();            

            // 20, Click Complete
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");

            // 21, Goto Manage>Credit card>Authorization history and verify Authorization History
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Replacement item"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                int i = uitcc_SOLines.IndexOf(row_SOLine);
                Double origty = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Orig qty"));
                lineConfirmationId = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");
                if (i % 2 == 0)
                {
                    Assert.IsTrue(origty > 0, "not a positive line");
                }
            }

            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            wnd_SO.ClickRibbonMenuItem("Manage", "Customer", "Credit card");
            wnd_DD = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" });
            dd_menuItem = wnd_DD.DetectControl<WinMenuItem>(new { Name = "Authorization history" });
            dd_menuItem.WinClick();

            wnd_CCHistory = UITestControl.Desktop.FindWinWindow(new { Name = "‪Credit card history‬ (‎‪1‬ - ‎‪ecm‬)‎‪ - ‎‪‪‪Sales order‬:", ClassName = "AxTopLevelFrame" });
            uitcc_SOLines = wnd_CCHistory.GetAllRecords("Grid");

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                if (row_SOLine.FriendlyName == "Row 1")
                {
                    String type = row_SOLine.GetColumnValueOfARecord("Type");
                    String status = row_SOLine.GetColumnValueOfARecord("Status");
                    Double Amount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Amount"));

                    Assert.AreEqual("Authorization", type, "Mismatch");
                    Assert.AreEqual("Settled", status, "Mismatch");
                    Assert.AreEqual(Amount, Netamount, "Amount mismatch");
                }
                else
                {
                    String type = row_SOLine.GetColumnValueOfARecord("Type");
                    String status = row_SOLine.GetColumnValueOfARecord("Status");
                    Double Amount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Amount"));

                    Assert.AreEqual("Finalization", type, "Mismatch");
                    Assert.AreEqual("Approved", status, "Mismatch");
                    Assert.AreEqual(Amount, Netamount, "Amount mismatch");
                }

            }
            wnd_CCHistory.SetFocus();
            wnd_CCHistory.CloseWindow();
            // 22, Invoice the Sale Order
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New returns");

            driver = driver.PortalFilter("Purchase order", lineConfirmationId);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");


            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled returns");
            driver = driver.PortalFilter("Purchase order", lineConfirmationId);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.Invoice();
            
            // 23, Verify "authorization history " in Manage tab->credit card ->Authorization History
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("Manage", "Customer", "Credit card");
            wnd_DD = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" });
            dd_menuItem = wnd_DD.DetectControl<WinMenuItem>(new { Name = "Authorization history" });
            dd_menuItem.WinClick();

            wnd_CCHistory = UITestControl.Desktop.FindWinWindow(new { Name = "‪Credit card history‬ (‎‪1‬ - ‎‪ecm‬)‎‪ - ‎‪‪‪Sales order‬:", ClassName = "AxTopLevelFrame" });
            uitcc_SOLines = wnd_CCHistory.GetAllRecords("Grid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(lineConfirmationId));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                if (row_SOLine.FriendlyName == "Row 1")
                {
                    String type = row_SOLine.GetColumnValueOfARecord("Type");
                    String status = row_SOLine.GetColumnValueOfARecord("Status");
                    Double Amount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Amount"));

                    Assert.AreEqual("Authorization", type, "Mismatch");
                    Assert.AreEqual("Settled", status, "Mismatch");
                    Assert.AreEqual(Amount, Netamount, "Amount mismatch");
                }
                else if (row_SOLine.FriendlyName == "Row 2")
                {
                    String type = row_SOLine.GetColumnValueOfARecord("Type");
                    String status = row_SOLine.GetColumnValueOfARecord("Status");
                    Double Amount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Amount"));

                    Assert.AreEqual("Finalization", type, "Mismatch");
                    Assert.AreEqual("Approved", status, "Mismatch");
                    Assert.AreEqual(Amount, Netamount, "Amount mismatch");
                }
                else
                {
                    throw new Exception("Authorization history error");
                }

            }
            wnd_CCHistory.SetFocus();
            wnd_CCHistory.CloseWindow();
            wnd_SO.SetFocus();
            wnd_SO.CloseWindow();
            #endregion

            #region Test Cleanup
            #endregion
        }

        [TestMethod]
        [TestCategory("SmokeFIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(107000)]
        [Description("FIN - Fraud report")]
        public void FinFraudReport()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string salesOrder = "ECM/Sales and Marketing/Reports/Sales orders";
            string OpenSaleOrderWithAddress = "Sales and Marketing/Reports/Sales orders/Open sales orders with addresses";

            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_SC = new WinWindow();
            WinWindow wnd_OpenSaleOrderWithAddress = new WinWindow();
            WinWindow wnd_‪afiCustSalesOpenOrders = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinButton btn_Select = new WinButton();
            WinButton btn_Ok = new WinButton();
            WinEdit edit_CreationDate = new WinEdit();
            string creation_Date = DateTime.Today.ToString("dd-MM-yyyy");
            DateTime creationDate = Convert.ToDateTime(creation_Date);
            HtmlRow row_Record = new HtmlRow();
            Dictionary<String, String> dict_ColumnAndValues = new Dictionary<String, String>();
            #endregion


            #region Test Steps
            // 1, Login to AX

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 2, Click on the Sales and Marketing -> Reports -> sales orders->Open Sales Order with addresses 
            wnd_AX.NavigateTo(salesOrder);
            wnd_AX.SelectFromTreeItems(OpenSaleOrderWithAddress);

            // 3, click ->Select button and choose Created date from the Calendar and Click ok 
            wnd_OpenSaleOrderWithAddress = UITestControl.Desktop.FindWinWindow(new { Name = "‪Open sales orders with addresses‬ (‎‪1‬)‎", ClassName = "AxTopLevelFrame" });
            btn_Select = wnd_OpenSaleOrderWithAddress.DetectControl<WinButton>(new { Name = "Select" });
            btn_Select.WinClick();

            wnd_afiCustSalesOpenOrders = UITestControl.Desktop.FindWinWindow(new { Name = "‪afiCustSalesOpenOrders‬ (‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxPopupFrame" });
            dict_ColumnAndValues = new Dictionary<string, string>();
            dict_ColumnAndValues.Add("Field", "Creation date");
            wnd_afiCustSalesOpenOrders.EnterValueToCell("RangeGrid", dict_ColumnAndValues, "Criteria", creation_Date);
            btn_Ok = wnd_afiCustSalesOpenOrders.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            wnd_OpenSaleOrderWithAddress = UITestControl.Desktop.FindWinWindow(new { Name = "‪Open sales orders with addresses‬ (‎‪1‬)‎", ClassName = "AxTopLevelFrame" });
            edit_CreationDate = wnd_OpenSaleOrderWithAddress.DetectControl<WinEdit>(new { Name = "Creation date" });
            string creationDateText = edit_CreationDate.Text;
            string[] creationDateTime = creationDateText.Split('.');
            Assert.AreEqual(creationDate.AddDays(-1).AddHours(18).AddMinutes(00).AddSeconds(00).ToString("dd-MMM-yyyy hh:mm:ss tt").ProcessString(), creationDateTime[0].ProcessString(), "Time Mismatch");
            Assert.AreEqual(creationDate.AddHours(17).AddMinutes(59).AddSeconds(59).ToString("dd-MMM-yyyy hh:mm:ss tt").ProcessString(), creationDateTime[2].ProcessString(), "Time Mismatch");
            btn_Ok = wnd_OpenSaleOrderWithAddress.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();
        
            // 4, Click OK button in Open sales order with address pop -up

            wnd_OpenSaleOrderWithAddress = UITestControl.Desktop.FindWinWindow(new { Name = "‪Open sales orders with addresses‬ (‎‪1‬)‎", ClassName = "AxTopLevelFrame" });
            wnd_OpenSaleOrderWithAddress.CloseWindow();

            #endregion

            #region Test Cleanup

            #endregion
        }

        [TestMethod]
        [TestCategory("SmokeFIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(114014)]
        [Description("Order to cash Financing Terms and Conditions - AAC")]
        public void OrderToCashFinancingTermsandConditions_AAC()
        {
            #region Test Data

            string PurchaseOrder = String.Empty;
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

            cardType = "Ashley";
            zipCode = "45420";

            address = "1115 Donald Ave";
            city = "Dayton";
            state = "OH";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_TermsAndConditions = new WinWindow();
            WinEdit edit_TermsAndConditions = new WinEdit();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            BrowserWindow browserWindow = new BrowserWindow();
            HtmlRow row_Record = new HtmlRow();
            IWebDriver driver = null;

            #endregion

            #region Test Steps

            #region Pre-requisite
            //1, Place an HD & DS item Order in Site core using Ashley Advantage Card
            CreateASaleOrderInSitecoreFIN();

            #endregion

            PurchaseOrder = saleOrder + 1.ToString("00");


            // 2, Log in to AX
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 3, Navigate to Accounts Receivable -> All Sales Order
            wnd_AX.NavigateTo(AddressPath);

            // 4, Click on the COrresponding SO
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

            // 5, Click on the Terms and Condition Option from top of the Sales Order
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("Main", "Terms and conditions", "Terms and conditions");

            // 6, Verify the Financial Term in the Page
            wnd_TermsAndConditions = UITestControl.Desktop.FindWinWindow(new { Name = "‪Terms and conditions‬ (‎‪1‬ - ‎‪ecm‬)‎‪ :‬‬‬‬", ClassName = "AxTopLevelFrame" });
            edit_TermsAndConditions = wnd_TermsAndConditions.DetectControl<WinEdit>(new { Name = "Terms and conditions" });
            //string TermsAndConditions = edit_TermsAndConditions.Text;
            //Assert.IsTrue(TermsAndConditions.Contains(financingOption.ToLower()));
            wnd_TermsAndConditions.CloseWindow();

            // 7, Select the lines in Sale order lines grid
            // Verify the Line Stage and Line Status of the lines
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("PO creation pending", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Open order", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            //Delivery line
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Delivery charge", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Open order", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            // 8, Wait till PO is auto generated for the SO
            // Enusre that PO is created for the Sales Order
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            // 9, Select the lines in Sale order lines grid
            // Verify the Line Stage and Line Status of the lines
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Unscheduled", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Open order", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            //Delivery line
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Delivery charge", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Open order", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            // 10, Invoice the HD lines via Portal
            // 11, Go to PTL application          
            // 12, Invoice the HD Purchase Order(s) via Homestore Portal (PTL)
            // 12.1, Click on the Scheduled cue icon        
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");


            // 12.2, Search and click on the purchase order
            // 12.3, Click Invoice Process => New Vendor Invoice => Post Invoice   
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.Invoice();
          
            // 13, Go to AX application - Sale Order form
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            // 14, Select the lines in Sale order lines grid
            // Verify the Line Stage and Line Status of the lines
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Invoiced", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Invoiced", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            //Delivery line
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Invoiced", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Invoiced", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            wnd_SO.SetFocus();
            wnd_SO.CloseWindow();
            #endregion

            #region Test Cleanup

            #endregion


        }

        [TestMethod]
        [TestCategory("SmokeFIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(114017)]
        [Description("Exchange")]
        public void Exchange()
        {
            #region Test Data
            string Path = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Address = "Accounts Payable/Common/Revenue share";

            quantity = "3";
            
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            //string saleOrder;
            string returnQty = "1";
            string exchangeQty = "1";
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_Infolog = new WinWindow();
            WinWindow RegisterCardBox = new WinWindow();
            BrowserWindow browserWindow = new BrowserWindow();
            HtmlRow row_Record = new HtmlRow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinWindow wnd_CCHistory = new WinWindow();
            WinWindow wnd_DD = new WinWindow();
            WinMenuItem dd_menuItem = new WinMenuItem();
            WinEdit edit_discountPercentage = new WinEdit();
            WinButton btn_OK = new WinButton();
            WinButton btn_Close = new WinButton();
            IWebDriver driver = null;
            string lineConfirmationId = string.Empty;
            WinButton btn_ExchangeItem = new WinButton();

            #endregion

            #region Test steps
            // 1.Create online sale
            // 1.1, Launch the @AshleyWebPortal in the browser
            // 1.2, Select the products and click on the Details button required and click 'Add to cart'
            // 1.3, If Required update the quantity and click 'Add to cart'
            // 1.4, Click on the Checkout
            // 1.5, Enter required @quantity
            // 1.6, Provide the Promo code in the "Appply to Dicount field"(ex: 20 % off)
            // 1.7, Verify the subtotal,Small Parcel Delivery(Shipping and Handling Charges) and Estimated Total for the items added
            // 1.8, Click on 'Proceed to checkout'
            // 1.9, In Shipping page enter @shippingname, @Country / region, @Address, @City, @State / Province, @ShipZip / Postalcode, @Phone number, @Email address and click on continue
            // 1.10, Mark the checkbox at the of the billing page and Click on continue
            // 1.11, 1.Enter the @Card1 (Visa / Master / Amex)number @Card2(AAC), @CVV, @ Zip Code, @Expiry Date, @first name and @last name and click continue 2.Enter the @Card2 (AAC, @Zipcode, @Expiry Date, @FinancingOption and click continue
            // 1.12, Verify the order details and click on "I accept terms and conditions checkbox"
            // 1.13, CLick on "I accept" and Click on 'Submit order'
            // 1.14, Note the Order Number and the date and time of the order placed

            #region Pre-requisite

            CreateASaleOrderInSitecoreFIN();

            #endregion

            //saleOrder = "725000143135";
            string PurchaseOrder = saleOrder + 1.ToString("00");

            // 2, Log in to AX
            // 2.1, Launch AX 2012 environment
            // 2.2, Put a valid username/ password and hit enter
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 3, Navigate to Accounts Receivable -> All sales Order
            wnd_AX.NavigateTo(AddressPath);

            // 4,  Filter Sales order field with the order confirmation number received from online sales
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

            #region Check Invoice steps for ordertest
            if(IsInvoiceNeeded)
            {
                // 5, Check the Status field of the Sales Order
                uitcc_SOLines = wnd_AX.GetAllRecords("Grid");
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String status = row_SOLine.GetColumnValueOfARecord("Status");
                    Assert.AreEqual(status, "Open order", "Status mismatch");
                }

                // 6, Double click on the sales order
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

                // 7, Goto Manage> Credit card > Authorization history and verify Authorization History
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.WaitForPurchaseOrderToBeGenerated();

                wnd_SO.ClickRibbonMenuItem("Manage", "Customer", "Credit card");
                wnd_DD = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" });
                dd_menuItem = wnd_DD.DetectControl<WinMenuItem>(new { Name = "Authorization history" });
                dd_menuItem.WinClick();

                wnd_CCHistory = UITestControl.Desktop.FindWinWindow(new { Name = "‪Credit card history‬ (‎‪1‬ - ‎‪ecm‬)‎‪ - ‎‪‪‪Sales order‬:", ClassName = "AxTopLevelFrame" });
                uitcc_SOLines = wnd_CCHistory.GetAllRecords("Grid");

                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String type = row_SOLine.GetColumnValueOfARecord("Type");
                    String status = row_SOLine.GetColumnValueOfARecord("Status");                  

                    Assert.AreEqual("Authorization", type, "Mismatch");
                    Assert.AreEqual("Approved", status, "Mismatch");
                }
                wnd_CCHistory.SetFocus();
                wnd_CCHistory.CloseWindow();

                // 8, Invoice the HD lines via Portal
                //   9, Go to PTL application
                // 10, Invoice the HD Purchase Order(s) via Homestore Portal(PTL)
                driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
                driver = driver.SelectTab("Homestore portal");
                driver.SelectFromRibbon("Purchase order", "Stages", "New");

                driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

                // 10.1, Click on the Scheduled cue icon
                driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");

                // 10.2, Search and click on the purchase order
                driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                // 10.3, Click Invoice Process => New Vendor Invoice => Post Invoice
                driver.Invoice();
               
                IsInvoiceNeeded = false;
            }
            #endregion

            // 11, Go to AX application -Sale Order form
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            // 12, Select the lines in Sale order lines grid
            // Verify the Line Stage and Line Status of the lines
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Invoiced", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Invoiced", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            // 13, click modify on the sales order Header menu
            //wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");

            //// 14, Check sales order Header stage
            //wnd_SO.ClickRibbonMenuItem("Main", "Show", "Header view");

            //wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            // 15, Double click on the sales order
            // 16, Navigate to Sell tab and click on credit note button
            wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
            wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "Create credit note:", ClassName = "AxTopLevelFrame" });
            // 17, Select the @adjustmenttype as Customer Return,@ReasonCode from the Dropdowns
            wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", "Exchange");
           // wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", "MfgDefect");
            WinEdit edt = new WinEdit(wnd_CreateCredit);
            edt.SearchProperties.Add("ControlType", "Edit", PropertyExpressionOperator.EqualTo);
            edt.SearchProperties.Add("Name", "Reason code", PropertyExpressionOperator.EqualTo);

            edt.SelectValueFromCustomComboBox("MfgDefect");

            // 18, Select the line quantity by clicking the checkbox on the Mark column
            wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", true);
            uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            string ItemCode = string.Empty;
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                // 19, Click Exchange Item
                wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", returnQty);
                btn_ExchangeItem = wnd_CreateCredit.DetectControl<WinButton>(new { Name = "Exchange item" });
                btn_ExchangeItem.WinClick();
                btn_ExchangeItem.WinClick();

                // 20, Select the Qunatity and Item, Click Apply
                WinWindow wnd_FindReplacementItem = UITestControl.Desktop.FindWinWindow(new { Name = "Find replacement item:", ClassName = "AxTopLevelFrame" });
                wnd_FindReplacementItem.SelectARecord("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, true, false);
                wnd_FindReplacementItem.EnterValueToCell("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, "Return quantity", returnQty);
                wnd_FindReplacementItem.EnterValueToCell("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, "Exchange qty", exchangeQty);

                WinButton btn_Apply = (WinButton)wnd_FindReplacementItem.DetectControl<WinButton>(new { Name = "Apply" });
                btn_Apply.WinClick();
            }


            // 21, Click on ok button
            btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
            btn_OK.WinClick();
            // 22, Close the Form
            WinWindow wnd_RMAForm = UITestControl.Desktop.FindWinWindow(new { Name = "‪Return order  -  RMA number:", ClassName = "AxTopLevelFrame" });
            wnd_SO.SetFocus();
            wnd_SO.CloseWindow();
            wnd_RMAForm.SetFocus();
            wnd_RMAForm.CloseWindow();
            // 23, Click Complete
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });         

            wnd_SO.ClickRibbonMenuItem("Manage", "Customer", "Credit card");
            wnd_DD = UITestControl.Desktop.DetectControl<WinWindow>(new { AccessibleName = "DropDown" });
            dd_menuItem = wnd_DD.DetectControl<WinMenuItem>(new { Name = "Authorization history" });
            dd_menuItem.WinClick();

            wnd_CCHistory = UITestControl.Desktop.FindWinWindow(new { Name = "‪Credit card history‬ (‎‪1‬ - ‎‪ecm‬)‎‪ - ‎‪‪‪Sales order‬:", ClassName = "AxTopLevelFrame" });
            uitcc_SOLines = wnd_CCHistory.GetAllRecords("Grid");

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String type = row_SOLine.GetColumnValueOfARecord("Type");
                String status = row_SOLine.GetColumnValueOfARecord("Status");

                Assert.AreEqual("Finalization", type, "Mismatch");
                Assert.AreEqual("Approved", status, "Mismatch");
            }
            wnd_CCHistory.SetFocus();
            wnd_CCHistory.CloseWindow();



            #endregion

            #region Test Cleanup
            #endregion

        }

        [TestMethod]
        [TestCategory("SmokeFIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(74768)]
        [Description("Generate vendor file for export")]
        public void GenerateVendorFileForExport()
        {
            #region Test Data

            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string Address_Periodic = "Accounts Payable/Periodic";
            string Address_Inquiries = "Accounts Payable/Inquiries/Manage outbound payments";
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_GVIFFE = new WinWindow();
            WinWindow wnd_IEFS = new WinWindow();
            WinEdit edit_FromDate = new WinEdit();
            WinEdit edit_ToDate = new WinEdit();
            WinEdit edit_PostingProfile = new WinEdit();
            WinButton btn_Ok = new WinButton();
            List<String> list_InfologMessages = new List<String>();
            List<String> Expected_Messages = new List<String>();
            WinRow lst_header = new WinRow();
            WinColumnHeader ch_IEFN = new WinColumnHeader();
            WinWindow edit_IEFN = new WinWindow();
            WinControl btn_recall = new WinControl();
            WinWindow wnd_IEFS_recall = new WinWindow();
            WinWindow win_recall = new WinWindow();
            WinEdit edit_reason = new WinEdit();
            WinButton ok_button = new WinButton();
            WinColumnHeader col_Status = new WinColumnHeader();
            WinRow grd_row = new WinRow();
            String IfhoLogMessages_Path = string.Empty;
            string[] splt = null;
            string filename = string.Empty;


            #endregion

            #region Test Steps

            // 1, AX 2012 Sign on
            // 1.1, Launch AX 2012 environment
            // 1.2, Put a valid username/password and hit enter
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 2, Go to Accounts payable->Periodic->generate vendor invoice file for export
            wnd_AX.NavigateTo(Address_Periodic);
            wnd_AX.SelectFromTreeItems("Generate vendor invoice file for export");
          

            // 3, Select @postingprofile, dates and click ok
            wnd_GVIFFE = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Generate vendor invoice file for export‬ (‎‪1‬)‎", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
        
            edit_PostingProfile = wnd_GVIFFE.DetectControl<WinEdit>(new { Name = "Posting profile" });
            edit_PostingProfile.Text = "PROD";

            edit_FromDate = wnd_GVIFFE.DetectControl<WinEdit>(new { Name = "From date" });
            edit_FromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            edit_ToDate = wnd_GVIFFE.DetectControl<WinEdit>(new { Name = "To date" });
            edit_ToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            btn_Ok = wnd_GVIFFE.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();
            Expected_Messages.Clear();
            Expected_Messages.Add("The following messages are just for your information");
            Expected_Messages.Add("Invoice file export process has completed");
            Expected_Messages.Add(@"File\\Rarcwivdap12154\aif_edi\Outbound\Payments\");
            Expected_Messages.Add("Number of invoices");
            Expected_Messages.Add("Total dollar amount");
            list_InfologMessages.Clear();


            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            if(list_InfologMessages[1].ToString() != "No records found.")
            {
                IfhoLogMessages_Path = list_InfologMessages[2].Replace("created", "").Trim();
                IfhoLogMessages_Path = IfhoLogMessages_Path.Replace("File", "").Trim();
                splt = IfhoLogMessages_Path.Split('\\');
                filename = splt[splt.Length - 1];
                IfhoLogMessages_Path = IfhoLogMessages_Path.Replace(filename, "").Trim();
                Expected_Messages[2] = list_InfologMessages[2].Replace("\\", " ");
                list_InfologMessages[2] = list_InfologMessages[2].Replace("\\", " ");
                for (int i = 0; i < Expected_Messages.Count; i++)
                {
                    bool status = list_InfologMessages[i].Contains(Expected_Messages[i]);
                    Assert.IsTrue(status, "InfoLog Messages are not in correct format");

                }
            }
            else
            {
                Assert.Fail("Must to recall the exported file which is created today OR No sale order created today");
            }
             
            // 4, Navigate to the path mentioned in the infolog (C:\AIF_EDI\outbound\Payments) and verify 
            DirectoryInfo hdDirectoryInWhichToSearch = new DirectoryInfo(IfhoLogMessages_Path);
            FileInfo[] filesInDir = hdDirectoryInWhichToSearch.GetFiles(filename);
            Assert.IsTrue(filesInDir.Count() == 1, "File not Exist");



            // 5, Check the "Vendor Group Posting Profile " in the file exported 

            // 6, Go to accounts payable->Inquiries->Manage outbound payments--> invoice Export file summary 
            wnd_AX.NavigateTo(Address_Inquiries);
            wnd_AX.SelectFromTreeItems("Invoice export file summary log");

            // 7, Verify the "Invoice Export File name" Field in the Overview tab  
            wnd_IEFS = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Invoice export file summary", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
            
           
            lst_header = (wnd_IEFS.DetectControl<WinTable>(new { Name = "Grid" }, PropertyExpressionOperator.Contains)).DetectControl<WinRow>(new { Name = "Top Row" }, PropertyExpressionOperator.Contains);
            
            ch_IEFN = lst_header.DetectControl<WinColumnHeader>(new { Name = "Invoice export file name" }, PropertyExpressionOperator.Contains);
            ch_IEFN.WinClick();
            edit_IEFN = (wnd_IEFS.DetectControl<WinWindow>(new { ClassName = "AxPaneWnd" }, PropertyExpressionOperator.Contains)).DetectControl<WinWindow>(new { ClassName = "AxEdit" }, PropertyExpressionOperator.Contains);
            edit_IEFN.EnterWinText(filename.Replace(".csv",""));
           // edit_IEFN.EnterWinText("104-000841");
            Keyboard.SendKeys("{ENTER}");

            // 8, Select the journal created and Click on Recall file button on the top
          
            wnd_IEFS.SelectARecord("Grid", new Dictionary<string, string>() { { "Invoice export file name", filename.Replace(".csv", "") } }, true, false);
            btn_recall = wnd_IEFS.DetectControl<WinControl>(new { Name = "Recall file" }, PropertyExpressionOperator.Contains);
            btn_recall.WinClick();
            // 9, Enter the reason and click ok
           
            wnd_IEFS_recall = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { ClassName = "AxPopupFrame" }, PropertyExpressionOperator.Contains);
            win_recall = wnd_IEFS_recall.DetectControl<WinWindow>(new { Name = "Recall file‬‬", ClassName = "AxPaneWnd" }, PropertyExpressionOperator.Contains);

            edit_reason = win_recall.DetectControl<WinEdit>(new { Name = "Reason" }, PropertyExpressionOperator.Contains);

            edit_reason.WinClick();
            edit_reason.EnterWinText("Test");
            ok_button = win_recall.DetectControl<WinButton>(new { Name = "OK" }, PropertyExpressionOperator.Contains);

            ok_button.WinClick();

            // 10, Verify the status of the journal created 
            
            grd_row = wnd_IEFS.FetchARecordFromATable("Grid", new Dictionary<string, string>() { { "Invoice export file name", filename.Replace(".csv", "") } });
            col_Status = lst_header.DetectControl<WinColumnHeader>(new { Name = "Invoice export file name" }, PropertyExpressionOperator.Contains);
            String Status = grd_row.GetColumnValueOfARecord("Status");
            Assert.AreEqual(Status, "Canceled", "File didn't recall");
            wnd_IEFS.CloseWindow();
            // 11, Go to Accounts payable->Periodic->generate vendor invoice file for export
            wnd_AX.NavigateTo(Address_Periodic);
            wnd_AX.SelectFromTreeItems("Generate vendor invoice file for export");

            // 12, Repeat step 3 with the same inputs and click ok
            wnd_GVIFFE = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Generate vendor invoice file for export‬ (‎‪1‬)‎", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);

            edit_PostingProfile = wnd_GVIFFE.DetectControl<WinEdit>(new { Name = "Posting profile" });
            edit_PostingProfile.Text = "PROD";

            edit_FromDate = wnd_GVIFFE.DetectControl<WinEdit>(new { Name = "From date" });
            edit_FromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            edit_ToDate = wnd_GVIFFE.DetectControl<WinEdit>(new { Name = "To date" });
            edit_ToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            btn_Ok = wnd_GVIFFE.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();
            Expected_Messages.Clear();
            Expected_Messages.Add("The following messages are just for your information");
            Expected_Messages.Add("Invoice file export process has completed");
            Expected_Messages.Add(@"File\\Rarcwivdap12154\aif_edi\Outbound\Payments\");
            Expected_Messages.Add("Number of invoices");
            Expected_Messages.Add("Total dollar amount");
            list_InfologMessages.Clear();


            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            if (list_InfologMessages[1].ToString() != "No records found.")
            {
                IfhoLogMessages_Path = list_InfologMessages[2].Replace("created", "").Trim();
                IfhoLogMessages_Path = IfhoLogMessages_Path.Replace("File", "").Trim();
                splt = IfhoLogMessages_Path.Split('\\');
                filename = splt[splt.Length - 1];
                IfhoLogMessages_Path = IfhoLogMessages_Path.Replace(filename, "").Trim();
                Expected_Messages[2] = list_InfologMessages[2].Replace("\\", " ");
                list_InfologMessages[2] = list_InfologMessages[2].Replace("\\", " ");
                for (int i = 0; i < Expected_Messages.Count; i++)
                {
                    bool status = list_InfologMessages[i].Contains(Expected_Messages[i]);
                    Assert.IsTrue(status, "InfoLog Messages are not in correct format");

                }
            }
            else
            {
                Assert.Fail("Failed to Recall exported file");
            }
            // 13, Go to the specified path and check for the exported file
            hdDirectoryInWhichToSearch = new DirectoryInfo(IfhoLogMessages_Path);
            filesInDir = hdDirectoryInWhichToSearch.GetFiles(filename);
            Assert.IsTrue(filesInDir.Count() == 1, "File not Exist");


            //14.Select the journal created and Click on Recall file button on the top


            wnd_AX.NavigateTo(Address_Inquiries);
            wnd_AX.SelectFromTreeItems("Invoice export file summary log");
            //filename = "104-000901.csv";

            wnd_IEFS = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Invoice export file summary", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);


            lst_header = (wnd_IEFS.DetectControl<WinTable>(new { Name = "Grid" }, PropertyExpressionOperator.Contains)).DetectControl<WinRow>(new { Name = "Top Row" }, PropertyExpressionOperator.Contains);

            ch_IEFN = lst_header.DetectControl<WinColumnHeader>(new { Name = "Invoice export file name" }, PropertyExpressionOperator.Contains);
            ch_IEFN.WinClick();
            edit_IEFN = (wnd_IEFS.DetectControl<WinWindow>(new { ClassName = "AxPaneWnd" }, PropertyExpressionOperator.Contains)).DetectControl<WinWindow>(new { ClassName = "AxEdit" }, PropertyExpressionOperator.Contains);
            edit_IEFN.EnterWinText(filename.Replace(".csv", ""));
            
            Keyboard.SendKeys("{ENTER}");

            

            wnd_IEFS.SelectARecord("Grid", new Dictionary<string, string>() { { "Invoice export file name", filename.Replace(".csv", "") } }, true, false);
            btn_recall = wnd_IEFS.DetectControl<WinControl>(new { Name = "Recall file" }, PropertyExpressionOperator.Contains);
            btn_recall.WinClick();
            //15.Enter the reason and click ok

            wnd_IEFS_recall = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { ClassName = "AxPopupFrame" }, PropertyExpressionOperator.Contains);
            win_recall = wnd_IEFS_recall.DetectControl<WinWindow>(new { Name = "Recall file‬‬", ClassName = "AxPaneWnd" }, PropertyExpressionOperator.Contains);

            edit_reason = win_recall.DetectControl<WinEdit>(new { Name = "Reason" }, PropertyExpressionOperator.Contains);

            edit_reason.WinClick();
            edit_reason.EnterWinText("Test");
            ok_button = win_recall.DetectControl<WinButton>(new { Name = "OK" }, PropertyExpressionOperator.Contains);

            ok_button.WinClick();

            //16.Verify the status of the journal created

            grd_row = wnd_IEFS.FetchARecordFromATable("Grid", new Dictionary<string, string>() { { "Invoice export file name", filename.Replace(".csv", "") } });
            col_Status = lst_header.DetectControl<WinColumnHeader>(new { Name = "Invoice export file name" }, PropertyExpressionOperator.Contains);
            Status = grd_row.GetColumnValueOfARecord("Status");
            Assert.AreEqual(Status, "Canceled", "File didn't recall");
            wnd_IEFS.CloseWindow();

            #endregion

            #region Test Cleanup

            #endregion
        }

        [TestMethod]
        [TestCategory("SmokeFIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(126595)]
        [Description("Sale Order With Recycle Fee And Recyle Fee Report")]
        public void SaleOrderWithRecycleFeeAndRecyleFeeReport()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Address = "Accounts Receivable/Reports/Ashley Reports";
            
            productCode = "M83X12";
            quantity = "6";
            zipCode = "02908";
            address = "43 NOLAN ST"; 
            city = "PROVIDENCE"; 
            state = "RI";
           
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_Infolog = new WinWindow();
            WinWindow wnd_TSTT = new WinWindow();
            WinWindow wnd_DD = new WinWindow();
            WinWindow wnd_CCHistory = new WinWindow();
            WinWindow wnd_RSR = new WinWindow();
            WinWindow wnd_Misc_Charges = new WinWindow();
            WinWindow wnd_AshleyMisc = new WinWindow();
            WinWindow wnd_Misc_DropDown = new WinWindow();
            WinWindow wnd_Recycle_Report_Form = new WinWindow();
            WinColumnHeader edit_SONumber = new WinColumnHeader();
            WinMenuItem dd_menuItem = new WinMenuItem();
            BrowserWindow browserWindow = new BrowserWindow();
            HtmlRow row_Record = new HtmlRow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            UITestControlCollection uitcc_MiscRecyLines = new UITestControlCollection();
            WinEdit edit_discountPercentage = new WinEdit();
            WinEdit edit_TASTA = new WinEdit();
            WinEdit edit_ads = new WinEdit();
            WinEdit edit_FromDate = new WinEdit();
            WinEdit edit_ToDate = new WinEdit();
            WinEdit edit_salesOption = new WinEdit();
            WinEdit edit_country = new WinEdit();
            WinEdit edit_findOption = new WinEdit();
            WinClient client_state = new WinClient();
            WinClient client_miscCharge = new WinClient();
            WinClient client_retailChannel = new WinClient();
            WinClient client_recycleReport = new WinClient();
            WinButton btn_OK = new WinButton();
            WinButton miscDropwdrown_Btn_OK = new WinButton();
            WinButton btn_Close = new WinButton();
            IWebDriver driver = null;



            #endregion

            #region Test Steps

            #region Pre-requisite
            //1, Place an HD & DS item Order in Site core using Ashley Advantage Card
            CreateASaleOrderInSitecoreFIN();

            #endregion

            string PurchaseOrder = saleOrder + 1.ToString("00");

            // 2, Log in to AX 
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 2.1, Navigate to Accounts Receivable -> All sales Order 
            wnd_AX.NavigateTo(AddressPath);

            // 2.2, Click on the above created SO 
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            // 2.3, Wait till PO is auto generated for the SO
            // Enusre that PO is created for the Sales Order      
            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            // 2.4, Select the lines in Sale order lines grid
            // 2.5, Verify the Line Stage and Line Status of the lines
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Unscheduled", lineStage, PurchaseOrder + "PO not generated");
                Assert.AreEqual("Open order", lineStatus, PurchaseOrder + "PO not generated");
            }

            // 3, Click Misc Charges in action pane
            wnd_SO.ClickRibbonMenuItem("afmMain", "View", "Misc. charges");

            // 3.1,  Check there are entries in Misc Cahrges Form
            wnd_Misc_Charges = UITestControl.Desktop.FindWinWindow(new { Name = "‪Misc", ClassName = "AxTopLevelFrame" },PropertyExpressionOperator.Contains);
            wnd_Misc_Charges = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = "Misc", ClassName = "AxTopLevelFrame" },PropertyExpressionOperator.Contains);
            uitcc_MiscRecyLines = wnd_Misc_Charges.GetAllRecords("Grid");
            foreach (WinRow row_MiscLine in uitcc_MiscRecyLines)
            {
                String salesOrderQty = row_MiscLine.GetColumnValueOfARecord("Sales order qty");
                String unitCharge = row_MiscLine.GetColumnValueOfARecord("Unit charge");
                String openChargeAmount = row_MiscLine.GetColumnValueOfARecord("Open charge amount");
                Double totalOpenChargeAmount = Convert.ToDouble(salesOrderQty) * Convert.ToDouble(unitCharge);

                Assert.AreEqual(Convert.ToDouble(openChargeAmount), totalOpenChargeAmount, "Recycling Fee Calculation is Wrong");
            }
            wnd_Misc_Charges.CloseWindow();
            // 4, Invoice the HD Order via Portal	
            // 5, Invoice the HD lines via Portal
            // 6, Go to PTL application
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            // 7 Invoice the HD Purchase Order(s) via Homestore Portal (PTL)
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            // 7.1, Click on the Scheduled cue icon

            // 7.2, Search and click on the purchase order
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 7.3, Click Invoice Process => New Vendor Invoice => Post Invoice
            driver.Invoice();
            driver.Close();

            // 7.4, Go to AX application - Sale Order form
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            // 7.5, Select the lines in Sale order lines grid
            // 7.6, Verify the Line Stage and Line Status of the lines
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");

            // 7.7, Verify the Line Stage and Line Status of the lines
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual("Invoiced", lineStage, PurchaseOrder + " not Invoiced.");
                Assert.AreEqual("Invoiced", lineStatus, PurchaseOrder + " not Invoiced.");
            }

            // 8, Go to Account Receivable -> Reports -> Ashley Reports -> Ashley miscellaneous charges audit report

            wnd_AX.NavigateTo(Address);
            wnd_AX.SelectFromTreeItems("Ashley miscellaneous charges audit report");

           // 9, Fill the Mandatory Fields and Click Ok
            wnd_AshleyMisc = UITestControl.Desktop.FindWinWindow(new { Name = "‪Ashley", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);

            edit_FromDate = wnd_AshleyMisc.DetectControl<WinEdit>(new { Name = "From date" });
            edit_FromDate.Text = DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy");

            edit_ToDate = wnd_AshleyMisc.DetectControl<WinEdit>(new { Name = "To date" });
            edit_ToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            edit_salesOption = wnd_AshleyMisc.DetectControl<WinEdit>(new { Name = "Sales option" });
            edit_salesOption.Text = "Invoiced";

            edit_country = wnd_AshleyMisc.DetectControl<WinEdit>(new { Name = "Country/region" });
            edit_country.Text = "USA";

            client_state = (wnd_AshleyMisc.DetectControl<WinEdit>(new { Name = "State" })).DetectControl<WinClient>(new { Name = "State lookup button" });
            client_state.WinClick();

            wnd_Misc_DropDown = UITestControl.Desktop.FindWinWindow(new { ClassName = "AxPopupFrame" }, PropertyExpressionOperator.Contains);
            wnd_Misc_DropDown.SelectARecord("Grid", new Dictionary<string, string>() { { "State", "RI" } }, true, false);
            miscDropwdrown_Btn_OK = wnd_Misc_DropDown.DetectControl<WinButton>(new { Name = "OK" });
            miscDropwdrown_Btn_OK.WinClick();

            client_miscCharge = (wnd_AshleyMisc.DetectControl<WinEdit>(new { Name = "Misc Charge" })).DetectControl<WinClient>(new { Name = "Misc Charge lookup button" });
            client_miscCharge.WinClick();

            wnd_Misc_DropDown = UITestControl.Desktop.FindWinWindow(new { ClassName = "AxPopupFrame" }, PropertyExpressionOperator.Contains);
            wnd_Misc_DropDown.SelectARecord("Grid", new Dictionary<string, string>() { { "Charges code", "RecycleFee" } }, true, false);
            miscDropwdrown_Btn_OK = wnd_Misc_DropDown.DetectControl<WinButton>(new { Name = "OK" });
            miscDropwdrown_Btn_OK.WinClick();

            client_retailChannel = (wnd_AshleyMisc.DetectControl<WinEdit>(new { Name = "Retail channel" })).DetectControl<WinClient>(new { Name = "Retail channel lookup button" });
            client_retailChannel.WinClick();

            wnd_Misc_DropDown = UITestControl.Desktop.FindWinWindow(new { ClassName = "AxPopupFrame" }, PropertyExpressionOperator.Contains);
            wnd_Misc_DropDown.SelectARecord("Grid", new Dictionary<string, string>() { { "Operating unit number", "7250" } }, true, false);
            miscDropwdrown_Btn_OK = wnd_Misc_DropDown.DetectControl<WinButton>(new { Name = "OK" });
            miscDropwdrown_Btn_OK.WinClick();

            btn_OK = wnd_AshleyMisc.DetectControl<WinButton>(new { Name = "OK" });
            btn_OK.WinClick();

            wnd_Recycle_Report_Form = UITestControl.Desktop.FindWinWindow(new { Name = "‪Ashley miscellaneous charges audit report‬ (‎‪1‬)‎", ClassName = "AxTopLevelFrame" });
            client_recycleReport = wnd_Recycle_Report_Form.DetectControl<WinClient>(new { Name = "Report Area" });


            #endregion

            #region Test Cleanup

            #endregion

        }


        #region Additional test attributes

        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}