﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Ashley.QA.Automation.Framework.Web;
using OpenQA.Selenium;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using System.Data;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.SmokeTests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class Smoke_PTL
    {    
        public Smoke_PTL()
        {
        }

        #region Global Variables
        
        public static string saleOrder;
        public static string productCode;
        public static string cardType;

        #endregion

        #region Sitecore order creation 
        
        public void CreateASaleOrderInSitecorePTL()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode ))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");
                string zipCode = row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                string address = row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                string city = row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                string state = row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                cardType = String.IsNullOrEmpty(cardType) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation
                
                if (string.IsNullOrEmpty(saleOrder))
                {
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                }
                
                #endregion
            }

        }


        #endregion


        [TestMethod]
        [TestCategory("SmokePTL")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91869)]
        [Description("PTL Smoke: Verify the PO Confirmation, Invoicing, Queue Movement")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91869", DataAccessMethod.Sequential)]
        public void PTLSmoke_VerifyThePOConfirmation_Invoicing_QueueMovement()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
         
            productCode = "Sofas";
            string quantity = TestContext.DataRow["Quantity"].ToString();
           
            string PurchaseOrder = string.Empty;

            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            IWebDriver driver = null;
            #endregion

            #region Test Steps

            #region Pre-requisite
            // 1, Launch Ashley SiteCore in the browser
            // 2, Select the HD @Product from @Category and @Subcategory
            // 3, Click on any one of the Product
            // 4, Enter required @Quantity and Click Add to Cart
            // 5, Click on Checkout
            // 6, Click on 'Proceed to checkout' 
            // 7, Click on Continue as Guest
            // 8, Enter @Firstname, @LastName, @Country /region, @Address, @City, @State /Province, @Zip /Postalcode ,@Phonenumber , @Email address and click on continue 
            // 9, Mark the checkbox same as shipping address 
            // 10, Click on continue 
            // 11, Enter the @Card (Visa/Master/Amex)number, @CVV, @ExpiryMonth @ExpiryYear ,first name and last name and click continue 
            // 12, Click on 'Submit order' 
            CreateASaleOrderInSitecorePTL();
           // saleOrder = "725000143095";
           PurchaseOrder = saleOrder + 1.ToString("00");
            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);            
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            // 13, Launch AX Vendor Portal @InternalURL,@ExternalURL application using the PTL User Role
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            // 14, Click on the New Cue 
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            // 15, Click on the Corresponding PO Number 
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");      
        
          // 16, Verify whether the user is allowed to enter ERP number and save the changes to the PO 
            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumber(ERPNumber, "Purchase order", "Homestore data");


            // 17, Open the unscheduled cue and select the same PO and verify the ERP date entry date 
            // 18, Select the Confirmed delivery date from the calendar option 
            // 19, Click on the Apply to lines button         
            driver.SelectFromRibbon("Purchase order", "Stages", "Unscheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
          //  driver.ClickACellinTable("Purchase order", PurchaseOrder);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            // 20, Verify whether Confirmed delivery date is updated under Lines and ASN details fast tab 
            // 21, Click on the save and close button
            // 22, Open the same PO again from the scheduled cue            
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");          
            
            List<bool> coll = driver.VerifyTheTableValues("Purchase order lines ", "confirmed delivery date");
            CollectionAssert.DoesNotContain(coll, false,"confirm delivery date mismatch");
            coll = driver.VerifyTheTableValues("ASN Details ", "confirmed delivery date");
            CollectionAssert.DoesNotContain(coll, false, "confirm delivery date mismatch");

            // 23, Click on the new vendor invoice under Invoice process
            // 24, click on the Post invoice button 
            driver.Invoice();

            // 25, Go to the Invoiced cue 
            // 26, Verify the Line Stage in the Purchase Order Line Fast tab
            driver.SelectFromRibbon("Purchase order", "Stages", "Invoiced");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);            
            driver.ClickACellinTable("Purchase order", PurchaseOrder);
            coll = driver.VerifyTheTableValues("Purchase order lines ", "line stage");
            CollectionAssert.DoesNotContain(coll, false, "line stage mismatch");

            #endregion

            #region Test Cleanup
            #endregion

        }

        #region Additional test attributes

        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}