﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Ashley.QA.Automation.Framework.Web;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using System.Data;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.RegressionTests
{
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    public class Reg_CCF
    {
        public Reg_CCF()
        {
        }

        public static string saleOrder;
        public string productCode;
        public string cardType;
        public string cardNo;
        public string cvv;
        public string firstName;
        public string lastName;
        public string address;
        public string city;
        public string state;
        public string zipCode;

        #region Sitecore order creation 
        
        public void CreateASaleOrderInSitecoreCCFReg()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode ))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");
                zipCode = row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                address = row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                city = row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                state = row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                cardType = cardType == string.Empty ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation

                if (string.IsNullOrEmpty(saleOrder))
                {
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                }

                #endregion
            }          

        }


        #endregion

        [TestMethod]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(114114)]
        [Description("Address Change results in Fulfiller change")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "114114", DataAccessMethod.Sequential)]
        public void AddressChangeResultsInFulfillerChange()
        {
            #region TestData

            string SaleOrder = String.Empty;
            string PurchaseOrder = String.Empty;

            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            
            string f_Address1 = "10301 San Francisco Drive";
            string f_State = "NM";
            string f_County = "Bernalillo";
            string f_City = "Albuquerque";
            string f_ZIPPostalCode = "87122";

            #endregion

            #region Local Variables

            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinWindow wnd_AddressMaintenance = new WinWindow();
            WinClient client_FastTab = new WinClient();
            WinButton btn_ApplyAndClose = new WinButton();

            Dictionary<String, String> dict_ColumnAndValues = new Dictionary<String, String>();
            List<String> list_InfologMessages = new List<String>();
            List<String> list_LineAndItemNumber = new List<String>();

            #endregion

            #region Pre-requisite

            //  1, Create Sales Order
            CreateASaleOrderInSitecoreCCFReg();

            #endregion

            #region Test Steps

            //  2, Sign-in to AX
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            //  3, Open the Sales Order
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(SaleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", SaleOrder } }, true, true);

            //  4, Click Sales order tab
            //  Click on Address maintenance => Address maintenance in action pane
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                int lineNumber = uitcc_SOLines.IndexOf(row_SOLine) + 1;
                String itemNumber = row_SOLine.GetColumnValueOfARecord("Item number");
                list_LineAndItemNumber.Add("Line number : '" + lineNumber + ".00' , Item Id : '" + itemNumber + "'");
            }
            wnd_SO.ClickRibbonMenuItem("Main", "Address maintenance group", "Address maintenance");

            //  5, Modify the delivery address to another address with different fulfiller
            //  Select the delivery line in Address tab and select the sales order in Orders tab
            //  Click Apply and close
            wnd_AddressMaintenance = UITestControl.Desktop.FindWinWindow(new { Name = "‪Address maintenance:", ClassName = "AxTopLevelFrame" });
            dict_ColumnAndValues = new Dictionary<string, string>();
            dict_ColumnAndValues.Add("Purpose", "Delivery");
            wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "Address 1", f_Address1);
            wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "State", f_State);
            wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "County", f_County);
            wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "City", f_City);
            wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "ZIP/postal code", f_ZIPPostalCode);

            client_FastTab = wnd_AddressMaintenance.ExpandFastTab("Orders", true);
            wnd_AddressMaintenance.CheckInsideACell("GridSalesOrder", new Dictionary<string, string>() { { "Sales order", SaleOrder } }, "Mark", true);

            btn_ApplyAndClose = wnd_AddressMaintenance.DetectControl<WinButton>(new { Name = "Apply and Close" });
            btn_ApplyAndClose.WinClick();

            //  6, Verify the infolog
            //  7, Click Close in Infolog
            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

            foreach (String lineAndItem in list_LineAndItemNumber)
            {
                List<String> list_FAndW = list_InfologMessages.FindAll(x => x.StartsWith(lineAndItem)).ToList();
                if (list_FAndW.Count == 2)
                {
                    Assert.AreNotEqual(list_FAndW.Extract("Current fulfiller"), list_FAndW.Extract("Suggested fulfiller"), "Fulfiller change hasn't occured for " + lineAndItem + ".\n Useful Information:\n" + String.Join("\n", list_InfologMessages));
                    Assert.AreNotEqual(list_FAndW.Extract("Current warehouse"), list_FAndW.Extract("Suggested warehouse"), "Warehouse change hasn't occured for " + lineAndItem + ".\n Useful Information:\n" + String.Join("\n", list_InfologMessages));
                }
                else
                {
                    Assert.Fail("Fulfiller and/or Warehouse change hasn't occured for " + lineAndItem + ".\n Useful Information:\n" + String.Join("\n", list_InfologMessages));
                }
            }

            #endregion

            #region Test Cleanup

            #endregion
        }

        [TestMethod]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(103115)]
        [Description("When adding new line to a SO auto-populate-->FR 6213 Item price;FR 6201,FR 6207 Delivery mode;FR 6209 Revenue share financial dimension;FR 6210 warehouse;FR 6208 vendor/fulfiller;Automatically adjust existing non-invoiced delivery charge for SO adjustment")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "103115", DataAccessMethod.Sequential)]
        public void WhenAddingNewLineToASOAutoPopulate()
        {
            #region Test Data

            string SaleOrder = TestContext.DataRow["SaleOrder"].ToString();
            string PurchaseOrder = TestContext.DataRow["PurchaseOrder"].ToString();
            string ItemNumber = "2880039";//TestContext.DataRow["Itemnumber"].ToString();
            string OrigQty = "7"; //TestContext.DataRow["Origqty"].ToString();

            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

          
            string ashleyFinancingOption = AX.Automation.Properties.Settings.Default.AshleyFinancingOption;

            #endregion

            #region Local Variables

            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinWindow wnd_PO = new WinWindow();
            WinClient client_FastTab = new WinClient();
            WinWindow wnd_NCCC = new WinWindow();
            WinButton btn_CompleteRegistration = new WinButton();
            WinRow row_SO = new WinRow();

            List<String> list_InfologMessages = new List<String>();
            List<String> list_LineConfirmationID = new List<String>();
            Dictionary<String, String> dict_ColumnAndValues = new Dictionary<String, String>();
            String message = String.Empty;
            String paymentConfirmation = String.Empty;
            String lineViewWarehouseNumber = String.Empty;
            String headerViewWarehouseNumber = String.Empty;
            Double d_NetAmount_BeforeAdding = 0;
            Double d_NetAmount_AfterAdding = 0;

            #endregion
            
            #region Pre-requisite

            //  1, Create Sales Order
            CreateASaleOrderInSitecoreCCFReg();

            #endregion

            #region Test Steps

            //  2, Sign-in to AX
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            //  3, Open the Sales Order
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(SaleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", SaleOrder } }, true, true);

            //  4, Verify if ERP number column is present in the Sales order line grid
            //  Try to edit ERP number field in Sales order lines grid
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("Sales order", "Maintain", "Modify");
            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

            row_SO = wnd_SO.FetchARecordFromATable("SalesLineGrid", new Dictionary<string, string>() { { "Line stage", "Delivery charge" } });
            Assert.IsNotNull(row_SO, "Please add atleast one HD item.");
            d_NetAmount_BeforeAdding = Convert.ToDouble(row_SO.GetColumnValueOfARecord("Net amount"));

            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                list_LineConfirmationID.Add(row_SOLine.GetColumnValueOfARecord("Line confirmation ID"));
                WinCell cell_ERPNumber = row_SOLine.GetCellOfARecord("ERP number");
                cell_ERPNumber.WinClick();
                Assert.IsFalse(cell_ERPNumber.DetectControl<WinEdit>(new { Name = "ERP number" }).Exists, "ERP number column is editable.");
            }
            wnd_SO.ClickRibbonMenuItem("Sales order", "Maintain", "Complete");
            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");
            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);

            //  5, Open the Purchase Order form of the SO
            //  Go to General tab and click on References
            //  Right click on the Purchase Order number in grid and click View details
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            wnd_SO.ClickRibbonMenuItem("General", "Related information", "Purchase order");
            wnd_PO = UITestControl.Desktop.FindWinWindow(new { Name = "Purchase order:", ClassName = "AxTopLevelFrame" });

            //  6, Click on Header view in the action pane
            wnd_PO.ClickRibbonMenuItem("Purchase order", "Show", "Header view");

            //  7, Go to general fasttab and verify if the field ERP number is present in the References section
            client_FastTab = wnd_PO.ExpandFastTab("General", true);
            Assert.IsTrue(client_FastTab.DetectControl<WinEdit>(new { Name = "ERP number" }).Exists, "ERP number is not available in the References Section of General Fast Tab.");

            //  8, Click Add line button in Sales order grid header
            //  Enter @Itemnumber in the Item number field
            //  Update @Origqty in the Orig Qty field
            wnd_SO.ClickRibbonMenuItem("Sales order", "Maintain", "Modify");
            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");
            wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Add line", null);

            dict_ColumnAndValues = new Dictionary<string, string>();
            dict_ColumnAndValues.Add("Line confirmation ID", "(null)");
            wnd_SO.EnterValueToCell("SalesLineGrid", dict_ColumnAndValues, "Item number", ItemNumber);
            Keyboard.SendKeys("{TAB}");
            wnd_SO.EnterValueToCell("SalesLineGrid", dict_ColumnAndValues, "Orig qty", OrigQty);

            //  9, Select the newly added line 
            wnd_SO.SelectARecord("SalesLineGrid", dict_ColumnAndValues, true, false);

            //  10,Click Remove button
            //  Click Yes in confirmation
            wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Remove", null);
            DynamicsAxCommonFunctions.HandleMessageBox("Confirm deletion", "Yes");
            Assert.IsNull(wnd_SO.FetchARecordFromATable("SalesLineGrid", dict_ColumnAndValues), "Newly added line is not removed.");

            //  11, Click Add line button in Sales order grid header
            //  Enter @Itemnumber in the Item number field
            //  Update @Origqty in the Orig Qty field
            wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Add line", null);
            Assert.IsNotNull(wnd_SO.FetchARecordFromATable("SalesLineGrid", dict_ColumnAndValues), "Empty Line is not added.");
            wnd_SO.EnterValueToCell("SalesLineGrid", dict_ColumnAndValues, "Item number", ItemNumber);
            Keyboard.SendKeys("{TAB}");
            wnd_SO.EnterValueToCell("SalesLineGrid", dict_ColumnAndValues, "Orig qty", OrigQty);

            //  12, Click Complete button in action pane
            wnd_SO.ClickRibbonMenuItem("Sales order", "Maintain", "Complete");
            message = DynamicsAxCommonFunctions.GetMessageBoxMessage("Microsoft Dynamics");
            Assert.IsTrue(message.StartsWith("Changes to the sales order has resulted in adjustment to delivery charge 'Home Delivery'."), "Changes to the sales order has resulted in adjustment to delivery charge 'Home Delivery' is not displayed in the Messagebox.\nUseful Information:\n" + message);
            DynamicsAxCommonFunctions.HandleMessageBox("Microsoft Dynamics", "Yes");

            //  15, Verify Info log
            message = DynamicsAxCommonFunctions.GetMessageBoxMessage("Microsoft Dynamics");
            Assert.IsTrue(message.StartsWith("New Charges to card : "), "New Charges to card is not displayed in the Messagebox.\nUseful Information:\n" + message);

            //  16, Click OK
            DynamicsAxCommonFunctions.HandleMessageBox("Microsoft Dynamics", "OK");

            //  17, Fill the below fields with correct values in the Payment page (Fill all the details of another new credit card) @CCNumber1, @Expires, @SecurityCode, @FirstNameonCard, @LastNameonCard, @Address1, @City, @State, @ZIPCode
            //  18, Click on the Continue button in the Payment page
            //  Close the Payment page
            paymentConfirmation = PortalCommonFunctions.FillPaymentInformation(cardNo, cvv, firstName, lastName, address, city, state, zipCode);
            Assert.IsTrue(paymentConfirmation.StartsWith("Credit Card has been entered successfully"), "'Credit Card has been entered successfully' is not displayed in the Webpage.\nUseful Information:\n" + paymentConfirmation);

            //  19, Click on the Complete Registration button located in the New customer credit card window
            //  20, Click on the Close button located in the Infolog pop up window
            wnd_NCCC = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = "New customer credit card" });
            wnd_NCCC.SetFocus();
            btn_CompleteRegistration = wnd_NCCC.DetectControl<WinWindow>(new { Name = "Complete Registration" }).DetectControl<WinButton>(new { Name = "Complete Registration" });
            btn_CompleteRegistration.WinClick();
            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

            //  21, Check the following field values for newly added line in grid
            //  Line confirmation ID
            //  Financial dimension using the path
            //  Line detials (fast tab) --> Financial dimensions(tab)-- > Revenue_Share_Account_ShipTo : ***********
            list_LineConfirmationID = list_LineConfirmationID.Distinct().ToList();
            dict_ColumnAndValues = new Dictionary<string, string>();
            dict_ColumnAndValues.Add("Line confirmation ID", SaleOrder + (list_LineConfirmationID.Count + 1).ToString("00"));
            row_SO = wnd_SO.FetchARecordFromATable("SalesLineGrid", dict_ColumnAndValues);
            Assert.AreNotEqual("(null)", row_SO.GetColumnValueOfARecord("Line confirmation ID"), "Line confirmation ID is not generated for the newly added line.");
            row_SO.SelectARecord(true, false);
            client_FastTab = wnd_SO.ExpandFastTab("Line details", true);
            client_FastTab.SelectTabInFastTab("Financial dimensions");
            Assert.IsTrue(!String.IsNullOrEmpty(client_FastTab.DetectControl<WinEdit>(new { Name = "Revenue_Share_Account_ShipTo" }).Text), "Revenue_Share_Account_ShipTo is not updated in the Financial dimensions Section of Line details Fast Tab.");

            //  22, Validate the warehouse number at header level and item level following this path:
            //  Line level:
            //  Click Line view-- > Line details-- > Product-- > Under the storage dimensions - Warehouse : (for ex : 7250) should be populated
            //  and
            //  Header level:
            //  Go to Sales order tab-- > Click on Header view-- > under heading Storage dimensions - Warehouse : (for ex: 7250)
            client_FastTab = wnd_SO.ExpandFastTab("Line details", true);
            client_FastTab.SelectTabInFastTab("Product");
            lineViewWarehouseNumber = client_FastTab.DetectControl<WinEdit>(new { Name = "Warehouse" }).Text;
            wnd_SO.ClickRibbonMenuItem("Sales order", "Show", "Header view");
            client_FastTab = wnd_SO.ExpandFastTab("General", true);
            headerViewWarehouseNumber = client_FastTab.DetectControl<WinEdit>(new { Name = "Warehouse" }).Text;
            Assert.AreEqual(lineViewWarehouseNumber, headerViewWarehouseNumber, "Line View and Header View Warehouse numbers doesn't match for the newly added line in Sales order.");

            //  23, Verify the Net amount field value for the delivery charge
            wnd_SO.ClickRibbonMenuItem("Sales order", "Show", "Line view");
            row_SO = wnd_SO.FetchARecordFromATable("SalesLineGrid", new Dictionary<string, string>() { { "Line stage", "Delivery charge" } });
            d_NetAmount_AfterAdding = Convert.ToDouble(row_SO.GetColumnValueOfARecord("Net amount"));
            Assert.IsTrue((d_NetAmount_BeforeAdding < d_NetAmount_AfterAdding), "There is no increase in Net Amount for Delivery Line after adding a new line to SO.");

            #endregion

            #region Test Cleanup

            #endregion
        }

        #region Additional test attributes

        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
