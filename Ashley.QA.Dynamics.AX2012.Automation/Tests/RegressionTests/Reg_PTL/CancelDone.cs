﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.Data;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using OpenQA.Selenium;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Automation.Web.Framework.Selenium;
using System.Globalization;
using System.Diagnostics;


namespace Ashley.QA.Dynamics.AX.Automation.Tests.RegressionTests.Reg_PTL
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class CancelDone
    {
        public CancelDone()
        {
        }

        #region Global Variables

        public static string saleOrder;
        public string productCode;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;


        #endregion

        #region Sitecore Order Creation
        public void CreateASaleOrderInSitecorePTLReg()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");
                string zipCode = row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                string address = row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                string city = row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                string state = row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                cardType = String.IsNullOrEmpty(cardType) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation
                IsInvoiceNeeded = false;
                //saleOrder = "725000144384";
                if (string.IsNullOrEmpty(saleOrder))
                {
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                    IsInvoiceNeeded = true;
                }
                #endregion
            }
        }
        #endregion


        [TestMethod]
        [WorkItem(103005)]
        [Description("PTL Lines Fast Tab - Add a checkbox to allow for removal of Canceled Orders from the Cancel cue in Portal")]
        public void PTLLinesFastTab_AddaCheckboxtoAllowForRemovalOfCanceledOrdersFromTheCancelCueInPortal()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Path_AccountsPayable = "ECM/Accounts payable/Common/Purchase orders/All purchase orders";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            IWebDriver driver = null;
            string DeliverRemainderQty = "1";
            string PartialCancelReasonCode = "Customer Expectation";
            string ERPNumber = string.Empty;
            List<string> list_output = new List<string>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");            

            List<string> ColumnList = driver.GetColumnValues("dynGridViewTable");
            for(int i=0; i<ColumnList.Count;i++)
            {
                if(ColumnList[i] == "Modified date and time")
                {
                    Assert.AreEqual(ColumnList[i + 1], "Cancel done", "'Cancel done' is not next to 'Modified Date and Time' column");
                }
            }

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Cancel done:", "false");
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Purchase order", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Value Mismatch : \n" + string.Join("\n", list_output));
            }
            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Line stage|Unscheduled", "Cancel done|disabled");
            dic_output = driver.VerifyPOHeaderValues("Purchase order lines", "", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Cancel done Check box is Enabled : \n" + string.Join("\n", list_output));
            }
            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data", true, true);

            driver.SelectFromRibbon("Purchase order", "Stages", "Unscheduled");
            driver.Quit();      
            wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {

                row_SOLine.SelectARecord(true, false);

                wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Update line", "Deliver remainder");

                wnd_URQ = UITestControl.Desktop.FindWinWindow(new { Name = "Update remaining quantity:", ClassName = "AxTopLevelFrame" });
                edit_DeliverRemainder = wnd_URQ.DetectControl<WinEdit>(new { Name = "Deliver remainder" });
                edit_DeliverRemainder.EnterWinTextV2(DeliverRemainderQty);
                wnd_URQ.SelectValueFromCustomDropdown("Reason", "Description", PartialCancelReasonCode);
                btn_Ok = wnd_URQ.DetectControl<WinButton>(new { Name = "OK" });
                btn_Ok.WinClick();
            }
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "Canceled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.WaitForLoadingInformation();
            driver.WaitForReady();

            IWebElement POLines = driver.ExpandFastTab("Purchase order lines");
            IWebElement AckCancel = POLines.FindElement(By.LinkText("Acknowledge cancellation"));
            AckCancel.MouseClick();            
            driver = driver.SwitchTo().DefaultContent();
            driver.WaitForLoadingInformation();
            driver.WaitForReady();
            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Line stage|Multiple", "Cancel done|enabled");
            dic_output = driver.VerifyPOHeaderValues("Purchase order lines", "", dic_POHeaderInputValues,true);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Cancel done Check box is Disabled : \n" + string.Join("\n", list_output));
            }

            bool verifyedFlag = driver.VerifyPOAvailabilityInCueList("Canceled", PurchaseOrder);
            Assert.IsFalse(verifyedFlag, "Cancel Done Po is not disappered from Canceled Cue");

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(Path_AccountsPayable);
            wnd_AX.ApplyFilter("Purchase order", PurchaseOrder);

            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Purchase order", PurchaseOrder } }, true, true);
            WinWindow wnd_PO = UITestControl.Desktop.FindWinWindow(new { Name = "‪Purchase order:‬‬‬‬", ClassName = "AxTopLevelFrame" });
            //String ConfirmDate = wnd_PO.GetFastTabValueTabDetails("Line details", "Line details", "afmDeliveryDetailsGrid", "Confirmed delivery date", ControlType.CheckBox);
            string checkboxValue = wnd_PO.GetSpecificColumnValue("LineSpec", "Cancel done",0);
            Assert.AreEqual("Yes", checkboxValue, "CheckBox is not checked");

        }

        [TestMethod]
        [WorkItem(103003)]
        [Description("PTL Header View - Add a checkbox to allow for removal of Canceled Orders from the Cancel cue in Portal")]
        public void PTLHeaderViewAddaCheckboxToAllowForRemovalOfCanceledOrdersFromTheCancelCueInPortal()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Path_AccountsPayable = "ECM/Accounts payable/Common/Purchase orders/All purchase orders";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;            
            // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            IWebDriver driver = null;          
            string ERPNumber = string.Empty;
            string HeaderLevelCancelReasonCode = "Customer Expectation";
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            List<string> ColumnList = driver.GetColumnValues("dynGridViewTable");
            for (int i = 0; i < ColumnList.Count; i++)
            {
                if (ColumnList[i] == "Modified date and time")
                {
                    Assert.AreEqual(ColumnList[i + 1], "Cancel done", "'Cancel done' is not next to 'Modified Date and Time' column");
                }
            }

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Cancel done:", "false");
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Purchase order", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Value Mismatch : \n" + string.Join("\n", list_output));
            }
            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Line stage|Unscheduled", "Cancel done|disabled");
            dic_output = driver.VerifyPOHeaderValues("Purchase order lines", "", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Cancel done Check box is Enabled : \n" + string.Join("\n", list_output));
            }
            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data", true, true);

            driver.SelectFromRibbon("Purchase order", "Stages", "Unscheduled");
            driver.Quit();
            wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));

            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");
           
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Cancel");

            wnd_PopUp = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Are you sure you want to cancel selected sales orders?‬ (‎‪1‬)", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
            wnd_PopUp.SelectValueFromCustomDropdown("Reason", "Description", HeaderLevelCancelReasonCode);
            btn_Yes = wnd_PopUp.DetectControl<WinButton>(new { Name = "Yes" });
            btn_Yes.WinClick();

            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));


            //  5.4, Click on Complete button
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "Canceled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.WaitForLoadingInformation();
            driver.WaitForReady();

            driver.SelectFromRibbon("Purchase order", "Acknowledge", "Acknowledge cancellation");
            driver.WaitForLoadingInformation();
            driver.WaitForReady();

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Line stage|Canceled", "Cancel done|enabled");
            dic_output = driver.VerifyPOHeaderValues("Purchase order lines", "", dic_POHeaderInputValues, true);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Cancel done Check box is Disabled : \n" + string.Join("\n", list_output));
            }

            bool verifyedFlag = driver.VerifyPOAvailabilityInCueList("Canceled", PurchaseOrder);
            Assert.IsFalse(verifyedFlag, "Cancel Done Po is not disappered from Canceled Cue");

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(Path_AccountsPayable);
            wnd_AX.ApplyFilter("Purchase order", PurchaseOrder);

            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Purchase order", PurchaseOrder } }, true, true);
            WinWindow wnd_PO = UITestControl.Desktop.FindWinWindow(new { Name = "‪Purchase order:‬‬‬‬", ClassName = "AxTopLevelFrame" });
            //String ConfirmDate = wnd_PO.GetFastTabValueTabDetails("Line details", "Line details", "afmDeliveryDetailsGrid", "Confirmed delivery date", ControlType.CheckBox);
            string checkboxValue = wnd_PO.GetSpecificColumnValue("LineSpec", "Cancel done", 0);
            Assert.AreEqual("Yes", checkboxValue, "CheckBox is not checked");

        }

        [TestMethod]
        [WorkItem(102940)]
        [Description("AX Lines Fast Tab - Add a checkbox to allow for removal of Canceled Orders from the Cancel cue in Portal")]
        public void AXLinesFastTabAddACheckboxToAllowForRemovalOfCanceledOrdersFromTheCancelCueInPortal()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Path_AccountsPayable = "ECM/Accounts payable/Common/Purchase orders/All purchase orders";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            WinWindow wnd_PO = new WinWindow();
            WinRow row_SO = new WinRow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            UITestControlCollection uitcc_POLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinButton btn_Close = new WinButton();         
            IWebDriver driver = null;
            string DeliverRemainderQty = "1";
            string PartialCancelReasonCode = "Customer Expectation";
            //string LineNumber = "10";
            string ERPNumber = string.Empty;
            List<String> list_InfologMessages = new List<String>();
            List<string> list_output = new List<string>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            Dictionary<String, String> dict_ColumnAndValues = new Dictionary<String, String>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            List<string> ColumnList = driver.GetColumnValues("dynGridViewTable");
            for (int i = 0; i < ColumnList.Count; i++)
            {
                if (ColumnList[i] == "Modified date and time")
                {
                    Assert.AreEqual(ColumnList[i + 1], "Cancel done", "'Cancel done' is not next to 'Modified Date and Time' column");
                }
            }

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Cancel done:", "false");
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Purchase order", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Value Mismatch : \n" + string.Join("\n", list_output));
            }
            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Line stage|Unscheduled", "Cancel done|disabled");
            dic_output = driver.VerifyPOHeaderValues("Purchase order lines", "", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Cancel done Check box is Enabled : \n" + string.Join("\n", list_output));
            }
            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data", true, true);

            driver.SelectFromRibbon("Purchase order", "Stages", "Unscheduled");
            driver.Quit();
            wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {

                row_SOLine.SelectARecord(true, false);
                wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Update line", "Deliver remainder");
                wnd_URQ = UITestControl.Desktop.FindWinWindow(new { Name = "Update remaining quantity:", ClassName = "AxTopLevelFrame" });
                edit_DeliverRemainder = wnd_URQ.DetectControl<WinEdit>(new { Name = "Deliver remainder" });
                edit_DeliverRemainder.EnterWinTextV2(DeliverRemainderQty);
                wnd_URQ.SelectValueFromCustomDropdown("Reason", "Description", PartialCancelReasonCode);
                btn_Ok = wnd_URQ.DetectControl<WinButton>(new { Name = "OK" });
                btn_Ok.WinClick();
            }
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);

            wnd_SO.ClickRibbonMenuItem("General", "Related information", "Purchase order");

            wnd_PO = UITestControl.Desktop.FindWinWindow(new { Name = "Purchase order:", ClassName = "AxTopLevelFrame" });
            wnd_PO.SetFocus();

            //  6, Click on Header view in the action pane
            wnd_PO.ClickRibbonMenuItem("PurchOrder", "Maintain", "Edit");

            wnd_PO.ClickCheckInsideACell("LineSpec", "Line number", "10", "Cancel done",false,true);
            string PoCheckboxValue = wnd_PO.GetSpecificColumnValue("LineSpec", "Cancel done",0);
            Assert.AreEqual("Yes", PoCheckboxValue, "CheckBox is not checked");
          

            wnd_PO.ClickRibbonMenuItem("PurchOrder", "Maintain", "Edit");
            btn_Close = wnd_PO.DetectControl<WinButton>(new { Name = "Close" });

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "Canceled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            bool verifyedFlag = driver.VerifyPOAvailabilityInCueList("Canceled", PurchaseOrder);
            Assert.IsFalse(verifyedFlag, "Cancel Done Po is not disappered from Canceled Cue");

            }


        [TestMethod]
        [WorkItem(102939)]
        [Description("AX Header View - Add a checkbox to allow for removal of Canceled Orders from the Cancel cue in Portal")]
        public void AXHeaderViewAddACheckboxToAllowForRemovalOfCanceledOrdersFromTheCancelCueInPortal()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Path_AccountsPayable = "ECM/Accounts payable/Common/Purchase orders/All purchase orders";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_PO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            WinButton btn_Close = new WinButton();
            WinClient client_FastTab = new WinClient();
            IWebDriver driver = null;
            string ERPNumber = string.Empty;
            string HeaderLevelCancelReasonCode = "Customer Expectation";
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            List<string> ColumnList = driver.GetColumnValues("dynGridViewTable");
            for (int i = 0; i < ColumnList.Count; i++)
            {
                if (ColumnList[i] == "Modified date and time")
                {
                    Assert.AreEqual(ColumnList[i + 1], "Cancel done", "'Cancel done' is not next to 'Modified Date and Time' column");
                }
            }

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Cancel done:", "false");
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Purchase order", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Value Mismatch : \n" + string.Join("\n", list_output));
            }
            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Line stage|Unscheduled", "Cancel done|disabled");
            dic_output = driver.VerifyPOHeaderValues("Purchase order lines", "", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Cancel done Check box is Enabled : \n" + string.Join("\n", list_output));
            }
            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data", true, true);

            driver.SelectFromRibbon("Purchase order", "Stages", "Unscheduled");
            driver.Quit();
            wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));

            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Cancel");

            wnd_PopUp = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Are you sure you want to cancel selected sales orders?‬ (‎‪1‬)", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
            wnd_PopUp.SelectValueFromCustomDropdown("Reason", "Description", HeaderLevelCancelReasonCode);
            btn_Yes = wnd_PopUp.DetectControl<WinButton>(new { Name = "Yes" });
            btn_Yes.WinClick();

            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));


            //  5.4, Click on Complete button
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

            wnd_SO.ClickRibbonMenuItem("General", "Related information", "Purchase order");

            wnd_PO = UITestControl.Desktop.FindWinWindow(new { Name = "Purchase order:", ClassName = "AxTopLevelFrame" });
            wnd_PO.SetFocus();
            wnd_PO.ClickRibbonMenuItem("PurchOrder", "Show", "Header view");

            wnd_PO.ClickRibbonMenuItem("PurchOrder", "Maintain", "Edit");

            client_FastTab = wnd_PO.ExpandFastTab("General",false);

            WinClient client_headerLabel = client_FastTab.DetectControl<WinClient>(new { Name = "Status"});
            WinCheckBox checkbox = client_headerLabel.DetectControl<WinCheckBox>(new { Name = "Cancel done" });
            checkbox.Checked = true;

            wnd_PO.ClickRibbonMenuItem("PurchOrder", "Maintain", "Edit");
            btn_Close = wnd_PO.DetectControl<WinButton>(new { Name = "Close" });

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "Canceled");
            //driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            bool verifyedFlag = driver.VerifyPOAvailabilityInCueList("Canceled", PurchaseOrder);
            Assert.IsFalse(verifyedFlag, "Cancel Done Po is not disappered from Canceled Cue");

        }


        #region Additional test attributes

            // You can use the following additional attributes as you write your tests:

            ////Use TestInitialize to run code before running each test 
            //[TestInitialize()]
            //public void MyTestInitialize()
            //{        
            //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            //}

            ////Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
