﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.Data;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Automation.Framework.Web;
using OpenQA.Selenium;
using Ashley.QA.Automation.Web.Framework.Enum;
using Ashley.QA.Automation.Web.Framework.Selenium;
using System.Linq;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.RegressionTests.Reg_PTL
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class DeliveryandInvoiceDataFastTAb
    {
        public DeliveryandInvoiceDataFastTAb()
        {
        }

        #region Global Variables

        public static string saleOrder;
        public string productCode;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;

        public string zipCode;
        public string shipZipCode;
        public string firstName;
        public string lastName;
        public string address;
        public string city;
        public string state;
        public string email;
        public string phoneNo;
        public string address2;
        public string phoneNo2;
        public string phoneNo3;
        public string alternateEmail;

        #endregion

        #region Sitecore Order Creation
        public void CreateASaleOrderInSitecorePTLReg()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");

                firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                cardType = String.IsNullOrEmpty(cardType) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();
                email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");

                if (String.IsNullOrEmpty(address))
                {
                    address = row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                    city = row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                    state = row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                    zipCode = row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                }

                #endregion

                #region Sale Order creation
                IsInvoiceNeeded = false;

                //saleOrder = "725000144314";
                if (string.IsNullOrEmpty(saleOrder))
                {
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear,null, address2, phoneNo2, phoneNo3, alternateEmail,shipZipCode);
                    IsInvoiceNeeded = true;
                }

                #endregion
            }
        }
        #endregion
        

        [TestMethod]
        [WorkItem(52917)]
        [Description("PTL FR  1812 - Verify the Delivery & Invoice data details")]
        public void PTLVerifytheDeliveryandInvoiceDataDetails()
        {

            #region Local Variables

            WinWindow wnd_SO = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            IWebDriver driver = null;
            var coll = new List<List<string>>();
            WinWindow wnd_AddressMaintenance = new WinWindow();
            WinClient client_FastTab = new WinClient();
            Dictionary<String, String> dict_ColumnAndValues = new Dictionary<String, String>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            List<string> list_output = new List<string>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();

            #endregion

            #region TestData

            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;           
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;

            productCode = "W669-88 ";
            address = "660 S HAWLEY RD";
            address2 = "APT 205";
            city = "WEST ALLIS";
            state = "WI";
            zipCode = "53214";
            phoneNo2 = "9234567892";
            phoneNo3 = "2345678656";
            alternateEmail = "abc@abc.com";

            string invoicePhone2 = "9874563210";
            string invoicePhone3 = "0123456789";
            string email2 = "Automation@codedUI.com";
            string email3 = "AutomationNew@codedUI.com";

            #endregion

            #region Test Steps

            #region Pre-requisite

            CreateASaleOrderInSitecorePTLReg();

            string PurchaseOrder = saleOrder + 1.ToString("00");


            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);


            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            #endregion

            // 1. Launch AX Vendor Portal application
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            // 2.Select NEW cue from Portal management activities from the home page.
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            // 3.Click on the Purchase order number
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 4.Expand the "Delivery" fast tab and verify the following details
            // 5. Verify the @Deliveryname present.
            //var list_deliveryTabInfo = new List<List<string>>();
            //list_deliveryTabInfo.Add(new List<string>());
            //list_deliveryTabInfo.Add(new List<string>());
            //list_deliveryTabInfo.Add(new List<string>());
            //list_deliveryTabInfo[0].Add("Delivery name:" + firstName + lastName);
            //list_deliveryTabInfo[0].Add("Address:" + address + address2 + city + "," + state + zipCode + "USA");
            ////            list_deliveryTabInfo[0].Add("Address:" + @" 950 FORRER BLVD
            ////KETTERING, OH  45420
            ////USA");
            ////list_deliveryTabInfo[1].Add("Requested delivery date:");
            ////list_deliveryTabInfo[1].Add("Delivery terms:");
            ////list_deliveryTabInfo[1].Add("Mode of delivery:");
            //list_deliveryTabInfo[2].Add("Customer delivery phone 1:" + phoneNo);
            //list_deliveryTabInfo[2].Add("Customer delivery phone 2:" + phoneNo2);
            //list_deliveryTabInfo[2].Add("Customer delivery phone 3:" + phoneNo3);
            //list_deliveryTabInfo[2].Add("Customer delivery email 1:" + email);
            //list_deliveryTabInfo[2].Add("Customer delivery email 2:" + alternateEmail);


            dic_POHeaderInputValues.Add("Delivery name:" , firstName + lastName);
            dic_POHeaderInputValues.Add("Address:", address + address2 + city + "," + state + zipCode + "USA");
       
            dic_output = driver.VerifyPOHeaderValues("Delivery", "Delivery name" , dic_POHeaderInputValues);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            dic_POHeaderInputValues = new Dictionary<string, string>();

            dic_POHeaderInputValues.Add("Customer delivery phone 1:", phoneNo);
            dic_POHeaderInputValues.Add("Customer delivery phone 2:", phoneNo2);
            dic_POHeaderInputValues.Add("Customer delivery phone 3:", phoneNo3);
            dic_POHeaderInputValues.Add("Customer delivery email 1:", email);
            dic_POHeaderInputValues.Add("Customer delivery email 2:", alternateEmail);
          

            dic_output = driver.VerifyPOHeaderValues("Delivery", "Customer delivery phone", dic_POHeaderInputValues);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Delivery Details are not displayed properly : \n" + string.Join("\n", list_output));
            }       

            driver.Quit();

            wnd_SO.SetFocus();

            wnd_SO.ClickRibbonMenuItem("Main", "Address maintenance group", "Address maintenance");
            wnd_AddressMaintenance = UITestControl.Desktop.FindWinWindow(new { Name = "‪Address maintenance:", ClassName = "AxTopLevelFrame" });

            dict_ColumnAndValues = new Dictionary<string, string>();
            dict_ColumnAndValues.Add("Purpose", "Invoice");
            wnd_AddressMaintenance.SelectARecord("GridLogisticsPostalAddress", dict_ColumnAndValues, true, false);
            
            client_FastTab = wnd_AddressMaintenance.ExpandFastTab("Contact information", true);

            wnd_AddressMaintenance.ClickTableMenuItem("ContactInfoActionPaneTab", "Add", null);
            dict_ColumnAndValues = new Dictionary<string, string>();
            wnd_AddressMaintenance.EnterValueIntoANewlyAddedCell("GridContactInfo", "Description", "IPhone2");
            wnd_AddressMaintenance.EnterValueIntoANewlyAddedCell("GridContactInfo", "Type", "Phone");
            wnd_AddressMaintenance.EnterValueIntoANewlyAddedCell("GridContactInfo", "Phone/Email", invoicePhone2);

            wnd_AddressMaintenance.ClickTableMenuItem("ContactInfoActionPaneTab", "Add", null);
            dict_ColumnAndValues = new Dictionary<string, string>();
            wnd_AddressMaintenance.EnterValueIntoANewlyAddedCell("GridContactInfo", "Description", "IEmail2");
            wnd_AddressMaintenance.EnterValueIntoANewlyAddedCell("GridContactInfo", "Type", "E-mail address");
            wnd_AddressMaintenance.EnterValueIntoANewlyAddedCell("GridContactInfo", "Phone/Email", email2);

            wnd_AddressMaintenance.ClickTableMenuItem("ContactInfoActionPaneTab", "Add", null);
            dict_ColumnAndValues = new Dictionary<string, string>();
            dict_ColumnAndValues.Add("International calling code", "1");
            wnd_AddressMaintenance.EnterValueIntoANewlyAddedCell("GridContactInfo", "Description", "IPhone3");
            wnd_AddressMaintenance.EnterValueIntoANewlyAddedCell("GridContactInfo", "Type", "Phone");
            wnd_AddressMaintenance.EnterValueIntoANewlyAddedCell("GridContactInfo", "Phone/Email", invoicePhone3);

            wnd_AddressMaintenance.ClickTableMenuItem("ContactInfoActionPaneTab", "Add", null);
            dict_ColumnAndValues = new Dictionary<string, string>();
            wnd_AddressMaintenance.EnterValueIntoANewlyAddedCell("GridContactInfo", "Description", "IEmail3");
            wnd_AddressMaintenance.EnterValueIntoANewlyAddedCell("GridContactInfo", "Type", "E-mail address");
            wnd_AddressMaintenance.EnterValueIntoANewlyAddedCell("GridContactInfo", "Phone/Email", email3);

            dict_ColumnAndValues = new Dictionary<string, string>();
            dict_ColumnAndValues.Add("Purpose", "Delivery");
            wnd_AddressMaintenance.SelectARecord("GridLogisticsPostalAddress", dict_ColumnAndValues, true, false);

            client_FastTab = wnd_AddressMaintenance.ExpandFastTab("Orders", true);
            wnd_AddressMaintenance.CheckInsideACell("GridSalesOrder", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Mark", true);

            WinButton btn_ApplyAndClose = wnd_AddressMaintenance.DetectControl<WinButton>(new { Name = "Apply and Close" });
            btn_ApplyAndClose.WinClick();

            List<string> list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            // 2.Select NEW cue from Portal management activities from the home page.
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            //var list_invoiceFastTabInfo = new List<List<string>>();
            //list_invoiceFastTabInfo.Add(new List<string>());
            //list_invoiceFastTabInfo.Add(new List<string>());
            //list_invoiceFastTabInfo.Add(new List<string>());
            //list_invoiceFastTabInfo[0].Add("Name:" + firstName + lastName);
            //list_invoiceFastTabInfo[0].Add("Address:" + address + address2 + city + "," + state + zipCode + "USA");
            ////Listoflist1[1].Add("Sales payment type:");
            ////Listoflist1[1].Add("Sales payment preauth exp date:");
            ////Listoflist1[1].Add("Sales payment transaction status:");
            //list_invoiceFastTabInfo[2].Add("Customer invoice phone 1:" + phoneNo);
            //list_invoiceFastTabInfo[2].Add("Customer invoice phone 2:"+ invoicePhone2);
            //list_invoiceFastTabInfo[2].Add("Customer invoice phone 3:"+ invoicePhone3);
            //list_invoiceFastTabInfo[2].Add("customer invoice email 1:" + email);
            //list_invoiceFastTabInfo[2].Add("Customer invoice email2:"+ email2);

            //List<bool> Invoice = driver.verifyTheFastTabValues("Invoice data ", list_invoiceFastTabInfo);
            //CollectionAssert.DoesNotContain(Invoice, false, "Invoice data Tab is Not in Correct Format");

            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            dic_POHeaderInputValues = new Dictionary<string, string>();

            dic_POHeaderInputValues.Add("Name:", firstName + lastName);
            dic_POHeaderInputValues.Add("Address:", address + address2 + city + "," + state + zipCode + "USA");

            dic_output = driver.VerifyPOHeaderValues("Invoice data", "Name", dic_POHeaderInputValues);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            dic_POHeaderInputValues = new Dictionary<string, string>();

            dic_POHeaderInputValues.Add("Customer invoice phone 1:", phoneNo);
            dic_POHeaderInputValues.Add("Customer invoice phone 2:", invoicePhone2);
            dic_POHeaderInputValues.Add("Customer invoice phone 3:", invoicePhone3);
            dic_POHeaderInputValues.Add("customer invoice email 1:", email);
            dic_POHeaderInputValues.Add("Customer invoice email2:", email2);


            dic_output = driver.VerifyPOHeaderValues("Invoice data", "Customer invoice phone", dic_POHeaderInputValues);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Invoice Details are not displayed properly : \n" + string.Join("\n", list_output));
            }


            driver.Quit();


            #endregion
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
