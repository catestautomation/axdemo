﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.Data;
using OpenQA.Selenium;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using Ashley.QA.Automation.Web.Framework.Selenium;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.RegressionTests.Reg_PTL
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class PurchaseOrderListViewOnTheCue
    {
        public PurchaseOrderListViewOnTheCue()
        {
        }

        #region Global Variables

        public static string saleOrder;
        public string productCode;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;
        string mLStagePOCreationPending = "PO creation pending";
        string mLStatusPOCreationPending = "Open order";
        string dLStagePOCreationPending = "Delivery charge";
        string dLStatusPOCreationPending = "Open order";

        string mLStageUnscheduled = "Unscheduled";
        string mLStatusUnscheduled = "Open order";
        string dLStageUnscheduled = "Delivery charge";
        string dLStatusUnscheduled = "Open order";

        #endregion

        #region Sitecore Order Creation
        public void CreateASaleOrderInSitecorePTLReg()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");
                string zipCode = row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                string address = row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                string city = row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                string state = row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                cardType = String.IsNullOrEmpty(cardType) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation
                IsInvoiceNeeded = false;
                saleOrder = "725000144496";
                if (string.IsNullOrEmpty(saleOrder))
                {
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                    IsInvoiceNeeded = true;
                }
                #endregion
            }
        }
        #endregion

        [TestMethod]
        [WorkItem(52941)]
        [Description("PTL FR 1773 -Verification of the PO List view")]
        public void PTLVerificationOfThePOListView()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string PurchaseOrder = string.Empty;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            IWebDriver driver = null;
            #endregion

            #region Test Steps

            #region Pre-requisite
            // 1, Launch Ashley SiteCore in the browser
            // 2, Select the HD @Product from @Category and @Subcategory
            // 3, Click on any one of the Product
            // 4, Enter required @Quantity and Click Add to Cart
            // 5, Click on Checkout
            // 6, Click on 'Proceed to checkout' 
            // 7, Click on Continue as Guest
            // 8, Enter @Firstname, @LastName, @Country /region, @Address, @City, @State /Province, @Zip /Postalcode ,@Phonenumber , @Email address and click on continue 
            // 9, Mark the checkbox same as shipping address 
            // 10, Click on continue 
            // 11, Enter the @Card (Visa/Master/Amex)number, @CVV, @ExpiryMonth @ExpiryYear ,first name and last name and click continue 
            // 12, Click on 'Submit order' 

            CreateASaleOrderInSitecorePTLReg();
            PurchaseOrder = saleOrder + 1.ToString("00");
            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            // 13, Launch AX Vendor Portal @InternalURL,@ExternalURL application using the PTL User Role
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            // 14, Click on the New Cue 
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            
            List<string> ExpectedColumnList = new List<string> { "Purchase order", "Created date and time","ERP number","Vendor account","Account name","Invoice name","Delivery name","Status","In use","User","User name","Cancel done" };
            List<string> ColumnList = driver.GetColumnValues("dynGridViewTable");

            for (int i = 0; i < ColumnList.Count; i++)
            {
                    Assert.AreEqual(ExpectedColumnList[i], ColumnList[i], "Any of the Column Value is not present in the PO List view");
            }

            List<string> ExpectedColumnSummary = new List<string> { "Line number", "Line stage", "Confirmed delivery date", "Adjustment type", "PO Qty", "Item number", "Product name", "Modified date and time" };
            List<string> ColumnSummary = driver.GetColumnValues("dynGridViewTable",1);

            for (int i = 0; i < ColumnSummary.Count; i++)
            {
                Assert.AreEqual(ExpectedColumnSummary[i], ColumnSummary[i], "Any of the Column Summary value is not present in the PO Summary view");
            }            

            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();

            String ExpectedColumnListValue = "Open order";
            String ColumnListValue = driver.GetSpecificColumnValues(null, "Status", 0, false);

            Assert.AreEqual(ExpectedColumnListValue, ColumnListValue, "Status value is Mismatch");

            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");         

            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.Invoice();

            driver.SelectFromRibbon("Purchase order", "Stages", "Invoiced");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.ClickACellinTable("Purchase order", PurchaseOrder);
            List<bool> coll = driver.VerifyTheTableValues("Purchase order lines ", "line stage");
            CollectionAssert.DoesNotContain(coll, false, "line stage mismatch");
       
            #endregion

        }



        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
