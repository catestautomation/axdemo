﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.Data;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using OpenQA.Selenium;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Web.Framework.Selenium;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using System.Linq;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.RegressionTests.Reg_PTL
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class RSAIndicatorOnPO
    {
        public RSAIndicatorOnPO()
        {
        }

        #region Global Variables

        public static string saleOrder;
        public string productCode;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;
        string mLStagePOCreationPending = "PO creation pending";
        string mLStatusPOCreationPending = "Open order";
        string dLStagePOCreationPending = "Delivery charge";
        string dLStatusPOCreationPending = "Open order";

        string mLStageUnscheduled = "Unscheduled";
        string mLStatusUnscheduled = "Open order";
        string dLStageUnscheduled = "Delivery charge";
        string dLStatusUnscheduled = "Open order";
        string RSAIndicator;

        #endregion

        #region Sitecore Order Creation
        public void CreateASaleOrderInSitecorePTLReg()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");
                string zipCode = row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                string address = row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                string city = row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                string state = row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                RSAIndicator = "425167";
                cardType = String.IsNullOrEmpty(cardType) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation
                IsInvoiceNeeded = false;
                //saleOrder = "725000144461";
                if (string.IsNullOrEmpty(saleOrder))
                {
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear,null,null,null,null,null,null, RSAIndicator);
                    IsInvoiceNeeded = true;
                }
                #endregion
            }
        }
        #endregion

        [TestMethod]
        [WorkItem(116305)]
        [Description("PTL AXFC72: Verify the Field Retail Sales associate Indicator(RSA) is  Present in the Purchase Order Header UI Validation")]
        public void PTLVerifyTheFieldRetailSalesAssociateIndicatorIsPresentInThePurchaseOrderHeaderUIValidation()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Path_AccountsPayable = "ECM/Accounts payable/Common/Purchase orders/All purchase orders";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;            
            // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            IWebDriver driver = null;
            string ERPNumber = string.Empty;
            string HeaderLevelCancelReasonCode = "Customer Expectation";
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            dic_POHeaderInputValues.Add("RSAIndicator:", "ToVerifyHeaderAlone");
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "References", dic_POHeaderInputValues, true);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Given Key is not present inside the Group", list_output);
            }

        }

        [TestMethod]
        [WorkItem(116308)]
        [Description("PTL AXFC72: Verify the Value in the Field Retail Sales associate ID(RSA) is Null in the Purchase Order Header and detail where the RSA Indicator is left blank in the Site core")]
        public void PTLVerifyTheValueInTheFieldRetailSalesAssociateIDIsNullInThePurchaseOrderHeader()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Path_AccountsPayable = "ECM/Accounts payable/Common/Purchase orders/All purchase orders";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            IWebDriver driver = null;
            string ERPNumber = string.Empty;
            string RSAIndicator = string.Empty;           
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");           

            dic_POHeaderInputValues.Add("RSAIndicator:", RSAIndicator);
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "References", dic_POHeaderInputValues);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("RSA Indicator Field is not Empty", list_output);
            }

        }

        [TestMethod]
        [WorkItem(125989)]
        [Description("PTL AXFC72:Verify the Value in the Field Retail Sales associate Indicator for entire PO")]      
        public void PTLVerifyTheValueInTheFieldRetailSalesAssociateIndicatorForEntirePO()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Path_AccountsPayable = "ECM/Accounts payable/Common/Purchase orders/All purchase orders";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            string adjustmentType = string.Empty;
            string reasonCode = string.Empty;
            string exchangeQty = "2";
            string returnQty = "2";
            string exchangeOrderNumber = string.Empty;
            string returnSaleOrder = string.Empty;


            // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_RMAForm = new WinWindow();
            WinWindow wnd_FindReplacementItem = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            IWebDriver driver = null;
            WinButton btn_OK = new WinButton();
            string lineConfirmationId = string.Empty;
            string ItemCode = string.Empty;
            WinButton btn_ExchangeItem = new WinButton();
            string ERPNumber = string.Empty;
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            dic_POHeaderInputValues.Add("RSAIndicator:", RSAIndicator);
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "References", dic_POHeaderInputValues,false);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");
 
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.Invoice();

            // 3, Go to Sell tab and click Credit note
            //wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
            wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "Create credit note:", ClassName = "AxTopLevelFrame" });

                // 4, Select @AdjustmentType and Select the @ReasonCode
            wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", "Exchange");
            wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", "MfgDefect", false);

                // 5, Select the original invoiced sales line in upper pane
            wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder} }, "Select all", false);
                // Modify the @ReturnQuantity in Quantity column (Main line) in grid(lower pane) and press Enter key
            uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                // 6, Highlight a line (item where Return Qty is modified) in lower pane
                // Click Exchange item button
                wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", returnQty);
                btn_ExchangeItem = wnd_CreateCredit.DetectControl<WinButton>(new { Name = "Exchange item" });
                btn_ExchangeItem.WinClick();
                btn_ExchangeItem.WinClick();

                // 7, Enter @ExchangeQuantity in Exchange Quantity column and @ReturnQuantity in Return quantity column
                // Click on Apply
                wnd_FindReplacementItem = UITestControl.Desktop.FindWinWindow(new { Name = "Find replacement item:", ClassName = "AxTopLevelFrame" });
                wnd_FindReplacementItem.SelectARecord("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, true, false);
                wnd_FindReplacementItem.EnterValueToCell("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, "Return quantity", returnQty);
                wnd_FindReplacementItem.EnterValueToCell("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, "Exchange qty", exchangeQty);

                WinButton btn_Apply = (WinButton)wnd_FindReplacementItem.DetectControl<WinButton>(new { Name = "Apply" });
                btn_Apply.WinClick();
            }
               

                // 8, Click OK in Create credit note form
            btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
            btn_OK.WinClick();

                // 9, Click Close in RMA form
                // wnd_SO.CloseWindow();
            wnd_RMAForm = UITestControl.Desktop.FindWinWindow(new { Name = "Return order  -  RMA number:", ClassName = "AxTopLevelFrame" });
            wnd_RMAForm.CloseWindow();
            UITestControlCollection coll = null;
            while (true)
            {              
                coll = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Sales order", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains).FindMatchingControls();
                if (coll.Count > 1)
                {
                    break;
                }
            }

            wnd_SO = (WinWindow)coll.First(x => !x.Name.Contains(saleOrder));
            wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");

                // Click Close in infolog
            List<string> lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

            // 13, Go to the Retun [Exchange] Sales order form
            //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });      

            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                exchangeOrderNumber = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");
            }

                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Close" });

            // 17, Schedule the DS lines via AS400
            // 18, Schedule the HD lines via Portal
            // 19, Go to PTL application
           driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

                // 20, Schedule the HD Negative (Return) Purchase Order(s) via Homestore Portal (PTL)
                // 20.1 Click HomeStore portal => Purchase order => New Returns cue icon
           driver = driver.SelectTab("Homestore portal");
           driver.SelectFromRibbon("Purchase order", "Stages", "New returns");


                // 20.2, Seach and click on the negative purchase order
           driver = driver.PortalFilter("Purchase order", exchangeOrderNumber);
           driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
           dic_POHeaderInputValues = new Dictionary<string, string>();
           dic_POHeaderInputValues.Add("RSAIndicator:", RSAIndicator);
           dic_output = driver.VerifyPOHeaderValues("Purchase order", "References", dic_POHeaderInputValues);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            driver.Quit();

            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
            wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "‪Create credit note‬:", ClassName = "AxTopLevelFrame" });

            // 4, Select @AdjustmentType and select the @Reasoncode
            wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", "Customer return");
            wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", "MfgDefect", false);

            // 5, Select the original invoiced sales line in upper pane
            wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", false);
            // Modify the @ReturnQuantity in Quantity column (for all Main lines) (Lower pane)
            // Select Delvery charge line
            uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", returnQty);
            }
            //List<string> lst_MessageValue = DynamicsAxCommonFunctions.HandleInfolog(true);
            //wnd_SO.CloseWindow();
            // 6, Click OK in Create credit note form
            btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
            btn_OK.WinClick();

            // 7, Click Close in RMA form
            wnd_RMAForm = UITestControl.Desktop.FindWinWindow(new { Name = "‪Return order  -  RMA number:", ClassName = "AxTopLevelFrame" });
            wnd_RMAForm.CloseWindow();

            coll = null;
            while (true)
            {
                // 11,Click Complete in Return SO form
                coll = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Sales order", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains).FindMatchingControls();
                if (coll.Count > 1)
                {
                    break;
                }
            }

            wnd_SO = (WinWindow)coll.First(x => !x.Name.Contains(saleOrder));

            // 9, Click Complete in Return SO form

            wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");

            // Click Close in infolog
            lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
 
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                returnSaleOrder = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");
                break;
            }
            
            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Close" });
           
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            // 18, Schedule the HD Negative (Return)Purchase Order(s) via Homestore Portal(PTL)
            // 18.1 Click HomeStore portal => Purchase order => New Returns cue icon
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New returns");

            // 18.2, Seach and click on the negative purchase order
            driver = driver.PortalFilter("Purchase order", returnSaleOrder);
            //driver.ClickACellinTable("Purchase order", PurchaseOrder );               
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("RSAIndicator:", RSAIndicator);
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "References", dic_POHeaderInputValues);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("RSA Indicator Field is Empty", list_output);
            }

        }




        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
