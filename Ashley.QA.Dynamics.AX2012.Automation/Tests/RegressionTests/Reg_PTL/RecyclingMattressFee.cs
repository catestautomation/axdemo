﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using OpenQA.Selenium;
using System.Data;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using Ashley.QA.Automation.Web.Framework.Selenium;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.RegressionTests.Reg_PTL
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class RecyclingMattressFee
    {
        public RecyclingMattressFee()
        {
        }

        #region Global Variables

        public static string saleOrder;
        public string productCode;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;

        #endregion

        #region Sitecore Order Creation
        public void CreateASaleOrderInSitecorePTLReg()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");
                string zipCode = "02908";//row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                string address = "43 NOLAN ST";// row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                string city = "PROVIDENCE";// row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                string state = "RI"; //row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                cardType = String.IsNullOrEmpty(cardType) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation
                IsInvoiceNeeded = false;
                //saleOrder = "725000144410";
                if (string.IsNullOrEmpty(saleOrder))
                {
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                    IsInvoiceNeeded = true;
                }
                #endregion
            }
        }
        #endregion

        [TestMethod]
        [WorkItem(120720)]
        [Description("PTL - Verify that the Mattress recycling fee is not displayed on any of the Purchase Order in PTL")]
        public void PTLVerifyThatTheMattressRecyclingFeeIsNotDisplayedOnAnyOfThePurchaseOrderInPTL()     
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath1 = AX.Automation.Properties.Settings.Default.AddressPath;
            string AddressPath2 = "ECM/Retail/Common/Products";//AX.Automation.Properties.Settings.Default.AddressPath;
            string ReleasedProductDetails = "Retail/Common/Products/Released products by category";
            // string Path_AccountsPayable = "Retail/Common/Products/Released product details";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            productCode= "M83X12";
            // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_RPD = new WinWindow();
            WinWindow wnd_VTA = new WinWindow();
            WinWindow wnd_prd = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            UITestControlCollection uitcc_POPriceLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            IWebDriver driver = null;
            string ERPNumber = string.Empty;
            List<string> list_output = new List<string>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
             CreateASaleOrderInSitecorePTLReg();
            

            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath1);

            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            wnd_AX.SetFocus();
            wnd_AX.NavigateTo(AddressPath2);
            wnd_AX.SelectFromTreeItems(ReleasedProductDetails);

            //Keyboard.SendKeys("{ENTER}");

            wnd_RPD = UITestControl.Desktop.FindWinWindow(new { Name = "‪Released product details‬", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
            wnd_RPD.SetFocus();

            WinTable table_Table = wnd_RPD.DetectControl<WinTable>(new { Name = "HeaderGrid" });
            WinColumnHeader column_Name = table_Table.DetectControl<WinColumnHeader>(new { Name = "Item number" });

            Mouse.Click(new Point(column_Name.BoundingRectangle.Location.X + column_Name.BoundingRectangle.Width / 2, column_Name.BoundingRectangle.Location.Y + column_Name.BoundingRectangle.Height / 2 + 10));       
            Keyboard.SendKeys(productCode);
            Keyboard.SendKeys("{ENTER}");

            wnd_RPD.ClickRibbonMenuItem("ActionPaneTabPurchase", "Trade agreements", "View trade agreements");
            Playback.Wait(3000);

            wnd_VTA = UITestControl.Desktop.FindWinWindow(new { Name = "‪View trade agreements‬", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
            wnd_VTA.SetFocus();

            table_Table = wnd_VTA.DetectControl<WinTable>(new { Name = "Grid" });
            column_Name = table_Table.DetectControl<WinColumnHeader>(new { Name = "From date" });
            column_Name.WinClick();
            column_Name.WinClick();
            column_Name.WinClick();


            String Item = null;
            String Amount = null;

            Item = wnd_VTA.GetSpecificColumnValue("Grid", "Item", 0);
            Amount = wnd_VTA.GetSpecificColumnValue("Grid", "Amount in transaction currency", 0);

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
        
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.WaitForReady();

            string unitPrice = driver.GetSpecificColumnValues("Purchase order lines", "Unit price", 0);
            string productName = driver.GetSpecificColumnValues("Purchase order lines", "Product Name", 0);

            Assert.AreEqual(Amount,unitPrice, "The Expected Amount in VTA- "+Amount+" is not Matching with Actual Unit Price in PTL-"+ unitPrice);
            Assert.AreEqual(Item, productName, "The Expected Item in VTA- "+Item+ " is not Matching with Actual Product Name in PTL-"+ productName);

        }

        [TestMethod]
        [WorkItem(120871)]
        [Description("PTL: Verify that the Mattress recycling fee is not displayed on any of the Purchase Order in PTL for non- Mattress items")]
        public void PTLVerifyThatTheMattressRecyclingFeeIsNotDisplayedOnAnyOfThePurchaseOrderInPTLForNonMattressItems()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath1 = AX.Automation.Properties.Settings.Default.AddressPath;
            string AddressPath2 = "ECM/Retail/Common/Products";//AX.Automation.Properties.Settings.Default.AddressPath;
            string ReleasedProductDetails = "Retail/Common/Products/Released products by category";
            // string Path_AccountsPayable = "Retail/Common/Products/Released product details";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            productCode = "2850138";
            // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_RPD = new WinWindow();
            WinWindow wnd_VTA = new WinWindow();
            WinWindow wnd_prd = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            UITestControlCollection uitcc_POPriceLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            IWebDriver driver = null;
            string ERPNumber = string.Empty;
            List<string> list_output = new List<string>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath1);

            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            wnd_AX.SetFocus();
            wnd_AX.NavigateTo(AddressPath2);
            wnd_AX.SelectFromTreeItems(ReleasedProductDetails);

            //Keyboard.SendKeys("{ENTER}");

            wnd_RPD = UITestControl.Desktop.FindWinWindow(new { Name = "‪Released product details‬", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
            wnd_RPD.SetFocus();

            WinTable table_Table = wnd_RPD.DetectControl<WinTable>(new { Name = "HeaderGrid" });
            WinColumnHeader column_Name = table_Table.DetectControl<WinColumnHeader>(new { Name = "Item number" });

            Mouse.Click(new Point(column_Name.BoundingRectangle.Location.X + column_Name.BoundingRectangle.Width / 2, column_Name.BoundingRectangle.Location.Y + column_Name.BoundingRectangle.Height / 2 + 10));
            Keyboard.SendKeys(productCode);
            Keyboard.SendKeys("{ENTER}");

            wnd_RPD.ClickRibbonMenuItem("ActionPaneTabPurchase", "Trade agreements", "View trade agreements");
            Playback.Wait(3000);

            wnd_VTA = UITestControl.Desktop.FindWinWindow(new { Name = "‪View trade agreements‬", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
            wnd_VTA.SetFocus();

            table_Table = wnd_VTA.DetectControl<WinTable>(new { Name = "Grid" });
            column_Name = table_Table.DetectControl<WinColumnHeader>(new { Name = "From date" });
            column_Name.WinClick();
            column_Name.WinClick();
            column_Name.WinClick();


            String Item = null;
            String Amount = null;

            Item = wnd_VTA.GetSpecificColumnValue("Grid", "Item", 0);
            Amount = wnd_VTA.GetSpecificColumnValue("Grid", "Amount in transaction currency", 0);

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.WaitForReady();

            string unitPrice = driver.GetSpecificColumnValues("Purchase order lines", "Unit price", 0);
            string productName = driver.GetSpecificColumnValues("Purchase order lines", "Product Name", 0);

            Assert.AreEqual(Amount, unitPrice, "The Expected Amount in VTA- " + Amount + " is not Matching with Actual Unit Price in PTL-" + unitPrice);
            Assert.AreEqual(Item, productName, "The Expected Item in VTA- " + Item + " is not Matching with Actual Product Name in PTL-" + productName);

        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
