﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using OpenQA.Selenium;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using System.Linq;
//using Ashley.QA.Automation.Framework.Web;
using System.Data;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using Ashley.QA.Automation.Web.Framework.Selenium;
using System.Threading;
using System.Diagnostics;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.RegressionTests.Reg_PTL
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class CreditReasonCodeOnSalesLineHistory
    {
        public CreditReasonCodeOnSalesLineHistory()
        {
        }
        #region Global Variables

        public static string saleOrder;
        public string productCode;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;
        string mLStagePOCreationPending = "PO creation pending";
        string mLStatusPOCreationPending = "Open order";
        string dLStagePOCreationPending = "Delivery charge";
        string dLStatusPOCreationPending = "Open order";

        string mLStageUnscheduled = "Unscheduled";
        string mLStatusUnscheduled = "Open order";
        string dLStageUnscheduled = "Delivery charge";
        string dLStatusUnscheduled = "Open order";

        #endregion

        #region Sitecore Order Creation
        public void CreateASaleOrderInSitecorePTLReg()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");
                string zipCode = row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                string address = row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                string city = row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                string state = row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                cardType = String.IsNullOrEmpty(cardType) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation
                IsInvoiceNeeded = false;
                if (string.IsNullOrEmpty(saleOrder))
                {
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                
                    IsInvoiceNeeded = true;
                }
               
                #endregion
            }
        }
        #endregion

        [TestMethod]
        [WorkItem(121782)]
        [Description("PTL DAX246: Credit Note Reason Validation for Exchange, Returns & Replacement")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91891", DataAccessMethod.Sequential)]
        public void CreditNoteReasonValidationForExchangeReturnAndReplacement()
        {

            int iterationCount = TestContext.DataRow.Table.Rows.IndexOf(TestContext.DataRow);
            if (iterationCount == 0)
            {

                #region Test Data

                string PurchaseOrder = TestContext.DataRow["PurchaseOrder"].ToString();
                string Path = AX.Automation.Properties.Settings.Default.AX_Path;
                string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

                string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
                string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
                string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;

                //string adjustmentType = "";
                //string reasonCode = "";
                string adjustmentTypeforExchange = "Exchange";
                string adjustmentTypeforReturn = "Customer return";
                string adjustmentTypeforReplacement = "Replacement item";
                string reasonCode = TestContext.DataRow["ReasonCode"].ToString();            

               
                //string itemCode = TestContext.DataRow["ItemCode"].ToString();
                string exchangeQty = TestContext.DataRow["ExchangeQuantity"].ToString();
                string returnQty = TestContext.DataRow["ReturnQuantity"].ToString();
                string replacementqty = "1";
                string exchangeOrderNumber = string.Empty;

                #endregion

                #region Local Variables
                WinWindow wnd_AX = new WinWindow();
                WinWindow wnd_SO = new WinWindow();
                WinWindow wnd_CreateCredit = new WinWindow();
                WinWindow wnd_RMAForm = new WinWindow();
                WinWindow wnd_FindReplacementItem = new WinWindow();
                BrowserWindow browserWindow = new BrowserWindow();
                HtmlRow row_Record = new HtmlRow();
                UITestControlCollection uitcc_SOLines = new UITestControlCollection();
                WinEdit edit_discountPercentage = new WinEdit();
                WinButton btn_OK = new WinButton();
                string lineConfirmationId = string.Empty;
                string ItemCode = string.Empty;
                WinButton btn_ExchangeItem = new WinButton();
                IWebDriver driver = null;
                string ERPNumber = string.Empty;
                string returnSaleOrder = string.Empty;

                #endregion

                #region Pre-requisite
                //  1,Prerequisite: Invoiced Sales order
                CreateASaleOrderInSitecorePTLReg();
              
                PurchaseOrder = saleOrder + 1.ToString("00");

                #endregion

                 #region Test Steps
                // 2, Go to AX application - Sale Order form
                wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
                wnd_AX.NavigateTo(AddressPath);
                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
              
                // 2. Pre Condition -Invoiced PO
                #region Invoice Process
                if (IsInvoiceNeeded)
                {
                    wnd_SO.WaitForPurchaseOrderToBeGenerated();
                    driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
                    driver = driver.SelectTab("Homestore portal");
                    driver.SelectFromRibbon("Purchase order", "Stages", "New");
                    driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                    driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
                    ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                    driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");


                    // 12.2, Search and click on the purchase order
                    // 12.3, Click Invoice Process => New Vendor Invoice => Post Invoice   
                    driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
                    driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                    driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
                    driver.Invoice();
                    driver.Quit();

                }
                #endregion
               
               
                #region Exchange

                // 3, Perform Exchange via AX
                wnd_SO.SetFocus();
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
                wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "Create credit note:", ClassName = "AxTopLevelFrame" });

                // 3.1, Select @AdjustmentType and Select the @ReasonCode
                wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", adjustmentTypeforExchange);
                wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", reasonCode);

                // 3.2, Select the original invoiced sales line in upper pane
                wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", false);
                // Modify the @ReturnQuantity in Quantity column (Main line) in grid(lower pane) and press Enter key
                uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                    // 3.3, Highlight a line (item where Return Qty is modified) in lower pane
                    // Click Exchange item button
                    wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", returnQty);
                    btn_ExchangeItem = wnd_CreateCredit.DetectControl<WinButton>(new { Name = "Exchange item" });
                    btn_ExchangeItem.WinClick();
                    btn_ExchangeItem.WinClick();

                    // 3.4, Enter @ExchangeQuantity in Exchange Quantity column and @ReturnQuantity in Return quantity column
                    // Click on Apply
                    wnd_FindReplacementItem = UITestControl.Desktop.FindWinWindow(new { Name = "Find replacement item:", ClassName = "AxTopLevelFrame" });
                    wnd_FindReplacementItem.SelectARecord("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, true, false);
                    //wnd_FindReplacementItem.EnterValueToCell("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, "Return quantity", returnQty);
                    wnd_FindReplacementItem.EnterValueToCell("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, "Exchange qty", exchangeQty);

                    WinButton btn_Apply = (WinButton)wnd_FindReplacementItem.DetectControl<WinButton>(new { Name = "Apply" });
                    btn_Apply.WinClick();
                }


                // 3.5, Click OK in Create credit note form
                btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
                btn_OK.WinClick();

                // 3.6, Click Close in RMA form
                //wnd_SO.CloseWindow();
                wnd_RMAForm = UITestControl.Desktop.FindWinWindow(new { Name = "Return order  -  RMA number:", ClassName = "AxTopLevelFrame" });
                wnd_RMAForm.CloseWindow();
                UITestControlCollection coll = null;
                while (true)
                {
                    // 3.7,Click Complete in Return SO form
                    coll = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Sales order", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains).FindMatchingControls();
                    if (coll.Count > 1)
                    {
                        break;
                    }
                }

                wnd_SO = (WinWindow)coll.First(x => !x.Name.Contains(saleOrder));
                wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");

                // Click Close in infolog
                List<string> lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

                // 3.8, Go to the Retun [Exchange] Sales order form
               // wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                // 3.9, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            

                // Verify the Purchase Order get Created
                wnd_SO.WaitForPurchaseOrderToBeGenerated();

                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
               // wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentTypeforExchange));
              
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    exchangeOrderNumber = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");
                    break;
                    
                }
                wnd_SO.CloseWindow();
                // 5, Launch AX Vendor Portal application
                driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
                driver.WaitForReady();

                // 6, Click on the "New Returns" cue
                driver = driver.SelectTab("Homestore portal");
                driver.SelectFromRibbon("Purchase order", "Stages", "New returns");
                
                // 7,Click on the PO Number
                driver = driver.PortalFilter("Purchase order", exchangeOrderNumber);
                driver.WaitForReady();
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                // 8, Expand the "Schedule" Fast line tab.
                var list_scheduleTabInfo = new List<List<string>>();
                // reasonCode = "Shipping Damage";

                // 9, Verify the Credit reason code is displaying for the corresponding sales order line.
                ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                list_scheduleTabInfo.Add(new List<string>() { "Credit reason code:" + (reasonCode == "ShipDmge" ? "Shipping Damage" : reasonCode) });
                List<bool> Schedule = driver.verifyTheFastTabValues("Schedule ", list_scheduleTabInfo,ERPNumber,false);

                CollectionAssert.DoesNotContain(Schedule, false, "Credit Reason Code is Wrong");

                // 16, Click on the "UnScheduled Returns" cue
                driver.SelectFromRibbon("Purchase order", "Stages", "UnScheduled returns");
                driver = driver.PortalFilter("Purchase order", exchangeOrderNumber);
                driver.WaitForReady();
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                list_scheduleTabInfo = new List<List<string>>();
                // reasonCode = "Shipping Damage";

                // 9, Verify the Credit reason code is displaying for the corresponding sales order line.               
                list_scheduleTabInfo.Add(new List<string>() { "Credit reason code:" + (reasonCode == "ShipDmge" ? "Shipping Damage" : reasonCode) });
                Schedule = driver.verifyTheFastTabValues("Schedule ", list_scheduleTabInfo,"",true);

                CollectionAssert.DoesNotContain(Schedule, false, "Credit Reason Code / ERP entry date is Wrong");

                // 16, Click on the "Scheduled Returns" cue
                driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled returns");
                driver = driver.PortalFilter("Purchase order", exchangeOrderNumber);
                driver.WaitForReady();
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                list_scheduleTabInfo = new List<List<string>>();
                // reasonCode = "Shipping Damage";

                // 9, Verify the Credit reason code is displaying for the corresponding sales order line.               
                list_scheduleTabInfo.Add(new List<string>() { "Credit reason code:" + (reasonCode == "ShipDmge" ? "Shipping Damage" : reasonCode) });
                Schedule = driver.verifyTheFastTabValues("Schedule ", list_scheduleTabInfo,"",null);

                CollectionAssert.DoesNotContain(Schedule, false, "Credit Reason Code is Wrong");

                // 14, Select a confirmed delivery date at header level and Click Apply to lines
                // 15, Click Save & Close
                //ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                //driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

                //// 16, Click on the "Scheduled Returns" cue
                //driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
                //driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                //driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                //// 17, Verify the Purchase Order Header

                //// 18, Verify that the confirmed delivery date is applied to all lines.
                //List<bool> coll1 = driver.VerifyTheTableValues("Purchase order lines ", "confirmed delivery date");
                //CollectionAssert.DoesNotContain(coll1, false, "confirm delivery date mismatch");

                //coll1 = driver.VerifyTheTableValues("ASN Details ", "confirmed delivery date");
                //CollectionAssert.DoesNotContain(coll1, false, "confirm delivery date mismatch");

                //// 19, Expand the "Schedule" Fast line tab Verify the Credit reason code is displaying for the corresponding sales order line.
                //Schedule = driver.verifyTheFastTabValues("Schedule ", list_scheduleTabInfo);

                //CollectionAssert.DoesNotContain(Schedule, false, "Credit Reason Code is Wrong");


                driver.Quit();
                #endregion

                #region Return
                //CustomerReturn Process
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
                wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "‪Create credit note‬:", ClassName = "AxTopLevelFrame" });

                // 4, Select @AdjustmentType and select the @Reasoncode
                wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", adjustmentTypeforReturn);
                wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", reasonCode);

                // 5, Select the original invoiced sales line in upper pane
                wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", true);
                // Modify the @ReturnQuantity in Quantity column (for all Main lines) (Lower pane)
                // Select Delvery charge line
                uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));

                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                    wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", returnQty);
                }
                //List<string> lst_MessageValue = DynamicsAxCommonFunctions.HandleInfolog(true);
                //wnd_SO.CloseWindow();
                // 6, Click OK in Create credit note form
                btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
                btn_OK.WinClick();

                // 7, Click Close in RMA form
                wnd_RMAForm = UITestControl.Desktop.FindWinWindow(new { Name = "‪Return order  -  RMA number:", ClassName = "AxTopLevelFrame" });
                wnd_RMAForm.CloseWindow();

                // 9, Click Complete in Return SO form
                coll = null;
                while (true)
                {
                    // 3.7,Click Complete in Return SO form
                    coll = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Sales order", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains).FindMatchingControls();
                    if (coll.Count > 1)
                    {
                        break;
                    }
                }

                wnd_SO = (WinWindow)coll.First(x => !x.Name.Contains(saleOrder));
                wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");

                // Click Close in infolog
                lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

                // 11, Go to the Retun Sales order form
                //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                
                // 12, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentTypeforReturn));
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    returnSaleOrder = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");
                    break;
                }
               
                // 13, Wait till PO is auto generated for the Return SO
                // Enusre that PO is created for the Return Sales Order
                wnd_SO.WaitForPurchaseOrderToBeGenerated();
                wnd_SO.CloseWindow();

                // 15, Schedule the DS lines via AS400
                // 16, Schedule the HD lines via Portal
                // 17, Go to PTL application
                driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

                // 18, Schedule the HD Negative (Return)Purchase Order(s) via Homestore Portal(PTL)
                // 18.1 Click HomeStore portal => Purchase order => New Returns cue icon
                driver = driver.SelectTab("Homestore portal");
                driver.SelectFromRibbon("Purchase order", "Stages", "New returns");

                // 18.2, Seach and click on the negative purchase order
                driver = driver.PortalFilter("Purchase order", returnSaleOrder);
                driver.WaitForReady();
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                //21.0, Expand the Schedule Fast Tab
                list_scheduleTabInfo = new List<List<string>>();
                // reasonCode = "Shipping Damage";

                // 9, Verify the Credit reason code is displaying for the corresponding sales order line.
                ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                list_scheduleTabInfo.Add(new List<string>() { "Credit reason code:" + (reasonCode == "ShipDmge" ? "Shipping Damage" : reasonCode) });
                Schedule = driver.verifyTheFastTabValues("Schedule ", list_scheduleTabInfo, ERPNumber, false);

                CollectionAssert.DoesNotContain(Schedule, false, "Credit Reason Code is Wrong");

                // 16, Click on the "UnScheduled Returns" cue
                driver.SelectFromRibbon("Purchase order", "Stages", "UnScheduled returns");
                driver = driver.PortalFilter("Purchase order", returnSaleOrder);
                driver.WaitForReady();
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                list_scheduleTabInfo = new List<List<string>>();
                // reasonCode = "Shipping Damage";

                // 9, Verify the Credit reason code is displaying for the corresponding sales order line.               
                list_scheduleTabInfo.Add(new List<string>() { "Credit reason code:" + (reasonCode == "ShipDmge" ? "Shipping Damage" : reasonCode) });
                Schedule = driver.verifyTheFastTabValues("Schedule ", list_scheduleTabInfo, "", true);

                CollectionAssert.DoesNotContain(Schedule, false, "Credit Reason Code / ERP entry date is Wrong");

                // 16, Click on the "Scheduled Returns" cue
                driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled returns");
                driver = driver.PortalFilter("Purchase order", returnSaleOrder);
                driver.WaitForReady();
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                list_scheduleTabInfo = new List<List<string>>();
                // reasonCode = "Shipping Damage";

                // 9, Verify the Credit reason code is displaying for the corresponding sales order line.               
                list_scheduleTabInfo.Add(new List<string>() { "Credit reason code:" + (reasonCode == "ShipDmge" ? "Shipping Damage" : reasonCode) });
                Schedule = driver.verifyTheFastTabValues("Schedule ", list_scheduleTabInfo, "", null);

                CollectionAssert.DoesNotContain(Schedule, false, "Credit Reason Code is Wrong");

                //// 14, Select a confirmed delivery date at header level and Click Apply to lines
                //// 15, Click Save & Close
                //ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                //driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

                //// 16, Click on the "Scheduled Returns" cue
                //driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
                //driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                //driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                //// 17, Verify the Purchase Order Header

                //// 18, Verify that the confirmed delivery date is applied to all lines.
                //coll1 = driver.VerifyTheTableValues("Purchase order lines ", "confirmed delivery date");
                //CollectionAssert.DoesNotContain(coll1, false, "confirm delivery date mismatch");

                //coll1 = driver.VerifyTheTableValues("ASN Details ", "confirmed delivery date");
                //CollectionAssert.DoesNotContain(coll1, false, "confirm delivery date mismatch");

                //// 19, Expand the "Schedule" Fast line tab Verify the Credit reason code is displaying for the corresponding sales order line.
                //Schedule = driver.verifyTheFastTabValues("Schedule ", list_scheduleTabInfo);

                //CollectionAssert.DoesNotContain(Schedule, false, "Credit Reason Code is Wrong");


                driver.Quit();

                #endregion

                #region Replacement

                // 37.Replacement
                // 37.1, Go to AX application - Sale Order form
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                // 37.2, Go to Sell tab in SO form and click Credit Note
                wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
                wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "‪Create credit note‬:", ClassName = "AxTopLevelFrame" });

                // 37.3, Select @AdjustmentType and select the @AdjCreditReasoncode
                wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", adjustmentTypeforReplacement);
                wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", reasonCode);

                // Click OK in Create credit note form
                wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", true);
               
                uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));

                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                    wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", replacementqty);
                }
                // Click OK in Create credit note form
                btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
                btn_OK.WinClick();
                wnd_SO.WaitForControlReady();               
                wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");
               
                lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);

                // 38, Verify the Purchase Order get Created
               // wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.WaitForPurchaseOrderToBeGenerated();                

                Stopwatch timer = new Stopwatch();
                timer.Start();
                double secs = 0;
                while (true)
                {
                    wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                    secs = timer.Elapsed.TotalSeconds;
                    if (secs > 100)
                    {
                        timer.Stop();
                        break;
                    }
                }


                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentTypeforReplacement));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    lineConfirmationId = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");
                    break;
                }

                // 39, Launch AX Vendor Portal application
                driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
                driver = driver.SelectTab("Homestore portal");

                // 40, Click on the "New Returns" cue
                driver.SelectFromRibbon("Purchase order", "Stages", "New returns");

                // 41, Click on the PO Number
                driver = driver.PortalFilter("Purchase order", lineConfirmationId);
                driver.WaitForReady();
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                // 42, Expand the "Schedule" Fast line tab.
                list_scheduleTabInfo = new List<List<string>>();
                // reasonCode = "Shipping Damage";

                // 9, Verify the Credit reason code is displaying for the corresponding sales order line.
                ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                list_scheduleTabInfo.Add(new List<string>() { "Credit reason code:" + (reasonCode == "ShipDmge" ? "Shipping Damage" : reasonCode) });
                Schedule = driver.verifyTheFastTabValues("Schedule ", list_scheduleTabInfo, ERPNumber, false);

                CollectionAssert.DoesNotContain(Schedule, false, "Credit Reason Code is Wrong");

                // 16, Click on the "UnScheduled Returns" cue
                driver.SelectFromRibbon("Purchase order", "Stages", "UnScheduled returns");
                driver = driver.PortalFilter("Purchase order", lineConfirmationId);
                driver.WaitForReady();
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                list_scheduleTabInfo = new List<List<string>>();
                // reasonCode = "Shipping Damage";

                // 9, Verify the Credit reason code is displaying for the corresponding sales order line.               
                list_scheduleTabInfo.Add(new List<string>() { "Credit reason code:" + (reasonCode == "ShipDmge" ? "Shipping Damage" : reasonCode) });
                Schedule = driver.verifyTheFastTabValues("Schedule ", list_scheduleTabInfo, "", true);

                CollectionAssert.DoesNotContain(Schedule, false, "Credit Reason Code / ERP entry date is Wrong");

                // 16, Click on the "Scheduled Returns" cue
                driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled returns");
                driver = driver.PortalFilter("Purchase order", lineConfirmationId);
                driver.WaitForReady();
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                list_scheduleTabInfo = new List<List<string>>();
                // reasonCode = "Shipping Damage";

                // 9, Verify the Credit reason code is displaying for the corresponding sales order line.               
                list_scheduleTabInfo.Add(new List<string>() { "Credit reason code:" + (reasonCode == "ShipDmge" ? "Shipping Damage" : reasonCode) });
                Schedule = driver.verifyTheFastTabValues("Schedule ", list_scheduleTabInfo, "", null);

                CollectionAssert.DoesNotContain(Schedule, false, "Credit Reason Code is Wrong");


                //// 14, Select a confirmed delivery date at header level and Click Apply to lines
                //// 15, Click Save & Close
                //ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                //driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

                //// 16, Click on the "Scheduled Returns" cue
                //driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
                //driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                //driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                //// 17, Verify the Purchase Order Header

                //// 18, Verify that the confirmed delivery date is applied to all lines.
                // coll1 = driver.VerifyTheTableValues("Purchase order lines ", "confirmed delivery date");
                //CollectionAssert.DoesNotContain(coll1, false, "confirm delivery date mismatch");

                //coll1 = driver.VerifyTheTableValues("ASN Details ", "confirmed delivery date");
                //CollectionAssert.DoesNotContain(coll1, false, "confirm delivery date mismatch");

                //// 19, Expand the "Schedule" Fast line tab Verify the Credit reason code is displaying for the corresponding sales order line.
                //Schedule = driver.verifyTheFastTabValues("Schedule ", list_scheduleTabInfo);

                //CollectionAssert.DoesNotContain(Schedule, false, "Credit Reason Code is Wrong");


                driver.Quit();

                #endregion



                #endregion
            }
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        
        private TestContext testContextInstance;
    }
}
