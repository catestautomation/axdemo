﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.Data;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using Ashley.QA.Automation.Framework.Windows;
using OpenQA.Selenium;
using Ashley.QA.Automation.Web.Framework.Enum;
using Ashley.QA.Automation.Web.Framework.Selenium;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using System.Linq;
using System.Globalization;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.RegressionTests.Reg_PTL
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class InvoicingAndDelivery
    {
        public InvoicingAndDelivery()
        {
        }

        #region Global Variables

        public static string saleOrder;
        public string productCode;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;
        string mLStagePOCreationPending = "PO creation pending";
        string mLStatusPOCreationPending = "Open order";
        string dLStagePOCreationPending = "Delivery charge";
        string dLStatusPOCreationPending = "Open order";

        string mLStageUnscheduled = "Unscheduled";
        string mLStatusUnscheduled = "Open order";
        string dLStageUnscheduled = "Delivery charge";
        string dLStatusUnscheduled = "Open order";

        #endregion

        #region Sitecore Order Creation
        public void CreateASaleOrderInSitecorePTLReg()
        {
            saleOrder = String.Empty;

            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");
                string zipCode = "60610"; // row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                string address = "1241 N DEARBORN ST"; // row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                string city = "CHICAGO"; // row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                string state = "IL"; // row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                cardType = String.IsNullOrEmpty(cardType) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation
                //saleOrder = "725000144829";
                if (string.IsNullOrEmpty(saleOrder))
                {
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                    IsInvoiceNeeded = true;
                }
                #endregion
            }
        }
        #endregion

        [TestMethod]
        [WorkItem(75306)]
        [Description("PTL FR-6044  - Verification of  Save & Close option to be disabled under edit invoice")]
        public void PTLVerificationOfSaveAndCloseOptionToBeDisabledUnderEditInvoice()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string PurchaseOrder = string.Empty;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            string group = "Commit";
            string action = "Save and close";
            string InvoiceQuantity = "4";

            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            IWebDriver driver = null;
            #endregion

            #region Test Steps

            #region Pre-requisite
            // 1, Launch Ashley SiteCore in the browser
            // 2, Select the HD @Product from @Category and @Subcategory
            // 3, Click on any one of the Product
            // 4, Enter required @Quantity and Click Add to Cart
            // 5, Click on Checkout
            // 6, Click on 'Proceed to checkout' 
            // 7, Click on Continue as Guest
            // 8, Enter @Firstname, @LastName, @Country /region, @Address, @City, @State /Province, @Zip /Postalcode ,@Phonenumber , @Email address and click on continue 
            // 9, Mark the checkbox same as shipping address 
            // 10, Click on continue 
            // 11, Enter the @Card (Visa/Master/Amex)number, @CVV, @ExpiryMonth @ExpiryYear ,first name and last name and click continue 
            // 12, Click on 'Submit order' 

            CreateASaleOrderInSitecorePTLReg();
            PurchaseOrder = saleOrder + 1.ToString("00");
            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            // 13, Launch AX Vendor Portal @InternalURL,@ExternalURL application using the PTL User Role
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            // 14, Click on the New Cue 
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            // 15, Click on the Corresponding PO Number 
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 16, Verify whether the user is allowed to enter ERP number and save the changes to the PO 
            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            // 20, Verify whether Confirmed delivery date is updated under Lines and ASN details fast tab 
            // 21, Click on the save and close button
            // 22, Open the same PO again from the scheduled cue            
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            driver.Invoice(group, action, InvoiceQuantity);

            IWebElement main_IFrame = driver.FindControl(Attributes.Class, "ms-dlgFrame");

            IWebDriver driver_iframe = driver.SwitchTo().Frame(main_IFrame);

            driver_iframe.SelectFromRibbon("Purchase order", "Commit", "Save and close");

            driver = driver.SwitchTo().DefaultContent();

            #endregion
        }

        [TestMethod]
        [WorkItem(75322)]
        [Description("PTL FR-6110 - Populate the Invoice Description with the value in the ERP field")]
        public void PTLPopulateTheInvoiceDescriptionWithTheValueInTheERPField()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string PurchaseOrder = string.Empty;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;

            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            IWebDriver driver = null;            
            #endregion

            #region Test Steps

            #region Pre-requisite
            // 1, Launch Ashley SiteCore in the browser
            // 2, Select the HD @Product from @Category and @Subcategory
            // 3, Click on any one of the Product
            // 4, Enter required @Quantity and Click Add to Cart
            // 5, Click on Checkout
            // 6, Click on 'Proceed to checkout' 
            // 7, Click on Continue as Guest
            // 8, Enter @Firstname, @LastName, @Country /region, @Address, @City, @State /Province, @Zip /Postalcode ,@Phonenumber , @Email address and click on continue 
            // 9, Mark the checkbox same as shipping address 
            // 10, Click on continue 
            // 11, Enter the @Card (Visa/Master/Amex)number, @CVV, @ExpiryMonth @ExpiryYear ,first name and last name and click continue 
            // 12, Click on 'Submit order' 

            CreateASaleOrderInSitecorePTLReg();
            PurchaseOrder = saleOrder + 1.ToString("00");
            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            // 13, Launch AX Vendor Portal @InternalURL,@ExternalURL application using the PTL User Role
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            // 14, Click on the New Cue 
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            // 15, Click on the Corresponding PO Number 
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 16, Verify whether the user is allowed to enter ERP number and save the changes to the PO 
            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            // 20, Verify whether Confirmed delivery date is updated under Lines and ASN details fast tab 
            // 21, Click on the save and close button
            // 22, Open the same PO again from the scheduled cue            
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            string Invoice_Description_Value = driver.InvoiceDescriptionInInvoiceForm("General", "Invoice details", "Invoice description");
            Assert.AreEqual(ERPNumber, Invoice_Description_Value,"Invoice Description Value is not matching with the ERP Number");

            #endregion
        }

        [TestMethod]
        [WorkItem(81781)]
        [Description("PTL-CD8-EP Invoiced Return PO is not updating Actual Delivery Date fields")]
        public void PTLEPInvoicedReturnPOIsNotUpdatingActualDeliveryDateFields()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            string adjustmentType = string.Empty;
            string reasonCode = string.Empty;
            string returnQty = "2";
            string exchangeOrderNumber = string.Empty;
            string returnSaleOrder = string.Empty;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_RMAForm = new WinWindow();
            WinWindow wnd_FindReplacementItem = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            IWebDriver driver = null;
            WinButton btn_OK = new WinButton();
            string lineConfirmationId = string.Empty;
            string ItemCode = string.Empty;
            WinButton btn_ExchangeItem = new WinButton();
            string ERPNumber = string.Empty;
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.Invoice();

            driver.Quit();

            // 3, Go to Sell tab and click Credit note
            wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
            wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "Create credit note:", ClassName = "AxTopLevelFrame" });

            // 4, Select @AdjustmentType and Select the @ReasonCode
            wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", "Customer return");
            wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", "MfgDefect", false);

            // 5, Select the original invoiced sales line in upper pane
            wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", false);
            // Modify the @ReturnQuantity in Quantity column (Main line) in grid(lower pane) and press Enter key
            uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", returnQty);
            }
            //List<string> lst_MessageValue = DynamicsAxCommonFunctions.HandleInfolog(true);
            //wnd_SO.CloseWindow();
            // 6, Click OK in Create credit note form
            btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
            btn_OK.WinClick();

            // 7, Click Close in RMA form
            wnd_RMAForm = UITestControl.Desktop.FindWinWindow(new { Name = "‪Return order  -  RMA number:", ClassName = "AxTopLevelFrame" });
            wnd_RMAForm.CloseWindow();

            UITestControlCollection coll = null;
            while (true)
            {
                // 11,Click Complete in Return SO form
                coll = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Sales order", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains).FindMatchingControls();
                if (coll.Count > 1)
                {
                    break;
                }
            }

            wnd_SO = (WinWindow)coll.First(x => !x.Name.Contains(saleOrder));
            wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");

            // Click Close in infolog
            List<string> lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

            // 13, Go to the Retun [Exchange] Sales order form
            //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });      

            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                returnSaleOrder = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");
            }

            // 17, Schedule the DS lines via AS400
            // 18, Schedule the HD lines via Portal
            // 19, Go to PTL application
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            // 20, Schedule the HD Negative (Return) Purchase Order(s) via Homestore Portal (PTL)
            // 20.1 Click HomeStore portal => Purchase order => New Returns cue icon
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New returns");


            // 20.2, Seach and click on the negative purchase order
            driver = driver.PortalFilter("Purchase order", returnSaleOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled returns");
            driver = driver.PortalFilter("Purchase order", returnSaleOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.Invoice();

            driver.SelectFromRibbon("Purchase order", "Stages", "Invoiced");
            driver = driver.PortalFilter("Purchase order", returnSaleOrder);
            driver.ClickACellinTable("Purchase order", returnSaleOrder);
            List<bool> collection = driver.VerifyTheTableValues("Purchase order lines ", "line stage");
            CollectionAssert.DoesNotContain(collection, false, "line stage mismatch");

            driver.Quit();

            wnd_SO.SetFocus();
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            string DeliveryTabValue = wnd_SO.GetFastTabValueTabDetails("Line details", "Delivery", "Delivery date", "Requested ship date", "Edit");
            String date = "";
            DateTime dt;
            dt = DateTime.Parse(DeliveryTabValue);
            date = dt.ToString("MM/dd/yyyy").Replace("-","/");
            //DateTime.TryParseExact(DeliveryTabValue, "dd/MMM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt);
            Assert.AreEqual(PortalCommonFunctionsSelenium.add, date);

            WinClient client_fastTab = wnd_SO.ExpandFastTab("Line details",false);

            client_fastTab.SelectTabInFastTab("ASN details");

            uitcc_SOLines = wnd_SO.GetAllRecords("afmDeliveryDetailsGrid");

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String ActualDeliverydate = row_SOLine.GetColumnValueOfARecord("Actual delivery date");
                DateTime dte;
                dte = DateTime.Parse(ActualDeliverydate);
                date = dte.ToString("MM/dd/yyyy").Replace("-", "/"); ;
                //DateTime.TryParseExact(ActualDeliverydate, "M/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dte);
                Assert.AreEqual(PortalCommonFunctionsSelenium.add, date);

            }

        }

        [TestMethod]
        [WorkItem(104337)]
        [Description("PTL FR-6505 - Verifying the Line Stage for Delivery Charge when a PO is Canceled")]
        public void PTLVerifyingTheLineStageForDeliveryChargeWhenAPOIsCanceled()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;           
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;            
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_PO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            WinButton btn_Close = new WinButton();
            WinClient client_FastTab = new WinClient();
            IWebDriver driver = null;
            string ERPNumber = string.Empty;
            string HeaderLevelCancelReasonCode = "Customer Expectation";
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data", false, true);

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.Quit();

            wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));

            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Cancel");

            wnd_PopUp = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Are you sure you want to cancel selected sales orders?‬ (‎‪1‬)", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
            wnd_PopUp.SelectValueFromCustomDropdown("Reason", "Description", HeaderLevelCancelReasonCode);
            btn_Yes = wnd_PopUp.DetectControl<WinButton>(new { Name = "Yes" });
            btn_Yes.WinClick();

            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));


            //  5.4, Click on Complete button
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "Canceled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            String LineStage = "Canceled";

            driver.VerifyLineStage(LineStage);

        }

        [TestMethod]
        [WorkItem(130129)]
        [Description("PTL- 3rd Party Invoice validation.")]
        public void PTL3rdPartyInvoiceValidation()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            productCode = "M83X12";

            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_PO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            WinButton btn_Close = new WinButton();
            WinClient client_FastTab = new WinClient();
            IWebDriver driver = null;
            string ERPNumber = string.Empty;
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data", false, true);

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            driver.Invoice();

            driver.SelectFromRibbon("Purchase order", "Stages", "Invoiced");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            String LineStage = "Invoiced";

            driver.VerifyLineStage(LineStage);

        }

        [TestMethod]
        [WorkItem(75316)]
        [Description("PTL FR-6042 & FR-6101 - Hide the unit price and net amount on the Edit Invoice form")]
        public void PTLHideTheUnitPriceAndNetAmountOnTheEditInvoiceForm()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string PurchaseOrder = string.Empty;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;

            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            IWebDriver driver = null;
            #endregion

            #region Test Steps

            #region Pre-requisite
            // 1, Launch Ashley SiteCore in the browser
            // 2, Select the HD @Product from @Category and @Subcategory
            // 3, Click on any one of the Product
            // 4, Enter required @Quantity and Click Add to Cart
            // 5, Click on Checkout
            // 6, Click on 'Proceed to checkout' 
            // 7, Click on Continue as Guest
            // 8, Enter @Firstname, @LastName, @Country /region, @Address, @City, @State /Province, @Zip /Postalcode ,@Phonenumber , @Email address and click on continue 
            // 9, Mark the checkbox same as shipping address 
            // 10, Click on continue 
            // 11, Enter the @Card (Visa/Master/Amex)number, @CVV, @ExpiryMonth @ExpiryYear ,first name and last name and click continue 
            // 12, Click on 'Submit order' 

            CreateASaleOrderInSitecorePTLReg();
            PurchaseOrder = saleOrder + 1.ToString("00");
            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            // 13, Launch AX Vendor Portal @InternalURL,@ExternalURL application using the PTL User Role
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            // 14, Click on the New Cue 
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            // 15, Click on the Corresponding PO Number 
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 16, Verify whether the user is allowed to enter ERP number and save the changes to the PO 
            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            // 20, Verify whether Confirmed delivery date is updated under Lines and ASN details fast tab 
            // 21, Click on the save and close button
            // 22, Open the same PO again from the scheduled cue            
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            string Invoice_Description_Value = driver.InvoiceDescriptionInInvoiceForm("General", "Invoice dates", "Invoice date", true);
            DateTime dte;
            dte = DateTime.Parse(Invoice_Description_Value);
            string date = dte.ToString("dd/MM/yyyy").Replace("-", "/");
            //DateTime.TryParseExact(ActualDeliverydate, "M/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dte);
            Assert.AreEqual(PortalCommonFunctionsSelenium.add, date);

            IWebElement elm_table = driver.FindElements(By.TagName("table")).First(x => x.GetAttribute("class") == "dynGridViewTable");

            List<IWebElement> list_columnHeaders = elm_table.FindElements(By.TagName("th")).ToList();

            string columnName1 = "Unit price";
            string columnName2 = "Net amount";

            list_columnHeaders.IndexOf(list_columnHeaders.First(x => x.Text.ProcessString() != columnName1.ProcessString()));
            list_columnHeaders.IndexOf(list_columnHeaders.First(x => x.Text.ProcessString() != columnName2.ProcessString()));


            #endregion
        }

        [TestMethod]
        [WorkItem(93294)]
        [Description("PTL- BUG -101039 - 92027 -Verify that the Invoiced PO should be listined in the Invoiced Cue PTL ")]
        public void PTLVerifyThatTheInvoicedPOShouldBeListinedInTheInvoicedCuePTL()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;

            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_PO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            WinButton btn_Close = new WinButton();
            WinClient client_FastTab = new WinClient();
            IWebDriver driver = null;
            string ERPNumber = string.Empty;
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data", false, true);

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            driver.Invoice();

            driver.SelectFromRibbon("Purchase order", "Stages", "Invoiced");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            driver.ClickACellinTable("Purchase order", PurchaseOrder);
            List<bool> coll = driver.VerifyTheTableValues("ASN Details ", "actual delivery date");
            CollectionAssert.DoesNotContain(coll, false, "Actual Delivery date is not present");

        }



        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}