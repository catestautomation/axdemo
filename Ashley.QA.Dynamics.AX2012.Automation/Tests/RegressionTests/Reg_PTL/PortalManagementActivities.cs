﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using System.Data;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using OpenQA.Selenium;
using Ashley.QA.Automation.Web.Framework.Selenium;
using System.Linq;
using System.Collections.ObjectModel;
using System.Globalization;
using Ashley.QA.Automation.Web.Framework.Enum;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.RegressionTests.Reg_PTL
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class PortalManagementActivities
    {
        public PortalManagementActivities()
        {
        }

        #region Global Variables

        public static string saleOrder;
        public string productCode;
        public string quantity;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;
        string mLStagePOCreationPending = "PO creation pending";
        string mLStatusPOCreationPending = "Open order";
        string dLStagePOCreationPending = "Delivery charge";
        string dLStatusPOCreationPending = "Open order";

        string mLStageUnscheduled = "Unscheduled";
        string mLStatusUnscheduled = "Open order";
        string dLStageUnscheduled = "Delivery charge";
        string dLStatusUnscheduled = "Open order";

        #endregion

        #region Sitecore Order Creation
        public void CreateASaleOrderInSitecorePTLReg()
        {
            saleOrder = String.Empty;

            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                if (String.IsNullOrEmpty(quantity))
                {
                    string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");
                }
                string zipCode = "60610"; // row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                string address = "1241 N DEARBORN ST"; // row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                string city = "CHICAGO"; // row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                string state = "IL"; // row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                cardType = String.IsNullOrEmpty(cardType) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation
                //saleOrder = "725000144829";
                if (string.IsNullOrEmpty(saleOrder))
                {
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                    IsInvoiceNeeded = true;
                }
                #endregion
            }
        }
        #endregion

        [TestMethod]
        [WorkItem(81766)]
        [Description("PTL-CD8-EP Cancelled cue does not display orders with a cancelled line")]
        public void PTLCancelledCueDoesNotDisplayOrdersWithACancelledLine()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_PO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            WinButton btn_Close = new WinButton();
            WinClient client_FastTab = new WinClient();
            IWebDriver driver = null;
            string ERPNumber = string.Empty;
            string HeaderLevelCancelReasonCode = "Customer Expectation";
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data", false, true);

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.Quit();

            wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));

            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Cancel");

            wnd_PopUp = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Are you sure you want to cancel selected sales orders?‬ (‎‪1‬)", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);
            wnd_PopUp.SelectValueFromCustomDropdown("Reason", "Description", HeaderLevelCancelReasonCode);
            btn_Yes = wnd_PopUp.DetectControl<WinButton>(new { Name = "Yes" });
            btn_Yes.WinClick();

            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));


            //  5.4, Click on Complete button
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");          
            bool CanceledPO = driver.VerifyPOAvailabilityInCueList("Canceled", PurchaseOrder);

            Assert.AreEqual(true, CanceledPO, "Po is not present in the canceled cue");

            bool NewPO = driver.VerifyPOAvailabilityInCueList("New", PurchaseOrder);

            Assert.AreEqual(false, NewPO, "Canceled Po is present in the New cue");
        }

        [TestMethod]
        [WorkItem(83258)]
        [Description("PLT-CD9.2-EP Returns Cues do not include orders with Service Items only")]
        public void PLTReturnsCuesDoNotIncludeOrdersWithServiceItemsOnly()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            string adjustmentType = string.Empty;
            string reasonCode = string.Empty;
            quantity = "2";
            string returnQty = "1";
            string exchangeOrderNumber = string.Empty;
            string returnSaleOrder = string.Empty;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_RMAForm = new WinWindow();
            WinWindow wnd_FindReplacementItem = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            IWebDriver driver = null;
            WinButton btn_OK = new WinButton();
            string lineConfirmationId = string.Empty;
            string ItemCode = string.Empty;
            WinButton btn_ExchangeItem = new WinButton();
            string ERPNumber = string.Empty;
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.Invoice();

            driver.Quit();

            // 3, Go to Sell tab and click Credit note
            wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
            wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "Create credit note:", ClassName = "AxTopLevelFrame" });

            // 4, Select @AdjustmentType and Select the @ReasonCode
            wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", "Customer return");

            WinEdit edt = new WinEdit(wnd_CreateCredit);
            edt.SearchProperties.Add("ControlType", "Edit", PropertyExpressionOperator.EqualTo);
            edt.SearchProperties.Add("Name", "Reason code", PropertyExpressionOperator.EqualTo);

            edt.SelectValueFromCustomComboBox("MfgDefect");

            //wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", "MfgDefect", false);

            // 5, Select the original invoiced sales line in upper pane
            wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", false);
            // Modify the @ReturnQuantity in Quantity column (Main line) in grid(lower pane) and press Enter key
            uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains(productCode));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", returnQty);
            }
            //List<string> lst_MessageValue = DynamicsAxCommonFunctions.HandleInfolog(true);
            //wnd_SO.CloseWindow();
            // 6, Click OK in Create credit note form
            btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
            btn_OK.WinClick();

            // 7, Click Close in RMA form
            wnd_RMAForm = UITestControl.Desktop.FindWinWindow(new { Name = "‪Return order  -  RMA number:", ClassName = "AxTopLevelFrame" });
            wnd_RMAForm.CloseWindow();

            UITestControlCollection coll = null;
            while (true)
            {
                // 11,Click Complete in Return SO form
                coll = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Sales order", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains).FindMatchingControls();
                if (coll.Count > 1)
                {
                    break;
                }
            }

            wnd_SO = (WinWindow)coll.First(x => !x.Name.Contains(saleOrder));
            wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");

            // Click Close in infolog
            List<string> lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

            // 13, Go to the Retun [Exchange] Sales order form
            //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });      

            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                returnSaleOrder = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");
            }

            // 17, Schedule the DS lines via AS400
            // 18, Schedule the HD lines via Portal
            // 19, Go to PTL application
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            // 20, Schedule the HD Negative (Return) Purchase Order(s) via Homestore Portal (PTL)
            // 20.1 Click HomeStore portal => Purchase order => New Returns cue icon
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New returns");

            bool NewReturnPO = driver.VerifyPOAvailabilityInCueList("New returns", PurchaseOrder);

            Assert.AreEqual(true, NewReturnPO, "Po is not present in the New Return cue");

        }

        [TestMethod]
        [WorkItem(83262)]
        [Description("PTL-CD9.2-EP Returns cue does not include exchange orders")]
        public void PTLReturnsCueDoesNotIncludeExchangeOrders()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            string adjustmentType = string.Empty;
            string reasonCode = string.Empty;
            quantity = "2";
            string returnQty = "1";
            string exchangeQty = "1";
            string exchangeOrderNumber = string.Empty;
            string returnSaleOrder = string.Empty;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_RMAForm = new WinWindow();
            WinWindow wnd_FindReplacementItem = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            IWebDriver driver = null;
            WinButton btn_OK = new WinButton();
            string lineConfirmationId = string.Empty;
            string ItemCode = string.Empty;
            WinButton btn_ExchangeItem = new WinButton();
            string ERPNumber = string.Empty;
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.Invoice();

            driver.Quit();

            // 3, Go to Sell tab and click Credit note
            wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
            wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "Create credit note:", ClassName = "AxTopLevelFrame" });

            // 4, Select @AdjustmentType and Select the @ReasonCode
            wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", "Exchange");

            WinEdit edt = new WinEdit(wnd_CreateCredit);
            edt.SearchProperties.Add("ControlType", "Edit", PropertyExpressionOperator.EqualTo);
            edt.SearchProperties.Add("Name", "Reason code", PropertyExpressionOperator.EqualTo);

            edt.SelectValueFromCustomComboBox("MfgDefect");

            // 5, Select the original invoiced sales line in upper pane
            wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", false);
            // Modify the @ReturnQuantity in Quantity column (Main line) in grid(lower pane) and press Enter key
            uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                // 6, Highlight a line (item where Return Qty is modified) in lower pane
                // Click Exchange item button
                wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", returnQty);
                btn_ExchangeItem = wnd_CreateCredit.DetectControl<WinButton>(new { Name = "Exchange item" });
                btn_ExchangeItem.WinClick();
                btn_ExchangeItem.WinClick();

                // 7, Enter @ExchangeQuantity in Exchange Quantity column and @ReturnQuantity in Return quantity column
                // Click on Apply
                wnd_FindReplacementItem = UITestControl.Desktop.FindWinWindow(new { Name = "Find replacement item:", ClassName = "AxTopLevelFrame" });
                wnd_FindReplacementItem.SelectARecord("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, true, false);
                wnd_FindReplacementItem.EnterValueToCell("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, "Return quantity", returnQty);
                wnd_FindReplacementItem.EnterValueToCell("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, "Exchange qty", exchangeQty);

                WinButton btn_Apply = (WinButton)wnd_FindReplacementItem.DetectControl<WinButton>(new { Name = "Apply" });
                btn_Apply.WinClick();
            }


            // 8, Click OK in Create credit note form
            btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
            btn_OK.WinClick();

            // 7, Click Close in RMA form
            wnd_RMAForm = UITestControl.Desktop.FindWinWindow(new { Name = "‪Return order  -  RMA number:", ClassName = "AxTopLevelFrame" });
            wnd_RMAForm.CloseWindow();

            UITestControlCollection coll = null;
            while (true)
            {
                // 11,Click Complete in Return SO form
                coll = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Sales order", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains).FindMatchingControls();
                if (coll.Count > 1)
                {
                    break;
                }
            }

            wnd_SO = (WinWindow)coll.First(x => !x.Name.Contains(saleOrder));
            wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");

            // Click Close in infolog
            List<string> lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

            // 13, Go to the Retun [Exchange] Sales order form
            //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });      

            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                returnSaleOrder = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");
            }

            // 17, Schedule the DS lines via AS400
            // 18, Schedule the HD lines via Portal
            // 19, Go to PTL application
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            // 20, Schedule the HD Negative (Return) Purchase Order(s) via Homestore Portal (PTL)
            // 20.1 Click HomeStore portal => Purchase order => New Returns cue icon
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New returns");

            bool NewReturnPO = driver.VerifyPOAvailabilityInCueList("New returns", PurchaseOrder);

            Assert.AreEqual(true, NewReturnPO, "Po is not present in the New Return cue");

        }

        [TestMethod]
        [WorkItem(98130)]
        [Description("PTL - Bug - 90448 - Confirmed Delivery date is getting updated for the Canceled Qty in ASN Fast Tab")]
        public void PTLConfirmedDeliveryDateIsGgettingUpdatedForTheCanceledQtyInASNFastTab()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Path_AccountsPayable = "ECM/Accounts payable/Common/Purchase orders/All purchase orders";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            string adjustmentType = string.Empty;
            string reasonCode = string.Empty;
            quantity = "2";
            string DeliverRemainderQty = "1";
            string PartialCancelReasonCode = "Customer Expectation";
            string exchangeOrderNumber = string.Empty;
            string returnSaleOrder = string.Empty;

            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_RMAForm = new WinWindow();
            WinWindow wnd_FindReplacementItem = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            IWebDriver driver = null;
            WinButton btn_OK = new WinButton();
            string lineConfirmationId = string.Empty;
            string ItemCode = string.Empty;
            WinButton btn_ExchangeItem = new WinButton();
            string ERPNumber = string.Empty;
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

            //  3.2, Select the main line in Sale order lines grid
            //  Click Update Line => Delivery Reminder
            //  3.3, Enter the @Deliverremainderqty
            //  Select the @PartialCancelReasoncode
            //  Click on OK
            //  3.4, Note: If required repeat from Step 2 to Step 3 for all the main lines available in the selected Sales order form
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {

                row_SOLine.SelectARecord(true, false);

                wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Update line", "Deliver remainder");

                wnd_URQ = UITestControl.Desktop.FindWinWindow(new { Name = "Update remaining quantity:", ClassName = "AxTopLevelFrame" });
                edit_DeliverRemainder = wnd_URQ.DetectControl<WinEdit>(new { Name = "Deliver remainder" });
                edit_DeliverRemainder.EnterWinTextV2(DeliverRemainderQty);
                wnd_URQ.SelectValueFromCustomDropdown("Reason", "Description", PartialCancelReasonCode);
                btn_Ok = wnd_URQ.DetectControl<WinButton>(new { Name = "OK" });
                btn_Ok.WinClick();
                break;
            }

            //  3.5, Click on Complete button
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

            //  3.6, Click Close in infolog
            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            IWebElement div_panel = driver.ExpandFastTab("ASN Details");

            IWebElement elm_table = div_panel.FindElements(By.TagName("table")).First(x => x.GetAttribute("class") == "dynGridViewTable");

            List<IWebElement> list_columnHeaders = elm_table.FindElements(By.TagName("th")).ToList();
            int columnIndex = list_columnHeaders.IndexOf(list_columnHeaders.First(x => x.Text.ProcessString() == "confirmeddeliverydate"));
            List<IWebElement> list_rows = elm_table.FindElement(By.TagName("tbody")).FindElements(By.TagName("tr")).Where(x => x.GetAttribute("id") != null && x.GetAttribute("id") != string.Empty).ToList();

            for (int index = 0; index < 2; index++)
            {
                IWebElement row = list_rows[index];
                List<IWebElement> list_cell = new List<IWebElement>();
                ReadOnlyCollection<IWebElement> coll_elements = (ReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].children;", row);
                string innerText = coll_elements[columnIndex].Text;
                if (index == 1)
                {
                    Assert.IsTrue(string.IsNullOrEmpty(innerText), "Confirmed delivery date is updated for Cancelled lines");

                }


            }
        }

        [TestMethod]
        [WorkItem(82319)]
        [Description("PTL-CD9-EP Scheduled cue does not retain PO's  that have been partially invoiced")]
        public void PTLScheduledCueDoesNotRetainPOThatHaveBeenPartiallyInvoiced()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string PurchaseOrder = string.Empty;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            string group = "Commit";
            string action = "Save and close";
            string InvoiceQuantity = "4";

            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            IWebDriver driver = null;
            #endregion

            #region Test Steps

            #region Pre-requisite
            // 1, Launch Ashley SiteCore in the browser
            // 2, Select the HD @Product from @Category and @Subcategory
            // 3, Click on any one of the Product
            // 4, Enter required @Quantity and Click Add to Cart
            // 5, Click on Checkout
            // 6, Click on 'Proceed to checkout' 
            // 7, Click on Continue as Guest
            // 8, Enter @Firstname, @LastName, @Country /region, @Address, @City, @State /Province, @Zip /Postalcode ,@Phonenumber , @Email address and click on continue 
            // 9, Mark the checkbox same as shipping address 
            // 10, Click on continue 
            // 11, Enter the @Card (Visa/Master/Amex)number, @CVV, @ExpiryMonth @ExpiryYear ,first name and last name and click continue 
            // 12, Click on 'Submit order' 

            CreateASaleOrderInSitecorePTLReg();
            PurchaseOrder = saleOrder + 1.ToString("00");
            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            // 13, Launch AX Vendor Portal @InternalURL,@ExternalURL application using the PTL User Role
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            // 14, Click on the New Cue 
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            // 15, Click on the Corresponding PO Number 
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 16, Verify whether the user is allowed to enter ERP number and save the changes to the PO 
            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            // 20, Verify whether Confirmed delivery date is updated under Lines and ASN details fast tab 
            // 21, Click on the save and close button
            // 22, Open the same PO again from the scheduled cue            
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            driver.Invoice(group, action, InvoiceQuantity);

            IWebElement main_IFrame = driver.FindControl(Attributes.Class, "ms-dlgFrame");

            IWebDriver driver_iframe = driver.SwitchTo().Frame(main_IFrame);

            driver_iframe.SelectFromRibbon("Purchase order", "Commit", "Save and close");

            driver = driver.SwitchTo().DefaultContent();

            driver.VerifyPOAvailabilityInCueList("Scheduled", PurchaseOrder);

            #endregion
        }

        [TestMethod]
        [WorkItem(93311)]
        [Description("PTL- BUG - 91225 - EP Actual delivery date and cancelled ASN lines do not match")]
        public void PTLActualDateAndCancelledASNLinesDoNotMatch()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Path_AccountsPayable = "ECM/Accounts payable/Common/Purchase orders/All purchase orders";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            string adjustmentType = string.Empty;
            string reasonCode = string.Empty;
            string InvoiceQuantity = "4";
            string Date1 = " ";
            string Date = null;
            string DeliverRemainderQty = "8";
            string PartialCancelReasonCode = "Customer Expectation";
            string exchangeOrderNumber = string.Empty;
            string returnSaleOrder = string.Empty;

            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_RMAForm = new WinWindow();
            WinWindow wnd_FindReplacementItem = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            IWebDriver driver = null;
            WinButton btn_OK = new WinButton();
            string lineConfirmationId = string.Empty;
            string ItemCode = string.Empty;
            WinButton btn_ExchangeItem = new WinButton();
            string ERPNumber = string.Empty;
            List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

            driver.Quit();

            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

            //  3.2, Select the main line in Sale order lines grid
            //  Click Update Line => Delivery Reminder
            //  3.3, Enter the @Deliverremainderqty
            //  Select the @PartialCancelReasoncode
            //  Click on OK
            //  3.4, Note: If required repeat from Step 2 to Step 3 for all the main lines available in the selected Sales order form
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {

                row_SOLine.SelectARecord(true, false);

                wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Update line", "Deliver remainder");

                wnd_URQ = UITestControl.Desktop.FindWinWindow(new { Name = "Update remaining quantity:", ClassName = "AxTopLevelFrame" });
                edit_DeliverRemainder = wnd_URQ.DetectControl<WinEdit>(new { Name = "Deliver remainder" });
                edit_DeliverRemainder.EnterWinTextV2(DeliverRemainderQty);
                wnd_URQ.SelectValueFromCustomDropdown("Reason", "Description", PartialCancelReasonCode);
                btn_Ok = wnd_URQ.DetectControl<WinButton>(new { Name = "OK" });
                btn_Ok.WinClick();
                break;
            }

            //  3.5, Click on Complete button
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
            Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

            //  3.6, Click Close in infolog
            list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
            Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            driver.GetSpecificColumnValues("Purchase order lines", "Confirmed delivery date", 0, true, null, Date1);

            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            driver.GetSpecificColumnValues("Purchase order lines", "Confirmed delivery date", 0, true, null, Date);

            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            driver.Invoice(InvoiceQuantity);

            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            IWebElement div_panel = driver.ExpandFastTab("ASN Details");

            IWebElement elm_table = div_panel.FindElements(By.TagName("table")).First(x => x.GetAttribute("class") == "dynGridViewTable");

            List<IWebElement> list_columnHeaders = elm_table.FindElements(By.TagName("th")).ToList();
            int columnIndex = list_columnHeaders.IndexOf(list_columnHeaders.First(x => x.Text.ProcessString() == "actualdeliverydate"));
            List<IWebElement> list_rows = elm_table.FindElement(By.TagName("tbody")).FindElements(By.TagName("tr")).Where(x => x.GetAttribute("id") != null && x.GetAttribute("id") != string.Empty).ToList();

            for (int index = 0; index < 10; index++)
            {
                IWebElement row = list_rows[index];
                List<IWebElement> list_cell = new List<IWebElement>();
                ReadOnlyCollection<IWebElement> coll_elements = (ReadOnlyCollection<IWebElement>)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].children;", row);
                string innerText = coll_elements[columnIndex].Text;
                if (index < 4 || index > 7)
                {
                    Assert.IsTrue(string.IsNullOrEmpty(innerText), "Actual delivery date column does not sort as a group");

                }
                else if (index > 3 && index < 8)
                {
                    DateTime dt;
                    DateTime.TryParseExact(innerText, "M/dd/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt);
                    Assert.AreEqual(PortalCommonFunctionsSelenium.add, dt.ToString("MM/dd/yyyy").Replace("-", "/"));
                }

            }


        #region Test Cleanup
        #endregion

    }

    #region Additional test attributes

    // You can use the following additional attributes as you write your tests:

    ////Use TestInitialize to run code before running each test 
    //[TestInitialize()]
    //public void MyTestInitialize()
    //{        
    //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
    //}

    ////Use TestCleanup to run code after each test has run
    //[TestCleanup()]
    //public void MyTestCleanup()
    //{        
    //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
    //}

    #endregion

    /// <summary>
    ///Gets or sets the test context which provides
    ///information about and functionality for the current test run.
    ///</summary>
    public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
