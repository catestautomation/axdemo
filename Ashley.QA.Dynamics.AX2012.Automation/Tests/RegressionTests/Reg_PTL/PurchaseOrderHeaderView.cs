﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.Data;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using OpenQA.Selenium;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Automation.Web.Framework.Selenium;
using System.Globalization;
using System.Diagnostics;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.RegressionTests.Reg_PTL
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class PurchaseOrderHeaderView
    {
        public PurchaseOrderHeaderView()
        {
        }

        #region Global Variables

        public static string saleOrder;
        public string productCode;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;
        string mLStagePOCreationPending = "PO creation pending";
        string mLStatusPOCreationPending = "Open order";
        string dLStagePOCreationPending = "Delivery charge";
        string dLStatusPOCreationPending = "Open order";

        string mLStageUnscheduled = "Unscheduled";
        string mLStatusUnscheduled = "Open order";
        string dLStageUnscheduled = "Delivery charge";
        string dLStatusUnscheduled = "Open order";

        #endregion



        #region Sitecore Order Creation
        public void CreateASaleOrderInSitecorePTLReg()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");
                string zipCode = row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                string address = row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                string city = row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                string state = row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                cardType = String.IsNullOrEmpty(cardType) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation
                IsInvoiceNeeded = false;
               // saleOrder = "725000144107";
                if (string.IsNullOrEmpty(saleOrder))
                {
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                    IsInvoiceNeeded = true;
                }
                #endregion
            }
        }
        #endregion


        [TestMethod]
        [WorkItem(52942)]
        [Description("PTL FR 1786 - Verfiy the PO header details section")]
        public void PTLFR1786VerfiyThePOHeaderDetailsSection()
        {
            #region Local Variables

            WinWindow wnd_SO = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            IWebDriver driver = null;
            string createdDateTime = string.Empty;
            string vendorAccountName = string.Empty;
            string shipTo = string.Empty;
            string revShareVendor = string.Empty;
            string saleOrderNoAX = string.Empty;
            string soCreatedDateTime = string.Empty;
            string soOnlineChannel = string.Empty;
            string isOkToText = string.Empty;
            List<string> list_output = new List<string>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            string poLastModifiedDateTime = string.Empty;
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();

            #endregion

            #region TestData

            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            string Path_AccountsPayable = "ECM/Accounts payable/Common/Purchase orders/All purchase orders";
            string marketingOption = "No";

            #endregion

            #region Test Steps

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();

            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);

            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("General", "Related information", "Purchase order");

            WinWindow wnd_PO = UITestControl.Desktop.FindWinWindow(new { Name = "Purchase order:", ClassName = "AxTopLevelFrame" });


            createdDateTime = wnd_PO.GetFastTabValueTabDetails("Line details", "Setup", "Date and time", "Created date and time");
            createdDateTime = createdDateTime.Split(' ')[2] + " " + createdDateTime.Split(' ')[3] + " " + createdDateTime.Split(' ')[4];
            //createdDateTime = DateTime.Parse(createdDateTime).ToString("MM/dd/yyyy hh:mm:ss tt").Replace("-", "/");

            wnd_SO.SetFocus();
            vendorAccountName = wnd_SO.GetFastTabValueTabDetails("Line details", "General", "Vendor", "Account name");
            shipTo = wnd_SO.GetFastTabValueTabDetails("Line details", "General", "Vendor", "Ship-To#");
            revShareVendor = wnd_SO.GetFastTabValueTabDetails("Line details", "General", "Vendor", "Rev ShareVendor");

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            //  1.1 Click HomeStore portal => Purchase order => New cue icon
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            //  1.2, Search and click on the purchase order
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            //driver.ClickACellinTable("Purchase order", PurchaseOrder );               
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            dic_POHeaderInputValues.Add("Purchase order:", PurchaseOrder);
            dic_POHeaderInputValues.Add("Created date and time:", createdDateTime);
            dic_POHeaderInputValues.Add("Name:", vendorAccountName);
            dic_POHeaderInputValues.Add("Vendor account:", revShareVendor);
            dic_POHeaderInputValues.Add("Ship to:", shipTo);

            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Purchase order", dic_POHeaderInputValues);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            wnd_AX.NavigateTo(Path_AccountsPayable);
            wnd_AX.ApplyFilter("Purchase order", PurchaseOrder);
            poLastModifiedDateTime = wnd_AX.GetSpecificColumnValue("PurchLine", "Modified date and time");

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Last modified date and time:", poLastModifiedDateTime);

            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Purchase order", dic_POHeaderInputValues);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            wnd_PO.SetFocus();
            saleOrderNoAX = wnd_PO.GetFastTabValueTabDetails("Line details", "Product", "Item reference", "Reference number");
            wnd_SO.SetFocus();
            wnd_SO.ClickRibbonMenuItem("Main", "Show", "Line view");
            soCreatedDateTime = wnd_SO.GetFastTabValueTabDetails("Sales order header", null, "References", "Online Sales Order Create Date/Time Stamp");
            soCreatedDateTime = soCreatedDateTime.Split(' ')[2] + " " + soCreatedDateTime.Split(' ')[3] + " " + soCreatedDateTime.Split(' ')[4];
            soOnlineChannel = wnd_SO.GetFastTabValueTabDetails("Sales order header", null, "References", "Online Channel (Channel ID)");
            isOkToText = wnd_SO.GetFastTabValueTabDetails("Sales order header", null, "References", "Is ok to text", ControlType.CheckBox);

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Sales order:", saleOrderNoAX);
            dic_POHeaderInputValues.Add("Created date time:", soCreatedDateTime);
            dic_POHeaderInputValues.Add("Online Channel (Channel ID):", soOnlineChannel);

            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Online sales order", dic_POHeaderInputValues);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Original ERP Number:", string.Empty);
            dic_POHeaderInputValues.Add("RMA number:", string.Empty);

            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Purchase order", dic_POHeaderInputValues);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Ok To Text Survey:", isOkToText);
            dic_POHeaderInputValues.Add("Marketing Opt In:", marketingOption);

            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Ok To Text Survey", dic_POHeaderInputValues, true);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Following Purchase order Header view values are not displayed properly : \n" + string.Join("\n", list_output));
            }

            #endregion
        }


        [TestMethod]
        [WorkItem(53019)]
        [Description("PTL FR1777 / FR1778 - Verify the Confirmed  Delivery Date is updated on PO")]
        public void PTLFR1777FR1778VerifyConfirmedDeliveryDateIsUpdatedOnPO()
        {

            #region Local Variables

            WinWindow wnd_SO = new WinWindow();
            IWebDriver driver = null;
            List<string> list_output = new List<string>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            string poLastModifiedDateTime = string.Empty;
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();

            #endregion

            #region TestData

            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            string Path_AccountsPayable = "ECM/Accounts payable/Common/Purchase orders/All purchase orders";

            #endregion

            #region Test Steps

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();
            
            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);

            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver.WaitForReady();

            // 6, Click on the "New Returns" cue
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);

            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data", true);

            driver.SelectFromRibbon("Purchase order", "Stages", "Unscheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            dic_POHeaderInputValues.Add("ERP entry date:", "NoDate");
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Home store data", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Purchase order Confirm delivery details got failure because of following mismatch : \n" + string.Join("\n", list_output));
            }

            driver.EnterERPNumberAndConfirmedDeliveryDate(string.Empty, "Purchase order", "Homestore data", false, false);

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Line stage|Scheduled", "Confirmed delivery date|RunTime");
            dic_POHeaderInputValues.Add("Line stage|Delivery charge", "Confirmed delivery date|RunTime");
            dic_output = driver.VerifyPOHeaderValues("Purchase order lines", "", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Purchase order Confirm delivery details got failure because of following mismatch : \n" + string.Join("\n", list_output));
            }

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("All", "Confirmed delivery date|RunTime");
            dic_output = driver.VerifyPOHeaderValues("ASN Details", "", dic_POHeaderInputValues, true);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Purchase order Confirm delivery details got failure because of following mismatch : \n" + string.Join("\n", list_output));
            }

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            //me
            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("ERP number:", ERPNumber);
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Home store data", dic_POHeaderInputValues);



            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Purchase order ERP Number is not present : \n" + string.Join("\n", list_output));
            }

            driver.EnterERPNumberAndConfirmedDeliveryDate(" ", "Purchase order", "Homestore data", true, false);

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("ERP number:", ERPNumber);
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Home store data", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Purchase order ERP Number is not present : \n" + string.Join("\n", list_output));
            }

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("All", "Confirmed delivery date|RunTime");
            dic_output = driver.VerifyPOHeaderValues("ASN Details", "", dic_POHeaderInputValues, true);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Confirmed delivery date is not present in : \n" + string.Join("\n", list_output));
            }

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(Path_AccountsPayable);
            wnd_AX.ApplyFilter("Purchase order", PurchaseOrder);

            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Purchase order", PurchaseOrder } }, true, true);
            WinWindow wnd_PO = UITestControl.Desktop.FindWinWindow(new { Name = "‪Purchase order:‬‬‬‬", ClassName = "AxTopLevelFrame" });
            String ConfirmDate = wnd_PO.GetFastTabValueTabDetails("Line details", "Line details", "afmDeliveryDetailsGrid", "Confirmed delivery date", ControlType.Table);
            ConfirmDate = Convert.ToDateTime(ConfirmDate).ToString("M/d/yyyy", CultureInfo.InvariantCulture);
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Confirmed delivery date", ConfirmDate);
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Home store data", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Confirmed delivery date mismatch in POrtal in AX : \n" + string.Join("\n", list_output));
            }
            #endregion
        }

        [TestMethod]
        [WorkItem(118294)]
        [Description("PTL Verify the ERP & RMA numbers when the PO is a Replacement order (Bug: 118035)")]
        public void PTLVerifytheERP_RMAnumberswhenthePOisaReplacementorder()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinButton btn_OK = new WinButton();
            IWebDriver driver = null;
            string lineConfirmationId = string.Empty;
            string ERPNumber = string.Empty;
            List<string> list_output = new List<string>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();

           
            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            // 2, Go to AX application - Sale Order form
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            #region Invoice Process

            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");


            // 12.2, Search and click on the purchase order
            // 12.3, Click Invoice Process => New Vendor Invoice => Post Invoice   
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.Invoice();

            #endregion

            // 3, Go to Sell tab and click Credit note
            wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
            wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "‪Create credit note‬:", ClassName = "AxTopLevelFrame" });

            // 4, Select @AdjustmentType and select the @AdjCreditReasoncode
            wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", "Replacement item");
            wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", "ShipDmge");

            // 5, Select the original invoiced sales line in upper pane
            wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", true);

            // Highlight the required main line(s) in grid(Lower pane)
            // Reduce the @Replacementqty in Quantity column
            // Perform this step for all the lines in lower pane
            uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));

            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", "1");
            }
            // 6, Click OK in Create credit note form
            btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
            btn_OK.WinClick();
            wnd_SO.WaitForControlReady();
            //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            // 10, Click Complete button in action pane
            wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");

            //Click Close in infolog
            List<string> lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);

            // 11, Wait till PO is auto generated for the new lines in SO
            // Enusre that PO is created for the replacement lines with new Line cconfirmation ID
            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            // 12, Select the Replacement lines in Sale order lines grid
            // Verify the Line Stage and Line Status of the lines
            // wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            Stopwatch timer = new Stopwatch();
            timer.Start();
            double secs = 0;
            while (true)
            {
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                secs = timer.Elapsed.TotalSeconds;
                if (secs > 100)
                {
                    timer.Stop();
                    break;
                }
            }

            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Replacement item"));
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                lineConfirmationId = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");
                break;
            }

            // 13, Schedule the DS lines via AS400
            // 14, Schedule the HD lines via Portal
            // 15, Schedule the HD Negative (Return) Purchase Order(s) via Homestore Portal (PTL)
            // 15.1 Click HomeStore portal => Purchase order => New Returns cue icon            
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New returns");

            // 15.2, Search and click on the purchase order
            driver = driver.PortalFilter("Purchase order", lineConfirmationId);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            dic_POHeaderInputValues = new Dictionary<string, string>();
            dic_POHeaderInputValues.Add("Original ERP Number", "");
            dic_POHeaderInputValues.Add("RMA number", "");
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "Purchase order", dic_POHeaderInputValues);
            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Value Mismatch : \n" + string.Join("\n", list_output));
            }

        }


        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
