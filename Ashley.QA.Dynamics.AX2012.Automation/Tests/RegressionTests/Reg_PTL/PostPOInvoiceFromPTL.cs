﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using System.Data;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Automation.Framework.Web;
using OpenQA.Selenium;
using Ashley.QA.Automation.Web.Framework.Enum;
using Ashley.QA.Automation.Web.Framework.Selenium;
using System.Linq;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.RegressionTests.Reg_PTL
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class PostPOInvoiceFromPTL
    {
        public PostPOInvoiceFromPTL()
        {
        }

        #region Global Variables

        public static string saleOrder;
        public string productCode;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;
        string mLStagePOCreationPending = "PO creation pending";
        string mLStatusPOCreationPending = "Open order";
        string dLStagePOCreationPending = "Delivery charge";
        string dLStatusPOCreationPending = "Open order";

        string mLStageUnscheduled = "Unscheduled";
        string mLStatusUnscheduled = "Open order";
        string dLStageUnscheduled = "Delivery charge";
        string dLStatusUnscheduled = "Open order";

        #endregion

        #region Sitecore Order Creation
        public void CreateASaleOrderInSitecorePTLReg()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();

            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity");
                string zipCode = row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode");
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                string address = row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                string city = row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                string state = row.Field<string>("State") == null ? string.Empty : row.Field<string>("State");
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber");
                cardType = String.IsNullOrEmpty(cardType) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", "");
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode");
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation
                IsInvoiceNeeded = false;

                if (string.IsNullOrEmpty(saleOrder))
                {
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                    IsInvoiceNeeded = true;
                }
                #endregion
            }
        }
        #endregion




        [TestMethod]
        [WorkItem(75265)]
        [Description("PTL FR -6148 / FR-1779 & FR 1894 - Verify Post PO invoice from the portal & Verify the Actual Delivery Date is back dated")]
        public void PTLVerifyPostPOInvoiceFromPortalAndVerifyActualDeliveryDateIsBackDated()
        {
            #region Local Variables

            WinWindow wnd_SO = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            IWebDriver driver = null;

            #endregion

            #region TestData

            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            
            #endregion

            #region Test Steps

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            CreateASaleOrderInSitecorePTLReg();

            string PurchaseOrder = saleOrder + 1.ToString("00");

            // 1.2, Open the Sales Order
            // 1.3, Go to ECM/Accounts receivable/Common/Sales orders/All sales orders
            // 1.4, Seach using the SO confirmation number in the grid and double click on the created SO
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);

            // 1.5, Select the lines in Sale order lines grid
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

            // Verify the Line Stage and Line Status of the lines    
            //Main line        
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual(mLStagePOCreationPending, lineStage, PurchaseOrder + "PO creation pending");
                Assert.AreEqual(mLStatusPOCreationPending, lineStatus, PurchaseOrder + "PO creation pending");
            }

            //Delivery line
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual(dLStagePOCreationPending, lineStage, PurchaseOrder + "PO creation pending");
                Assert.AreEqual(dLStatusPOCreationPending, lineStatus, PurchaseOrder + "PO creation pending");
            }

            // 1.6, Wait till PO is auto generated for the SO
            // Enusre that PO is created for the Sales Order      
            wnd_SO.WaitForPurchaseOrderToBeGenerated();

            // 1.7, Select the lines in Sale order lines grid
            //  Verify the Line Stage and Line Status of the lines
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual(mLStageUnscheduled, lineStage, PurchaseOrder + "PO not generated");
                Assert.AreEqual(mLStatusUnscheduled, lineStatus, PurchaseOrder + "PO not generated");
            }

            //Delivery line
            uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
            uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains("Delivery"));
            foreach (WinRow row_SOLine in uitcc_SOLines)
            {
                String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage");
                String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status");

                Assert.AreEqual(dLStageUnscheduled, lineStage, PurchaseOrder + "PO not generated");
                Assert.AreEqual(dLStatusUnscheduled, lineStatus, PurchaseOrder + "PO not generated");
            }

            #endregion

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

            //  1.1 Click HomeStore portal => Purchase order => New cue icon
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");

            //  1.2, Search and click on the purchase order
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            //driver.ClickACellinTable("Purchase order", PurchaseOrder );               
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 2.1, Enter the @POERPnumber & @POConfirmeddeliverydate          
            // 2.2, Click Save and Close
            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);

            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data", true);
            //IWebDriver driver_confirm = driver.SwitchTo().Frame(driver.FindControl(Attributes.Class, "ms-dlgFrame"));

            //IWebElement elm_yes = driver_confirm.FindElements(By.TagName("input")).ToList().First(x => x.GetAttribute("value") == "Yes");
            //elm_yes.Focus();
            //elm_yes.MouseClick();

           // driver = driver.SwitchTo().DefaultContent();
           // driver.WaitForReady();

            // string driverTitle = driver.Title;

            // 3.1, Open the Unscheduled Cue

            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "Unscheduled");

            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            //driver.ClickACellinTable("Purchase order", PurchaseOrder );               
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 3.2, Verify the ERP Entry date
            string returnVaue = driver.VerifyERPEntryDate("Purchase order", "Homestore data", "ERP entry date");

            Assert.IsFalse(string.IsNullOrEmpty(returnVaue), "ERP Entry Date is not Populated");

            // 4.1 Select the Confirm deivery date from the calendar option
            // 5.1 Click on the Apply to Lines Button
            driver.EnterERPNumberAndConfirmedDeliveryDate("", "Purchase order", "Homestore data", false);

            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
            driver.VerifyTheTableValues("Purchase order lines ", "confirmed delivery date");
            driver.VerifyTheTableValues("ASN Details ", "confirmed delivery date");
            
            //    driver.FocusBrowserWindow();

            string warningMessage = driver.ActualDeliveryPriorDate("General", "Other invoice details");
            Assert.AreEqual(warningMessage, "You have entered an earlier date. Click OK to continue or cancel to re-enter the date.", "Confirm delivery date warning is not come");

            driver = driver.SwitchTo().DefaultContent();
            driver.Quit();

            #endregion
        }


        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
