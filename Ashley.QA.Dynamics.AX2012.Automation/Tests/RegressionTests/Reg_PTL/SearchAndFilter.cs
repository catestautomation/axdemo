﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using OpenQA.Selenium;
using Ashley.QA.Automation.Web.Framework.Selenium;
using Ashley.QA.Automation.Web.Framework.Enum;
using System.Linq;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.RegressionTests.Reg_PTL
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class SearchAndFilter
    {
        public SearchAndFilter()
        {
        }

        [TestMethod]
        [WorkItem(54350)]
        [Description("PTL FR1191 / FR1193 - Verify the ability to search for a PO")]
        public void PTLVerifyTheAbilityToSearchForAPO()
        {
            #region Test Data            
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            string PurchaseOrder = "72500";
            #endregion

            #region Local Variables           
            BrowserWindow browserWindow = new BrowserWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            IWebDriver driver = null;            
            string ERPNumber = string.Empty;
            #endregion

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            driver.WaitForReady();

            IWebElement QuickViewFilter = driver.FindControl(Attributes.Class, "dynFilterToolBarRight");

            IWebElement QuickViewFilterDropDown = QuickViewFilter.FindControl(Attributes.Class, "dynQuickFilterField");

            QuickViewFilterDropDown.Click();

            List<IWebElement> list_columnHeaders = QuickViewFilterDropDown.FindElements(By.TagName("option")).ToList();
            int columnIndex = list_columnHeaders.IndexOf(list_columnHeaders.First(x => x.Text.ProcessString() == "vendoraccount"));

            IWebElement AdvanceFilter = QuickViewFilter.FindControl(Attributes.Class, "dynFilterToolBarShowHide");
            AdvanceFilter.Click();
            
            IWebElement AddFilter = driver.FindControl(Attributes.Class, "dynFilterToolBar");
            IWebElement AddFilterPanel = AddFilter.FindControl(Attributes.Class, "dynFilterToolBarButtonsPanel");
            IWebElement AddFilterPanelText = AddFilterPanel.FindControl(Attributes.Class, "dynFilterToolBarButtonText");
            AddFilterPanelText.Click();

            IWebElement SelectAddFilter = driver.FindControl(Attributes.Class, "dynFilterToolBar");
            IWebElement Filter_Opt = SelectAddFilter.FindControl(Attributes.Class, "dynExpressionArea").FindElement(By.TagName("table"));
            IWebElement SelectAddFilterDropdown = Filter_Opt.FindElements(By.TagName("tr")).Last();
            IWebElement SelectDropDown = SelectAddFilterDropdown.FindIdenticalControls(Attributes.Class, "dyn-propselect").Last();            
            SelectDropDown.Click();

            IWebElement SelectOption = driver.FindControl(Attributes.Class, "AxFilterFields").FindElement(By.TagName("select"));
            IWebElement FilterDropDown = SelectOption.FindElements(By.TagName("option")).First(x => x.GetAttribute("value") == "OrderAccount");
            FilterDropDown.DrawHighlight();
            FilterDropDown.Focus();
            FilterDropDown.Click();


        }

        [TestMethod]
        [WorkItem(54347)]
        [Description("PTL FR1192 - Verify the ability to search multiple criteria at once")]
        public void PTLVerifyTheAbilityToSearchMultipleCriteriaAtOnce()
        {
            #region Test Data            
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            string PurchaseOrder = "72500";
            string VendorAccount = "9972400_308";
            string DeliveryName = "Steve";
            string InvoiceName = "Steve";
            string PurchaseType = "Purchase order";
            string ApprovalStatus = "Confirmed";
            string DirectDelivery = "Yes";
            string PurchaseOrderValue = "PurchId";
            string VendorAccountValue = "OrderAccount";
            string DeliveryNameValue = "SalesTable!DeliveryName";
            string InvoiceNameValue = "SalesTable!SalesName";
            string PurchaseTypeValue = "PurchaseType";
            string ApprovalStatusValue = "DocumentState";
            string DirectDeliveryValue = "MCRDropShipment";
            #endregion

            #region Local Variables           
            BrowserWindow browserWindow = new BrowserWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            IWebDriver driver = null;
            string ERPNumber = string.Empty;
            #endregion

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");           

            IWebElement QuickViewFilter = driver.FindControl(Attributes.Class, "dynFilterToolBarRight");

            IWebElement AdvanceFilter = QuickViewFilter.FindControl(Attributes.Class, "dynFilterToolBarShowHide");
            AdvanceFilter.Click();

            driver.AddFilter(PurchaseOrderValue, PurchaseOrder, true);

            driver.AddFilter(VendorAccountValue, VendorAccount, true);

            driver.AddFilter(DeliveryNameValue, DeliveryName, true);

            driver.AddFilter(InvoiceNameValue, InvoiceName, true);

            driver.AddFilter(PurchaseTypeValue, PurchaseType, true);

            driver.AddFilter(ApprovalStatusValue, ApprovalStatus, true);

            driver.AddFilter(DirectDeliveryValue, DirectDelivery, true);

            IWebElement AddFilter = driver.FindControl(Attributes.Class, "dynFilterToolBar");
            IWebElement AddFilterPanel = AddFilter.FindControl(Attributes.Class, "dynFilterToolBarButtonsPanel");
            IWebElement ApplyFilter = AddFilterPanel.FindControl(Attributes.Class, "dynApplyFilterImage");
            ApplyFilter.Click();

            driver.WaitForLoadingInformation();

        }



        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
