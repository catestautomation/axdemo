﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using Ashley.QA.SiteCore.Automation;
using Ashley.QA.SiteCore.Automation.Utils;

namespace Ashley.QA.Dynamics.AX2012.Automation
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class CodedUITest1
    {
        public CodedUITest1()
        {
        }

        [TestMethod]
        public void Sample()
        {            
            string address = "ECM/Accounts receivable/Common/Sales orders/All sales orders";
            
            //WinWindow window = UIAutomationControl.Desktop.FindWinWindow(new { Name = "‪Microsoft Dynamics AX‬ - ‎‪Ashley Furniture Industries, Inc.:", ClassName = "AxMainFrame" });

           // WinWindow wnd_SO = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            WinWindow wnd_CN = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Create credit note:", ClassName = "AxTopLevelFrame" });

           // window.NavigateToSpecifiedAddress(address);

            //window.ClickMenuItemFromRibbonGallery("Manage", "Customer", "Prices");

            //bool flag = window.GetStatusOfMenuItemFromRibbonGallery("Manage", "Customer", "Rebate");

           //   window.ApplyFilter("Sales order", "725000010664");

           // bool flag = window.GetStatusOfOrderAvailability("Sales order", "725000010664");

          //  window.DoubleClickACellInWinTable("Sales order", "725000010664");

         //   WinWindow wnd_SO = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            
            //   wnd_SO.SelectARowInWinTable("SalesLineGrid","Item number", "1110035");

        //    Dictionary<string, string> str = wnd_SO.GetLineDetailsFromGivenTab("General", "Line stage|Line status");//"Delivery","Delivery date control");//"General", "Line stage");//"Delivery", "End date range by MOD");

            //  wnd_SO.SelectARowInWinTable("SalesLineGrid","Item number", "Home Delivery");
    //            Dictionary<string, string> str1 = wnd_SO.GetLineDetailsFromGivenTab("Sales order header", "", "Release status");//("Line details","General", "Line stage|Line status");

           // window.ChooseOptionsFromFileMenu("File", "Refresh");

           // wnd_CN.CheckAllCellCheckboxFromWinTable("Invoice_Lines", "Item", "1110035");

           // wnd_CN.CheckAllCellCheckboxFromWinTable("Invoice_Heading", "Sales order", "725000010665");

            // wnd_CN.CheckSpecifiedCellCheckboxFromWinTable("Invoice_Lines", "Item", "1110035");

           // wnd_CN.CheckSpecifiedCellCheckboxFromWinTable("Invoice_Heading", "Sales order", "725000010665");

            //wnd_CN.SelectValueFromWinCombobox("Adjustment type", "Exchange");

            wnd_CN.SelectValueFromCustomWinCombobox("Reason code", "Reason code", "WrgPcOrd");

        }

        [TestMethod]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91908)]
        [Description("Address Maintenance")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91908", DataAccessMethod.Sequential)]
        public void AddressMaintenance()
        {
            #region Initialization

            #endregion

            #region TestData

            string Path = @"C:\Program Files (x86)\Microsoft Dynamics AX\60\Client\Bin\Ax32.exe";

            string AddressPath = "ECM/Accounts receivable/Common/Sales orders/All sales orders";

            string saleOrderNumber = string.Empty;

            string purchaseOrderNumber = string.Empty;

            string siteCoreURL = "http://qa.p2.ashleyretail.com/";

            string SaleOrderNumber = "725000020688";

            string str_InfologWarningMessage = TestContext.DataRow["InfologWarningMessage"].ToString();

            string str_InfologWarningMessage1 = TestContext.DataRow["InfologWarningMessage1"].ToString();

            string str_InfologSingleWarning = TestContext.DataRow["InfologSingleWarning"].ToString();

            #endregion

            #region Local Variables

            WinWindow wnd_AX = new WinWindow();

            List<string> lst_ExpectedValue = new List<string>();

            List<string> lst_ActualValue = new List<string>();
            
            #endregion

            #region TestSteps

            #region Place Sale Order in SiteCore

            //saleOrderNumber = SiteCoreSharedMethods.PlaceSaleOrderAsaGuestUser(siteCoreURL, out purchaseOrderNumber, TestContext);

            #endregion

            #region Launch AX and Navigate to All Sales Order

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            wnd_AX.NavigateToSpecifiedAddress(AddressPath);

            #endregion

            #region Filter Values And Open the Filtered Sale Order

            wnd_AX.ApplyFilter("Sales order", SaleOrderNumber);

            wnd_AX.DoubleClickACellInWinTable("Sales order", SaleOrderNumber);

            #endregion

            #region Navigate to Sale Order Tab and Click Address Maintenance

            WinWindow wnd_SO = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            wnd_SO.ClickMenuItemFromRibbonGallery("Sales order", "Address maintenance group", "Address maintenance");

            WinWindow wnd_AddressMaintenance = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Address maintenance:", ClassName = "AxTopLevelFrame" });

            #endregion

            #region Leave First Name Empty And Enter Last Name 

            wnd_AddressMaintenance.SelectTableMenuItems("Add");

            wnd_AddressMaintenance.EnterValueToSpecifiedCellFromWinTable("GridLogisticsPostalAddress", "Last name", string.Empty, "Analytics");

            wnd_AddressMaintenance.ClickSpecifiedCellFromWinTable("GridLogisticsPostalAddress", "Purpose", "Invoice");

            #region Infolog Validation

            lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog();

            lst_ExpectedValue = str_InfologWarningMessage.Split(',').ToList();

            Assert.IsTrue(lst_ExpectedValue.All(lst_ActualValue.Contains), "There is a mismatch in the infolog Error Message. \r\n Expected:" + string.Join(",",lst_ExpectedValue) + "\r\n Actual: " + string.Join(",",lst_ActualValue));

            #endregion

            #endregion

            #region Leave Last Name Empty And Enter First Name

            wnd_AddressMaintenance.SelectTableMenuItems("Remove");

            wnd_AddressMaintenance.SelectTableMenuItems("Add");

            wnd_AddressMaintenance.EnterValueToSpecifiedCellFromWinTable("GridLogisticsPostalAddress", "First name", string.Empty, "Customer");

            wnd_AddressMaintenance.ClickSpecifiedCellFromWinTable("GridLogisticsPostalAddress", "Purpose", "Invoice");

            #region Infolog Validation

            lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog();

            lst_ExpectedValue = str_InfologWarningMessage1.Split(',').ToList();

            Assert.IsTrue(lst_ExpectedValue.All(lst_ActualValue.Contains), "There is a mismatch in the infolog Error Message. \r\n Expected:" + string.Join(",", lst_ExpectedValue) + "\r\n Actual: " + string.Join(",", lst_ActualValue));

            #endregion

            #endregion

            #region Remove First Name and Press Tab Button

            wnd_AddressMaintenance.EnterValueToSpecifiedCellFromWinTable("GridLogisticsPostalAddress", "First name", string.Empty, " ");

            wnd_AddressMaintenance.EnterValueToSpecifiedCellFromWinTable("GridLogisticsPostalAddress", "Last name", string.Empty, "Analytics");

            Keyboard.SendKeys("{TAB}");

            #region Infolog Validation

            lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog();

            lst_ExpectedValue = str_InfologSingleWarning.Split(',').ToList();

            Assert.IsTrue(lst_ExpectedValue.All(lst_ActualValue.Contains), "There is a mismatch in the infolog Error Message. \r\n Expected:" + string.Join(",", lst_ExpectedValue) + "\r\n Actual: " + string.Join(",", lst_ActualValue));

            #endregion

            #endregion

            #region  Remove Line

            wnd_AddressMaintenance.EnterValueToSpecifiedCellFromWinTable("GridLogisticsPostalAddress", "Last name", string.Empty, "Analytics");

            wnd_AddressMaintenance.SelectTableMenuItems("Remove");

            WinButton Btn_Close = wnd_AddressMaintenance.DetectControl<WinButton>(new { Name = "Close" });

            Btn_Close.WinClick();

            #endregion


            #endregion
        }

        [TestMethod]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91908)]
        [Description("Apply Discounts - Header level / Line level")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91908", DataAccessMethod.Sequential)]
        public void ApplyDiscountsHeaderlevelLinelevel()
        {
            #region Initialization

            #endregion

            #region TestData

            string Path = @"C:\Program Files (x86)\Microsoft Dynamics AX\60\Client\Bin\Ax32.exe";

            string AddressPath = "ECM/Accounts receivable/Common/Sales orders/All sales orders";

            string saleOrderNumber = string.Empty;

            string SaleOrderNumber = "725000020708";

            string DiscountAmount = "100.00";

            string DiscountPercent = "10%";

            #endregion

            #region Local Variables

            WinWindow wnd_AX = new WinWindow();

            List<string> lst_ExpectedValue = new List<string>();

            List<string> lst_ActualValue = new List<string>();

            string FinalValue = string.Empty;

            string InitialValue = string.Empty;

            string DiscountAmountValue = string.Empty;

            string DiscountPercentValue = string.Empty;

            #endregion

            #region TestSteps

            #region Launch AX and Navigate to All Sales Order

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            wnd_AX.NavigateToSpecifiedAddress(AddressPath);

            #endregion

            #region Filter Values And Open the Filtered Sale Order

            wnd_AX.ApplyFilter("Sales order", SaleOrderNumber);

            wnd_AX.DoubleClickACellInWinTable("Sales order", SaleOrderNumber);

            #endregion

            #region Apply line level discount in $ (positive) for the sales order

            #region Click Modify Button and Apply Line Level Discounts

            WinWindow wnd_SO = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            wnd_SO.ClickMenuItemFromRibbonGallery("Sales order", "Maintain", "Modify");

            wnd_SO.SelectARowInWinTable("SalesLineGrid", "Line stage", "Unscheduled");

            wnd_SO.ClickMenuItemFromWinTable("Add line discount");

            WinWindow wnd_SDiscount = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Sales order discount:", ClassName = "AxTopLevelFrame" });

            WinEdit edt_DicountAmount = wnd_SDiscount.DetectControl<WinEdit>(new { Name = "Manual discount $" });

            edt_DicountAmount.EnterWinText(DiscountAmount);

            wnd_SDiscount.SelectValueFromCustomWinCombobox("Reason code", "Reason code", "ChngMind");

            WinButton btn_ApplyAndClose = wnd_SDiscount.DetectControl<WinButton>(new { Name = "Apply and Close" });

            btn_ApplyAndClose.WinClick();

            #region Infolog Validation

            lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog();

            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

            #endregion

            #region Complete Button Click in SO Page

            wnd_SO.ClickMenuItemFromRibbonGallery("Sales order", "Maintain", "Complete");

            #region Infolog Validation

            lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog();

            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

            #endregion

            #endregion

            

            #endregion

            #endregion

            #region Apply header level discount in % (positive) for the sales order 
            
            wnd_SO = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            wnd_SO.ClickMenuItemFromRibbonGallery("Sales order", "Maintain", "Modify");

            wnd_SO.ClickMenuItemFromRibbonGallery("Sales order", "Manual discounts", "Sales order discount");

            wnd_SDiscount = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Sales order discount:", ClassName = "AxTopLevelFrame" });

            WinEdit edt_DicountPercentage = wnd_SDiscount.DetectControl<WinEdit>(new { Name = "Manual discount %" });

            edt_DicountPercentage.EnterWinText(DiscountPercent);

            wnd_SDiscount.SelectValueFromCustomWinCombobox("Reason code", "Reason code", "CustSat");
            
            btn_ApplyAndClose = wnd_SDiscount.DetectControl<WinButton>(new { Name = "Apply and Close" });

            btn_ApplyAndClose.WinClick();

            #region Infolog Validation

            lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog();

            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

            #endregion

            #region Complete Button Click in SO Page

            wnd_SO.ClickMenuItemFromRibbonGallery("Sales order", "Maintain", "Complete");

            #region Infolog Validation

            lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog();

            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

            #endregion

            #endregion

            #endregion

            #region Discount History Validation

            wnd_SO.ClickMenuItemFromWinTable("Discount history", null);

            WinWindow wnd_DH = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Discount history:", ClassName = "AxTopLevelFrame" });

            InitialValue = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_DH, "ManualDiscounts", "Discount offer Id", "", "Net amount of sales line");

            DiscountAmountValue = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_DH, "ManualDiscounts", "Discount offer Id", "ChngMind", "Net amount of sales line");

            DiscountPercentValue = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_DH, "ManualDiscounts", "Discount offer Id", "CustSat", "Net amount of sales line");

            #region Discount Amount Calculation and Validation

            FinalValue = (double.Parse(InitialValue.Replace(",", "")) - double.Parse(DiscountAmount)).ToString();

            Assert.AreEqual(FinalValue, DiscountAmountValue.Replace(",", ""), "There is a mismatch in discount amount after Manual discount $");

            FinalValue = string.Empty;

            FinalValue = Math.Round((double.Parse(DiscountAmountValue.Replace(",", "")) - ((double.Parse(DiscountAmountValue.Replace(",", "")) * double.Parse(DiscountPercent.Replace("%", "")))) / 100), 2).ToString();

            Assert.AreEqual(FinalValue, DiscountPercentValue.Replace(",", ""), "There is a mismatch in discount amount after Manual discount $");

            #endregion

            #region Discount Values Validation

            DiscountAmountValue = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_DH, "ManualDiscounts", "Discount offer Id", "ChngMind", "Discount $");

            DiscountPercentValue = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_DH, "ManualDiscounts", "Discount offer Id", "CustSat", "Discount %");

            Assert.AreEqual(DiscountAmountValue, DiscountAmount, "There is a mismatch in discount amount after Manual discount $");

            Assert.AreEqual(DiscountPercentValue.Remove(DiscountAmountValue.IndexOf('.') - 1) + "%", DiscountPercent, "There is a mismatch in discount amount after Manual discount %");

            #endregion
            
            #endregion

            #region Post-Condition

            WinButton Btn_Close = wnd_DH.DetectControl<WinButton>(new { Name = "Close" });

            Btn_Close.WinClick();

            #endregion

            #endregion
        }

        [TestMethod]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91887)]
        [Description("Cancel Sales Order  - Header level / Line level")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91887", DataAccessMethod.Sequential)]
        public void CancelSalesOrderHeaderlevelLinelevel()
        {
            #region Initialization

            #endregion

            #region TestData

            string Path = @"C:\Program Files (x86)\Microsoft Dynamics AX\60\Client\Bin\Ax32.exe";

            string AddressPath = "ECM/Accounts receivable/Common/Sales orders/All sales orders";

            string saleOrderNumber = string.Empty;

            string SaleOrderNumber = "725000020713";

            string Act_Qty = "5.00";

            string Exp_DeliverRemainder = "3.00";

            string Exp_ReasonCode = "Changed Mind";

            string Exp_CanceledQty = "2.00";

            string Exp_NonInvoicedQty = "3.00";

            string Exp_SaleOrderQty = "3.00";

            #endregion

            #region Local Variables

            WinWindow wnd_AX = new WinWindow();

            List<string> lst_ExpectedValue = new List<string>();

            List<string> lst_ActualValue = new List<string>();

            string Act_LineStageValue = string.Empty;

            string Act_SalesOrderQty = string.Empty;

            string Act_CanceledQty = string.Empty;

            string Act_NonInvoicedQty = string.Empty;

            string Act_ReasonCode = string.Empty;

            string Act_DeliverRemainder = string.Empty;

            #endregion

            #region TestSteps

            #region Launch AX and Navigate to All Sales Order

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            wnd_AX.NavigateToSpecifiedAddress(AddressPath);

            #endregion

            #region Filter Values And Open the Filtered Sale Order

            wnd_AX.ApplyFilter("Sales order", SaleOrderNumber);

            wnd_AX.DoubleClickACellInWinTable("Sales order", SaleOrderNumber);

            #endregion

            #region Partial Cancel Line Level

            WinWindow wnd_SO = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            wnd_SO.ClickMenuItemFromRibbonGallery("Sales order", "Maintain", "Modify");

            wnd_SO.SelectARowInWinTable("SalesLineGrid", "Line stage", "Unscheduled");

            wnd_SO.ClickMenuItemFromWinTable("Deliver remainder", "Update line");

            WinWindow wnd_UpdateQty = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Update remaining quantity:", ClassName = "AxTopLevelFrame" });

            WinEdit edt_deliverRemainder = wnd_UpdateQty.DetectControl<WinEdit>(new { Name = "Deliver remainder" });

            edt_deliverRemainder.EnterWinText(Exp_DeliverRemainder);

            wnd_UpdateQty.SelectValueFromCustomWinCombobox("Reason", "Cancelation reason description", "Changed Mind");

            WinButton btn_OK = wnd_UpdateQty.DetectControl<WinButton>(new { Name = "OK" });

            btn_OK.WinClick();

            #region Grid Validations After Partial Cancel
            
            wnd_SO = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            Act_LineStageValue = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_SO, "SalesLineGrid", "Cancelation reason", "Changed Mind", "Line stage");

            Assert.AreEqual(Act_LineStageValue, "Multiple", "Error Updating Line Stage Value after Partial Cancel");

            Act_SalesOrderQty = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_SO, "SalesLineGrid", "Cancelation reason", "Changed Mind", "Sales order qty");

            Assert.AreEqual(Act_SalesOrderQty, Exp_SaleOrderQty, "Error Updating Sale Order Qty Value after Partial Cancel");

            Act_CanceledQty = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_SO, "SalesLineGrid", "Cancelation reason", "Changed Mind", "Canceled Qty");

            Assert.AreEqual(Act_CanceledQty, Exp_CanceledQty, "Error Updating Canceled Qty Value after Partial Cancel");

            Act_NonInvoicedQty = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_SO, "SalesLineGrid", "Cancelation reason", "Changed Mind", "Non-invoiced Qty");

            Assert.AreEqual(Act_NonInvoicedQty, Exp_NonInvoicedQty, "Error Updating Non-Invoiced Qty Value after Partial Cancel");

            Act_ReasonCode = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_SO, "SalesLineGrid", "Line stage", "Multiple", "Cancelation reason");

            Assert.AreEqual(Act_ReasonCode, Exp_ReasonCode, "Error Updating Reason Code after Partial Cancel");

            Act_DeliverRemainder = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_SO, "SalesLineGrid", "Line stage", "Multiple", "Deliver remainder");

            Assert.AreEqual(Act_DeliverRemainder, Exp_DeliverRemainder, "Error Updating Reason Code after Partial Cancel");


            #endregion

            #region Click Complete in SO Page and Infolog Validation

            wnd_SO.ClickMenuItemFromRibbonGallery("Sales order", "Maintain", "Complete");

            #region Infolog Validation

            lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog();

            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

            #endregion


            #endregion

            #endregion

            #region Full Cancel Header Level

            wnd_SO = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            wnd_SO.ClickMenuItemFromRibbonGallery("Sales order", "Maintain", "Modify");

            wnd_SO.ClickMenuItemFromRibbonGallery("Sales order", "Maintain", "Cancel");

            WinWindow wnd_CancelOrder = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Are you sure you want to cancel:", ClassName = "AxPopupFrame" });

            wnd_CancelOrder.SelectValueFromCustomWinCombobox("Reason", "Cancelation reason description", "Changed Mind");

            WinButton Btn_Yes = wnd_CancelOrder.DetectControl<WinButton>(new { Name = "Yes" });

            Btn_Yes.WinClick();

            #region Infolog Validation

            lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog();

            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

            #endregion

            #region Grid Validations After Partial Cancel

            wnd_SO = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

            Act_LineStageValue = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_SO, "SalesLineGrid", "Cancelation reason", "Changed Mind", "Line stage");

            Assert.AreEqual(Act_LineStageValue, "Canceled", "Error Updating Line Stage Value after Partial Cancel");

            Act_CanceledQty = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_SO, "SalesLineGrid", "Cancelation reason", "Changed Mind", "Canceled Qty");

            Assert.AreEqual(Act_CanceledQty, Act_Qty, "Error Updating Canceled Qty Value after Partial Cancel");

            Act_NonInvoicedQty = DynamicsAxCommonFunctions.GetValuefromWinTable(wnd_SO, "SalesLineGrid", "Cancelation reason", "Changed Mind", "Non-invoiced Qty");

            Assert.AreEqual(Act_NonInvoicedQty, "0.00", "Error Updating Non-Invoiced Qty Value after Partial Cancel");

            #endregion

            #region Click Complete in SO Page and Infolog Validation

            wnd_SO.ClickMenuItemFromRibbonGallery("Sales order", "Maintain", "Complete");

            #region Infolog Validation

            lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog();

            Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

            #endregion


            #endregion



            #endregion

            #endregion

        }



        [TestMethod]
        public void sample()
        {


            WinWindow wnd_CancelOrder = UIAutomationControl.Desktop.FindWinWindow(new { Name = "Are you sure you want to cancel:", ClassName = "AxPopupFrame" });


        }


        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
